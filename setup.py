#!/usr/bin/env python3
"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
import os
import contextlib
import platform
import glob
from setuptools import setup
from setuptools import find_packages
from setuptools import Extension
from Cython.Build import cythonize

exec(open('src/alpha_aligner/__init__.py').read())    #Get __version__

HERE = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(HERE, 'readme.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

# Discern the OS:
USING_LINUX = platform.system() == 'Linux'
USING_MAC = platform.system() == 'Darwin'

# Install the Ubuntu packages:
# Do this instead?
# https://jichu4n.com/posts/how-to-add-custom-build-steps-and-commands-to-setuppy/
if USING_LINUX:
    pass

# Fix symbolic links to the appropriate GenomeTools binaries:
if "clean" in sys.argv:
    with contextlib.suppress(FileNotFoundError):
        os.remove('src/alpha_aligner/gtdir')

else:
    if USING_LINUX:
        gtdir = 'gt-1.5.8-Linux_x86_64-64bit-barebone/'
    elif USING_MAC:
        gtdir = 'gt-1.5.8-Darwin_i386-64bit'

    with contextlib.suppress(FileExistsError):
        os.symlink(gtdir, 'src/alpha_aligner/gtdir', True)

setup(
    setup_requires=['wheel', 'cython'],
    ext_modules=cythonize(Extension('alpha_aligner.ccode.eqclasses',
                                    ['src/alpha_aligner/ccode/eqclasses.pyx']),
                          language_level='2'),
    zip_safe=False,

    # This is the name of your project. The first time you publish this
    # package, this name will be registered for you. It will determine how
    # users can install this project, e.g.:
    #
    # $ pip install sampleproject
    #
    # And where it will live on PyPI: https://pypi.org/project/sampleproject/
    #
    # There are some restrictions on what makes a valid project name
    # specification here:
    # https://packaging.python.org/specifications/core-metadata/#name
    name='alpha_aligner',  # Required

    # Versions should comply with PEP 440:
    # https://www.python.org/dev/peps/pep-0440/
    #
    # For a discussion on single-sourcing the version across setup.py and the
    # project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version=__version__,

    # This is a one-line description or tagline of what your project does. This
    # corresponds to the "Summary" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#summary
    description='Explore multiple (AL)ignments of (PHA)ges.',  # Optional

    # This is an optional longer description of your project that represents
    # the body of text which users will see when they visit PyPI.
    #
    # Often, this is the same as your README, so you can just read it in from
    # that file directly (as we have already done above)
    #
    # This field corresponds to the "Description" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#description-optional
    long_description=LONG_DESCRIPTION,  # Optional

    # Denotes that our long_description is in Markdown; valid values are
    # text/plain, text/x-rst, and text/markdown
    #
    # Optional if long_description is written in reStructuredText (rst) but
    # required for plain-text or Markdown; if unspecified, "applications should
    # attempt to render [the long_description] as text/x-rst; charset=UTF-8 and
    # fall back to text/plain if it is not valid rst" (see link below)
    #
    # This field corresponds to the "Description-Content-Type" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#description-content-type-optional
    long_description_content_type='text/markdown',  # Optional (see note above)

    # This should be a valid link to your project's main homepage.
    #
    # This field corresponds to the "Home-Page" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#home-page-optional
    url='https://bitbucket.org/thekswenson/alpha',  # Optional

    # This should be your name or the name of the organization which owns the
    # project.
    author='Krister M. Swenson',  # Optional

    # This should be a valid email address corresponding to the author listed
    # above.
    author_email='swenson@lirmm.fr',  # Optional

    # Classifiers help users find your project by categorizing it.
    #
    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Bio-Informatics',

        # Pick your license as you wish
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate you support Python 3. These classifiers are *not*
        # checked by 'pip install'. See instead 'python_requires' below.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3 :: Only',
    ],

    # This field adds keywords for your project which will appear on the
    # project page. What does your project relate to?
    #
    # Note that this is a string of words separated by whitespace, not a list.
    keywords='bacteriophage alignment',  # Optional

    # When your source code is in a subdirectory under the project root, e.g.
    # `src/`, it is necessary to specify the `package_dir` argument.
    package_dir={'': 'src'},  # Optional

    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    #
    # Alternatively, if you just want to distribute a single Python file, use
    # the `py_modules` argument instead as follows, which will expect a file
    # called `my_module.py` to exist:
    #
    #   py_modules=["my_module"],
    #
    #packages=['alpha_aligner', 'alpha_aligner.ccode', 'alpha_aligner.pyinclude'],  # Required
    packages=find_packages(where='src'),  # Required

    # Specify which Python versions you support. In contrast to the
    # 'Programming Language' classifiers above, 'pip install' will check this
    # and refuse to install the project if the version does not match. See
    # https://packaging.python.org/guides/distributing-packages-using-setuptools/#python-requires
    python_requires='>=3.7, <4',

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[],
    #install_requires=['networkx', 'python-igraph', 'numpy', 'pygraphviz',
    #                  'matplotlib', 'biopython', 'pycairo', 'PyGObject'],

    # List additional groups of dependencies here (e.g. development
    # dependencies). Users will be able to install these using the "extras"
    # syntax, for example:
    #
    #   $ pip install sampleproject[dev]
    #
    # Similar to `install_requires` above, these must be valid existing
    # projects.
    #extras_require={  # Optional
    #    'dev': ['cython'],
    #    #'test': ['coverage'],
    #},

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    package_data={  # Optional
        'alpha_aligner': ['config_template.ini',
                          'matchAlphabet.txt',
                          'images/*.png',
                          'images/*.svg',
                          'images/*.icns',
                          'gtdir/bin/gt',
                          'gtdir/bin/gtdata/**/*.lua',
                          'glade/alpha.css',
                          'glade/alpha_ui_nostack.glade'],
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    #data_files=[('my_data', ['data/data_file'])],  # Optional
    data_files=[('/usr/share/applications/', ['installscript/swenson.alpha.desktop']),
                ('/usr/share/icons/hicolor/scalable/apps/', ['src/alpha_aligner/images/swenson.alpha.svg']) ],
                #('share/pixmaps/', ['src/alpha_aligner/images/alpha-aligner.svg']) ],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    #entry_points={  # Optional
    #    'console_scripts': [
    #        'alpha = bin/alpha:main',
    #        'matchtool = bin/matchtool:main',
    #        'sequencetool = bin/sequencetool:main',
    #    ],
    #},
    scripts=['bin/alpha', 'bin/sequencetool', 'bin/matchtool'],

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    #
    # Examples listed include a pattern for specifying where the package tracks
    # issues, where the source is hosted, where to say thanks to the package
    # maintainers, and where to support the project financially. The key is
    # what's used to render the link text on PyPI.
    project_urls={  # Optional
        'Bug Reports': 'https://bitbucket.org/thekswenson/alpha/issues',
        'Source': 'https://bitbucket.org/thekswenson/alpha/src/master/',
        'KMS': 'http://www.lirmm.fr/~swenson/alpha/alpha.htm',
    },
)

