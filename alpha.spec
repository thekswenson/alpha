# -*- mode: python -*-

block_cipher = None


a = Analysis(['alpha'],
             pathex=['.'],
             binaries=[],
             datas=[('lib/*.txt', 'lib'),
                    ('lib/images', 'lib/images'),
                    ('lib/gt-1.5.8-Linux_x86_64-64bit-barebone', 'lib/gtdir')],
             hiddenimports=['igraph.vendor.texttable'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='alpha',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='alpha')
