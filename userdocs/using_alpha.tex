\documentclass{article}
\usepackage{graphicx,verbdef,xspace,graphicx}
\usepackage[hidelinks,colorlinks,citecolor=black,linkcolor=black,urlcolor=blue]{hyperref}

\graphicspath{{fig/}}


% Ensure that the minus sign is the "-" character in
% listings environments for cut and paste operations from pdf docs
\makeatletter
\newcommand*\verbfont{%
    \normalfont\ttfamily\hyphenchar\font\m@ne\@noligs}
\makeatother

\verbdef{\ssequencetool}{sequencetool}
\newcommand{\sequencetool}{\ssequencetool\xspace}
\verbdef{\aalpha}{alpha}
\renewcommand{\alpha}{\aalpha\xspace}

\newcommand{\eg}{\emph{e.g.}\xspace}
\newcommand{\ie}{\emph{i.e.}\xspace}

\newcommand{\launch}{``Launch Alpha''\xspace}

\newcommand{\qtext}[1]{``{\texttt{#1}}''}
\newcommand{\hotkey}[1]{\texttt{#1}}


% \includgraphics neads natwidth and natheight for use with htlatex, so we have
% to use these \ifdefined\HCode checks all over the place.
%
% Button images:
\newcommand{\enterbutton}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.51in,natheight=0.27in]{enter.pdf}\xspace
\else
  \includegraphics[height=3mm]{enter.pdf}\xspace
\fi
}
\newcommand{\contractedbutton}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.51in,natheight=0.27in]{graph_wide.pdf}\xspace
\else
  \includegraphics[height=3mm]{graph_wide.pdf}\xspace
\fi 
}
\newcommand{\expandedbutton}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.30in,natheight=0.26in]{expand.pdf}\xspace
\else
  \includegraphics[height=3mm]{expand.pdf}\xspace
\fi
}
\newcommand{\backbutton}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.17in,natheight=0.17in]{back.pdf}\xspace
\else
  \includegraphics[height=3mm]{back.pdf}\xspace
\fi
}
\newcommand{\findbutton}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.17in,natheight=0.17in]{find.pdf}\xspace
\else
  \includegraphics[height=3mm]{find.pdf}\xspace
\fi
}
\newcommand{\themenu}{
\ifdefined\HCode
  \includegraphics[height=15px,natwidth=0.17in,natheight=0.17in]{menu.pdf}\xspace
\else
  \includegraphics[height=3mm]{menu.pdf}\xspace
\fi 
}

% Buttons:
\newcommand{\theopenbutton}{the ``Open'' button (hotkey \hotkey{Ctrl-o})\xspace}
\newcommand{\theenterbutton}{the ``entry'' \enterbutton button (hotkey \hotkey{Enter})\xspace}
\newcommand{\thecontractedbutton}{the ``contracted graph'' \contractedbutton button (hotkeys \hotkey{g}, \hotkey{G})\xspace}
\newcommand{\theexpandbutton}{the ``expanded graph'' \expandedbutton button (hotkeys \hotkey{e}, \hotkey{E})\xspace}
\newcommand{\thebackbutton}{the ``back'' \backbutton button (hotkey \hotkey{Delete/Backspace} or middle-click)\xspace}
\newcommand{\theshowbutton}{the ``Show Small Nodes'' menu \themenu item (hotkey \hotkey{s})\xspace}
\newcommand{\thenewwinbutton}{the ``Open in New Window'' menu \themenu item (hotkey \hotkey{n})\xspace}
\newcommand{\thematchbox}{the match entry box (hotkey \hotkey{m})\xspace}
\newcommand{\thefindbox}{the find \findbutton box (hotkey \hotkey{f})\xspace}

\newcommand{\theshowwholegraphbutton}{the ``Show the whole graph'' item\xspace}
\newcommand{\theminmatchlenbox}{the ``Minimum Match Length'' box\xspace}

% Modify the head for HTML version:
\ifdefined\HCode
\AtBeginDocument{%
\Configure{@HEAD}{\HCode{<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Crimson+Text|Crimson+Text:b|Comfortaa"> \Hnewline}}
\ConfigureEnv{quote}{\Tg<quote>}{\Tg</quote>}{}{}
}
\fi

%  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

\title{Using the Alpha aligner} 
\author{Krister M. Swenson}

\begin{document}

% CSS for HTML version:
\ifdefined\HCode
\Css {
body {
  margin-left: 10\%; margin-right: 10\%;
  background-color: \#D8DBDD;
}
:root {
  scroll-behavior: smooth;
  font-family: 'Crimson Text', serif;
  font-size: 18px;
}
h1,h2,h3 {
  font-family: Comfortaa, sans;
  text-shadow: 2px 2px 2px \#aaa;
  margin-top: 3em
}
h4,h5 {
  font-family: Comfortaa, sans;
  margin-top: 2em
}
a {
  color: \#275787;
  text-decoration: underline;
  text-decoration-color: \#E0E0E0;
}
a:hover {
  color: \#800000;
  text-decoration-color: \#A0A0A0;
}
a:active {
  color: red;
  text-decoration: none;
}
}
\fi


%  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

\maketitle


\begin{center}
\includegraphics[width=1.5in]{alpha-aligner.pdf}
\end{center}

\tableofcontents

%||||||||||||||||||||||||||||||||'----____----'|||||||||||||||||||||||||||||||||
\section{Introduction}
\label{secIntroduction}

Bacteriophages are characterized by extreme mosaicism. Since mostly co-linearly ordered modules contain different variants that have very little sequence similarity, traditional multiple alignments are not appropriate for bacteriophage genomes.

Alpha is a browser designed for detailed comparative studies of bacteriophage
genomes.  It provides a convenient way to compute and view the partial order
induced by exact matches\footnote{computed using GenomeTools~\cite{GRE:STEI:KUR:2013}}.

This document describes how to use Alpha in this context, providing
tips for the novice user.
The tool is introduced in~\cite{BerardCPGBS16}.
Please reference it if you use Alpha in your research.

The workflow followed by a typical user interested in studying a set
of phage genomes is the following:
\begin{enumerate}
  \item\label{s1} Create a fasta file containing the genomes of interest.
  \item\label{s2} Launch an interactive session by clicking the alpha icon, or by using the \alpha command in the terminal.
\end{enumerate}
These steps will be described in the coming sections.
First, we show how to install the software under Ubuntu Linux and macOS.

\begin{itemize}
\item[**] \textit{Please cite us~\cite{BerardCPGBS16} if you use Alpha in your work:}\\
  S{\`e}verine B{\'e}rard, Annie Chateau, Nicolas Pompidor, Paul Guertin, Anne
  Bergeron, and Krister~M. Swenson.
  \newblock Aligning the unalignable: bacteriophage whole genome alignments.
  \newblock {\em BMC Bioinformatics}, 17(1):30, 2016.

\item[**] The code is hosted at \url{https://bitbucket.org/thekswenson/alpha}.
\end{itemize}


%||||||||||||||||||||||||||||||||'----____----'|||||||||||||||||||||||||||||||||
\section{Installation}
\label{secInstall}

These are the instructions for easy installation under Ubuntu or macOS.
Refer to \href{https://bitbucket.org/thekswenson/alpha/wiki/Installation%20Instructions}{the wiki} for detailed instruction on installing Alpha if you are using another Linux distribution or a version of macOS older than High-Sierra.

\subsection{Easy installation for Ubuntu}
\label{secAutoinstall}

We have a \href{https://launchpad.net/~thekswenson/+archive/ubuntu/alpha-aligner}{PPA} for easy installation and updates of Alpha.
To install alpha run the following commands in a terminal:

\begin{enumerate}
  \item \qtext{sudo add-apt-repository universe}
  \item \qtext{sudo add-apt-repository ppa:thekswenson/alpha-aligner}
  \item \qtext{sudo apt update}
  \item \qtext{sudo apt install alpha-aligner}
\end{enumerate}


\subsection{Easy installation for macOS}

These instructions will work for macOS 10.13 High-Sierra and later:

\begin{enumerate}
  \item Download \href{https://mega.nz/folder/zhxkwYBT#TDxKRv3teQgAGOxIDZwNEg}{the .dmg file.}
  \item Double-click it.
  \item Drag the Alpha icon into the application folder.
\end{enumerate}

\subsection{Somewhat easy installation for Windows}

See the instructions at \url{https://bitbucket.org/thekswenson/alpha}.



%||||||||||||||||||||||||||||||||'----____----'|||||||||||||||||||||||||||||||||
\section{Using Alpha}

Alpha is a tool for looking at common sequence structure in a set of phages.
In general the tool can be used in two ways: 1) for focused study of an area
of interest or 2) for browsing larger structure in the multiple alignment.


%   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
\subsection{Overview}

When launching Alpha, the first thing you see is the file loading dialog.
\begin{center}
\ifdefined\HCode
  \includegraphics[width=400px,natwidth=720,natheight=232]{intro_dialog.png}
\else
  \includegraphics[width=3in]{intro_dialog.png}
\fi
\end{center}
Choose a \texttt{.fasta} file or a saved \texttt{.alpha} to open.
The genomes will then be loaded into the dialog
(see Figure~\ref{fig:introLoaded}).
\begin{figure}[]
\begin{center}
\ifdefined\HCode
  \includegraphics[width=400px,natwidth=720,natheight=379]{intro_loaded.png}
\else
  \includegraphics[width=3in]{intro_loaded.png}
\fi
\end{center}
\caption{After a \texttt{.fasta} files is loaded, the genomes appear in the
selection list. Specific intervals of interest can be be selected to speed up
computation. NCBI annotation for the given genomes will be downloaded if
selected. The entire graph can be viewed instead of the just the anchors
(this setting should be used on specific intervals of the genomes, since the
graph can be very large).}
\label{fig:introLoaded}
\end{figure}
Make your desired configuration and then click \launch.


\subsubsection{Anchor Mode}

Unless the ``Show the whole graph'' box is checked, Alpha is opened in
\emph{anchor mode}.
An \emph{anchor} is a node corresponding to a perfect (\ie all columns
are identical) alignment that includes all input genomes.
The initial window displays a graph with some of the anchors
(see Figure~\ref{figAnchors}).
The boxes around the nodes indicate that some anchors have been omitted from
the display between those anchors.
\begin{figure}[]
\begin{center}
\ifdefined\HCode
  \includegraphics[width=720px,natwidth=12.66in,natheight=3.94in]{anchor_view.pdf}
\else
  \includegraphics[width=4.8in]{anchor_view.pdf}
\fi
\end{center}
\caption{The anchor view with some of the UI elements labeled.}
\label{figAnchors}
\end{figure}

The purpose of the anchor view is to allow the user to target parts of the
alignment while avoiding the costly step of computing the graph over the
entire genome. You can focus on an area of the genome by highlighting two
nodes and then clicking \theenterbutton.
This will show more anchors.

Anchor mode is finished when either \thecontractedbutton or \theexpandbutton is pressed.
The only way to return to anchor mode is to go back using \thebackbutton.


\subsubsection{Browsing Basics}

Use the mouse to move the image.  Zoom with the mouse wheel.
Select nodes by clicking on them.
A selected node has a target in the upper-right corner that gives access to
a menu.
Hover the mouse over a button to see the hotkey and description of the button.
A reference to the UI elements appears in Section~\ref{sec:UIref}.

See more detail for a specific part of the graph by highlighting two nodes and
pressing the \theenterbutton.
Exit anchor mode by pressing \thecontractedbutton or \theexpandbutton.
The views are stored in a stack, where previous graphs can be viewed using
\thebackbutton.

\subsubsection{Node Menus: Annotations, Alignments and Indices}

When a node is highlighted, a menu is accessible by clicking on the red target that appears on the corner of the node.
This menu contains different entries depending the identity of, and how many  (1 or 2) nodes are highlighted.
The potential entries are the following:

\bigskip
\begin{tabular}{|p{2.5cm}|p{8cm}|}
\hline
Item (\# nodes)&  Description \\
\hline
\hline
Annotations (1)&
 Show the annotations associated with the region for the node. \\
\hline
Export Multiple Alignment (1)&
 Save the gapless alignment that the node represents to a file. \\
\hline
Export Indices (2)&
 Save the indices for the intervals for the two highlighted nodes.  \\
\hline
Split (1)&
 Split the node in two. \\
\hline
Merge (2)&
 Merge two nodes. Only two nodes that are adjacent or ``trapped'' between
 the same pair of nodes can be merged into one.  \\
\hline
\end{tabular}
\bigskip

The annotations in the following example show that there is a ``PemK-like protein'' that shares 10 bases (the right-most node) with ``hypothetical proteins'' despite having very few exact matches within the preceding 600+ bases of the protein.
\begin{center}
\ifdefined\HCode
  \includegraphics[width=720px,natwidth=1430,natheight=521]{annotations.png}
\else
  \includegraphics[width=4.8in]{annotations.png}
\fi
\end{center}


%   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -
\subsection{Tutorial}

Say we would like to compare the region of phage phiNM2 between nucleotides
13000 and 14500 with the homologous regions in phiETA3, phiNM1, and B236.
Download \href{https://bitbucket.org/thekswenson/alpha/raw/0c35f39b89d90609977136d582cf49799dc0fcf6/data/tutorial/Eta3EtCie.fasta}{Eta3EtCie.fasta} to get started.
Launch Alpha and choose the file you just downloaded.
You will see the screen from Figure~\ref{fig:introLoaded}.
Press the \launch button.

You will see a linear ordering of a sampling of all \emph{anchors}.
phiNM2 is genome B.   We're interested in the interval B:[13000-14500] so
click on the node with interval B:[12955-12970] and the node with interval
B:[14691-14705] so that our desired interval will be include in the next view.
Now click on \thecontractedbutton.
The result will look like Figure~\ref{fig:AnchorExample}.
We can see that there have been significant insertions/deletions that
distinguish phiNM1 and phiNM2 from the other strains.
Select \theshowbutton to view the minor differences between the
strains.  Select it again to hide them.
\begin{figure}[]
\begin{center}
\ifdefined\HCode
  \includegraphics[width=720px,natwidth=2041,natheight=457]{Fig4_Eta3EtCieDeletion.png}
\else
  \includegraphics[width=4.8in]{Fig4_Eta3EtCieDeletion.pdf}
\fi
\end{center}
\caption{A local alignment of four {\em S. aureus} bacteriophages.
In column 3, three phages are in a 587 bp exact alignment, with 99~\% identical
columns.  A major deletion in phage B236 spans columns 2, 3, and 4, and the
corresponding arrow is dotted to reflect the fact that some base pairs are not
shown,  6 bp in this case. }
\label{fig:AnchorExample}
\end{figure}

An alternate method to focus on this part of the alignment would be to load
the fasta file, and choose the ``Select Intervals'' tab.
Press \theopenbutton in the top left corner.
You will see this after you have entered the intervals:
\begin{center}
\ifdefined\HCode
  \includegraphics[width=400px,natwidth=720,natheight=379]{select_intervals.png}
\else
  \includegraphics[width=3.0in]{select_intervals.png}
\fi
\end{center}
Alpha will now only process the chosen interval when you press \launch.
Close the new window you just created.

\subsubsection{Multiple Windows and CPUs}

The computation of some graphs can take minutes.
While a single graph will be computed on a single thread,
the inconvenience of waiting can be avoided by computing the next graph on a new thread, opening the result in a new window.
This way, the current view can continue to be explored while the new graph is
being computed.
To compute the next view in a new window either
\begin{itemize}
  \item activate \thenewwinbutton before clicking one of the three graph buttons, or
  \item press \hotkey{Shift} when using the hotkeys \hotkey{Enter}, \hotkey{G}, or \hotkey{E}.
\end{itemize}

To try this feature, open a second window again with \theopenbutton and launch a new Alpha instance with the same input file.
Highlight the nodes B:[36914-37018] and B:[40503-40566] and then activate \thenewwinbutton, before clicking \thecontractedbutton.
See that a new window is opened once the computations have finished.
You can close these two windows now and continue with the tutorial.


\subsubsection{Advanced exploration}

Now we will play with the match length to get a finer resolution version of
the same genomic area.
Go to you primary window and click on \thematchbox and enter 8.
Press \theenterbutton.
Alpha will recompute a graph, trying a minimum match length of 8.
The match length 8 is so short, though, that there are multiple
matches of that length for the same nucleotides, creating a cycle in the graph.
Alpha now automatically tries larger match lengths and finds that 11 creates
an acyclic graph.
The large center node in column 3 has grown while the differences between
phiETA3 and phiNM1/phiNM2 in column 4 have shrunk.

To see the cycle caused by the multiple length-8 matches, press \thebackbutton
and then enter 8 in \thematchbox again. This time, press \theexpandbutton.
You will see the full graph, including the cycle.
It turns out that there is an 8bp match between phiNM1/phiNM2 and phiETA3 just
before the interval B:[13272-13320] that leads to the cycle.


\subsection{General Exploration of a Multiple Alignment}

We will explore the same dataset by viewing the graph in its entirety.
In the welcome dialog, activate \theshowwholegraphbutton.
Note that this process can take a while, and that the resulting graph will have many nodes!
To calibrate the size of the graph, we can specify a longer match length in \theminmatchlenbox.
Enter 35 in that box and press \launch.

The result is a large graph (partial order) that you can browse for interesting
features.  At the interval between nodes A:[9130-9164] and A:[11574-11608] we
see an interesting region where B236 (genome D) has a different variant from
the others (hint: you can find a node using \thefindbox).  Look at this region in more detail by selecting the two nodes
and pressing \theenterbutton.
The graph is recomputed with match length 15.
The result is depicted in Figure~\ref{figVariantExample}.
\begin{figure}[]
\begin{center}
\ifdefined\HCode
  \includegraphics[width=820px,natwidth=19.79in,natheight=2.71in]{bigVariant.png}
\else
  \includegraphics[width=4.8in]{bigVariant.pdf}
\fi
\end{center}
\caption{A local alignment of four {\em S. aureus} bacteriophages.  B236
appears to have a variant different from the other three strains. }
\label{figVariantExample}
\end{figure}

Press \thebackbutton to go to the previous view.
Between the nodes A:[16963-17099] and A:[30811-30853] we see an interval where
A and D match for periods interspersed with non-matching segments.
Highlight them and then press \theenterbutton to show a more accurate view of the alignment between these two genomes.

\subsection{UI Reference}
\label{sec:UIref}

You can hover the pointer over any button to get a description of the button, along with the hotkeys.

\noindent
\begin{tabular}{|p{0.9in}|p{3.6in}|}
  \hline
  \multicolumn{2}{|c|}{Buttons:} \\
  \hline
  \includegraphics[height=3.9mm]{print.pdf} print
    & [\hotkey{p}] Print the visible area of the graph or export it to a .pdf or .svg file. \\
  \hline
  \includegraphics[height=3.9mm]{find.pdf} find
    & [\hotkey{f}] Find a node by text matching. \\
  \hline
  Current match~length
    & Shows the match length used to create the current graph. \\
  \hline
  Change length of~matches
    & [\hotkey{m}]
      This will be the match length used when creating a graph in the next
      view. If you change this and press Enter then the current view will
      be updated with a new match length.
      An entry of 0 will automatically compute the smallest possible
      match length at least as big at 15.\\
  \hline
  \enterbutton
  \newline Enter in
    & [\hotkey{Enter} or \hotkey{Shift-Enter}]
      Go into a selected contracted node.
	  If two nodes are selected, view the graph between these nodes.
      The length in the match length box will be used.\\
  \hline
  \contractedbutton
  \newline Get contracted graph
    & [\hotkey{g}, \hotkey{G}]
      Get the contracted graph.  If a node or nodes are highlighted
      then the graph will be computed only on this interval.
      The length in the new match length box will be used. All subsequent
      browsing will be done in graph mode until the back button returns us
      to the anchor mode. \\
  \hline
  \expandedbutton
  \newline Get expanded graph
    & [\hotkey{e}, \hotkey{E}]
      Get the expanded graph.  If a node or nodes are highlighted
      then the graph will be computed only on this interval.
      The length in the new match length box will be used. All subsequent
      browsing will be done in graph mode until the back button returns us
      to the anchor mode. \\
  \hline
  \hline
  \multicolumn{2}{|c|}{\themenu Menu Items:} \\
  \hline
  Show Small Nodes
    & [\hotkey{s}]
      Show/hide the small nodes of the graph.  Replace the dashed lines with
      the small nodes they represent. \\
  \hline
  Moveable Nodes
    & Detach the nodes from their fixed positions. When this is activated you
      can drag the nodes to new positions. \\
  \hline
  Open in New Window
    & [\hotkey{n}]
      Open the next graph in a new window. Alternatively, you can hold
      \hotkey{Shift} when using one of the hotkeys that creates a new graph. \\
  \hline
  Remove cycles from next graph
    & [\hotkey{c}]
      For the next graph that will be computed, attempt to remove the least
      number of edges (``feedback vertex'') so that that graph acyclic.
      This may take a while. We recommend increasing the match length or
      using \theexpandbutton instead. \\
  \hline
\end{tabular}


\subsection{Using Alpha from the commandline}

For Ubuntu type
\begin{verbatim}
alpha -f
\end{verbatim}
(or \qtext{/usr/bin/alpha -f} if \qtext{/usr/bin} is not in your path).
For macOS type
\begin{verbatim}
/Applications/Alpha.app/Contents/MacOS/alpha -f
\end{verbatim}

%||||||||||||||||||||||||||||||||'----____----'|||||||||||||||||||||||||||||||||
\section{Fetching a set of genomes}
\label{secFetch}

The program called \sequencetool will fetch sequences and put them in a fasta
file.
NCBI requires an email address to make requests.
Open Alpha and choose any fasta file, even if it is empty.
Now click the ``use NSCBI annotation'' box. This will register your
email address in the file \qtext{\textasciitilde/.alpha/alpha\textunderscore{}config.ini}.
You can close the Alpha window and then use \sequencetool.
On Ubuntu
\begin{verbatim}
sequencetool -a accessions.txt seqset.fa
\end{verbatim}
On macOS
\begin{verbatim}
/Applications/Alpha.app/Contents/MacOS/sequencetool -a accessions.txt seqset.fa
\end{verbatim}


%||||||||||||||||||||||||||||||||'----____----'|||||||||||||||||||||||||||||||||
\section{Acknowledgements}
\label{secAcknowledgements}

The graphical interface is based on the program \verb|xdot| that is hosted 
at \url{https://github.com/jrfonseca/xdot.py}.
Matches are found using \verb|GenomeTools|~\cite{GRE:STEI:KUR:2013}
(\url{http://genometools.org/}).



\bibliographystyle{plain}
\bibliography{alpha}

\end{document}
