# pygraphviz (and parts of networkx) depends on executable programs like dot.
# Including just this binary is not sufficient since pyinstaller does not find
# the libraries in lib/graphviz/, nor the config file.

binaries=[
          ('/usr/local/bin/dot', '.'),
          ('/usr/local/bin/nop', '.'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_pango.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_core.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_dot_layout.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_core.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_gd.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_neato_layout.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_quartz.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_visio.6.dylib', 'graphviz'),
          ('/usr/local/Cellar/graphviz/*/lib/graphviz/libgvplugin_webp.6.dylib', 'graphviz'),
         ]

datas=[
       ('/usr/local/Cellar/graphviz/*/lib/graphviz/config6', 'graphviz'),
      ]
