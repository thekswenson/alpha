#!/usr/bin/env python3
# Krister Swenson                                                Summer 2015
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Import a bunch of stuff and catch the exceptions telling the user what to do.
"""
import sys

try:
  import argparse
  import networkx
  import igraph
  import cairo
  import gi
  import BioSQL
  from Bio import SeqIO
  from BioSQL.BioSeq import DBSeqRecord
  
  from alpha_aligner import *
  from alpha_aligner.AlphaApp import *
  from alpha_aligner.Match import *
  from alpha_aligner.InterGraph import *
  from alpha_aligner.PhageUI import *
  from alpha_aligner.matchfuncs import *
  from alpha_aligner.shapes import *
  from alpha_aligner.xdot import *
  from alpha_aligner.Annotations import *
  from alpha_aligner.userconfig import *
  from alpha_aligner.iofuncs import *
  from alpha_aligner.ccode.eqclasses import *
    
  from alpha_aligner.pyinclude.genseqfuncs import *
  from alpha_aligner.pyinclude.usefulfuncs import *
  from alpha_aligner.pyinclude.ncbi import *

except Exception as e:
  print(e)
  sys.exit(1)




#        _________________________
#_______/        Functions        \_____________________________________________




#        _________________________
#_______/    Code Graveyard       \_____________________________________________





################################################################################
################################################################################


def main():
  global MIN_LENGTH

  desc = 'Import a lot of stuff and print useful messages for failure.'
  parser = argparse.ArgumentParser(description=desc)
  
  args = parser.parse_args()


#        ______________________
#_______/    Do Everything     \________________________________________________



if __name__ == "__main__":
  #import doctest
  #doctest.testmod()
  main()
  sys.exit(0)
