#!/bin/bash
# Krister Swenson                                                Summer 2015
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#___________________________________________________________________________
#
#  Install Alpha.  Check dependencies and report issues.
#

#Preliminaries: __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

read -d '' usage <<- EOF
usage: installalpha [-h] VMATCH_DIRECTORY
  -h print this message
EOF


    #Get the arguments:
OPTIND=1
while getopts "h" OPTIONS ; do
    case ${OPTIONS} in
        h|-help) echo "${usage}"; exit;;
    esac
done
shift $(($OPTIND - 1))

if [[ $2 || ! $1 ]]; then
  echo "$usage"
  exit 1
fi

vmatchdir=$1


# _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _

function checkInstalled
  {
  program=$1
  message=$2

  command -v $program >/dev/null 2>&1 || { echo >&2 $message; exit 1; }
  }


function installPackagesUbuntu
  {
  sudo apt-get install python-biopython python-matplotlib \
               python-networkx python-pygraphviz python-igraph clustalo \
               python-numpy ncbi-blast+
  }



# _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _ - _


   #Check platform.
isubuntu=`python -mplatform | grep Ubuntu`


extra=
if [[ $isubuntu ]]; then
  extra=" sudo apt-get install mercurial"
fi
message="Install Mercurial.$extra"

   #Check for Mercurial installation.
checkInstalled "hg" $message
   #Check for python installation.
checkInstalled "python" "Install python."

   #Clone the code.
echo "downloading the code..."
hg clone https://bitbucket.org/thekswenson/alpha
hg clone https://bitbucket.org/thekswenson/pyinclude

   #Check for background software:
alpha/installscript/checkimports
rc=$?
if [[ $rc != 0 ]]; then
  if [[ $isubuntu ]]; then
    echo "installing missing packages..."
    installPackagesUbuntu
  else
    echo "Install the support programs!"
    exit 1
  fi
fi

   #Link to vmatch directory:
if [[ "$vmatchdir" != /* ]]; then
  vmatchdir="$(pwd)/$vmatchdir"
fi

if [[ -e "$vmatchdir/vmatch" && -e "$vmatchdir/mkvtree" ]]; then
  echo 'creating link to vmatch.'
  rm -f alpha/lib/vmatch
  ln -s $vmatchdir alpha/lib/vmatch
else
  echo "ERROR: Cannot find $vmatchdir/vmatch and $vmatchdir/mkvtree."
  exit 1
fi

echo "done."
