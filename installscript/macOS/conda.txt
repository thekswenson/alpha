miniconda:
  https://docs.conda.io/en/latest/miniconda.html

create environment
  conda activate alphaenv (add python=3.7 for pyinstaller)

Packages:
- conda-forge (conda install -c conda-forge gtk3)
  #OLD# gtk3 pygobject librsvg igraph numpy adwaita-icon-theme graphviz pygraphviz=1.5 biopython

  conda install graphviz  pkg-config
  conda install -c conda-forge gtk3 pygobject librsvg adwaita-icon-theme libcanberra fonts-conda-forge glib

- anaconda
  pkgconfig

- others:
  (gnome-themes-standard? -- don't think so)

Run pip:

- pip
  python3 -m pip install --install-option="--include-path=$CONDA_PREFIX/include" --install-option="--library-path=$CONDA_PREFIX/lib" pygraphviz
  python3 -m pip install numpy biopython networkx python-igraph matplotlib cython

setup.py build
 and then
python3 -m pip install .

For pyinstaller:
python3 -m pip install pyinstaller
 or
conda install pyinstaller

set DYLD_FALLBACK_LIBRARY_PATH
