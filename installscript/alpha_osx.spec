# -*- mode: python -*-

from alpha_aligner import __version__

block_cipher = None


a = Analysis(['bin/alpha'],
             pathex=['src'],
             binaries=[
                       #('/usr/local/bin/dot', 'alpha_aligner'),
                       #('/usr/local/bin/nop', 'alpha_aligner'),
                       #('/usr/local/Cellar/graphviz/*/lib/*.dylib', '.'),
                       #('/usr/local/Cellar/graphviz/*/lib/graphviz/*.dylib', 'graphviz'),
                      ],
             datas=[('src/alpha_aligner/*.txt', 'alpha_aligner'),
                    ('src/alpha_aligner/*.ini', 'alpha_aligner'),
                    ('src/alpha_aligner/images', 'alpha_aligner/images'),
                    ('src/alpha_aligner/glade', 'alpha_aligner/glade'),
                    ('src/alpha_aligner/gt-1.5.8-Darwin_i386-64bit', 'alpha_aligner/gtdir'),
                    #('/usr/local/Cellar/graphviz/*/lib/graphviz/config6', 'graphviz'),
                   ],
             hiddenimports=['pkg_resources.py2_warn',
                            'igraph.vendor.texttable',
                            'pygraphviz'],
             hookspath=['installscript'],
             runtime_hooks=[],
             excludes=['FixTk', 'tcl', 'tk', '_tkinter', 'tkinter', 'Tkinter'],
             #excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='alpha',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          disable_windowed_traceback=False,
          argv_emulsation=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='alpha')
app = BUNDLE(coll,
             name='Alpha.app',
             icon='src/alpha_aligner/images/icns/alpha-aligner.icns',
             bundle_identifier='fr.lirmm.kms.alpha',
             info_plist={
                'NSPrincipalClass': 'NSApplication',
                'NSAppleScriptEnabled': False,
                'LSMinimumSystemVersion': '10.13.0',
                # Version found in alpha_aligner.__version__:
                'CFBundleShortVersionString': __version__,
                # Same as previous but must be bumped when sent to Apple:
                'CFBundleVersion': __version__,
                'CFBundleDocumentTypes': [
                    {
                        'CFBundleTypeName': 'Alpha Aligner',
                        'CFBundleTypeIconFile': 'src/alpha_aligner/images/icns/alpha-aligner.icns',
                        'CFBundleTypeRole': 'Viewer',
                        'LSItemContentTypes': ['cnrs.kms.alpha'],
                        'LSBundleTypeExtensions': ['alpha', 'fasta', 'fa'],
                        'NSExportableTypes': ['alpha'],
                        'LSHandlerRank': 'Alternate'
                        }
                    ]
                },
)
