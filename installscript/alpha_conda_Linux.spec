# -*- mode: python3 -*-

block_cipher = None


a = Analysis(['bin/alpha'],
             pathex=['src/'],
             binaries=[],
             datas=[('src/alpha_aligner/*.txt', 'alpha_aligner'),
                    ('src/alpha_aligner/*.ini', 'alpha_aligner'),
                    ('src/alpha_aligner/images', 'alpha_aligner/images'),
                    ('src/alpha_aligner/glade/alpha.css', 'alpha_aligner/glade'),
                    ('src/alpha_aligner/glade/*.glade', 'alpha_aligner/glade'),
                    ('src/alpha_aligner/gt-1.5.8-Linux_x86_64-64bit-barebone', 'alpha_aligner/gtdir'),
                   ],
             hiddenimports=['igraph.vendor.texttable',
                            'pkg_resources.py2_warn'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

                 #Make a single executable:
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='alpha',
          debug=False,
          strip=False,
          upx=True,
          console=True )

                 #Make the whole directory:
#exe = EXE(pyz,
#          a.scripts,
#          exclude_binaries=True,
#          name='alpha',
#          debug=False,
#          strip=False,
#          upx=True,
#          console=True )
#coll = COLLECT(exe,
#               a.binaries,
#               a.zipfiles,
#               a.datas,
#               strip=False,
#               upx=True,
#               name='alpha')
