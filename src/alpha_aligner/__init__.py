from pathlib import Path
import os
import sys

# The version of Alpha
__version__ = '2.2.0'

# The place where configuration files are stored.
ALPHA_CONFIG_DIR = f'{Path.home()}/.alpha'

# Environment variables for dot and nop (graphviz) libraries to be found
# by pyinstaller.
#  _MEIPASS is set by pyinstaller when a frozen bundle is run.
BUNDLE_DIR = getattr(sys, '_MEIPASS',os.path.abspath(os.path.dirname(__file__)))

os.environ['PATH'] += os.pathsep + BUNDLE_DIR
