# Copyright 2008 Jose Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Modified by Krister Swenson.
"""
Object definitions for Shapes extracted from xdot.py.
"""

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('PangoCairo', '1.0')
gi.require_version('Pango', '1.0')
import cairo
from gi.repository import Pango
from gi.repository import Gdk
from gi.repository import PangoCairo
from gi.repository import GdkPixbuf
from gi.repository import GObject

import math
import operator
from itertools import chain
from typing import List, Tuple


_inf = float('inf')
_get_bounding = operator.attrgetter('bounding')


def intersects(objectbox: Tuple[float, float, float, float],
               boundingbox: Tuple[float, float, float, float]):
    if not boundingbox or not objectbox:
        return True

    x0, y0, x1, y1 = boundingbox
    x2, y2, x3, y3 = objectbox
    # It's easier to know when they are NOT intersecting...
    # not (x3 <= x0 or x2 >= x1 or y3 <= y0 or y2 >=y1) == 
    return x3 > x0 and x2 < x1 and y3 > y0 and y2 < y1
    
class Shape(object):
    """Abstract base class for all the drawing shapes."""
    bounding = (-_inf, -_inf, _inf, _inf)

    def __init__(self):
        pass

    def _intersects(self, bounding):
        return intersects(self.bounding, bounding)

    def set_boundingbox(self):
        raise NotImplementedError

    def _fully_in(self, bounding):
        x0, y0, x1, y1 = bounding
        x2, y2, x3, y3 = self.bounding
        return x0 <= x2 and x3 <= x1 and y0 <= y2 and y3 <= y1

    def draw(self, cr, highlight=False, bounding=None):
        """Draw this shape with the given cairo context"""
        if bounding is None or self._intersects(bounding):
            if highlight or bounding:
                self._draw(cr, highlight, bounding)
            else:
                self._draw(cr)

    def _draw(self):
        raise NotImplementedError

    def copyInto(self, shape):
        """
        Copy the member data of self into the given instance.
        """
        if not isinstance(shape, Shape):
            raise NotImplementedError

    def select_pen(self, highlight):
        if highlight:
            if not hasattr(self, 'highlight_pen'):
                self.highlight_pen = self.pen.highlighted()
            return self.highlight_pen
        else:
            return self.pen

    def search_text(self, regexp):
        return False

    @staticmethod
    def _bounds_from_points(points):
        x0, y0 = points[0]
        x1, y1 = x0, y0
        for i in range(1, len(points)):
            x, y = points[i]
            x0, x1 = min(x0, x), max(x1, x)
            y0, y1 = min(y0, y), max(y1, y)
        return x0, y0, x1, y1

    @staticmethod
    def _envelope_bounds(*args):
        xa = ya = _inf
        xb = yb = -_inf
        for bounds in args:
            for x0, y0, x1, y1 in bounds:
                xa, xb = min(xa, x0), max(xb, x1)
                ya, yb = min(ya, y0), max(yb, y1)
        return xa, ya, xb, yb


    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        raise NotImplementedError

class ClickTarget(Shape):
    """
    A target to draw in the bottom corner of a node that can be clicked. This
    will bring up the menu as though right-click was done.
    """

    def __init__(self, parent: 'DNode', x: int, y: int, visible=False):
        super().__init__()

        self.x = x #: Upper right corner x
        self.y = y #: Upper right corner y
        self.radius = 15
        self.visible = visible
        self.node: DNode = parent

        self.fillcolor = (1.0, 0, 0, 0.3)
        self.linecolor = (1, 1, 1, 0.5)
        self.linewidth = 4

    def draw(self, cr: cairo.Content, 
             bounding: Tuple[float, float, float, float] = None,
             x: float=None, y: float=None):
        if x != None:
            assert y != None
            self.x = x
            self.y = y

        self._draw(cr, bounding)

    def _draw(self, cr: cairo.Content,
              bounding: Tuple[float, float, float, float] = None):
        """
        Draw the ClickTarget if L{self.visible} is True.
        """
        if not self.visible and bounding and self._intersects(bounding):
            return

        cr.set_source_rgba(*self.fillcolor)
        cr.arc(self.x - self.radius/2, self.y + self.radius/2, self.radius,
               0, 2*math.pi)
        cr.fill()

        cr.set_line_width(self.linewidth)
        cr.set_source_rgba(*self.linecolor)
        cr.arc(self.x - self.radius/2, self.y + self.radius/2, self.radius/2,
               0, 2*math.pi)
        cr.stroke()

    def is_inside(self, x: float, y: float) -> bool:
        """
        Is this point inside the ClickTarget?
        """
        x1, y1, x2, y2 = self.bounding
        return self.visible and x > x1 and x < x2 and y > y1 and y < y2

    @property
    def bounding(self):
        """
        Return the bounding box corners (ULx, ULy, BRx, BRy).
        """
        return (self.x - (3/2)*self.radius, self.y - self.radius/2,
                self.x + self.radius/2, self.y + (3/2)*self.radius)




class AnnotShape(Shape):
    """
    An annotation box that knows how to draw itself (L{draw()}) and knows how
    to draw a connecting line to its DNode (L{draw_connector}).
    """

    def __init__(self, textshape: 'TextShape', pen: 'Pen'):
        """
        @param textshape: contains the annotations
        """
        super().__init__()

        self.text: 'TextShape' = textshape
        self.pen: 'Pen' = pen
        self.pen.linewidth = 0.5
        self.x: int = self.text.x                     #:The horizontal center
        self.y: int = self.text.y + self.text.descent #:The vertical bottom
        self.w: int = None
        self.h: int = None

        self.fillcolor = (0.95, 0.95, 0.95)
        self.boxcolor = (0, 0, 0)

        self.connectoralpha = 0.1  #:Transparency for the connector line

        self.visible = True        #:True if this should be drawn.

        self.xmargin = 15          #:The horizontal margin
        self.ymargin = 10          #:The vertical margin


    def _draw(self, cr: cairo.Content,
              bounding: Tuple[float, float, float, float] = None):
        """
        Draw the AnnotShape if L{visible} is True, given the Cairo context.
        """
        if not self.visible:
            return

        if not self.w:
            self.w: int = self.text.get_width(cr)
            self.h: int = self.text.get_height(cr)

        if bounding == None or intersects(self.bounding, bounding):
                 #Draw the background:
            cr.set_source_rgb(*self.fillcolor)
            cr.rectangle(self.x - self.w/2 - self.xmargin,
                         self.y - self.h - self.ymargin,
                         self.w + 2*self.xmargin, self.h + 2*self.ymargin)
            cr.fill()

                 #Draw the frame:
            cr.set_source_rgb(*self.boxcolor)
            cr.rectangle(self.x - self.w/2 - self.xmargin,
                         self.y - self.h - self.ymargin,
                         self.w + 2*self.xmargin, self.h + 2*self.ymargin)

            pen = self.select_pen(False)
            cr.set_dash(pen.dash)
            cr.set_line_width(pen.linewidth)
            cr.set_source_rgba(*pen.color)
            cr.stroke()

            self.text.draw(cr)

    def draw(self):
        pass

    def makePickleable(self):
        """
        Make sure the text object doesn't have the Layout object set.
        """
        self.text.makePickleable()

    def draw_connector(self, cr, xpos, ypos,
                       bounding: Tuple[float, float, float, float]):
        """
        Draw the connector line from the given point to this annotation box.
        """
        x1, y1 = min(self.x, xpos), min(self.y, ypos)
        x2, y2 = max(self.x, xpos), max(self.y, ypos)

            #If the connecter intesects the boundingbox:
        if bounding == None or intersects((x1, y1, x2, y2), bounding):
            cr.save()
            cr.set_source_rgba(0,0,0, self.connectoralpha)
            cr.set_line_width(5)
            cr.set_line_cap(cairo.LINE_CAP_ROUND)
            cr.move_to(xpos, ypos)
            cr.translate(self.x, self.y)
            cr.line_to(0.0, 0.0)
            cr.stroke()
            cr.restore()
        
    @property
    def bounding(self):
        """
        Return the bounding box corners (ULx, ULy, BRx, BRy).
        """
        return (self.x - self.w/2 - self.xmargin,
                self.y - self.h - self.ymargin,
                self.x + self.w/2 + self.xmargin,
                self.y + self.ymargin)

    def is_inside(self, x, y):
        x1, y1, x2, y2 = self.bounding
        return self.visible and x > x1 and x < x2 and y > y1 and y < y2

    def update_position(self, dx, dy):
        self.text.update_position(dx, dy)
        self.x -= dx
        self.y -= dy


class TextShape(Shape):
    """ Draw text. """

    LEFT, CENTER, RIGHT = -1, 0, 1

    def __init__(self, pen, x, y, j, w, t):
        super().__init__()
        self.pen = pen.copy()
        self.x = x
        self.y = y
        self.descent = 2                  # Correction for y position (should
                                          # get this from font metrics)
        self.j = j                        # Centering
        self.w = w                        # width
        self.t = t                        # text
        self.layout: Pango.Layout = None  # the cached layout

        self.drawny = -_inf               # top of the text that was drawn
        self.drawnheight = 0              # height of the text that was drawn

    def _draw(self, cr, highlight=None, bounding=None):
        if not self.layout:
            self.set_layout(cr)

        width, height, f = self.get_draw_parameters()

        y = self.y - height + self.descent

        if bounding is None or (y <= bounding[3] and bounding[1] <= y + height):
            x = self.x - 0.5 * (1 + self.j) * width
            cr.move_to(x, y)
            self.drawny = y
            self.drawnheight = height

            cr.save()
            cr.scale(f, f)
            cr.set_source_rgba(*self.select_pen(highlight).color)
            PangoCairo.show_layout(cr, self.layout)
            cr.restore()

        if False:  # DEBUG
            # show where dot thinks the text should appear
            cr.set_source_rgba(1, 0, 0, .9)
            x = self.x - 0.5 * (1 + self.j) * width
            cr.move_to(x, self.y)
            cr.line_to(x + self.w, self.y)
            cr.stroke()

    def set_layout(self, cr: cairo.Context):
        """
        Set self.layout based on the markup in self.t.

        Parameters
        ----------
        cr : cairo.Context
            the context to get the layout from
        """
        layout = PangoCairo.create_layout(cr)

        # set font options
        # see http://lists.freedesktop.org/archives/cairo/2007-February/009688.html
        context = layout.get_context()
        fo = cairo.FontOptions()
        fo.set_antialias(cairo.ANTIALIAS_DEFAULT)
        fo.set_hint_style(cairo.HINT_STYLE_NONE)
        fo.set_hint_metrics(cairo.HINT_METRICS_OFF)
        try:
            PangoCairo.context_set_font_options(context, fo)
        except TypeError:
            # XXX: Some broken pangocairo bindings show the error
            # 'TypeError: font_options must be a cairo.FontOptions or None'
            pass
        except KeyError:
            # cairo.FontOptions is not registered as a foreign
            # struct in older PyGObject versions.
            # https://git.gnome.org/browse/pygobject/commit/?id=b21f66d2a399b8c9a36a1758107b7bdff0ec8eaa
            pass

        # set font
        font = Pango.FontDescription(self.pen.fontname)

        # https://developer.gnome.org/pango/stable/PangoMarkupFormat.html
        markup = GObject.markup_escape_text(self.t)
        if self.pen.bold:
            markup = '<b>' + markup + '</b>'
        if self.pen.italic:
            markup = '<i>' + markup + '</i>'
        if self.pen.underline:
            markup = '<span underline="single">' + markup + '</span>'
        if self.pen.strikethrough:
            markup = '<s>' + markup + '</s>'
        if self.pen.superscript:
            markup = '<sup><small>' + markup + '</small></sup>'
        if self.pen.subscript:
            markup = '<sub><small>' + markup + '</small></sub>'

        success, attrs, text, accel_char = Pango.parse_markup(markup, -1, '\x00')
        assert success
        layout.set_attributes(attrs)

        font.set_family(self.pen.fontname)
        font.set_absolute_size(self.pen.fontsize*Pango.SCALE)
        layout.set_font_description(font)

        # set text
        layout.set_text(text, -1)

        # cache it
        self.layout = layout
        PangoCairo.update_layout(cr, layout)

    def get_draw_parameters(self) -> Tuple[float, float, float, float]:
        """
        Due to differences between fonts, the exact text size may have to be
        calculated.

        Returns
        -------
        Tuple[float, float, float, float]
            The corrected width, height, epsilon to descend the text by, and
            scaling factor.
        """
        width, height = self.layout.get_size()
        width = float(width)/Pango.SCALE
        height = float(height)/Pango.SCALE

        # we know the width that dot thinks this text should have
        # we do not necessarily have a font with the same metrics.
        # scale it so that the text fits inside its box
        if self.w and width > self.w:
            f = self.w / width
            width = self.w  # equivalent to width *= f
            height *= f
            self.descent *= f
        else:
            f = 1.0
        
        return width, height, f

    def get_height(self, cr: cairo.Content=None):
        """
        Return the height of the layout.
        """
        if not self.layout:
            assert cr
            self.set_layout(cr)

        return self.get_draw_parameters()[1]

    def get_width(self, cr: cairo.Content=None):
        """
        Return the width of the layout.
        """
        if not self.layout:
            assert cr
            self.set_layout(cr)

        return self.get_draw_parameters()[0]
        

    def makePickleable(self):
        """
        Get rid of the cached Layout object.
        """
        self.layout = None

    def search_text(self, regexp):
        return regexp.search(self.t) is not None

    @property
    def bounding(self):
        """
        Return the bounding box of this text.
        """
        x, w, j = self.x, self.w, self.j
        if self.drawnheight == 0:
          return x - 0.5 * (1 + j) * w, -_inf, x + 0.5 * (1 - j) * w, _inf
        else:
          return (x - 0.5 * (1 + j) * w, self.drawny,
                  x + 0.5 * (1 - j) * w, self.drawny + self.drawnheight)

    def set_boundingbox(self):
        """
        Do nothing, since the bounding box is a propery.
        """
        pass

    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @warning: not sure if self.j (centering) needs to be modified!?

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        self.x -=  dx
        self.y -=  dy


class ImageShape(Shape):

    def __init__(self, pen, x0, y0, w, h, path):
        super().__init__()
        self.pen = pen.copy()
        self.x0 = x0
        self.y0 = y0
        self.w = w
        self.h = h
        self.path = path

    def _draw(self, cr):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.path)
        sx = float(self.w)/float(pixbuf.get_width())
        sy = float(self.h)/float(pixbuf.get_height())
        cr.save()
        cr.translate(self.x0, self.y0 - self.h)
        cr.scale(sx, sy)
        Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0)
        cr.paint()
        cr.restore()

    @property
    def bounding(self):
        x0, y0 = self.x0, self.y0
        return x0, y0 - self.h, x0 + self.w, y0

    def set_boundingbox(self):
        """
        Do nothing, since the bounding box is a propery.
        """
        pass


    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        self.x0 -= dx
        self.y0 -= dy


class EllipseShape(Shape):

    def __init__(self, pen, x0, y0, w, h, filled=False):
        super().__init__()
        self.pen = pen.copy()
        self.x0 = x0
        self.y0 = y0
        self.w = w
        self.h = h
        self.filled = filled

    def _draw(self, cr, highlight=None, bounding=None):
        cr.save()
        cr.translate(self.x0, self.y0)
        cr.scale(self.w, self.h)
        cr.move_to(1.0, 0.0)
        cr.arc(0.0, 0.0, 1.0, 0, 2.0*math.pi)
        cr.restore()
        pen = self.select_pen(highlight)
        if self.filled:
            cr.set_source_rgba(*pen.fillcolor)
            cr.fill()
        else:
            cr.set_dash(pen.dash)
            cr.set_line_width(pen.linewidth)
            cr.set_source_rgba(*pen.color)
            cr.stroke()

    @property
    def bounding(self):
        x0, y0, w, h = self.x0, self.y0, self.w, self.h
        bt = 0 if self.filled else self.pen.linewidth / 2.
        w, h = w + bt, h + bt
        return x0 - w, y0 - h, x0 + w, y0 + h


    def set_boundingbox(self):
        """
        Do nothing, since the bounding box is a propery.
        """
        pass
            

    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        self.x0 -= dx
        self.y0 -= dy


class PolygonShape(Shape):

    def __init__(self, pen, points, filled=False):
        super().__init__()
        self.pen = pen.copy()
        self.points = points
        self.filled = filled

        self.set_boundingbox()

    def set_boundingbox(self):
        x0, y0, x1, y1 = Shape._bounds_from_points(self.points)
        bt = 0 if self.filled else self.pen.linewidth / 2.
        self.bounding = x0 - bt, y0 - bt, x1 + bt, y1 + bt

    def _draw(self, cr, highlight=None, bounding=None):
        x0, y0 = self.points[-1]
        cr.move_to(x0, y0)
        for x, y in self.points:
            cr.line_to(x, y)
        cr.close_path()
        pen = self.select_pen(highlight)
        if self.filled:
            cr.set_source_rgba(*pen.fillcolor)
            cr.fill_preserve()
            cr.fill()
        else:
            cr.set_dash(pen.dash)
            cr.set_line_width(pen.linewidth)
            cr.set_source_rgba(*pen.color)
            cr.stroke()
            

    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        
       
        self.points[len(self.points)-1][0]-=dx
        self.points[len(self.points)-1][1]-=dy
        self.points[0][0]-=dx
        self.points[0][1]-=dy
        """
        for p in self.points:
            p[0] -= dx/2
            p[1] -= dy/2
   

class LineShape(Shape):

    def __init__(self, pen, points):
        super().__init__()
        self.pen = pen.copy()
        self.points = points

        self.set_boundingbox()

    def set_boundingbox(self):
        x0, y0, x1, y1 = Shape._bounds_from_points(self.points)
        bt = self.pen.linewidth / 2.
        self.bounding = x0 - bt, y0 - bt, x1 + bt, y1 + bt

    def _draw(self, cr, highlight=None, bounding=None):
        x0, y0 = self.points[0]
        cr.move_to(x0, y0)
        for x1, y1 in self.points[1:]:
            cr.line_to(x1, y1)
        pen = self.select_pen(highlight)
        cr.set_dash(pen.dash)
        cr.set_line_width(pen.linewidth)
        cr.set_source_rgba(*pen.color)
        cr.stroke()

    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        for p in self.points:
            p[0] -= dx
            p[1] -= dy


class BezierShape(Shape):

    def __init__(self, pen, points, filled=False):
        super().__init__()
        self.pen = pen.copy()
        self.points = points
        self.filled = filled

        self.set_extremalpoints()
        self.set_boundingbox()

    def set_extremalpoints(self):
        x0, y0 = self.points[0]
        xa = xb = x0
        ya = yb = y0
        for i in range(1, len(self.points), 3):
            (x1, y1), (x2, y2), (x3, y3) = self.points[i:i+3]
            for t in self._cubic_bernstein_extrema(x0, x1, x2, x3):
                if 0 < t < 1:  # We're dealing only with Bezier curves
                    v = self._cubic_bernstein(x0, x1, x2, x3, t)
                    xa, xb = min(xa, v), max(xb, v)
            xa, xb = min(xa, x3), max(xb, x3)  # t=0 / t=1
            for t in self._cubic_bernstein_extrema(y0, y1, y2, y3):
                if 0 < t < 1:  # We're dealing only with Bezier curves
                    v = self._cubic_bernstein(y0, y1, y2, y3, t)
                    ya, yb = min(ya, v), max(yb, v)
            ya, yb = min(ya, y3), max(yb, y3)  # t=0 / t=1
            x0, y0 = x3, y3

        bt = 0 if self.filled else self.pen.linewidth / 2.

        self.xa = xa
        self.ya = ya
        self.xb = xb
        self.yb = yb
        self.bt = bt

    def set_boundingbox(self):
        self.set_extremalpoints()
        self.bounding = (self.xa - self.bt, self.ya - self.bt,
                         self.xb + self.bt, self.yb + self.bt)

    @staticmethod
    def _cubic_bernstein_extrema(p0, p1, p2, p3):
        """
        Find extremas of a function of real domain defined by evaluating
        a cubic bernstein polynomial of given bernstein coefficients.
        """
        # compute coefficients of derivative
        a = 3.*(p3-p0+3.*(p1-p2))
        b = 6.*(p0+p2-2.*p1)
        c = 3.*(p1-p0)

        if a == 0:
            if b == 0:
                return ()  # constant
            return (-c / b,)  # linear

        # quadratic
        # compute discriminant
        d = b*b - 4.*a*c
        if d < 0:
            return ()

        k = -2. * a
        if d == 0:
            return (b / k,)

        r = math.sqrt(d)
        return ((b + r) / k, (b - r) / k)

    @staticmethod
    def _cubic_bernstein(p0, p1, p2, p3, t):
        """
        Evaluate polynomial of given bernstein coefficients
        using de Casteljau's algorithm.
        """
        u = 1 - t
        return p0*(u**3) + 3*t*u*(p1*u + p2*t) + p3*(t**3)

    def _draw(self, cr, highlight=None, bounding=None):
        x0, y0 = self.points[0]
        cr.move_to(x0, y0)
        for i in range(1, len(self.points), 3):
            (x1, y1), (x2, y2), (x3, y3) = self.points[i:i+3]
            cr.curve_to(x1, y1, x2, y2, x3, y3)
        pen = self.select_pen(highlight)
        if self.filled:
            cr.set_source_rgba(*pen.fillcolor)
            cr.fill_preserve()
            cr.fill()
        else:
            cr.set_dash(pen.dash)
            cr.set_line_width(pen.linewidth)
            cr.set_source_rgba(*pen.color)
            cr.stroke()
       
    def update_position(self, dx, dy):
        """
        Update the position of the endpoints.
        
        @note: all the points should be shifted so that they may be redrawn
               properly, but this is unimplemented.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        for p in self.points:
            p[0] -= dx
            p[1] -= dy

    def update_sort_edge(self,dx,dy):
        for p in self.points:
            if p is not self.points[len(self.points)-1]:
                p[0] -= dx/2
                p[1] -= dy/2
        self.points[0][0]-=dx/2
        self.points[0][1]-=dy/2

    def update_ent_edge(self,dx,dy):
        for p in self.points:
            if p is not self.points[0]:
                p[0] -= dx/2
                p[1] -= dy/2
        self.points[len(self.points)-1][0]-=dx/2
        self.points[len(self.points)-1][1]-=dy/2
        

class CompoundShape(Shape):

    def __init__(self, shapes):
        super().__init__()
        self.shapes: List[Shape] = shapes
        self.set_boundingbox()

    def set_boundingbox(self):
        for shape in self.shapes:
            shape.set_boundingbox()
        self.bounding = Shape._envelope_bounds(map(_get_bounding, self.shapes))

    def copyInto(self, cshape):
        """
        Copy the member data of self into the given instance.
        """
        if isinstance(cshape, CompoundShape):
            Shape.copyInto(self, cshape)
        else:
            raise NotImplementedError
        cshape.shapes = self.shapes

    def _draw(self, cr: cairo.Context, highlight: bool,
              bounding: Tuple[float, float, float, float]):
        """
        Draw everything in this CompoundShape.

        Parameters
        ----------
        cr : cairo.Context
            the context to draw it in
        highlight : bool
            highlight the shape in red?
        bounding : Tuple[float, float, float, float]
            the bounding box of the currently viewable area
        """
            # If the following statement is true, that means that there was
            # text as part of this CompoundShape, and it has only been drawn
            # once before. The reason is that TextShape height is only know
            # once it's been drawn the first time.
        if self.bounding[1] == -_inf:
            self.set_boundingbox()

        wasdrawn = False
            # If the whole node is in the boundingbox, then make every shape
            # automatically pass the boundingbox test.
        if bounding is not None and self._fully_in(bounding):
            bounding = None
            wasdrawn = True

            # Draw the shapes that intersect the boudingbox.
        for shape in self.shapes:
            if bounding is None or shape._intersects(bounding):
                shape._draw(cr, highlight, bounding)
                wasdrawn = True

        return wasdrawn

    def search_text(self, regexp):
        for shape in self.shapes:
            if shape.search_text(regexp):
                return True
        return False


    def update_position(self, dx, dy):
        """
        Update the position of everything so that it may be redrawn in a
        shifted location.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        for sh in self.shapes:
            sh.update_position(dx,dy)


class Url(object):

    def __init__(self, item, url, highlight=None):
        self.item = item
        self.url = url
        if highlight is None:
            highlight = set([item])
        self.highlight = highlight


class Jump(object):

    def __init__(self, item, x, y, highlight=None):
        self.item = item
        self.x = x
        self.y = y
        if highlight is None:
            highlight = set([item])
        self.highlight = highlight


class Element(CompoundShape):
    """Base class for graph nodes and edges."""

    def __init__(self, shapes):
        super().__init__(shapes=shapes)

    def copyInto(self, element):
        """
        Copy the member data of self into the given instance.
        """
        if isinstance(element, Element):
          CompoundShape.copyInto(self, element)
        else:
          raise NotImplementedError
          
    def is_inside(self, x, y):
        return False

    def get_url(self, x, y):
        return None

    def get_jump(self, x, y):
        return None


class DNode(Element):
    """
    A DNode knows it's position, width, height, and it's two corner points
    ((x1,y1),(x2,y2)).  It also has a unique "url", and can be associated with
    an annotation box (AnnotShape).

    @note: the Shapes associated with this node are all stored in shapes,
           whereas the annotation is drawn independently with
           L{draw_annotation()}.
    """

    def __init__(self, id=None, x=0, y=0, w=0, h=0, shapes=[], url=-2):
        super().__init__(shapes=shapes)

        self.id = id          #: Unique string representing node
        self.x = x            #: x position
        self.y = y            #: y position
        self.w = w            #: width of node
        self.h = h            #: height of node

        self.x1 = x - 0.5*w   #: The left? side of the DNode.
        self.y1 = y - 0.5*h   #: The top? of the DNode.
        self.x2 = x + 0.5*w   #: The right? side of the DNode.
        self.y2 = y + 0.5*h   #: The bottom? of the DNode.

        self.url = url    #: MatchGraph.codeTOnode[int(url)] is MNode.

        self.annot: AnnotShape = None #: The AnnotShape attached to this DNode.
        self.clicktarget = ClickTarget(self, self.x2, self.y1)

    def is_inside(self, x, y):
        return self.x1 <= x and x <= self.x2 and self.y1 <= y and y <= self.y2

    def copyInto(self, other: 'DNode'):
        """
        Copy the instance variable of this DNode into the given DNode.
        """
        if isinstance(other, DNode):
          Element.copyInto(self, other)
        else:
          raise NotImplementedError

        other.id = self.id
        other.x = self.x
        other.y = self.y
        other.w = self.w
        other.h = self.h

        other.x1 = self.x1
        other.y1 = self.y1
        other.x2 = self.x2
        other.y2 = self.y2

        other.url = self.url
        other.annot = self.annot

    def _draw(self, cr: cairo.Context, highlight: bool,
              bounding: Tuple[float, float, float, float]):
        """
        Draw this node.

        This calls CompoundShape._draw() to do most of the work, adding a
        ClickTarget in the upper corner.

        Parameters
        ----------
        cr : cairo.Context
            the context to draw the shapes in
        highlight : bool
            is this node highlighted?
        bounding : Tuple[float, float, float, float]
            the bounding box for the viewable region
        """
        super()._draw(cr, highlight, bounding) 

        if highlight:         #If node is highlighed, draw the ClickTarget
            self.clicktarget.visible = True
            self.clicktarget.draw(cr, bounding, self.x2, self.y1)
        else:
            self.clicktarget.visible = False

    def draw_annotation(self, cairocontext: cairo.Context,
                        bounding: Tuple[float, float, float, float]):
        """
        Draw the associated AnnotShape if it is defined and marked as visible.
        """
        if(self.annot and self.annot.visible):
            self.annot._draw(cairocontext, bounding)

    def draw_connector(self, cairocontext,
                        bounding: Tuple[float, float, float, float]):
        """
        Draw the connector to the associated AnnotShape if it is defined
        and marked as visible.
        """
        if self.annot and self.annot.visible:
           xpos = self.x
           ypos = self.y - (self.h/2)  #Draw connector to our current position.
           self.annot.draw_connector(cairocontext, xpos, ypos, bounding)


    def _get_longest_line(self, text):
        """
        Return the number of characters in the longest line of the given string.
        """
        i=0
        s=0
        list=text.split('\n')
        while(i<len(list)):
            if len(list[i])>s:
                s=len(list[i])
            i=i+1
        return s

    def activate_annotation(self):
        raise NotImplementedError

    def deactivate_annotation(self):
        """
        Hide the annotation associated with this node.
        """
        if self.annot:
            self.annot.visible = False


    def annot_visible(self):
        """
        Return True if this nodes annotation is visible.
        """
        return self.annot and self.annot.visible



    def update_position(self, dx, dy):
        """
        Given a pair of delta that are scaled for the current zoom level,
        update the position and the Shapes (used for drawing) associated with 
        this node.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        self.x -= dx
        self.y -= dy

        self.x1 -= dx
        self.y1 -= dy
        self.x2 -= dx
        self.y2 -= dy

        for sh in self.shapes:
            sh.update_position(dx, dy)

    def makePickleable(self):
        """
        Clear all cached layouts in the TextShapes.
        """
        for shape in self.shapes:
            if isinstance(shape, TextShape):
                shape.makePickleable()

        if self.annot:
            self.annot.makePickleable()

    def get_url(self, x, y):
        """
        Return the Node's unique identifier.
        """
        if self.url is None:
            return None
        if self.is_inside(x, y):
            return Url(self, self.url)
        return None

    def get_jump(self, x, y):
        if self.is_inside(x, y):
            return Jump(self, self.x, self.y)
        return None

    def __repr__(self):
        return "<Node %s>" % self.id


def square_distance(x1, y1, x2, y2):
    deltax = x2 - x1
    deltay = y2 - y1
    return deltax*deltax + deltay*deltay


class DEdge(Element):

    def __init__(self, src, dst, points, shapes):
        super().__init__(shapes=shapes)
        self.src = src
        self.dst = dst
        self.points = points

    RADIUS = 10

    def is_inside_begin(self, x, y):
        return square_distance(x, y, *self.points[0]) <= self.RADIUS*self.RADIUS

    def is_inside_end(self, x, y):
        return square_distance(x, y, *self.points[-1]) <= self.RADIUS*self.RADIUS

    def is_inside(self, x, y):
        if self.is_inside_begin(x, y):
            return True
        if self.is_inside_end(x, y):
            return True
        return False

    def get_jump(self, x, y):
        if self.is_inside_begin(x, y):
            return Jump(self, self.dst.x, self.dst.y, highlight=set([self, self.dst]))
        if self.is_inside_end(x, y):
            return Jump(self, self.src.x, self.src.y, highlight=set([self, self.src]))
        return None

    def __repr__(self):
        return "<Edge %s -> %s>" % (self.src, self.dst)

    def update_position_out_edges(self, dx, dy):
        """
        Given a pair of delta that are scaled for the current zoom level,
        update the position and the Shapes (used for drawing) associated with 
        the edges going out.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        
        for sh in self.shapes:
            if isinstance(sh, BezierShape):
                sh.update_sort_edge(dx,dy)

    def update_position_in_edges(self, dx, dy):
        """
        Given a pair of delta that are scaled for the current zoom level,
        update the position and the Shapes (used for drawing) associated with 
        the edges coming in.

        @param dx: the delta x (scaled for the current zoom level)
        @type dx:  float
        @param dy: the delta y (scaled for the current zoom level)
        @type dy:  float
        """
        
        for sh in self.shapes:
            if isinstance(sh, BezierShape):
                sh.update_ent_edge(dx,dy)
            if isinstance(sh, PolygonShape):
                sh.update_position(dx,dy)





class DGraph(Shape):
    """
    The shapes for displaying a graph (a refactoring of xdot.py).
    """

    def __init__(self, width=1, height=1, shapes=(), nodes=(), edges=(),
                 outputorder='breadthfirst'):
        super().__init__()

        self.width = width
        self.height = height
        self.shapes: List[Shape] = shapes
        self.nodes: List[DNode] = nodes
        self.edges: List[DEdge] = edges
        self.outputorder = outputorder

        self.set_boundingbox()

    def set_boundingbox(self):
        for shape in chain(self.getShapes(), self.getDNodes(), self.getDEdges()):
            shape.set_boundingbox()

        self.bounding = Shape._envelope_bounds(
            map(_get_bounding, self.getShapes()),
            map(_get_bounding, self.getDNodes()),
            map(_get_bounding, self.getDEdges()))

    def get_exitEdg_by_node(self, node):
        ed=[]
        for e in self.getDEdges():
            if e.src is node:
                ed.append(e)
        return ed

    def get_entEdg_by_node(self, node):
        ed=[]
        for e in self.getDEdges():
            if e.dst is node:
                ed.append(e)
        return ed

    def update_position(self):
        pass

    def get_size(self):
        return self.getWidth(), self.getHeight()

    def _draw_shapes(self, cr, bounding):
        for shape in self.getShapes():
            if bounding is None or shape._intersects(bounding):
                shape._draw(cr, highlight=False, bounding=bounding)

    def _draw_nodes(self, cr, bounding, highlight_items):
        for node in self.getDNodes():
            if bounding is None or node._intersects(bounding):
                node._draw(cr, highlight=(node in highlight_items),
                           bounding=bounding)

    def _draw_edges(self, cr, bounding, highlight_items):
        for edge in self.getDEdges():
            if bounding is None or edge._intersects(bounding):
                should_highlight = any(e in highlight_items
                                       for e in (edge, edge.src, edge.dst))
                edge._draw(cr, highlight=should_highlight, bounding=bounding)

    def _draw(self, cr: cairo.Context, highlight_items: List[DNode]=None,
              bounding: Tuple[float, float, float, float]=None):
        """
        Draw the graph and the annotations.

        Parameters
        ----------
        cr : cairo.Context
            the context to draw the graph in
        highlight_items : List[DNode], optional
            the nodes to highlight, by default None
        bounding : Tuple[float, float, float, float], optional
            the bounding box of the full widget, by default None
        """
        drawgraph = False
        if bounding is None or self._intersects(bounding):
            drawgraph = True

        if drawgraph and self._fully_in(bounding):
            bounding = None             #Draw all shapes without checking

        if highlight_items is None:
            highlight_items = ()

            #Draw boxes around nodes (if any), and white graph background.
        if drawgraph:
            cr.set_source_rgba(0.0, 0.0, 0.0, 1.0)
            cr.set_line_cap(cairo.LINE_CAP_BUTT)
            cr.set_line_join(cairo.LINE_JOIN_MITER)

            self._draw_shapes(cr, bounding)

            #Draw annotations connector next.
        for node in self.getDNodes():
          node.draw_connector(cr, bounding)

            #Draw edges and nodes.
        if drawgraph:
            if self.outputorder == 'edgesfirst':
                self._draw_edges(cr, bounding, highlight_items)
                self._draw_nodes(cr, bounding, highlight_items)
            else:
                self._draw_nodes(cr, bounding, highlight_items)
                self._draw_edges(cr, bounding, highlight_items)

            #Draw the annotations on top.
        for node in self.getDNodes():
            node.draw_annotation(cr, bounding)


    def get_element(self, x, y):
        """
        Return the element (Node, Annotation, etc.) that covers this point.
        """
        for node in reversed(self.getDNodes()): #Reverse the list so that the
            if node.annot and node.annot.is_inside(x, y):
                return node.annot               #last drawn (topmost) is first.
            if node.clicktarget.is_inside(x, y):
                return node.clicktarget

        for node in self.getDNodes():
            if node.is_inside(x, y):
                return node

    def get_url(self, x, y):
        for node in self.getDNodes():
            url = node.get_url(x, y)
            if url is not None:
                return url
        return None

    def get_jump(self, x, y):
        for edge in self.getDEdges():
            jump = edge.get_jump(x, y)
            if jump is not None:
                return jump
        for node in self.getDNodes():
            jump = node.get_jump(x, y)
            if jump is not None:
                return jump
        return None

    def getDNodes(self, graph=None):
        """
        Return the list of DNodes for this DGraph.

        :note: overridden in InterGraph
        """
        return self.nodes

    def getDEdges(self):
        """
        Return the list of DEdges for this DGraph.

        :note: overridden in InterGraph
        """
        return self.edges

    def getShapes(self):
        """
        Return the list of Shapes for this DGraph.
        """
        return self.shapes

    def getWidth(self):
        """
        Return the width of this DGraph.
        """
        return self.width

    def getHeight(self):
        """
        Return the height of this DGraph.
        """
        return self.height


class Pen(object):
    """Store pen attributes."""

    def __init__(self):
        # set default attributes
        self.color = (0.0, 0.0, 0.0, 1.0)
        self.fillcolor = (0.0, 0.0, 0.0, 1.0)
        self.linewidth = 1.0
        self.fontsize = 14.0
        self.fontname = "Serif"
        self.bold = False
        self.italic = False
        self.underline = False
        self.superscript = False
        self.subscript = False
        self.strikethrough = False
        self.overline = False

        self.dash = ()

    def copy(self):
        """Create a copy of this pen."""
        pen = Pen()
        pen.__dict__ = self.__dict__.copy()
        return pen

    def highlighted(self):
        pen = self.copy()
        pen.color = (1, 0, 0, 1)
        pen.fillcolor = (1, .8, .8, 1)
        return pen


