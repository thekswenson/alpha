# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Stuff to deal with annotatons (under construction)
"""
from typing import Callable
from typing import Dict
import time

from Bio import Entrez
from Bio import SeqRecord

from .pyinclude.ncbi import fetchGBRecords
from .pyinclude.ncbi import accFromDescription
from .pyinclude.usefulfuncs import invdict
from .pyinclude.usefulfuncs import printerr
from .pyinclude.ncbi import InvalidAccessionError
from .userconfig import getEmail
from .userconfig import getRecordsDir
from .userconfig import DEFAULT_CONFIG


#        _________________________
#_______/        Functions        \_____________________________________________


class Annotations(object):
  """
  Keep track of annotations downloaded from NCBI.

  Attributes
  ----------
  idTOrec : Dict[str, str]
      map gid to genbank record
  """

  def __init__(self, idTOstr: Dict[str, str]=None, updatefunc: Callable=None):
    """
    Download annotations for the given genomes.

    Notes
    -----
    Call download() to download the annotation.
    Call isReady() to find out if the object is ready to call getAnnotations().

    Parameters
    ----------
    idTOstr : Dict[str, str]
        map gid to genome description
    updatefunc :
        send update messages to this function
    """
    self.idTOrec: Dict[str, SeqRecord] = {}
    self._ready = False  #: Did the object load properly?

    if idTOstr:
      self.download(idTOstr, updatefunc)


  def download(self, idTOstr: Dict[str, str]=None, updatefunc: Callable=None):
    """
    Download annotations for the given genomes.

    Parameters
    ----------
    idTOstr : Dict[str, str]
        map gid to genome description
    updatefunc :
        send update messages to this function

    Returns
    ----------
    True if we succesfully downloaded the annotations.
    """
    disablingstr = '  - DISABLING annotation support.'
    try:
      self.initializeIDTOREC(idTOstr, updatefunc)
      self._ready = True

    except AnnotationNotFoundError as e:
      print(f'WARNING: {e}\n{disablingstr}')
      if updatefunc: updatefunc(f'{e}\n{disablingstr}')
      time.sleep(5)
      return False

    except IOError as e:
      print(f'Check your network connection:\n\t{e}\n{disablingstr}')
      if updatefunc:
        updatefunc(f'Check your network connection!\n\t{e}\n{disablingstr}')
      time.sleep(5)
      return False

    except InvalidAccessionError as e:
      print(f'Problem with an Accession:\n\t{e}\n{disablingstr}')
      if updatefunc: updatefunc(f'{e}\n{disablingstr}')
      time.sleep(5)
      return False

    except NoNCBIEmailError as e:
      print(f'WARNING: {e}\n{disablingstr}')
      if updatefunc: updatefunc(f'{e}\n{disablingstr}')
      time.sleep(5)
      return False

    return True

  def isReady(self):
    """
    Return True if this is a usable object (the records downloaded).
    """
    return self._ready

  def initializeIDTOREC(self, idTOstr, updatefunc: Callable=None) -> None:
    """
    Initialize the dictionnary self.idTOrec.

    :note:        Severine's code

    :param idTOstr:    the dictionnary mapping genome id with their string
                       description
    :param updatefunc: function used to send update messages to the user
    """
    idTOacc = {}
    Entrez.email = getEmail()
    if not Entrez.email:
      raise NoNCBIEmailError(f'Add your email address to'
                             f' "{DEFAULT_CONFIG}"!')

    for genome in idTOstr:
      accession = accFromDescription(idTOstr[genome], updatefunc)
      if accession:
        idTOacc[genome] = accession

    if not idTOacc:
      message = 'No NCBI accession numbers found. Check your FASTA format.'
      if updatefunc: updatefunc(message)
      raise AnnotationNotFoundError(message)

    if updatefunc: updatefunc('Fetching NCBI annotations...')
    self.idTOrec = fetchGBRecords(idTOacc.values(), invdict(idTOacc),
                                  getRecordsDir())



  def getAnnotations(self, gid, start, end, warn=True):
    """
    Get the annotations recorded on genome gid between positions start and end

    @note: if there is no value in the self.idTOrec dictionary for this gid,
           then return an empty list.

    @param gid:   genome id
    @param start: starting position of the subsequence of the genome to look for
                  annotations
    @param end:   ending position  of the subsequence of the genome to look for
                  annotations
    @param warn:  warn the user if IDTOREC is not defined
    @return:      a list of annotations with start and end
                  (annotation,start,end); start and end are pythonic indices, for
                  example 0 to 5 gives the first to the fifth element
    """
    assert(self.isReady())

    information = []
    if not self.idTOrec:
      if warn:
        printerr('WARN: self.idTOrec is not defined!')
      return information

    if gid not in self.idTOrec:
      return information

    overlaprequired = 0   #For the moment we take all overlapping annotations
    for f in self.idTOrec[gid].features:
      s = int(f.location.start)
      e = int(f.location.end)
      if(('product' in f.qualifiers and (f.type == 'gene' or f.type == 'CDS'))
         and
         ((s >= start and e <= end) or # gene in interval
          (s < start and e > end)   or # interval in gene/CDS
                                       #Proportaion of annotation in interval
          ((e <= end and (e-start)/float(e-s) > overlaprequired) or
           (s >= start and (end-s)/float(e-s) > overlaprequired)))):
        annotation = f.qualifiers['product'][0]
        information.append((annotation,s,e))

    return information

class AnnotationNotFoundError(Exception):
  def __init__(self, value):
    super().__init__()
    self.value = value

  def __str__(self):
    return self.value

class NoNCBIEmailError(Exception):
  def __init__(self, value):
    super().__init__()
    self.value = value

  def __str__(self):
    return self.value
