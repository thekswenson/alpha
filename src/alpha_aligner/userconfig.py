"""
Deal with user and system configuration.  All the get and set functions
read and write files to and the from the user configuration file at
DEFAULT_CONFIG.
"""
import os
from configparser import ConfigParser

from . import ALPHA_CONFIG_DIR

THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))

TEMPLATE_CONFIG = f'{THIS_FILE_DIR}/config_template.ini'
DEFAULT_CONFIG = f'{ALPHA_CONFIG_DIR}/alpha_config.ini'

def getEmail(configfile: str=DEFAULT_CONFIG):
  """
  Read an email address from the config file.
  """
  email = getVal('userinfo', 'email', configfile)
  if not email:
    return email

  if '@' in email:
    return email
  else:
    raise EmailFormatError(f'ERROR: email "{email}" format unrecognized.')

def setEmail(email: str, configfile: str=DEFAULT_CONFIG):
  """
  Set the email address in the config file.
  """
  if '@' not in email:
    raise EmailFormatError(f'ERROR: email "{email}" format unrecognized.')

  setVal('userinfo', 'email', email, configfile)

def getLastOpened(configfile: str=DEFAULT_CONFIG):
  """
  Read the last opened path from the config file.
  """
  return getVal('paths', 'lastopened', configfile)

def setLastOpened(path: str, configfile: str=DEFAULT_CONFIG):
  """
  Set the last opened path in the config file.
  """
  setVal('paths', 'lastopened', path, configfile)

def getRecordsDir(configfile: str=DEFAULT_CONFIG):
  """
  Read the records dir path from the config file.
  """
  return getVal('paths', 'recordsdir', configfile)

def setRecordsDir(path: str, configfile: str=DEFAULT_CONFIG):
  """
  Set the records dir path in the config file.
  """
  setVal('paths', 'recordsdir', path, configfile)

def getVal(section: str, name: str, configfile: str=DEFAULT_CONFIG):
  """
  Get a value from the config file.
  """
  config = ConfigParser()
  config.read(configfile)
  if not config.read(configfile) or not config.has_option(section, name):
    return ''

  return config[section][name]

def setVal(section: str, name: str, val: str, configfile: str=DEFAULT_CONFIG):
  """
  Set a value in the config file.
  """
  config = ConfigParser()
  if not config.read(configfile):
    config = createSettingFile()

  config[section][name] = val
  with open(configfile, 'w') as f:
    config.write(f)
  
def createSettingFile(configfile: str=DEFAULT_CONFIG) -> ConfigParser:
  """
  Create the config file. Return the parser that was created.
  """
  if os.path.exists(configfile):
    raise ConfigFileExistsError(f'{configfile} already exists!')

  config = ConfigParser()
  config.read_file(open(TEMPLATE_CONFIG))

  config['paths']['recordsdir'] = f'{ALPHA_CONFIG_DIR}/records'

  if not os.path.exists(ALPHA_CONFIG_DIR):
    os.makedirs(ALPHA_CONFIG_DIR)
  with open(configfile, 'w') as f:
    config.write(f)

  return config
  



class ConfigFileExistsError(Exception):
  def __init__(self, value):
    super().__init__()
    self.value = value

  def __str__(self):
    return self.value

class EmailFormatError(Exception):
  def __init__(self, value):
    super().__init__()
    self.value = value

  def __str__(self):
    return self.value
