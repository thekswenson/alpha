# Redefine the interface to eqclasses.c.
#

cdef extern from "ceqclasses.c":
  ctypedef struct EqClassSet:
      int numclasses   #Length of the following lists.
      int **classlist  #The list of classes where entries 2i and 2i+1 are the
                       #genome id and start index.
      int *classsizes  #The size of each class (# of elements).
      int *classwidths #The width of each class (size of interval).
      pass

  EqClassSet* newEqClassSet(int nclasses)
  void freeEqClassSet(EqClassSet* cset)

  #Compute equivalence classes of positions, such that adjacent positions that
  #are all equivalent are glued together into single blocks of equivalent
  #positions.
  #
  #The memory for the given lists is not freed.
  EqClassSet* geteqclasses( int numgens, int* lenlist, int nummatches,
                            int* gen1, int* start1, int* gen2, int* start2,
                            int* matchlens )
