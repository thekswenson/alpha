# Interface to eqclasses C code.
import sys
import os
from libc.stdlib cimport malloc
from libc.stdlib cimport free
import itertools

cimport ceqclasses

from ..Match import Match

cdef class EqClassSet:
  cdef ceqclasses.EqClassSet *_c_eqclassset

  def __cinit__(self, numclasses):
    self._c_eqclassset = ceqclasses.newEqClassSet(numclasses)
    if(self._c_eqclassset is NULL):
      raise MemoryError()

  def __dealloc__(self):
    if(self._c_eqclassset is not NULL):
      ceqclasses.freeEqClassSet(self._c_eqclassset)


  def get_classes(self, indTOid):
    """
    Build a list of equivalence classes of matches from the C equivalence
    classes.

    @param indTOid: maps list index to genome id.
    @return: return a list of frozensets of Match instances.
    """
    eqclasslist = []
    for i in range(self._c_eqclassset.numclasses):
      width = self._c_eqclassset.classwidths[i]

      pairlist = []
      for j in range(self._c_eqclassset.classsizes[i]):
        pairlist.append((self._c_eqclassset.classlist[i][2*j],
                         self._c_eqclassset.classlist[i][2*j+1]))
      matchlist = []
      for (qint,qstart), (sint,sstart) in itertools.combinations(pairlist, 2):
        qid = indTOid[qint]
        sid = indTOid[sint]
        if(qid < sid):
          matchlist.append(Match(qid, sid,
                           match=(qstart, qstart+width, sstart, sstart+width)))
        else:
          matchlist.append(Match(sid, qid,
                           match=(sstart, sstart+width, qstart, qstart+width)))

      eqclasslist.append(frozenset(matchlist))

    return eqclasslist




def getEqClasses(idTOmatchlists, idTOlen):
  """
  Return equivalence classes of matches that correspond to the equal
  intervals, based on the given match lists.
  """
  allmatches = set(itertools.chain(*idTOmatchlists.values()))

  numgenomes = len(idTOmatchlists)

  #for m in idTOmatchlists['C']:
  #  s = m['C']
  #  if s.s <= 21930 <= s.e:
  #    print(f'found: {m}')

  idTOind = {}
  indTOid = {}
  cdef int *lengthlist = <int *>malloc(numgenomes * sizeof(int))
  for genseqid, gid in enumerate(idTOmatchlists):
    idTOind[gid] = genseqid
    indTOid[genseqid] = gid
    lengthlist[genseqid] = idTOlen[gid]

  cdef int nummatches = len(allmatches)

  cdef int *gen1 = <int *>malloc(nummatches * sizeof(int))
  cdef int *gen2 = <int *>malloc(nummatches * sizeof(int))
  cdef int *start1 = <int *>malloc(nummatches * sizeof(int))
  cdef int *start2 = <int *>malloc(nummatches * sizeof(int))
  cdef int *matchlens = <int *>malloc(nummatches * sizeof(int))
  for i, m in enumerate(allmatches):
    gen1[i] = idTOind[m.gid1]
    start1[i] = m.s1
    gen2[i] = idTOind[m.gid2]
    start2[i] = m.s2
    matchlens[i] = m.e1-m.s1

  eqc = ceqclasses.geteqclasses(numgenomes, lengthlist, nummatches,
                                gen1, start1, gen2, start2, matchlens)

  free(lengthlist)
  free(gen1)
  free(gen2)
  free(start1)
  free(start2)
  free(matchlens)

      #Build the matches from the return EqClassSet
      # (TODO: figure out how to put this into a function):
  eqclasslist = []
  for i in range(eqc.numclasses):
    width = eqc.classwidths[i]

    pairlist = []
    for j in range(eqc.classsizes[i]):
      pairlist.append((eqc.classlist[i][2*j], eqc.classlist[i][2*j+1]))

    matchlist = []
    for (qint,qstart), (sint,sstart) in itertools.combinations(pairlist, 2):
      qid = indTOid[qint]
      sid = indTOid[sint]
      if(qid < sid):
        matchlist.append(Match(qid, sid,
                         match=(qstart, qstart+width, sstart, sstart+width)))
      else:
        matchlist.append(Match(sid, qid,
                         match=(sstart, sstart+width, qstart, qstart+width)))

      #if 'C' in matchlist[-1]:
      #  s = matchlist[-1]['C']
      #  if 'K' in matchlist[-1]:
      #    print(f'match {s.s, s.e} {matchlist[-1]}')
      #  if s.s <= 21900 <= s.e:
      #    print(f'match! {matchlist[-1]}')

    eqclasslist.append(frozenset(matchlist))

  ceqclasses.freeEqClassSet(eqc)

  return eqclasslist
