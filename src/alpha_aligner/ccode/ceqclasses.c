/* Union-find algorithm to build alignment graph 
 * Now with built-in cycle detection for the same low low price *
 * Paul Guertin, Sept 2016 
 *
 * INPUT FORMAT:
 * 3  <- number of genomes
 * 8 
 * 8 
 * 8  <- length of each genome, in order. Genomes are numbered from 0.
 * 2  <- number of matches
 * 0 2 1 2 4  <- first match: genome1 start1 genome2 start2 length
 * 1 4 2 4 4
 * and so on, one line per match
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* KMS notes (not sure yet if they're correct):
 *
 * Integer elements (that are used in the union-find algorithm -- Paul calls
 * these "positions") are assigned to genome indices so that genome i
 * corresponds to elements
 *   [ genomelens[0] + ... + genomelens[i-1],
 *     genomelens[0] + ... + genomelens[i-1] + genomelens[i]-1 ].
 *
 * So, start[i] = genomelens[0] + ... + genomelens[i-1] is the element
 * corresponding to the first index of genome i.
 *
 * Match j has length mlens[j] and is between genomes g1[j] and g2[j], starting
 * at indices s1[j] and s2[j] respectively. So the first element covered by 
 * match j is start[g1[j]] + s1[j].
 *
 */

/* How many genomes, how many matches, sum of lengths of all genomes */
int nbgenomes, nbmatches, totlen;

/* For each match: genome1, start1, genome2, start2, length */
int *g1, *s1, *g2, *s2, *mlens;

/* For each genome: length and start position */
int *genomelens, *start;

/* Arrays representing the equivalence classes */
int *genome,  /* Genome number */
    *pos,     /* Position in genome */
    *parent,  /* Union-find parent */
    *rank,    /* Union-find rank */
    *mate,    /* Circular list of equivalence class members */
    *next,    /* Doubly linked, circular list of class representatives */
    *prev,    /* Same as above, points to predecessor */
    *size,    /* Number of positions in equivalence class */
    *width,   /* Length covered by equivalence class */
    *visited, /* Used for depth-first search cycle finding */
    *onstack; /* This also */

/*-----------------*/

typedef struct _EqClassSet {
  int numclasses;    //The length of the following lists.
  int **classlist;   //The list of classes where entries 2i and 2i+1 are the
                     //genome id and start index.
  int *classsizes;   //The size of each class (# of elements).
  int *classwidths;  //The width of each class (size of interval).
} EqClassSet;


EqClassSet* newEqClassSet( int nclasses )
{
  EqClassSet* s = malloc(sizeof(EqClassSet));
  s->numclasses = nclasses;

  s->classlist = malloc( nclasses * sizeof(int*) );
  s->classsizes = malloc( nclasses * sizeof(int) );
  s->classwidths = malloc( nclasses * sizeof(int) );
  if(s->classlist == NULL || s->classsizes == NULL || s->classwidths == NULL) {
    return NULL;
    }

  return s;
}


void freeEqClassSet( EqClassSet *cset )
{
  for( int i=0; i<cset->numclasses; i++) {
    free( cset->classlist[i] );
  }
  free( cset->classlist );
  free( cset->classsizes );
  free( cset->classwidths );
  free( cset );
}

int get_numclasses(EqClassSet *cset)
{
  return cset->numclasses;
}

int** get_classlist(EqClassSet *cset)
{
  return cset->classlist;
}

int* get_classsizes(EqClassSet *cset)
{
  return cset->classsizes;
}

int* get_classwidths(EqClassSet *cset)
{
  return cset->classwidths;
}


/*-----------------*/


int find( int );
EqClassSet* getclasses( void );
int* geteqclass( int );
int getnumberofclasses( void );



/*-----------------*/


/* Safely allocate array of ints */
int *allocate( int size )
{
  int *p = malloc( size * sizeof(int) );
  if( p == NULL ) {
    perror( "Memory allocation error." );
    exit(-1);
  }
  return p;
}

/* Read genome and match data from file, allocate arrays */
void readmatches( FILE *f )
{
  int i;

  if(!fscanf( f, "%d", &nbgenomes )) {
    printf("Problem parsing input file: number of genomes.\n");
    exit(1);
  }
  genomelens = allocate( nbgenomes );
  start = allocate( nbgenomes );
  totlen = 0;
  for( i=0; i<nbgenomes; i++ ) {
    if(!fscanf( f, "%d", genomelens+i )) {
      printf("Problem parsing input file: genome lengths.\n");
      exit(1);
    }
    start[i] = totlen;
    totlen += genomelens[i];
  }
  genome = allocate( totlen );
  pos = allocate( totlen );
  size = allocate( totlen );
  parent = allocate( totlen );
  rank = allocate( totlen );
  mate = allocate( totlen );
  prev = allocate( totlen );
  next = allocate( totlen );
  width = allocate( totlen );
  visited = allocate( totlen );
  onstack = allocate( totlen );

  if(!fscanf( f, "%d", &nbmatches )) {
    exit(1);
  }
  g1 = allocate( nbmatches );
  s1 = allocate( nbmatches );
  g2 = allocate( nbmatches );
  s2 = allocate( nbmatches );
  mlens = allocate( nbmatches );
  for( i=0; i<nbmatches; i++ ) {
    if(!fscanf( f, "%d %d %d %d %d", g1+i, s1+i, g2+i, s2+i, mlens+i )) {
      printf("Problem parsing input file: match lines.\n");
      exit(1);
    }
  }
}

void freemem( void )
{
  free( genomelens ); free( start );
  free( g1 ); free( s1 ); free( g2 ); free( s2 ); free( mlens );
  free( genome );  free( pos ); free( parent ); free( rank ); free( mate );
  free( next ); free( prev ); free( size ); free( width );
  free( visited ); free( onstack );
}


void initclasses( void )
{
  int i, j, k;

  k = 0;
  for( i=0; i<nbgenomes; i++ ) {
    for( j=0; j<genomelens[i]; j++ ) {
      genome[k] = i;
      pos[k] = j;
      size[k] = 1;
      width[k] = 1;
      parent[k] = k;
      rank[k] = 0;
      mate[k] = k;
      prev[k] = k-1;
      next[k] = k+1;
      k++;
    }
  }
  prev[0] = k-1;
  next[k-1] = 0;  /* Circularize the list */
//  printf( "Classes from 0 to %d\n", k-1 );
}

void printclass( int x )
{
  int xroot, y;

  xroot = find( x );
// Alpha does not print equivalence classes of size 1
  if( size[xroot] == 1 ) 
    return;
//  printf( "Width %d ", width[xroot] );
//  printf( "Prev: (%d,%d), Next: (%d,%d)\n", genome[prev[xroot]],
//    pos[prev[xroot]], genome[next[xroot]], pos[next[xroot]] );
  y = xroot;
  do {
    printf( "(%d,%d) ", genome[y], pos[y] );
    y = mate[y];
  } while( y != xroot );  
  printf( "\n" );
}

void printclasstofile( FILE *f, int x )
{
  int xroot, y;

  xroot = find( x );
// Alpha does not print equivalence classes of size 1
  if( size[xroot] == 1 ) 
    return;
  fprintf( f, "%d ", width[xroot] );
//  printf( "Prev: (%d,%d), Next: (%d,%d)\n", genome[prev[xroot]],
//    pos[prev[xroot]], genome[next[xroot]], pos[next[xroot]] );
  y = xroot;
  do {
    fprintf( f, "%d %d ", genome[y], pos[y] );
    y = mate[y];
  } while( y != xroot );  
  fprintf( f, "\n" );
}

void printtofile( char *filename )
{
  int x, first;
  FILE *f;

  f = fopen( filename, "w" );
  assert( f != NULL );
  
  first = find(0);
  x = first;
  do { 
    printclasstofile( f, x );
    x = next[x];
  } while( x != first );
  fclose( f );
}

void printall( void )
{
  int x, first;
  
  first = find(0);
  x = first;
  do { 
    //printf( "Classe %d, next = %d\n", x, next[x] );
    printclass( x );
    x = next[x];
  } while( x != first );
  printf( "-----\n" );
}

/* Return the representative of a position */
int _find( int x )
{  
  if( parent[x] != x )
    parent[x] = _find(parent[x]);  /* TODO: Turn into a loop for speed */
  return parent[x];
}

int find( int x )
{
  int y = _find(x);
//  printf( "find(%d) = %d\n", x, y );
  return y;
}

/* Join classes containing x and y */
void join( int x, int y )
{
  int xroot, yroot;
  int temp;

//  printf( "join(%d, %d)\n", x, y );
  xroot = find( x );
  yroot = find( y );
  if( xroot == yroot )
    return;
  if( rank[xroot] < rank[yroot] ) {
    parent[xroot] = yroot;
    size[yroot] += size[xroot];
    temp = yroot[mate];
    yroot[mate] = xroot[mate];
    xroot[mate] = temp;
    next[prev[xroot]] = next[xroot];
    prev[next[xroot]] = prev[xroot];
  }
  else {
    parent[yroot] = xroot;
    size[xroot] += size[yroot];
    temp = xroot[mate];      
    xroot[mate] = yroot[mate];
    yroot[mate] = temp;
    next[prev[yroot]] = next[yroot];
    prev[next[yroot]] = prev[yroot];
    if( rank[xroot] == rank[yroot] ) {
      rank[xroot]++;
    }
  }
}

void joinmatches( void )
{
  int x, y, i, j;

  for( i=0; i<nbmatches; i++ ) {
    x = start[g1[i]] + s1[i];
    y = start[g2[i]] + s2[i];
    for( j=0; j<mlens[i]; j++ ) {
//      printf( "match %d g1=%d (start %d) pos1=%d g2=%d (start %d) pos2=%d call join %d %d\n", 
//          i, g1[i], start[g1[i]], s1[i], g2[i], start[g2[i]], s2[i], x+j, y+j );
//      fflush(stdout);
      join( x+j, y+j );
    }
  }
}

/* Make singletons from an equivalence class */
void explode( int x )
{
  int y, z, nextmate;

  //printf( "We explode (%d,%d)\n", genome[x], pos[x] );  

  y = x;
  z = mate[y];

  while( z != x ) {
    //printf( "(%d,%d) est un rep.\n", genome[z], pos[z] );
    nextmate = mate[z];

    /* Make z a singleton */
    parent[z] = z;
    size[z] = 1;
    mate[z] = z;
    rank[z] = 0;

    /* Insert z in the list of classes */
    next[z] = next[y];
    prev[z] = y;
    prev[next[y]] = z;
    next[y] = z;  

    y = z;
    z = nextmate;
  }

  /* Make x a singleton */
  size[x] = 1;
  mate[x] = x;
  rank[x] = 0;
}

/* Explode all classes that need exploding (i.e. split node into singletons).*/
/* These are the classes that contain multiple matches per genome! */
void explodeclasses( void )
{
  int x, y, first, oldnext;
  int seen[nbgenomes];

  first = find(0);
  x = first;
  do {
    if( size[x] == 1 ) {
      x = next[x];
      continue;
    }
    oldnext = next[x];
    memset( seen, 0, nbgenomes*sizeof(int) );
    seen[genome[x]] = 1;
    y = mate[x];
    while( y != x ) {
      if( seen[genome[y]] ) {
        explode( x );
        break;
      }
      seen[genome[y]] = 1;
      y = mate[y];
    }
    x = oldnext;
  } while( x != first );
}

/* Glue two equivalence classes into a wider one */
int glue( int x )
{
  int y, nextclass, widthx;

  widthx = width[x];
  if( x+widthx > totlen-1 || (genome[x+widthx] != genome[x]) )
    return 0;
  nextclass = find(x+widthx);
  if( size[x] != size[nextclass] )
    return 0;
  for( y = mate[x]; y != x; y = mate[y] ) {
    if( y+widthx > totlen-1 || (genome[y+widthx] != genome[y]) )
      return 0;
    if( find(y+widthx) != nextclass )
      return 0;
  }
//  printf( "Gluing (%d,%d) and (%d,%d)\n",
//    genome[x], pos[x], genome[nextclass], pos[nextclass] );
  prev[next[nextclass]] = prev[nextclass];
  next[prev[nextclass]] = next[nextclass];
  width[x] += width[nextclass];   /* was: width[x]++ */
  return 1;
}

/* Glue all classes that need gluing */
void glueclasses( void )
{
  int x, first;
  int changed;

  do {
    changed = 0;
    first = find(0);
    x = first;
    do {
      changed |= glue( x );
      x = next[x];
    } while( x != first );
  } while( changed );
}

/* Cycle detection. We don't cheat and use tsort,
 * unlike some people I could name. */

/* Does depth-first search of the graph starting at x,
 * returns 1 if a cycle is found, 0 if not. */
int cycledetect( int x )
{
  int y, yroot;

  visited[x] = 1;
  onstack[x] = 1;
  y = mate[x];
  yroot = find(y);
  while( yroot != x ) {
    yroot = find(y);
    if( !visited[yroot] && cycledetect( yroot ) ) return 1;
    else if( onstack[yroot] ) return 1;
    y = mate[y];
    yroot = find(y);
  }
  onstack[x] = 0;
  return 0;
}

/* Returns 1 if the graph has cycles, 0 if not. */
int hascycles( void )
{
  int first, x;

  /* Unmark all classes */
  first = find(0);
  x = first;
  do {
    visited[x] = 0;
    onstack[x] = 0;
    x = next[x];
    assert( x == find(x) );
  } while( x != first );

  /* Look for cycles everywhere. We need this since the graph could be non-connected */
  first = find(0);
  x = first;
  do {
    if( !visited[x] && cycledetect(x) ) return 1;
    x = next[x];
  } while( x != first );
  return 0;
}


/* Compute equivalence classes of positions, such that adjacent positions that
 * are all equivalent are glued together into single blocks of equivalent
 * positions.
 *
 * The memory for the given lists is freed.
*/
EqClassSet* geteqclasses( int numgens, int* lenlist, int nummatches,
                          int* gen1, int* start1, int* gen2, int* start2,
                          int* matchlens )
{
    //Set the global variables:
  nbgenomes = numgens;
  genomelens = lenlist;
  nbmatches = nummatches;
  g1 = gen1;
  s1 = start1;
  g2 = gen2;
  s2 = start2;
  mlens = matchlens;

  start = allocate( nbgenomes );
  totlen = 0;
  int i;
  for( i=0; i<nbgenomes; i++ ) {
    start[i] = totlen;
    totlen += genomelens[i];
  }
  genome = allocate( totlen );
  pos = allocate( totlen );
  size = allocate( totlen );
  parent = allocate( totlen );
  rank = allocate( totlen );
  mate = allocate( totlen );
  prev = allocate( totlen );
  next = allocate( totlen );
  width = allocate( totlen );
  visited = allocate( totlen );
  onstack = allocate( totlen );

  initclasses();
  //printf( "Beginning\n" );
  //printall();
  //printf( "Avant joinmatches\n" ); fflush( stdout );
  joinmatches();
  //printf( "After joining\n" ); fflush( stdout );
  //printall();
  //printf( "After print\n" );
  explodeclasses();  // Destroy classes with multiple matches per genome.
  //printf( "After exploding\n" );
  //printall();

  EqClassSet *classset = NULL;
  if( hascycles() ) {
    printf( "Has cycles.\n" );
  }
  else {
    glueclasses();
    //printf( "After gluing\n" );
    //printall();

    classset = getclasses();
  }


  return classset;
}



/* Return the number of equivalence classes.
 */
int getnumberofclasses()
{
  int first, counter, x;

  counter = 0;
  x = find(0);
  first = x;
  do {
    if( size[x] > 1 )
      counter++;
    x = next[x];
  } while( x != first );
  return counter;
}


/* Return equivalence class information.
 */
EqClassSet* getclasses( void )
{
  int x, first, i;

  first = find(0);

  EqClassSet* classset = newEqClassSet( getnumberofclasses() );

  x = first;
  i = 0;
  do { 
    if( size[x] > 1 ) {
      classset->classlist[i] = geteqclass( x );
      classset->classsizes[i] = size[x];
      classset->classwidths[i] = width[x];
      i++;
    }
    x = next[x];
  } while( x != first );

  return classset;
}



/* Return the eqclass for the given set.
 * size[x] is the length of the list returned.
 */
int* geteqclass( int x )
{
  int xroot, y;

  xroot = find( x );
    // Alpha does not print equivalence classes of size 1
  if( size[xroot] == 1 ) 
    return 0;

  int *retlist = allocate( 2 * size[xroot] * sizeof(int) );

  //   printf( "Prev: (%d,%d), Next: (%d,%d)\n", genome[prev[xroot]],
  //   pos[prev[xroot]], genome[next[xroot]], pos[next[xroot]] );
  y = xroot;
  int i=0;
  do {
    retlist[i] = genome[y];
    i++;
    retlist[i] = pos[y];
    i++;
    y = mate[y];
  } while( y != xroot );  

  return retlist;
}



int main( int argc, char **argv )
{
  FILE *f;

  f = fopen( argv[1], "r" );
  if( !f ) {
    printf( "File %s cannot be opened.\n", argv[1] );
    return -1;
  }
  readmatches( f );
  fclose( f );

    //Get rid of cruft allocated by readmatches:
  //free(genome);
  //free(start);
  //free(pos);
  //free(size);
  //free(parent);
  //free(rank);
  //free(mate);
  //free(prev);
  //free(next);
  //free(width);
  //free(visited);
  //free(onstack);

  EqClassSet *classset = geteqclasses( nbgenomes, genomelens, nbmatches,
                                       g1, s1, g2, s2, mlens );


  int thisgen, thispos;
  for( int i=0; i<classset->numclasses; i++ ) {
    printf("Class Width %d\n", classset->classwidths[i]);
    for( int j=0; j<classset->classsizes[i]; j++ ) {
      thisgen = classset->classlist[i][j*2];
      thispos = classset->classlist[i][j*2+1];
      printf("(%d,%d)\n", thisgen, thispos);
    }
    printf("\n");
  }
  
  freeEqClassSet( classset );
  printtofile("eqclasses_in.txt");
  freemem();

  return 0;
}  


/*
int main( int argc, char **argv )
{
  FILE *f;

  f = fopen( argv[1], "r" );
  if( !f ) {
    printf( "File %s cannot be opened.\n", argv[1] );
    return -1;
  }
  readmatches( f );
  fclose( f );

  int i;
  printf( "On a %d genomes.\n", nbgenomes );
  printf( "Voici leurs longueurs:\n" );
  for( i=0; i<nbgenomes; i++ ) {
    printf( "%d %d\n", i, genomelens[i] );
  }

  initclasses();
  printf( "Beginning\n" );
  printf( "Avant joinmatches\n" ); fflush( stdout );
  joinmatches();
  printf( "After joining\n" ); fflush( stdout );
  printf( "After print\n" );
  explodeclasses();
  printf( "After exploding\n" );
  if( hascycles() ) {
    printf( "Has cycles. Stop.\n" );
  }
  else {
    glueclasses();
    printf( "After gluing\n" );
    printall();
    printtofile();
  }

  freemem();
  return 0;
}  
*/
