# Krister Swenson                                                Fall 2014
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
A MatchGraph graph meant for display and interaction.
"""
import sys
import itertools

from copy import deepcopy
from typing import List
from typing import Tuple
from typing import Iterable
import networkx as nx
from collections import defaultdict as defd

from networkx.classes import DiGraph

from .Match import MatchGraph
from .Match import NAMENODELEFT
from .Match import NAMENODERIGHT
from .Match import MNode
from .pyinclude.usefulfuncs import pairwise, printerr
from .shapes import DGraph
from .shapes import DEdge
from .shapes import TextShape
#from feedbackvertex import fvs_via_ic

from .xdot import XDotParser, run_graphviz, ParseError


## CONSTANTS:


#        _________________________
#_______/        Classes          \_____________________________________________


class InterGraph(MatchGraph, DGraph):
  """
  This is a MatchGraph meant to be displayed interactively.
  It adds the ability for a MatchGraph to be displayed using the attributes
  and functionality of the DGraph, and makes it easier to program interactive
  features (such as moving nodes and edges).

  :note: This graph has two graphs (G and GB), only one of which will be
         displayed.  To switch the one to be displayed use L{toggleActive()}.
         The graph GB and G have different MNodes with the same intervals.
         This is done so that each keep their independent DNode and DEdge
         positions.

  :note: To use it you create an instance and then call the L{prepare()} method
         before calling L{xdot.DotWindow.set_dotcode()} with this graph as the
         "dgraph" keyword argument.


  :ivar Gact:  pointer to the graph that will be displayed (G or GB).
  :ivar GB:    the same graph as L{self.G} (inherited from MatchGraph), except
               only with the Big enough nodes (as determined by
               self.mindisplaysize).
  :ivar titlepair:   (title, subtitle) the title for this graph
  :ivar annotations: the Annotations to display in the nodes
  :ivar fast:        do a fast rendering?
  :ivar addnames:    add names to the ends of the graph?
  :ivar grouppairs:  group pairs of nodes where all are equadistant?
  """
  #InterGraph
  def __init__(self, *args, **kwargs):
    """
    Constructor that accepts all arguments of the MatchGraph constructor.

    :kwarg anchorgraph:  True if this is an anchor graph.
    :type  anchorgraph:  bool
    :kwarg issampling:   True if this graph is a sampling of nodes.
    :type  issampling:   bool
    :kwarg removecycles: True if we should remove cycles from the graph.
    :type  removecycles: bool
    """
      #Do this so positional arguments can be used with MatchGraph.__init__().
    self._issampling = False    #:Is graph a sampling (anchor graph only)?
    if 'issampling' in kwargs:
      self._issampling = kwargs.pop('issampling')

    self._isanchorgraph = self._issampling  #:Is graph an anchor graph?
    if 'anchorgraph' in kwargs:
      self._isanchorgraph = kwargs.pop('anchorgraph')

    removecycles = False     #:Should we remove edges to make acyclic.
    if 'removecycles' in kwargs:
      removecycles = kwargs.pop('removecycles')

    self.Gact: DiGraph = None
    super().__init__(*args, **kwargs)

    if removecycles:
      self._removeCycles()

    self.Gact = self.G
    self.G.graph['ready'] = False   #Mark graph as NOT ready to be displayed.
                                    # (prepare() must be called first)
    self.G.graph['namenodesleft'] = []   #Left name DNodes (not MNodes).
    self.G.graph['namenodesright'] = []  #Right name DNodes (not MNodes).
    self.G.graph['nodeTOleftnnedges'] = defd(list)  #Edges on left name node.
    self.G.graph['nodeTOrightnnedges'] = defd(list) #Edges on right name node.
    self.GB: nx.DiGraph = None
    if not self._isanchorgraph:
      self.GB = self.getBigNodesGraph()

    self.fast = False        #:Do a fast rendering?
    self.addnames = {}       #:Add names to the ends of the graph?
    self.grouppairs = False  #:Group pairs of nodes where all are equadistant?
    self.titlepair: Tuple[str, str] = None

    self.set_boundingbox()


  def getCopiedSubgraph(self, nodes: Iterable[MNode]):
    """
    Return a InterGraph that is the induced subgraph of the given nodes.

    @param nodes: the nodes to limit the subgraph to
    @type  nodes: iterable of MNodes
    """
    subgraphcopy = self.G.subgraph(nodes).copy()
    subgraphcopy.graph = deepcopy(self.G.graph)
    return InterGraph(graph=subgraphcopy,
                      seqfile=self._seqfile, iTOid=self._iTOid,
                      idTOlen=self.idTOlen, anchorgraph=self._isanchorgraph,
                      annotations=self.annotations, normfuncs=self._norm)


  def prepare(self, fast=False, addnames=False, grouppairs=False,
              showsmall=True, refresh=False):
    """
    Prepare the graph to be displayed if Gact.graph['ready'] is False.
    This essentially calls toAgraph if Gact.graph['ready'] is False, and then
    sets up this graph's nodes and edges accordingly.

    @note: if self.blocks is set, then we hierarchically organize subgraphs

    @param fast:     do a dirtier but quicker layout
    @param addnames: if set, then add nodes showing the names of the genomes
                     at the begining and end
    @type addnames:  False, or a dictionary mapping gid to phage description
    @param grouppairs: if True, group pairs of nodes that have the same distance
                       between them for all genomes
    @type showsmall:   show small nodes in the graph (display L{self.G})
    @param refresh:    force a refresh of the view
    """
    self.fast = fast
    self.addnames = addnames
    self.grouppairs = grouppairs

    if showsmall or self._isanchorgraph:
      self.Gact = self.G
    else:
      self.Gact = self.GB            #Show only the big nodes.
                                     #If this graph is already laid out
    if not refresh and self.Gact.graph['ready']:
      return                         #then don't compute the layout.

    self.setCoordinates(self.Gact)   #Compute the graph layout.
    self.set_boundingbox()           #Set the viewable bounding box.
    self.Gact.graph['ready'] = True  #Mark the graph as ready to be displayed.


  def _removeCycles(self):
    """
    Remove the cycles from the graph by: 1. removing in-edges from any node
    that has in-edges from two nodes with the same gid, and then 2. taking
    cycles one-by-one, removing a random edge from the cycle.
    """
    #print fvs_via_ic(self.G, 10)   #Compute feedback vertex.
        #Remove in-edges from any node that has in-edges from two nodes with
        #the same gid:
    for n in self.G:
      nodes = [m for (m,_) in self.G.in_edges(n)]
      for o,p in itertools.combinations(nodes, 2):
        if o.gids & p.gids:
          self.G.remove_edges_from(list(self.G.in_edges(n)))

      nodes = [m for (_,m) in self.G.out_edges(n)]
      for o,p in itertools.combinations(nodes, 2):
        if o.gids & p.gids:
          self.G.remove_edges_from(list(self.G.out_edges(n)))

        #Remove a single edge from one cycles at a time:
    printerr('removing cycles', end='')
    cycleiter = nx.algorithms.cycles.simple_cycles(self.G)
    while not nx.is_directed_acyclic_graph(self.G):
      printerr('.', end='')
      cycle = next(cycleiter)
      self.G.remove_edges_from(list(self.G.in_edges(cycle[0])))
    ##print sum(1 for _ in nx.cycle_basis(self.G))
    ##print sum(1 for _ in nx.simple_cycles(self.G))
    printerr('\n')



  def setCoordinates(self, graph: DiGraph):
    """
    Use dot (graphviz) to set the coordinates of the given graph.
    """
      #Get dot format graph:
    ag = self.toAgraph(graph, self.fast, self.addnames, self.grouppairs)
    xdotcode = run_graphviz(ag.string()) #Compute the layout.
    assert xdotcode is not None

      #Get the coordinates of the all of the Shapes:
    try:
      parser = XDotParser(xdotcode)   #Innitialize the parser.
      width, height, nodes, edges, shapes = parser.getGraphShapes()
    except ParseError as ex:
      sys.exit("ERROR parsing xdotcode: {}".format(ex))

      #Store the information and assign the DEdges to the edges of G:
    for n in nodes:
      if n.url == NAMENODELEFT:
        graph.graph['namenodesleft'].append(n)
      elif n.url == NAMENODERIGHT:
        graph.graph['namenodesright'].append(n)
      else:
        mynode = graph.graph['codeTOnode'][int(n.url)]
        n.copyInto(mynode)
    self.nodes = graph.nodes()

    for e in edges:
      if e.src.url == NAMENODELEFT:
        graph.graph['nodeTOleftnnedges'][e.src].append(e)
        mnode = graph.graph['codeTOnode'][int(e.dst.url)]
        graph.graph['nodeTOleftnnedges'][mnode].append(e)

      elif e.dst.url == NAMENODERIGHT:
        graph.graph['nodeTOrightnnedges'][e.dst].append(e)
        mnode = graph.graph['codeTOnode'][int(e.src.url)]
        graph.graph['nodeTOrightnnedges'][mnode].append(e)

      else:
        a = graph.graph['codeTOnode'][int(e.src.url)]
        b = graph.graph['codeTOnode'][int(e.dst.url)]
        try:
          graph[a][b]['DEdges'].append(e)
        except:
          raise Exception(f'edge missing in graph (please report bug)!\n{e}')

      #Store other Shape information (the boxes grouping nodes in anchor view):
    graph.graph['Shapes'] = shapes
      #Store width and height of the graph:
    graph.graph['width'] = width
    graph.graph['height'] = height

  def setTitlePair(self, titlepair: Tuple[str, str]):
    self.titlepair = titlepair

  def toggleActive(self):
    """
    Make the other graph the active (displayed) graph.
    """
    showsmall = True
    if self.showingAll():
      showsmall = False

    self.prepare(self.fast, self.addnames, self.grouppairs, showsmall)


  def showingAll(self):
    """
    Return true if all nodes are being shown (not just the big ones).
    """
    return self.Gact is self.G

  def sizeDifference(self):
    """
    Return the difference, in number of nodes, between self.G and self.GB.
    """
    return self.G.number_of_nodes() - self.GB.number_of_nodes()

  def categorizeBlocks(self, *args, **kwargs):
    """
    Do the same as L{MatchGraph.categorizeBlocks()}, and then reinnitialize
    L{self.GB} so that it corresponds to L{self.G} (missing the small nodes).
    """
    super().categorizeBlocks(*args, **kwargs)
    self.GB = None
    if not self._isanchorgraph:
      self.GB = self.getBigNodesGraph()
    

  def getBigNodesGraph(self) -> nx.DiGraph:
    """
    Return a graph to assign to L{self.GB} based on L{self.G}.
    (the graph with only big enough nodes)
    """
    newnodes = []
    numnew = sum(1 for n in self.G       #Check the number of new nodes.
                 if n.isDummy() or n.length > self.mindisplaysize)

    for n in self.G:
      if numnew < 2 or n.isDummy() or n.length > self.mindisplaysize:
        new = deepcopy(n)
        if n in self.nodeTOsubgraph:    #Ensure both nodes share same subraph.
          self.nodeTOsubgraph[new] = self.nodeTOsubgraph[n]
        newnodes.append(new)

    retgraph = MatchGraph(nodes=newnodes, addloners=False,
                          normfuncs=self._norm).G

    retgraph.graph['ready'] = False
    retgraph.graph['namenodesleft'] = []
    retgraph.graph['namenodesright'] = []
    retgraph.graph['nodeTOleftnnedges'] = defd(list)
    retgraph.graph['nodeTOrightnnedges'] = defd(list)
    return retgraph


  def getSampling(self, remlen=10, addends=False):
    """
    Return a periodic sampling of the nodes of the graph.
    Do this by grouping a set of adjacent anchors if the distance between
    each pair is the same for all genomes.

    @note: this should be used with a graph composed only of anchors.
    @warn: this only works with a DAG.

    @param remlen:  nodes shorter than this will be discarded if they're not
                    boardering a block
    @param addends: add dummy nodes to the ends
    """
    assert(self._isanchorgraph)

    if not self._issampling:
      nodelist = self.getLinearExtension() #Find a linear ordering.
      nodes = [nodelist[0]]                #Always include first dummy node.

      if(set((gid, nodelist[0][gid][0]) for gid in nodelist[0].gids) !=
         set((gid, nodelist[1][gid][0]) for gid in nodelist[1].gids)):
        nodes.append(nodelist[1])          #Add first non-dummy node.

      inblock = False
      for a,b in pairwise(nodelist[1:-1]): #Visit adjacent (in ordering) pairs
        if not self._eqDists(a,b):
          if inblock:      #Transition out of a block:
            nodes.append(a)
            nodes.append(b)
          else:             #We're out of a block so check sizes:
            if a.length > remlen:
              nodes.append(a)
            if b.length > remlen:
              nodes.append(b)
          inblock = False
        else:               #We're in a block:
          inblock = True

      #if len(nodelist) > 3 and len(nodes) < 2: #If there was just one block:
      #  nodes.append(nodelist[1])
      if nodes[-1] != nodelist[-2]:       #Always include second to last node.
        nodes.append(nodelist[-2])

      nodes.append(nodelist[-1])

      return InterGraph(nodes=nodes, addloners=False, compactnodes=False,
                        seqfile=self._seqfile, idTOi=self._idTOi,
                        addends=addends, idTOstr=self.idTOstr, gidset=self.gids,
                        idTOlen=self.idTOlen, issampling=True,
                        annotations=self.annotations, normfuncs=self._norm)
    return self

  def isAnchorGraph(self):
    """
    Return True if this is an anchor graph.
    """
    return self._isanchorgraph

  def _codeTOnode(self):
    """
    Return the codeTOnode dictionary for the active graph.
    """
    return self.Gact.graph['codeTOnode']

  #def nodeTOsubgraph(self):
  #  """
  #  Return the nodeTOsubgraph dictionary for the active graph.
  #  """
  #  return self.Gact.graph['nodeTOsubgraph']

  def makePickleable(self):
    """
    Get rid of elements from the shapes that are not pickleable.
    Specifically, clear all cached layouts in the underlying DGraph.
    """
    if self.G:
      for node in self.getDNodes(self.G):
        node.makePickleable()
    if self.GB:
      for node in self.getDNodes(self.GB):
        node.makePickleable()

        #In case there is a TextShape not associated with a DNode.
    for shape in self.getShapes():
      if isinstance(shape, TextShape):
        shape.makePickleable()


  def getDNodes(self, graph: DiGraph=None) -> List[MNode]:
    """
    Return the list of MNodes for the graph to display (Gact).

    :param graph: If given, return the nodes from this graph.
    """
    if not graph:
      graph = self.Gact

    if graph and graph.graph['ready']:
      return graph.graph['namenodesleft']+\
             graph.graph['namenodesright']+\
             list(graph.nodes())

    return DGraph.getDNodes(self)


  def getDEdges(self) -> List[DEdge]:
    """
    Return the list of DEdges from Gact, along with the edges to and from
    the name nodes.
    """
    if self.Gact and self.Gact.graph['ready']:
      retval = []
      for _, _, des in self.Gact.edges(data=True):
        retval.extend(des['DEdges'])

      leftedges = []
      for elist in self.Gact.graph['nodeTOleftnnedges'].values():
        for e in elist:
          leftedges.append(e)
      rightedges = []
      for elist in self.Gact.graph['nodeTOrightnnedges'].values():
        for e in elist:
          rightedges.append(e)

      return retval+list(set(leftedges))+list(set(rightedges))

    return DGraph.getDEdges(self)


  def getShapes(self):
    """
    Return the list of Shapes for this graph (the boxes drawn to group nodes).
    """
    if self.Gact and self.Gact.graph['ready']:
      return self.Gact.graph['Shapes']

    return DGraph.getShapes(self)

  def getWidth(self):
    """
    Return the width of this graph.
    """
    if self.Gact and self.Gact.graph['ready']:
      return self.Gact.graph['width']

    return DGraph.getWidth(self)

  def getHeight(self):
    """
    Return the height of this graph.
    """
    if self.Gact and self.Gact.graph['ready']:
      return self.Gact.graph['height']

    return DGraph.getHeight(self)

  def __contains__(self, node):
    if isinstance(node, MNode):
      return node in self.Gact
    else:
      return NotImplementedError


  def get_sortEdg_by_node(self, node):
    """
    Return the DEdges exiting the given DNode.
    """
    retlist = []
    if isinstance(node, MNode):
      for u,v in self.Gact.out_edges(node):
        for e in self.Gact[u][v]['DEdges']:
          retlist.append(e)

      if node in self.Gact.graph['nodeTOrightnnedges']:
        retlist.extend(self.Gact.graph['nodeTOrightnnedges'][node])

    elif node in self.Gact.graph['nodeTOleftnnedges']:
      retlist.extend(self.Gact.graph['nodeTOleftnnedges'][node])

    return retlist

  
  def get_entEdg_by_node(self, node):
    """
    Return the DEdges entering the given DNode.
    """
    retlist = []
    if isinstance(node, MNode):
      for u,v in self.Gact.in_edges(node):
        for e in self.Gact[u][v]['DEdges']:
          retlist.append(e)

      if node in self.Gact.graph['nodeTOleftnnedges']:
        retlist.extend(self.Gact.graph['nodeTOleftnnedges'][node])

    elif node in self.Gact.graph['nodeTOrightnnedges']:
      retlist.extend(self.Gact.graph['nodeTOrightnnedges'][node])

    return retlist

  def replaceNodeMerge(self, triples_new, old1, old2, vertical):
    """
    Replace the old nodes with a new one that has the given triples.

    @param triples_new: the triple for a new MNode
    @param old1: MNode to replace
    @param old2: MNode to replace
    @param vertical: if False, then in-edges from old1 and out-edges from node2
                     will be inherited.  if True, then all in and out-edges
                     from both import edges are inherited.

    @return: return the new MNode created  
    """
    length_new = triples_new[0][2] - triples_new[0][1]
    new = self._addNodeFromTriples(triples_new)

    if all(length_new == t[2] - t[1] for t in triples_new): 
      a = self._getContractedBlockSimilarity(new, new, self._seqfile)
      if a != length_new:
        new.similarity = a
    else:
      new.similarity = 0
    
    nodesforin = [old1]
    nodesforout = [old2]
    if vertical:
      nodesforin.append(old2)
      nodesforout.append(old1)

      #Add the edges, ensuring the gnms is labeled with gids for the edge.
    for (innode,_, data) in self.Gact.in_edges(nodesforin, data=True):
      if new in self.Gact[innode]:
        self.Gact[innode][new]['gnms'] |= data['gnms']
        self.Gact[innode][new]['label'] |= data['label']
      else:
        self.Gact.add_edge(innode, new)
        self.Gact[innode][new].update(data)

    for (_,outnode, data) in self.Gact.out_edges(nodesforout, data=True):
      if outnode in self.Gact[new]:
        self.Gact[new][outnode]['gnms'] |= data['gnms']
        self.Gact[new][outnode]['label'] |= data['label']
      else:
        self.Gact.add_edge(new, outnode)
        self.Gact[new][outnode].update(data)

    self.Gact.remove_node(old2)
    self.Gact.remove_node(old1)
    self.markNotReady()          #Mark graph as NOT ready to be displayed.

    return new


  def replaceNodeSplit(self, lefttriple: List[Tuple[str, int, int]],
                       righttriple: List[Tuple[str, int, int]], old: MNode):
    """
    Replace the old node with a new node for each set of triples.

    @param lefttriple:  triples (gid, start,end) for the new left node
    @param righttriple: triples (gid, start,end) for the new right node 
    @param old:         the MNode that we are replacing

    @return: the newly created MNodes  
    """
    lnode = self._addNodeFromTriples(lefttriple)
    rnode = self._addNodeFromTriples(righttriple)

    if old.isContracted():       #Recalculate the similarity:
      rnode.similarity =\
        self._getContractedBlockSimilarity(rnode, rnode, self._seqfile)

      lnode.similarity =\
        self._getContractedBlockSimilarity(lnode, lnode, self._seqfile)
    
    for (innode,_, data) in self.Gact.in_edges([old], data=True):
      self.Gact.add_edge(innode, lnode)
      self.Gact[innode][lnode].update(data)

    self.Gact.add_edge(lnode, rnode, DEdges=[], label=' '.join(lnode.gids),
                       gnms=set(zip(lnode.gids,[0]*len(lnode.gids))))

    for (_,outnode, data) in self.Gact.out_edges([old], data=True):
      self.Gact.add_edge(rnode, outnode)
      self.Gact[rnode][outnode].update(data)

    self.Gact.remove_node(old)
    self.markNotReady()          #Mark graph as NOT ready to be displayed.

    return [lnode, rnode]


  def markNotReady(self):
    """
    Mark graph as not ready to be displayed (disassociate edges with
    DEdges).
    """
    self.Gact.graph['ready'] = False
    for u,v in self.Gact.edges():
      self.Gact[u][v]['DEdges'] = []

    self.Gact.graph['namenodesleft'] = []   #Left name DNodes (not MNodes).
    self.Gact.graph['namenodesright'] = []  #Right name DNodes (not MNodes).
    self.Gact.graph['nodeTOleftnnedges'] = defd(list)  #Edges left name node.
    self.Gact.graph['nodeTOrightnnedges'] = defd(list) #Edges right name node.



  def _addNodeFromTriples(self, triples):
    """
    Add the given node to the graph.
    If the graph is normalized, then take normalized (graph) indices, rather
    than genome indices.

    @param triples: triples [gid, start, end] for the new node in graph
                    indices
    """
    if self._norm.normalizing():  #If we are dealing with normalized indices
      newtriples = []
      for gid, s,e in triples:     #then unnormalize them.
        newtriples.append((gid, self._norm.Ii(gid, s), self._norm.Ii(gid, e)))
      triples = newtriples

    node = MNode(triples=triples, similarity=0, code=self._codecounter,
                 annotations=self.annotations, norm=self._norm)
    self._codecounter += 1

    if self._norm.normalizing():
      node.normalize()             #Re-normalize the indices.

    self._addNode(node)            #Add the node to the graph.

    return node


  def _addNode(self, node):
    """
    Add the given node to the graph.

    @param node: the MNode to add
    """
    self.Gact.add_node(node)
    self.Gact.graph['codeTOnode'][node.code] = node
    self.Gact.nodes[node]['URL'] = node.code
