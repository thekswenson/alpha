"""
MakeSet(x) initializes disjoint set for object x
Find(x) returns representative object of the set containing x
Union(x,y) makes two sets containing x and y respectively into one set
"""

def MakeSet(x):
    x.parent = x
    x.rank   = 0
    x.next   = x   # circular list of all members of an eq class

def Union(x, y, roots):
    xRoot = Find(x)
    yRoot = Find(y)

    temp = xRoot.next
    xRoot.next = yRoot.next
    yRoot.next = temp

    if xRoot.rank > yRoot.rank:
        yRoot.parent = xRoot
        roots.discard( yRoot )
    elif xRoot.rank < yRoot.rank:
        xRoot.parent = yRoot
        roots.discard( xRoot )
    elif xRoot != yRoot:
        yRoot.parent = xRoot
        roots.discard( yRoot )
        xRoot.rank = xRoot.rank + 1

def Find(x):
    if x.parent != x:
        x.parent = Find(x.parent)
    return x.parent




class UFNode(object):
    """Object used by Union-find algorithm to group positions in eq classes
    @ivar gid: genome id
    @ivar pos: position in genome
    @ivar parent: parent in union-find tree
    @ivar rank: path length to root union-find tree
    @ivar next: pointer to next member in circular list of all eq class members """
    def __init__(self, gid, pos):
        self.gid = gid
        self.pos = pos
        self.parent = self
        self.rank = 0
        self.next = self
        self._pair = (gid,pos)
    def __lt__(self, other):
        return (self.gid < other.gid) or (self.gid==other.gid and self.pos<other.pos)
    def __eq__(self, other):
        return not self<other and not other<self
    def __ne__(self, other):
        return self<other or other<self
    def __gt__(self, other):
        return other<self
    def __ge__(self, other):
        return not self<other
    def __le__(self, other):
        return not other<self
    def __hash__(self):
        return hash(self._pair)

    def __str__(self):
        return 'gid {}, pos {}'.format(self.gid,self.pos)


def mapIndicesTransitivelyUF(idTOmatchlists):
  """
  Each match implies an interval of equivalent indices.
  Build a structure that maps indices of one genome to indices of others,
  defined by the matches.  The equivalences are transfered transitively.

  @note: uses Union-find

  @note: the end indices (which actually denote the position AFTER the last
         match) are considered equivalent

  @param idTOmatchlists: map gid to a list of matches

  @return: retval[id1][i] gives a set of equivalent locations to the site at
           index i of gid1
  """
  
  roots = set()
  beenseen = {}
  for gid, mlist in idTOmatchlists.items():
    for m in mlist:
      for i1,i2 in zip(range(m.sd1.s,m.sd1.e), range(m.sd2.s,m.sd2.e)):
        site1 = UFNode(m.sd1.gid, i1)
        site2 = UFNode(m.sd2.gid, i2)
        if site1 in beenseen:
            site1 = beenseen[site1]
        else:
            beenseen[site1] = site1
            roots.add(site1)
        if site2 in beenseen:
            site2 = beenseen[site2]
        else:
            beenseen[site2] = site2
            roots.add(site2)
        Union(site1, site2, roots)

  def foo(roots):
    idTOiTOlocset = defaultdict(dict)
    for root in roots:
      locset = set()
      locset.add((root.gid, root.pos))
      current=root.next
      while current != root:
        locset.add((current.gid, current.pos))
        current = current.next
      for gid,pos in locset:
        idTOiTOlocset[gid][pos] = locset
    return idTOiTOlocset

  return foo(roots)




