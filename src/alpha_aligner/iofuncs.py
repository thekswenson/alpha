# Copyright 2017 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
These are functions related to input and output of fasta files and alpha
files.
"""
import os
from functools import reduce
THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))

import sys
import pickle
import random
import string
from Bio import SeqIO
from math import log, ceil
from itertools import product
from typing import Dict
from typing import List
from typing import Tuple

from .Annotations import Annotations
from .pyinclude.usefulfuncs import printnow



#        _________________________
#_______/        Classes          \_____________________________________________


class InputInfo(object):
  """
  Hold information about the input so that it can easily be shared between
  functions.  The usual way to use this is to make an empty object and then
  call self.setGenomeMapping().

  :note: if selectgenomes is set, then this object represents a subset of the
         genomes in the fasta file.

  @ivar seqfile:  the fasta file with the sequences
  @ivar gidset:   the set of genome ids (gid)
  @ivar idTOi:    map gid to ordering of the seqfile
  @ivar iTOid:    map ordering of the seqfile to gid
  @ivar idTOlen:  map gid to genome length
  @ivar idTOstr:  map gid to the string header in the original fasta file
  @ivar idTOint:  map gid to the intervals we will include in the analyses

  @ivar selectgenomes: use gidset to select genomes from seqfile
  @ivar ignoredups:    remove overlaps from matches
                       (ignore locations that match multiple locations in other
                        genome)
  @ivar verbose:       print messages to terminal
  """
  #InputInfo
  def __init__(self, seqfile=None, gidset=None, idTOi: Dict=None,
               iTOid: Dict=None, idTOlen: Dict=None, idTOstr: Dict=None,
               idTOint: Dict=None, selectgenomes=False, ignoredups=False,
               verbose=False, printnormalized=False):
    self.seqfile = seqfile
    self.alphafile = ''
    self.gidset = gidset
    self.idTOi = idTOi if idTOi else {}
    self.iTOid = iTOid if iTOid else {}
    self.idTOlen = idTOlen if idTOlen else {}
    self.idTOstr = idTOstr if idTOstr else {}
    self.idTOint = idTOint if idTOint else {}

    self.selectgenomes = selectgenomes
    self.ignoredups = ignoredups
    self.verbose = verbose
    self.printnormalized = printnormalized
    self.records = None
    self.annotations: Annotations = None


  def setAlphaFile(self, filename: str):
    self.alphafile = filename

  def setGenomeMapping(self, fastafile, listgenomes=False, printlengths=False,
                       includegids=False):
    """
    Compute the dictionaries associated to the genomes.
  
    @warn: exit from the program if the file does not exist.
  
    @param listgenomes:  print info about the genomes.
    @param printlengths: print the genome lengths.
    @param includegids:  only include these gids as keys in idTOstr
    """
    try:
      self.records = list(SeqIO.parse(fastafile, 'fasta'))
    except IOError as e:
      sys.exit('Problem opening the genome fasta file.\n{}'.format(e))
  
    if len(self.records) < 2:
      sys.exit('The fasta file "{}" has {} entry(ies)!'.
               format(fastafile, len(self.records)))
    alphabet = string.ascii_uppercase+string.ascii_lowercase
    reps = int(ceil(log(len(self.records))/log(len(alphabet))))
    intTOid = list((reduce(lambda a,b: a+b, e)
                    for e in product(alphabet, repeat=reps)))
  
    self.seqfile = fastafile
    self.idTOi = {}
    self.iTOid = {}
    self.idTOlen = {}
    self.idTOstr = {}

    totallength = 0
    for i,r in enumerate(self.records):
      self.idTOi[intTOid[i]] = i
      self.iTOid[i] = intTOid[i]
      namestr = r.description
      if not includegids or intTOid[i] in includegids:
        self.idTOstr[intTOid[i]] = namestr
        self.idTOlen[intTOid[i]] = len(r)

      if listgenomes or printlengths:
        if not includegids or intTOid[i] in includegids:
          printnow('{}: {}  {}'.format(i+1, intTOid[i], r.description))
          if printlengths:
            printnow('   length = {}'.format(len(r)))
            totallength += len(r)
  
    if printlengths:
      printnow('Total length is {}.'.format(totallength))

    self.gidset = set(includegids) if includegids else set(self.idTOi.keys())


  def setIdTOint(self, idTOinterval):
    """
    Set idTOint. Adjust self.gidset and self.selectgenomes accordingly.
    """
    assert(self.idTOi)

    newset = set(idTOinterval.keys())
    self.idTOint = idTOinterval
    if newset < self.gidset:
      self.gidset = newset
      self.selectgenomes = True

  def fillMissingIdTOint(self, idTOinterval, gidset=None):
    """
    Set idTOint by filling in the intervals for missing genomes with (0,1),
    which signifies full intervals.

    :warn: idTOi must be set if gidset is not given.
    """
    assert(self.idTOi)

    self.idTOint = idTOinterval

    if not gidset:
      gidset = list(self.idTOi.keys())

    for gid in gidset:                 #Fill in undefined intervals.
      if gid not in self.idTOint:                
        self.idTOint[gid] = (0,1)                 

  def getIncludedLines(self, gidset=None):
    """
    Return a set of indices for self.gidset.

    :param gidset: if given, then use this set rather than self.gidset
    """
    if not gidset:
      gidset = self.gidset

    lines = set()
    for gid in gidset:
      lines.add(self.idTOi[gid])

    return lines


  def getGIDSubset(self, gids) -> 'InputInfo':
    """
    Return an InputInfo object with a subset of gids.

    @note: idTOi is adapted to a file with only len(gids) number of lines.
    """
    newii = InputInfo(self.seqfile, gids, ignoredups=self.ignoredups,
                      verbose=self.verbose)

    newii.selectgenomes = True

    newi = 0
    for _, gid in self.iTOid.items():
      if gid in gids:
        newii.idTOi[gid] = newi
        newii.iTOid[newi] = gid
        newi += 1

        newii.idTOlen[gid] = self.idTOlen[gid]
        newii.idTOstr[gid] = self.idTOstr[gid]
        if self.idTOint and gid in self.idTOint:
          newii.idTOint[gid] = self.idTOint[gid]

    return newii

  def getGIDsSortedByI(self) -> List[str]:
    """
    Return the list of the GIDs sorted by appearance in the fastafile.
    """
    return sorted(self.idTOi, key=lambda gid: self.idTOi[gid])

  def getLinesToIntervals(self) -> List[Tuple[int, int]]:
    """
    Return a list of intervals ordered by fasta file order.
    """
    intervals = []
    for gid in self.getGIDsSortedByI():
      if gid in self.idTOint:
        interval = self.idTOint[gid]
        diff = interval[1]-interval[0]
        if diff < 3 and diff >=0:         #Add characters to empty intervals.
          intervals.append(None)
        else:
          intervals.append(interval)
      else:
        intervals.append((0,3))

    return intervals


#        _________________________
#_______/        Functions        \_____________________________________________






def loadGraphFromFile(filename: str, matchdir: str):
  """
  Load a saved graph.

  @return: (graph, minlen, anchormode, O, showall) where 
           minlen is the minimum length used to create the graph,
           anchormode tells whether these are only anchors or not, and showall
           says whether small nodes are ommited or not.
  """
  try:
    with open(filename, 'rb') as f:
      ig, fastasequences, minlen, anchormode, showall = pickle.load(f)
  except Exception as e:
    sys.exit("ERROR: bad (or old) gpickle file format.\n({})".format(str(e)))

  seqfile = createTempSeqFile(fastasequences, matchdir)
  O = InputInfo()
  O.setGenomeMapping(seqfile)
  ig.setFastaFile(seqfile)         #Use the new temp sequence file.
  O.setAlphaFile(filename)
  ig.setAlphaFile(filename)

  return ig, O, minlen, anchormode, showall


def createTempSeqFile(fastasequences, matchdir: str):
  """
  Create a temporary fasta file from the given list of fasta lines.
  """
  filename = '{}/TMP_{}.fasta'.format(matchdir, random.random())
  with open(filename, 'w') as f:
    f.writelines(fastasequences)

  return filename


def intervalsFromStr(specstr):
  """
  Return a dictionary mapping gid to interval from a string of the format
  'A:2000-3000,B:4000-5000,C:3300-4300'.
  """
  idTOinterval = {}
  try:
    for token in specstr.split(','):
      gid, interval = token.split(':')
      idTOinterval[gid] = list(map(int, interval.split('-')))
      if len(idTOinterval[gid]) != 2:
        raise Exception
  except:
    sys.exit('Unexpected format for intervals:\n"{}"'.format(specstr))

  return idTOinterval


def intervalsFromFile(specfile):
  """
  Return a dictionary mapping gid to interval from a file with each line
  having a string of the format 'A:2000-3000'.
  """
  with open(specfile) as f:
    return intervalsFromStr(','.join(f.readlines()))


