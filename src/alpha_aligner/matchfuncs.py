# Krister Swenson                                                Spring 2015
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Functions for dealing with matches.
"""
import sys
import os
from functools import reduce
THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))

import subprocess
import shutil
from collections import defaultdict
from itertools import chain
from itertools import combinations
from threading import Event
from random import random
from typing import Dict
from typing import Tuple
from typing import List
from typing import Set
from typing import FrozenSet
from typing import Callable
import networkx as nx
import igraph
#import gc

from Bio import SeqIO
from Bio import AlignIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.Align import MultipleSeqAlignment

from .InterGraph import InterGraph
from .Match import Match
from .Match import MNode
from .Match import MatchGraph
from .Match import NormIndFuncs
from .Match import GraphOnIntervalError
from .iofuncs import InputInfo
from .userconfig import ALPHA_CONFIG_DIR
from .Annotations import Annotations
#from unionfind import mapIndicesTransitivelyUF

from .pyinclude.usefulfuncs import pairwise
from .pyinclude.usefulfuncs import printnow
from .pyinclude.usefulfuncs import printerr
from .pyinclude.usefulfuncs import invdict
from .pyinclude.usefulfuncs import binomial_fast
from .pyinclude.genseqfuncs import sliceFastaFile
from .pyinclude.genseqfuncs import getGenomeLengths

from .feedbackvertex import fvs_via_ic


try:                      #Imports for cython interface:
  from .ccode.eqclasses import getEqClasses
except Exception as e:    #The soft link to the .so is probably missing.
  print('{}\n---- Did you run the Alpha installer?! ----\n'.format(e))


### Constants:
VMATCH_SCRATCH = '_vmatch_scratch/'  #Directory to store the vmatch files in.
GT_SCRATCH_PREFIX = f'_gt_scratch_'  #The child dir prefix (one per run of GT)
ADAPT_LEN = 100     #Distance between two anchors must be greater that this to
                    #adapt a lower l on a subproblem.
ALPHABET = THIS_FILE_DIR+'/matchAlphabet.txt' #The alphabet to use.
MIN_LENGTH = 15     #The default minimum match length.
MIN_MODULESIZE = 20 #Ignore modules smaller than this (for all genomes).
#ALPHABET = 'dna'   #The alphabet to use.


#{ Running gt:

def runGT(seqfile, scratchdir, matchfile=None, minlength=False,
          alphabet=ALPHABET, setminlen=False):
  """
  Run gt (genometools) on the given fasta file.

  @param seqfile:   the input fasta files containing the sequences
  @param scratchdir:the directory to do temporary computations in
  @param matchfile: the gt filename.  if None then we create it.
  @param minlength: the minimum length of matches for gt to find
                    (use MIN_LENGTH if not set).
  @param alphabet:  the type of alphabet.  "dna" and "protein" work. a custom
                    alphabet can be given with "FILENAME" where FILENAME
                    has lines with characters from the alphabet.  the last line
                    has wildcard characters that don't match anything.  see
                    the vmatch manual appendix B for full formatting.
  @param setminlen: set the minlength using the maximum length self-match.

  @return: (matchoutput, minlen) where 'matchoutput' is the name of the
           written gt file and 'minlen' is the minimum length setting
           used with gt
  """
  if not minlength:
    minlength = MIN_LENGTH
  if setminlen:
    minlength = getMaxSelfMatch(seqfile, scratchdir)

  seqfile_name = os.path.basename(seqfile)  

  cwd = os.getcwd()
  os.chdir(scratchdir)

  gtseqfile = seqfile
  if not os.path.isabs(seqfile):
    gtseqfile = f'{cwd}/{seqfile}'

  try:
    if alphabet == 'dna' or alphabet == 'protein':
      subprocess.check_call([THIS_FILE_DIR+"/gtdir/bin/gt", "suffixerator",
                             "-suf", "-"+alphabet, "-lcp", "-tis", "-db",
                             gtseqfile])
    else:
      subprocess.check_call([THIS_FILE_DIR+"/gtdir/bin/gt", "suffixerator",
                             "-suf", "-smap", alphabet, "-lcp", "-tis", "-db",
                             gtseqfile])
    os.chdir(cwd)

    if not matchfile:
      matchfile = '{}/{}_match_{}.txt'.format(scratchdir,
                                              os.path.splitext(seqfile_name)[0],
                                              minlength)
    with open(matchfile,'w') as f:
      subprocess.check_call([THIS_FILE_DIR+"/gtdir/bin/gt", "repfind", "-l",
                             str(minlength), "-scan", "-ii",
                             scratchdir+seqfile_name], stdout=f)
  except OSError as e:
    gtdir = THIS_FILE_DIR+'/gtdir/bin'
    if os.path.exists(gtdir):
      files = '\n  '.join(os.listdir(gtdir))
      message = f'ERROR:\n{e}\n'+\
                f'       gt (genometools) may be malinstalled in the'+\
                f' "{THIS_FILE_DIR}" directory?\n'+\
                f'       The executable "bin/gt" should be in'+\
                f' that directory.\n'+\
                f'       We see the following files:\n  '+files
    else:
      message = f'ERROR: it appears as though you have not installed gt in'+\
                f'\n       the "{THIS_FILE_DIR}" directory.\n'+\
                f'       You should put the gt directory (or a symlink to it)'+\
                f'\n       at "{THIS_FILE_DIR}/gtdir".'

    raise GTError(message)

  except subprocess.CalledProcessError:
    sys.exit('ERROR: gt (genometools) died. See error above.')

  return matchfile, minlength

def getGTScratch(basedir: str) -> str:
  """
  Return a random directory name to be used for computing matches with GT.
  """
  if not os.path.isdir(ALPHA_CONFIG_DIR):
    os.mkdir(ALPHA_CONFIG_DIR)

  if not os.path.isdir(basedir):
    os.mkdir(basedir)

  return f'{basedir}/{GT_SCRATCH_PREFIX}{random()}/'

def readMatchFile(matchfile, iTOid, usegenomes=set(), verbose=False,
                  vmatch=False):
  """
  Read in a match file.

  @note: remove matches between substrings of the same genome (repeats).
         (^^ not sure this note is still relevant actually)

  @param matchfile: filename of a match output file
  @param iTOid:     maps integer id in match file gid to 
  @param usegenomes:list of genome numbers to use (others ignored, zero indexed)
  @param verbose:   print information about the matches
  @param vmatch:    if True, then match file format is Vmatch rather than GT

  @return: list of all Match objects for each genome, set of all gids from
           which matches are taken, and length of minimum non repeat match
  @rtype:  maps gid to a list of Matches, set of strings, integer
  """
         #Build lists of matches for each genome:
  matchlists = defaultdict(list)
  longselfmatch = 0
  empty = True
  gidset = set()
  totallength = 0
  minlen = sys.maxsize
  with open(matchfile, 'r') as f:
    for line in f:
      if line[0] != '#':
        empty = False
        if vmatch:  #Vmatch
          (alen,aid,astart,_,blen,bid,bstart,_,_,_,_) = line.split()
        else:        #GT (genometools)
          (alen,aid,astart,_,blen,bid,bstart) = line.split()

        alen, astart = int(alen), int(astart)
        blen, bstart = int(blen), int(bstart)
        totallength += blen
        aid, bid = iTOid[int(aid)], iTOid[int(bid)]
        if aid == bid:
          longselfmatch = max(alen, longselfmatch)

        elif not usegenomes or (aid in usegenomes and bid in usegenomes):
          gidset.add(aid)
          gidset.add(bid)
          m = Match(aid, bid,
                    match=(astart, astart+alen, bstart, bstart+blen))
          minlen = min(alen, minlen)
          matchlists[aid].append(m)
          matchlists[bid].append(m)

  if verbose:
    print('Total match length is {}.'.format(totallength))

  if empty:
    return None, None, None

  return matchlists, gidset, minlen


def getMaxSelfMatch(infile, scratchdir: str, vmatch=False):
  """
  Return the maximum length self match over all sequences in the given file.

  @param infile:     the fasta file with input sequences
  @param scratchdir: temporary dir to use more matches
  @param vmatch:     if True, then use Vmatch instead of GT (genometools)

  @return: maximum length self match
  @rtype:  integer
  """
  if vmatch:
    scratchdir = VMATCH_SCRATCH
  else:
    scratchdir = getGTScratch(scratchdir)

  os.mkdir(scratchdir)

  maxlens = []
  singleseqfile = scratchdir+'tmp.fa'
  matchfile = scratchdir+'tmpmatches.txt'

      #Run gt/vmatch for a low value of l and then find the longest match:
  for r in SeqIO.parse(infile, 'fasta'):
    SeqIO.write(r, singleseqfile, 'fasta')
    if vmatch:
      runVmatch(singleseqfile, matchfile, MIN_LENGTH)
    else:
      runGT(singleseqfile, scratchdir, matchfile, MIN_LENGTH)
    with open(matchfile, 'r') as f:
      maxlen = MIN_LENGTH
      for line in f:
        if line[0] != '#':
          (alen,_,_,_,_,_,_,_,_,_,_) = line.split()
          maxlen = max(int(alen)+1, maxlen)
      maxlens.append(maxlen)

  shutil.rmtree(scratchdir)

  return max(maxlens)


#} ________________________________________________________________________


#{ Running vmatch:

def runVmatch(seqfile, matchfile=None, minlength=False, alphabet=ALPHABET,
              setminlen=False):
  """
  Run vmatch on the given fasta file.

  @param seqfile:   the input fasta files containing the sequences
  @param matchfile: the vmatchfilename.  if None then we create it.
  @param minlength: the minimum length of matches for vmatch to find
                    (use MIN_LENGTH if not set).
  @param alphabet:  the type of alphabet.  "dna" and "protein" work. a custom
                    alphabet can be given with "FILENAME" where FILENAME
                    has lines with characters from the alphabet.  the last line
                    has wildcard characters that don't match anything.  see
                    the vmatch manual appendix B for full formatting.
  @param setminlen: set the minlength using the maximum length self-match.

  @return: (vmatchoutput, minlen) where 'vmatchoutput' is the name of the
           written vmatch file and 'minlen' is the minimum length setting
           used with vmatch
  """
  if not minlength:
    minlength = MIN_LENGTH
  if setminlen:
    minlength = getMaxSelfMatch(seqfile, SCRATCHDIR, True)

  seqfile_name = os.path.basename(seqfile)  

  cwd = os.getcwd()
  if not os.path.isdir(VMATCH_SCRATCH):
    os.mkdir(VMATCH_SCRATCH)
  os.chdir(VMATCH_SCRATCH)
  try:
    if alphabet == 'dna' or alphabet == 'protein':
      subprocess.check_call([THIS_FILE_DIR+"/vmatch/mkvtree", "-pl",
                            "-"+alphabet, "-allout","-db", "../"+seqfile])
    else:
      subprocess.check_call([THIS_FILE_DIR+"/vmatch/mkvtree","-pl", "-smap",
                            alphabet, "-allout","-db", "../"+seqfile])
    os.chdir(cwd)
  
    if not matchfile:
      matchfile= '{}_vmatch_{}.txt'.format(os.path.splitext(seqfile_name)[0],
                                           minlength)
    with open(matchfile,'w') as f:
      subprocess.check_call([THIS_FILE_DIR+"/vmatch/vmatch", "-l",
                            str(minlength), VMATCH_SCRATCH+seqfile_name],
                            stdout=f)
  except OSError:
    vmatchdir = THIS_FILE_DIR+'/vmatch'
    printerr(vmatchdir)
    if os.path.exists(vmatchdir):
      files = '\n  '.join(os.listdir(vmatchdir))
      sys.exit(f'ERROR: vmatch appears to be malinstalled in the'
               f' {THIS_FILE_DIR} directory.\n'
               f'       The executables "vmatch" and "mkvtree" should be in'
               f' that directory.\n'
               f'       We see the following files:\n  '+files)
    else:
      sys.exit(f'ERROR: it appears as though you have not installed vmatch in'
               f'\n       the {THIS_FILE_DIR} directory.\n'
               f'       You should put the vmatch directory (or a link to it)'
               f'\n       at "{THIS_FILE_DIR}".')
  except subprocess.CalledProcessError:
    sys.exit('ERROR: vmatch died. See error above.')

  return matchfile, minlength



#} ________________________________________________________________________




#{ Organizing Matches:

def processMatches(idTOmatchlists: Dict[str, List[Match]],
                   idTOlen) -> List[FrozenSet[Match]]:
  """
  Create equivalence classes on the given Matches.

  @param idTOmatchlists: map gid to list of all Matches for that gid
  @param idTOlen:        map gid to genome length

  @return: return list of sets of Matches such that the matches all concern
           the same set of equivalent sites
  """
  eqclasses = processMatchesNEW(idTOmatchlists, idTOlen)

  #   #Compare to the old method:
  #eqclassesold = processMatchesOLD(idTOmatchlists)
  #print('differences:')
  #printDifferencesInEqClasses(eqclasses, eqclassesold)

  return eqclasses


def printDifferencesInEqClasses(eqclasses1, eqclasses2):
  """
  Print the differences between the two lists of sets of equivelent indices.
  """
  setversion1 = set()
  for mset in eqclasses1:
    setversion1.add(frozenset(triple for m in mset
                                     for triple in (m.triple1, m.triple2)))

  setversion2 = set()
  for mset in eqclasses2:
    setversion2.add(frozenset(triple for m in mset
                                     for triple in (m.triple1, m.triple2)))

  #print setversion1
  #print setversion2

  
  in1not2 = setversion1 - setversion2
  in2not1 = setversion2 - setversion1

  print('len1: {}'.format(len(setversion1)))
  print('len2: {}'.format(len(setversion2)))
  print('shared len: {}'.format(len(setversion1 & setversion2)))
  print('in1not2: {}'.format(in1not2))
  print('in2not1: {}'.format(in2not1))




def processMatchesNEW(idTOmatchlists: Dict[str, List[Match]],
                      idTOlen) -> List[FrozenSet[Match]]:
  """
  Compute the equivalence classes of matches using the match graph.
  """
  return getEqClasses(idTOmatchlists, idTOlen)


def processMatchesNEW_FILE(idTOmatchlists, idTOlen, writeonly=False):
  """
  The function meant to replace processMatches.abs  It needs two
  additional arguments, idTOi and idTOlen and returns a list of frozensets of
  matches.

  @note: this version write to a file.

  @param writeonly: only write the input file for the eqclasses binary.
  """
  allmatches = set(chain(*idTOmatchlists.values()))

  infile = 'eqclasses_in.txt'
  outfile = 'eqclasses_out.txt'

  with open(infile, 'w') as f:
     #Print number of genomes
    f.write(str(len(idTOlen))+'\n')
    genseqid = 0
    idTOseq = {}
    seqTOid = {}
    for gid, i in idTOlen.items():
      idTOseq[gid] = genseqid
      seqTOid[genseqid] = gid
        #For each genome, print its length
      f.write(str(i)+'\n')
      genseqid = genseqid + 1
        #Print number of matches
    f.write(str(len(allmatches))+'\n')
        #Print each match: genome1, start1, genome2, start2, length using
        #sequential id
    for m in allmatches:
      f.write(str(idTOseq[m.gid1])+' '+str(m.s1)+' '+str(idTOseq[m.gid2])+
              ' '+str(m.s2)+' '+str(m.e1-m.s1)+'\n')

  if writeonly:
    return

  subprocess.call([f"{THIS_FILE_DIR}/ccode/eqclasses", infile ])

  with open(outfile,'r') as f:
    eqclasslist = []
    for line in f:
      eqlist = line.split()
      width = int(eqlist[0])   # Width of equivalence class
      pairlist = list(zip( eqlist[1::2], eqlist[2::2] ))
     #   print "Width", width, pairlist
      matchlist = []
      for x,y in combinations(pairlist,2):
        qid = seqTOid[int(x[0])]
        sid = seqTOid[int(y[0])]
        qstart = int(x[1])
        sstart = int(y[1])
        if qid < sid:
          matchlist.append(Match(qid, sid,
                           match=(qstart, qstart+width, sstart, sstart+width))
                          )
        else:
          matchlist.append(Match(sid, qid,
                           match=(sstart, sstart+width, qstart, qstart+width))
                          )
        
      eqclasslist.append(frozenset(matchlist))

  return eqclasslist




def processMatchesOLD(idTOmatchlists, fixoverlaps=True):
  """
  Fix overlaps in matches, chop the matches up, and then create equivalence
  classes on the resulting matches.

  @return: return the chopped up idTOmatchlists and the computed eqclasses
  """
      #Deal with transitive overlaps in matches:
  if fixoverlaps:
    idTOmatchlists = fixMatchOverlaps(idTOmatchlists)
  else:
    maxoverlap = checkMatchesForOverlaps(idTOmatchlists)
    if maxoverlap:
      printnow('Maximum overlap is {}. Use "-f".'.format(maxoverlap))

      #Breakpoints are between i and i-1 for index i
      # (e.g. intervals [h,i-1] and [i,j] both have breakpoints at index i).
  #printnow('cutting matches...')
  idTObreakpoints = getBreakpointsTrans(idTOmatchlists)
  idTOmatchlists = cutMatches(idTOmatchlists, idTObreakpoints)

  #printnow('partitioning matches...')
  eqclasses = partitionMatchesIntoEqClasses(idTOmatchlists)

  #printClasses( eqclasses )

  return eqclasses




def printClasses(eqclasses):
  """
  Print the given list of equivalence classes.
  """
  for eclass in eqclasses:
    gl = set()
    for match in eclass:
      gl.add( (match.gid1, match.s1) )
      gl.add( (match.gid2, match.s2) )
      len = match.e1 - match.s1 
    print("Width", len, list(gl))


def getBreakpoints(matchintlists):
  """
  Return a list of breakpoints for each genome, given the intervals from the
  matches for each genome.  They will be ordered from lowest to highest.

  @param matchintlists: maps gid to list of intervals
  @type matchintlists:  string to list of pairs
  """
  breakpoints = defaultdict(list) #The breakpoints for each sequence.
  for gid, mintervals in matchintlists.items():
    bpset = set(chain(*mintervals))
    breakpoints[gid] = sorted(bpset)

  return breakpoints



def getBreakpointsTransGRAPH(idTOmatchlists):
  """
  Return a list of breakpoints for each genome, given the intervals from the
  matches for each genome.  They will be ordered from lowest to highest.
  Breakpoints will be transferred between genomes through matches.

  @warn: we assume that location i in genome A matches at most one location
         in genome B.

  @param idTOmatchlists: maps gid to list of matches
  @type idTOmatchlists:  string to list of Match objects

  @return: dictionary mapping gid to list of breakpoints
  """
    #idTOiTOlocset[gid][i] is the set of all equivalent location pairs to
    #location (gid, i).
  idTOiTOlocset = mapIndicesTransitively(idTOmatchlists)

    #Get the initial breakpoints per genome:
  idTObpset = bpDictFromMatchLDict(idTOmatchlists)

     #Transfer the breakpoints from a genome to all equivalent indices:
  for gid, bps in idTObpset.items():
    for bp in bps:            #Transfer each breakpoint:
      for gid2,i2 in idTOiTOlocset[gid][bp]:
        idTObpset[gid2].add(i2)
      
     #Turn the sets into lists:
  return {gid: sorted(idTObpset[gid]) for gid in idTObpset.keys()}



def getBreakpointsTrans(idTOmatchlists):
  """
  Return a list of breakpoints for each genome, given the intervals from the
  matches for each genome.  They will be ordered from lowest to highest.
  Breakpoints will be transferred between genomes through matches.

  @warn: we assume that location i in genome A matches at most one location
         in genome B.

  @param idTOmatchlists: maps gid to list of matches
  @type idTOmatchlists:  string to list of Match objects

  @return: dictionary mapping gid to list of breakpoints
  """
  #Each match implies an interval of equivalent indices.
  #Build a structure that maps indices of one genome to indices of others,
  #defined by the matches
  #  (idTOiTOidTOi[id1][i][id2] gives the corresponding index in genome 2).
  idTOiTOidTOi = defaultdict(lambda: defaultdict(dict))
  def getBreakpointsTrans_SLOWPART(idTOiTOidTOi):
    for mlist in idTOmatchlists.values():
      for m in mlist:
        for i in range(m.sd1.s, m.sd1.e):
          idTOiTOidTOi[m.sd1.gid][i][m.sd2.gid] = m.sd2.s + i-m.sd1.s
        for i in range(m.sd2.s, m.sd2.e):
          idTOiTOidTOi[m.sd2.gid][i][m.sd1.gid] = m.sd1.s + i-m.sd2.s
  getBreakpointsTrans_SLOWPART(idTOiTOidTOi)


     #Get the initial breakpoints per genome:
  bpsets = bpDictFromMatchLDict(idTOmatchlists)

     #Transfer the breakpoints from a genome to all equivalent indices:
     #(fix this to use transferBreakpoint later)
  transfered = True
  while(transfered):
    transfered = False
    for gid, bpl in bpsets.items():
      for bp in bpl:            #Transfer each breakpoint:
        #transferBreakpoint(bpsets, idTOiTOidTOi, gid, bp)
        for gid2,i in idTOiTOidTOi[gid][bp].items():
          if i not in bpsets[gid2]:
            bpsets[gid2].add(i)
            transfered = True
      
     #Turn the sets into lists:
  return {gid: sorted(bpsets[gid]) for gid in bpsets.keys()}




#from graph_tool.all import *
#def mapIndicesTransitivelyGRAPH_TOOL(idTOmatchlists):
#  """
#  Each match implies an interval of equivalent indices.
#  Build a structure that maps indices of one genome to indices of others,
#  defined by the matches.  The equivalences are transfered transitively.
#
#  @note: the end indices (which actually denote the position AFTER the last
#         match) are considered equivalent
#
#  @param idTOmatchlists: map gid to a list of matches
#
#  @return: retval[id1][i] gives a set of equivalent locations to the site at
#           index i of gid1
#  """
#    #Build a graph where the nodes are locations in a genome.  There is an
#    #edge if the locations are equivalent through a match.
#  G = Graph(directed=False)       #A graph-tool graph.
#  spotTOi = {}
#  #iTOspot = G.new_vertex_property("object")
#  iTOspot = {}
#  pairlist = []
#  counter = 0
#  for gid, mlist in idTOmatchlists.iteritems():
#    for m in mlist:
#      for i1,i2 in izip(xrange(m.sd1.s,m.sd1.e+1), xrange(m.sd2.s,m.sd2.e+1)):
#        spot1 = (m.sd1.gid, i1)
#        spot2 = (m.sd2.gid, i2)
#        if spot1 in spotTOi:
#          i1 = spotTOi[spot1]
#        else:
#          i1 = counter
#          counter += 1
#          iTOspot[i1] = spot1
#          spotTOi[spot1] = i1
#        if spot2 in spotTOi:
#          i2 = spotTOi[spot2]
#        else:
#          i2 = counter
#          counter += 1
#          iTOspot[i2] = spot2
#          spotTOi[spot2] = i2
#
#        pairlist.append((i1,i2))
#
#  G.add_edge_list(pairlist)
#
#
#  vTOcomp,_ = label_components(G)   #Get the component map.
#  cTOvlist = defaultdict(list)
#  for v in G.vertices():            #Organize vertices by component.
#    cTOvlist[vTOcomp[v]].append(v)
#
#    #The connected components are the equivalent locations.
#  idTOiTOlocset = defaultdict(dict)
#  for cc in cTOvlist.itervalues():
#    locset = set(iTOspot[v] for v in cc)
#    for v in cc:
#      gid,i = iTOspot[v]
#      idTOiTOlocset[gid][i] = locset
#
#  return idTOiTOlocset



def mapIndicesTransitively(idTOmatchlists):
  """
  Each match implies an interval of equivalent indices.
  Build a structure that maps indices of one genome to indices of others,
  defined by the matches.  The equivalences are transfered transitively.

  @note: the end indices (which denote the position AFTER the last match)
         are NOT considered equivalent

  @param idTOmatchlists: map gid to a list of matches

  @return: retval[id1][i] gives a set of equivalent locations to the site at
           index i of gid1
  """
    #Build a graph where the nodes are locations in a genome.  There is an
    #edge if the locations are equivalent through a match.
  G = igraph.Graph()
  spotTOv = {}
  iTOspot = {}
  pairlist = []
  counter = 0
  for _, mlist in idTOmatchlists.values():
    for m in mlist:
      for i1,i2 in zip(range(m.sd1.s,m.sd1.e), range(m.sd2.s,m.sd2.e)):
        spot1 = (m.sd1.gid, i1)
        spot2 = (m.sd2.gid, i2)
        if spot1 in spotTOv:
          i1 = spotTOv[spot1]
        else:
          i1 = counter
          counter += 1
          iTOspot[i1] = spot1
          spotTOv[spot1] = i1
        if spot2 in spotTOv:
          i2 = spotTOv[spot2]
        else:
          i2 = counter
          counter += 1
          iTOspot[i2] = spot2
          spotTOv[spot2] = i2

        pairlist.append((i1,i2))
  G.add_vertices(counter)
  G.add_edges(pairlist)

  def foo2(G):
      #The connected components are the equivalent locations.
    idTOiTOlocset = defaultdict(dict)
    for cc in G.components():
      locset = set(iTOspot[v] for v in cc)
      for v in cc:
        gid,i = iTOspot[v]
        idTOiTOlocset[gid][i] = locset
    return idTOiTOlocset

  return foo2(G)





def mapIndicesTransitivelyNX(idTOmatchlists):
  """
  Each match implies an interval of equivalent indices.
  Build a structure that maps indices of one genome to indices of others,
  defined by the matches.  The equivalences are transfered transitively.

  @note: the end indices (which actually denote the position AFTER the last
         match) are considered equivalent

  @param idTOmatchlists: map gid to a list of matches

  @return: retval[id1][i] gives a set of equivalent locations to the site at
           index i of gid1
  """
    #Build a graph where the nodes are locations in a genome.  There is an
    #edge if the locations are equivalent through a match.
  G = nx.Graph()
  for gid, mlist in idTOmatchlists.items():
    for m in mlist:
      for i1,i2 in zip(range(m.sd1.s,m.sd1.e+1), range(m.sd2.s,m.sd2.e+1)):
        G.add_edge((m.sd1.gid,i1), (m.sd2.gid,i2))

    #The connected components are the equivalent locations.
  idTOiTOlocset = defaultdict(dict)
  for cc in nx.connected_components(G):
    locset = set(cc)
    for gid,i in cc:
      idTOiTOlocset[gid][i] = locset

    #Check the computed values (TEMPORARY):
  #idTOiTOlocset2 = mapIndicesTransitivelyHIGHMEM(idTOmatchlists)
  #printerr('done')
  #for gid,iTOlocset in idTOiTOlocset.iteritems():
  #  if idTOiTOlocset2[gid] != iTOlocset:
  #    printerr('bad:\n{}\n!=\n{}'.format(iTOlocset, idTOiTOlocset2[gid]))
  #for gid,iTOlocset in idTOiTOlocset2.iteritems():
  #  if idTOiTOlocset[gid] != iTOlocset:
  #    printerr('bad:\n{}\n!=\n{}'.format(iTOlocset, idTOiTOlocset[gid]))

  return idTOiTOlocset



#from memory_profiler import profile
#@profile
def mapIndicesTransitivelyHIGHMEM(idTOmatchlists):
  """
  Each match implies an interval of equivalent indices.
  Build a structure that maps indices of one genome to indices of others,
  defined by the matches.  The equivalences are transfered transitively.

  @warn: memory quadratic in the number of genomes!

  @param idTOmatchlists: map gid to a list of matches

  @return: retval[id1][i][id2] gives the list of corresponding indices for
           index i of genome id1 in genome id2.
  @rtype:  a 3D dictionary mapping gid to location to gid to set of locations
  """
    #Build the direct mapping via the matches:
  idTOiTOidTOi = defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
  for gid, mlist in idTOmatchlists.items():
    for m in mlist:
      for i in range(m.s1, m.e1):
        idTOiTOidTOi[m.gid1][i][m.gid2].add(m.s2 + i-m.s1)
      for i in range(m.s2, m.e2):
        idTOiTOidTOi[m.gid2][i][m.gid1].add(m.s1 + i-m.s2)

    #Extend the direct mapping transitively:
  idTOiTOlocset = defaultdict(dict)
  for gid1,iTOidTOi in idTOiTOidTOi.items():
    for i1 in iTOidTOi.keys():
      locstack = [(gid1,i1)]
      visited = idTOiTOlocset[gid1][i1] = set(locstack)
      while(locstack):
        gid2,i2 = locstack.pop()
        toadd = []
        for gid,ilist in idTOiTOidTOi[gid2][i2].items():
          toadd += [(gid,i) for i in ilist]
        locstack += list(set(toadd) - visited)
        for p in toadd:
          visited.add(p)

  return idTOiTOlocset



def mapIndicesTransitivelySLOW(idTOmatchlists):
  """
  Each match implies an interval of equivalent indices.
  Build a structure that maps indices of one genome to indices of others,
  defined by the matches.  The equivalences are transfered transitively.

  @param idTOmatchlists: map gid to a list of matches

  @return: retval[id1][i][id2] gives the list of corresponding indices for
           index i of genome id1 in genome id2.
  @rtype:  a 3D dictionary mapping gid to location to gid to set of locations
  """
    #Build the direct mapping via the matches:
  idTOiTOidTOi = defaultdict(lambda: defaultdict(lambda: defaultdict(set)))
  for gid, mlist in idTOmatchlists.items():
    for m in mlist:
      for i in range(m.s1, m.e1):
        idTOiTOidTOi[m.gid1][i][m.gid2].add(m.s2 + i-m.s1)
      for i in range(m.s2, m.e2):
        idTOiTOidTOi[m.gid2][i][m.gid1].add(m.s1 + i-m.s2)

    #Extend the direct mapping transitively:
  idTOiTOlocset = defaultdict(dict)
  for gid1,iTOidTOi in idTOiTOidTOi.items():
    for i1 in iTOidTOi.keys():
      locstack = [(gid1,i1)]
      visited = idTOiTOlocset[gid1][i1] = set(locstack)
      while(locstack):
        gid2,i2 = locstack.pop()
        toadd = []
        for gid,ilist in idTOiTOidTOi[gid2][i2].items():
          toadd += [(gid,i) for i in ilist]
        locstack += list(set(toadd) - visited)
        for p in toadd:
          visited.add(p)

    #Check the computed values:
  #for gid1,iTOlocset in idTOiTOlocset.iteritems():
  #  for i1,lset in iTOlocset.iteritems():
  #    for (gid2,i2) in iter(lset):
  #      assert(lset == idTOiTOlocset[gid2][i2])

  return idTOiTOlocset




def bpDictFromMatchLDict(idTOmatchlists):
  """
  Convert a matchlist dictionary to an breakpoint set dictionary.

  @param idTOmatchlists: a list of all Match objects for each genome
  @type  idTOmatchlists: map gid to match list
  @return: a set of all breakpoint boundaries for each genome.
           An interval staring at index i and
           ending at index j will have i and j+1 in the list.
  @rtype:  map gid to a set of indices (integers).
  """
  matchints = defaultdict(set)   #Maps genome id (gid) to set of indices.
  for mlist in idTOmatchlists.values():
    for m in mlist:
      matchints[m.sd1.gid].add(m.sd1.s)
      matchints[m.sd1.gid].add(m.sd1.e)
      matchints[m.sd2.gid].add(m.sd2.s)
      matchints[m.sd2.gid].add(m.sd2.e)
  return matchints


def intervalDictFromMatchlDict(idTOmatchlists):
  """
  Convert a matchlist dictionary to an intervallist dictionary.

  @param idTOmatchlists: a list of all Match objects for each genome
  @type  idTOmatchlists: map gid to match list
  @return: a list of all breakpoint boundaries for each genome.
           An interval staring at index i and
           ending at index j will be a pair (i,j+1).
  @rtype:  map gid to a list of index pairs (integers).
  """
  matchints = defaultdict(set)   #Maps genome id (gid) to list of intervals.
  for mlist in idTOmatchlists.values():
    for m in mlist:
      matchints[m.gid1].add((m.s1,m.e1))
      matchints[m.gid2].add((m.s2,m.e2))
  return matchints



def transferBreakpoint(idTObpset, idTOiTOidTOi, gid, bp, beenseen=set()):
  """
  Recursively transfer this breakpoint to all equivalent locations of the given
  location.
  """
  beenseen.add(gid)
  for gid2,i in idTOiTOidTOi[gid][bp].items():
    if gid2 not in beenseen:
      idTObpset[gid2].add(i)
      transferBreakpoint(idTObpset, idTOiTOidTOi, gid2, i, beenseen)


def getBreakpointsTrans__OLD(idTOmatchlists, idTOintlists):
  """
  Return a list of breakpoints for each genome, given the intervals from the
  matches for each genome.  They will be ordered from lowest to highest.
  Breakpoints will be transferred between genomes through matches.

  @param idTOmatchlists: maps gid to list of matches
  @type idTOmatchlists:  string to list of Match objects
  @param idTOintlists: maps gid to list of intervals
  @type idTOintlists:  string to list of pairs
  """
  bpsets = defaultdict(set)
  for gid, mintervals in idTOintlists.items():
    bpsets[gid] = set(chain(*mintervals))

  bplists = defaultdict(list) #The breakpoints for each sequence.
  for gid, mlist in idTOmatchlists.items():
    bplists[gid] = sorted(bpsets[gid])
    for m in mlist:
      this = m[gid]
      that = m[gid].other
      si = bplists[gid].index(this.s)
      ei = bplists[gid].index(this.e)
      for bp in bplists[gid][si+1:ei]:
        bpsets[that.gid].add(that.s + (bp-this.s))

  for gid in bplists:
    bplists[gid] = sorted(bpsets[gid])

  return bplists


def cutMatches(idTOmatchlists, idTObreakpoints):
  """
  Cut the matches up in the given matchlists based on the breakpoints.
  """
  newidTOmatchlists = defaultdict(list)
  for gid,matchlist in idTOmatchlists.items():
    newmatchlist = []
    for m in matchlist:
      si = idTObreakpoints[gid].index(m[gid].s)
      ei = idTObreakpoints[gid].index(m[gid].e)
      ogid = m[gid].otherid
      osi = idTObreakpoints[ogid].index(m[ogid].s)
      oei = idTObreakpoints[ogid].index(m[ogid].e)
      breakpoints = set()
      if ei-si > 1:    #If we have breakpoints in one genome, 
        breakpoints  = set(idTObreakpoints[gid][si+1:ei])
      if oei-osi > 1:  #If we have breakpoints in the other genome, 
        for bp in idTObreakpoints[ogid][osi+1:oei]:   #translate them.
          breakpoints.add(m[gid].s + bp-m[ogid].s)

      if breakpoints:  #If we have a breakpoint cutting this match:
        newmatchlist += dice(gid, m, sorted(breakpoints))
      else:             #If we don't have a breakpoint cutting this match:
        newmatchlist.append(m)
    newidTOmatchlists[gid] = newmatchlist

  return newidTOmatchlists


def dice(gid, match, cutspots):
  """
  Repeatedly cut the given match at the given spots.
  """
  newmatchlist = []
  for cut in cutspots:
    begm,endm = match.cut(gid, cut)
    newmatchlist.append(begm)
    match = endm
  newmatchlist.append(match)
    
  return newmatchlist



def partitionMatchesIntoEqClasses(idTOmatchlists):
  """
  Partition the matches into equivalence classes.

  @note: the eqclasses should probably just be MNodes since we build MNodes
         from them eventually anyway.

  @param idTOmatchlists:  maps gid to a list of all matches including that
                          genome
  @return: a list of frozensets holding equivalent matches
  """
    #Build a graph where the nodes are intervals.  There is an edge between
    #intervals if they share a match (the edge is labeled with that match).
  G = nx.Graph()
  for mlist in idTOmatchlists.values():
    for m in mlist:
      t1, t2 = m.getTriples()
      G.add_edge(t1,t2, match=m)

  classes = []
  for cc in nx.connected_components(G):
    classes.append(frozenset((data['match']
                              for _,_,data in G.edges(cc, data=True))))
  return classes



def partitionMatchesIntoEqClassesMEDSLOW(idTOmatchlists):
  """
  Partition the matches into equivalence classes.

  @note: the eqclasses should probably just be MNodes since we build MNodes
         from them eventually anyway.

  @param idTOmatchlists:  maps gid to a list of all matches including that
                          genome
  @return: a list of frozensets holding equivalent matches
  """
    #Organize the matches by interval:
  idTOintTOeqset = defaultdict(lambda: defaultdict(set))
  for matchlist in idTOmatchlists.values():
    for match in matchlist:
      id1,int1 = match.sd1.gid, (match.sd1.s, match.sd1.e)
      id2,int2 = match.sd2.gid, (match.sd2.s, match.sd2.e)

      eqset1 = eqset2 = None
      if int1 in idTOintTOeqset[id1]:     #Check if interval has been seen.
        eqset1 = idTOintTOeqset[id1][int1]
      if int2 in idTOintTOeqset[id2]:
        eqset2 = idTOintTOeqset[id2][int2]

      if eqset1 and eqset2:               #Do something if it's been seen:
        eqset = eqset1 | eqset2
      elif eqset1:
        eqset = eqset1
      elif eqset2:
        eqset = eqset2
      else:
        eqset = set()

      eqset.add(match)
      for m in eqset:                      #Assign new set for each interval.
        idTOintTOeqset[m.sd1.gid][(m.sd1.s,m.sd1.e)] = eqset
        idTOintTOeqset[m.sd2.gid][(m.sd2.s,m.sd2.e)] = eqset

  eqclasses = set()                        #Turn the dictionary into a set:
  for gid in idTOintTOeqset:
    for interval in idTOintTOeqset[gid]:
      eqclasses.add(frozenset(idTOintTOeqset[gid][interval]))
  return list(eqclasses)


def partitionMatchesIntoEqClassesSLOW(idTOmatchlists):
  """
  Partition the matches into equivalence classes.

  @note: the eqclasses should probably just be MNodes since we build MNodes
         from them anyway.

  @param idTOmatchlists:  maps gid to a list of all matches including that
                          genome
  @return: a list of frozensets holding equivalent matches
  """
    #Organize the matches by interval.
  idTOintTOeqset = defaultdict(lambda: defaultdict(set))
  for matchlist in idTOmatchlists.values():
    for match in matchlist:
      (id1,s1,e1),(id2,s2,e2) = match.getTriples()

      eqset1 = eqset2 = None
      if (s1,e1) in idTOintTOeqset[id1]:  #Check if interval has been seen.
        eqset1 = idTOintTOeqset[id1][(s1,e1)]
      if (s2,e2) in idTOintTOeqset[id2]:
        eqset2 = idTOintTOeqset[id2][(s2,e2)]

      if eqset1 and eqset2:               #Do something if it's been seen:
        eqset = eqset1 | eqset2
      elif eqset1:
        eqset = eqset1
      elif eqset2:
        eqset = eqset2
      else:
        eqset = set()

      eqset.add(match)
      for (gid,s,e) in chain(*(m.getTriples() for m in eqset)):
        idTOintTOeqset[gid][(s,e)] = eqset #Assign new set for each interval.

  eqclasses = set()                        #Turn the dictionary into a set:
  for gid in idTOintTOeqset:
    for interval in idTOintTOeqset[gid]:
      eqclasses.add(frozenset(idTOintTOeqset[gid][interval]))
  return list(eqclasses)


def getGraphOnInterval(n1: MNode, n2:MNode, anchormode, O: InputInfo,
                       minlength, matchdir, expand=False, includeends=True,
                       removecycles=False, compactnodes=True,
                       annotations: Annotations=None):
  """
  Return a graph computed between the given nodes.  Only compute the anchors if
  anchormode is True.

  :note: the given nodes must be on the same gid sets.

  :param n1:          MNode on the left
  :param n2:          MNode on the right
  :param anchormode:  if True, then get the graph only for the anchors
  :param O:           InputInfo object
  :param minlength:   the minimum match length to use
                      (could use a higher one if there are cycles)
  :param matchdir:    temp dir to put matches in
  :param expand:      do not contract the nodes
  :param includeends: include the interval inside the endpoint nodes if True
  :param removecycles:remove the cycles from the graph instead of trying to
                      find a longer minimum match length
  :param annotations: show these annotations


  :return: (graph, minlength, cycles) where minlength is the new min length of
           matches and cycles is True if there were cycles detected.
  """
  cfcycles = True
  if anchormode or expand: #If we're only computing anchors or not contracting
    cfcycles = False        #then don't check for cycles.
  
  assert(n1.gids == n2.gids)
  eqclasses,G,minlength,cycles = recomputeClassesForNodes(n1,n2, minlength, O,
                                             matchdir,
                                             includeends=includeends,
                                             checkforcycles=cfcycles,
                                             removecycles=removecycles,
                                             compactnodes=compactnodes,
                                             annotations=annotations)

  if anchormode:
    if not eqclasses:
      raise GraphOnIntervalError('No matches found in the interval.')

    anchors = G.getAnchors()
    if not anchors:
      raise GraphOnIntervalError('No anchors found in the interval.')

    return (InterGraph(nodes=anchors, addloners=False, compactnodes=False,
                       seqfile=O.seqfile, idTOi=O.idTOi,
                       gidset=O.gidset, idTOlen=O.idTOlen, anchorgraph=True,
                       annotations=annotations, normfunc=n1.norm),
            minlength, cycles)

  if not G:
    raise GraphOnIntervalError('No matches found in the interval.')

  if not expand:    #Contract the blocks that are contractable.
    G.categorizeBlocks(contract=True, ignoreends=False)

  return G, minlength, cycles


def removeClassesNotBetween(eqclasses, O: InputInfo):
  """
  Exclude any class not corresponding to a substring matching something in the
  given intervals.

  @param eqclasses: sets of equivalent Matches.
  @param O:         InputInfo object
  """
      #The set of special gids:
  specialgids = frozenset(gid for gid, interval in O.idTOint.items()
                          if interval != (0,1))

      #Build a list of MNodes (for programming convenience):
  nodes = []
  for eqclass in eqclasses:
    nodes.append(MNode(eqclass))

  idTOwraps = {gid: True if start > end else False
               for gid, (start, end) in O.idTOint.items()}
                              #Map gid to True if the interval wraps.

      #For each special genome, mark the min and max matches and indices.
  idTOmax = {}
  idTOmin = {}
  for gid in O.gidset:
    idTOmax[gid] = 0
    idTOmin[gid] = sys.maxsize

  idTOmaxnode = {}
  idTOminnode = {}
  for node in nodes:
    for gid in specialgids:
      if gid in node:
        if(node[gid][0] > O.idTOint[gid][0] and   #Starts after start and
           idTOmin[gid] > node[gid][0]):          #is smallest seen.
          idTOmin[gid] = node[gid][0]
          idTOminnode[gid] = node
        elif(node[gid][0] < O.idTOint[gid][1] and #Starts before end and
             idTOmax[gid] < node[gid][0]):        #is largest seen.
          idTOmax[gid] = node[gid][0]
          idTOmaxnode[gid] = node


      #For one of the special genomes, find a first node matching something
      #after a special start node, and a last node matching something before a
      #speical end node:
  specialgid = next(iter(specialgids))
  start = idTOmin[specialgid]
  end = idTOmax[specialgid]

  for node in nodes:
    if(specialgid in node.gids and     #If we're in the interval for specialid
       ((not idTOwraps[specialgid] and
         node[specialgid][0] > start and node[specialgid][0] < end) or
        (idTOwraps[specialgid] and
         node[specialgid][0] > start or node[specialgid][0] < end))):

      for gid in node.gids:    #NOTE: !assume there's a node before wrapping!
        if gid != specialgid and idTOmin[gid] >= node[gid][0]:
          idTOmin[gid] = node[gid][0]
          idTOminnode[gid] = node

        elif gid != specialgid and idTOmax[gid] <= node[gid][0]:
          idTOmax[gid] = node[gid][0]
          idTOmaxnode[gid] = node


      #Keep classes that are between min and max for each gid:
  newclasses = set()
  for node in nodes:
    for gid in O.gidset:
      if gid in node:
        if idTOwraps[gid]:                      #wraps
          if node[gid][0] >= idTOmin[gid] or node[gid][0] <= idTOmax[gid]:
            newclasses.add(node.matchclass)

        elif(node[gid][0] >= idTOmin[gid] and    #doesn't wrap
             node[gid][0] <= idTOmax[gid]):
          newclasses.add(node.matchclass)


  return newclasses



def recomputeClassesForNodes(n1,n2, minlength, O, matchdir: str,
                             includeends=False, checkforcycles=True,
                             removecycles=False, compactnodes=True,
                             annotations: Annotations=None):
  """
  Recompute the equivalence classes on the interval between the given MNodes.

  @param n1:             the node on the left
  @param n2:             the node on the right
  @param minlength:      the length parameter to use with gt/vmatch (if zero
                         then it is automatically computed)
  @param O:              the InputInfo object
  @param matchdir:       temp dir to put matches in
  @param includeends:    include the intervals from the endpoint nodes
  @param checkforcycles: if True, return Nones if the graph has cycles and
                         try many lengths until there are no cycles
  @param removecycles:   remove the cycles from the graph instead of trying to
                         find a longer minimum match length
  @param annotations:    show these node annotations for the returned graph

  @return: (newclasses, ig, tlen, cycledetected) where newclasses are the
           equivalence classes, ig is the graph on those classes, tlen is the
           length used for matches, and cycledetected is True if there were
           cycles detected for shorter values of tlen
  """
  idTOinterval = nodesToIntervals(n1, n2, minlength, includeends)

  return recomputeClassesForIntervals(idTOinterval, minlength, O, matchdir,
                                      None, n1.getNormFuncs(),
                                      checkforcycles, removecycles,
                                      compactnodes, annotations=annotations)


def nodesToIntervals(n1: MNode, n2: MNode, minlength=0,
                     includeends=True) -> Dict[str, Tuple]:
  """
  Return a dictionary mapping gids to intervals that correspond to the given
  nodes.

  @param n1:          the node on the left
  @param n2:          the node on the right
  @param minlength:   if set, then raise an error if there is no interval at
                      least this long
  @param includeends: include the intervals from the endpoint nodes

  @returns: map of gid to the intervals between (or including) the nodes
  """
  i1,i2 = 2,1
  if includeends:
    i1,i2 = 1,2              #Include the nodes' intervals as well.

  norm = n1.getNormFuncs()
  idTOinterval = {}
  intervallens = []
  for t1,t2 in zip(n1.triplesGEN, n2.triplesGEN):
    idTOinterval[t1[0]] = (t1[i1],t2[i2])
    if norm and norm.normalizing(): #Use normalized indices to calculate length.
      secondindex = norm.I(t2[0], t2[i2])
      if secondindex == None:
        secondindex = norm.I(t2[0], t2[i2]-1)+1

      length = secondindex - norm.I(t1[0], t1[i1])
    else:
      length = t2[i2]-t1[i1]

    intervallens.append(length)

  if minlength and max(intervallens) <= max(minlength, MIN_LENGTH):
    raise GraphOnIntervalError('No matches found in the given interval.')
  
  return idTOinterval


def recomputeClassesForIntervals(idTOinterval: Dict[str, Tuple[int, int]],
                                 minlength, O: InputInfo,
                                 matchdir: str, gidset=None,
                                 normfuncs: NormIndFuncs=None,
                                 checkforcycles=True, removecycles=False,
                                 compactnodes=True, addends=False,
                                 updatefunc: Callable=None,
                                 cycleupdatefunc: Callable=None,
                                 annotations: Annotations=None,
                                 killevent: Event=None):
  """
  Recompute the equivalence classes on the given intervals.

  @note: if idTOintervals is None, then compute classes on whole genomes

  @param idTOinterval:   map gid to interval
  @param minlength:      the length parameter to use with gt/vmatch (if zero
                         then it is automatically computed)
  @param gidset:         the set of gids to use (ignore those not in gidset)
  @param normfuncs:      the object used for normalizing functions.
  @param matchdir:       temp dir to compute matches in
  @param checkforcycles: if True, return None if the graph has cycles and
                         try many lengths until there are no cycles
  @param removecycles:   remove the cycles from the graph instead of trying to
                         find a longer minimum match length
  @param compactnodes:   compact nodes with few nucleotides (to not show
                         the interval information)
  @param addends:        add the dummy end nodes to the graph that is returned
  @param updatefunc:     function to send update messages to
  @param cycleupdatefunc:function to call to update user about cycle detection
  @param annotations:    show these node annotations
  @param killevent:      terminate computation on this event


  @return: (newclasses, ig, tlen, cycledetected) where newclasses are the
           equivalence classes, ig is the graph on those classes, tlen is the
           length used for matches, and cycledetected is True if there were
           cycles detected for shorter values of tlen
  """
  tlen = minlength
  ig = None
  cycledetected = False
  while True:                #Loop until we find a cycle-less match length:
    if cycledetected and cycleupdatefunc:
      cycleupdatefunc(tlen)
    elif updatefunc:
      updatefunc(f'Calculating matches length {tlen} or longer...')

    if idTOinterval:
      idTOmatchlists, _, tlen = getMatchesOnIntervals(O, idTOinterval, tlen,
                                                      matchdir)
    else:
      idTOmatchlists, _, tlen = getMatches(O, matchdir, tlen)

    if killevent and killevent.is_set(): return (None, None, None, None)
    if not idTOmatchlists:   #If there are no matches
      newclasses = []
      break                  #stop looking in these intervals

    if updatefunc and not cycledetected:
      updatefunc('Grouping matches...')

    newclasses = processMatches(idTOmatchlists, O.idTOlen)
    if not newclasses:
      break
    if killevent and killevent.is_set(): return (None, None, None, None)

    ig = InterGraph(newclasses, seqfile=O.seqfile, idTOi=O.idTOi,
                    removecycles=removecycles, gidset=gidset,
                    idTOlen=O.idTOlen, compactnodes=compactnodes,
                    addends=addends, annotations=annotations,
                    normfuncs=normfuncs)

    if not checkforcycles or ig.isDAG():
      break                  #If we're cycle-less then we're done.

    if minlength is not None:
      tlen += 1              #try a larger min match length.
    else:
      try:
        printerr('\n\n'.join(map(str, next(ig.simpleCycles()))))
      except KeyError:
        printerr("There is a cycle that can't be printed since you have an "+\
                 "old (buggy) version of networkx!")
      raise GraphOnIntervalError('WARNING: graph has cycles'+\
                                 ' (output to terminal)!')

    if not cycledetected and cycleupdatefunc:
      cycleupdatefunc(tlen)
    cycledetected = True
    if killevent and killevent.is_set(): return (None, None, None, None)

  return newclasses, ig, tlen, cycledetected





def getMatchesAndEQClasses(O, minlength, matchdir, updatefunc: Callable=None):
  """
  Compute the matches.  From the matches, compute the equivalence classes.

  @note: If O.idTOint is non-None, map genome id to the interval of interest.
         Genomes that have no key are excluded. If the interval
         length is positive but less than 3, then include the
         whole genome.
         If start is after end, then include the begining and 
         end (as though the sequence wraps).

  @note: recomputeClassesForIntervals() for a function that does the same thing
         but adapts the match length according to the presence of cycles.
  
  @param O:         the InputInfo object.
  @param minlength: the minimum length to use for matches.
  @param matchdir:  the temporary match dir.
  @param updatefunc:a function to post update messages through.

  @return: (eqclasses, minlength) where eqclasses is a list of sets of Matches
           such that the matches all concern the same set of equivalent sites,
           and minlength which was the minimum match length used to build
           the eqclasses (this can be different than the given argument if
           it is increased due to cycles).
  """
      #Compute the matches:
  if O.verbose: printerr('Computing matches...')
  if updatefunc: updatefunc(f'Computing matches length {minlength} or longer...')
  idTOmatchlists, gidsused, minlength = getMatches(O, matchdir, minlength)
                                                                
      #Turn the matches into equivalence classes:
  if O.verbose: printerr('Classifying matches...')
  if updatefunc: updatefunc('Classifying matches...')
  eqclasses = processMatches(idTOmatchlists, O.idTOlen)

      #Trim matches not sandwitched by matches from special intervals:
  if O.idTOint:                                                   
    if O.verbose: printerr('Trimming matches...')
    if updatefunc: updatefunc('Trimming matches...')
    eqclasses = removeClassesNotBetween(eqclasses, O)

  return eqclasses, gidsused, minlength


def getMatches(O: InputInfo, scratchdir, minlength=None):
  """
  Compute matches between all pairs of sequences.

  :note: If O.idTOint is non-None, map genome id to the interval of interest.
         genomes that have no key are excluded. If the interval
         length is positive but less than 3, then include the
         whole genome.
         If start is after end, then include the begining and 
         end (as though the sequence wraps).

  :note: if O.selectgenomes is true, use only the lines from the fastafile
         that correspond to the O.gidset

  :param O:          the InputInfo object.
  :param scratchdir: the temporary directory to store matches in.
  """
  if O.idTOint:
    idTOmatchlists, gidsused, minlength = getMatchesOnIntervals(O, O.idTOint,
                                                                minlength,
                                                                scratchdir)
  else:
    seqfile = O.seqfile
    tempO = O
    scratchdir = getGTScratch(scratchdir)
    os.mkdir(scratchdir)
    if O.selectgenomes:
      seqfile = scratchdir+'tmpslice.fa'   #Limit the genome set if specified.

      sliceFastaFile(O.seqfile, seqfile, includelines=O.getIncludedLines())
      tempO = O.getGIDSubset(O.gidset)

        #Run gt if necessary:
    matchfile, minlength = runGT(seqfile, scratchdir, None, minlength)
        #Get the matches from the gt file:
    idTOmatchlists, gidsused, minlength = readMatchFile(matchfile,
                                                        tempO.iTOid,
                                                        tempO.gidset,
                                                        tempO.verbose)
    shutil.rmtree(scratchdir)

    if O.ignoredups:
      #idTOmatchlists, gidTOtrim = removeMatchOverlaps(idTOmatchlists)
      if O.verbose: printerr('Removing overlapping matches...')
      idTOmatchlists, _ = ignoreDuplications(idTOmatchlists)

  if idTOmatchlists == None:
    if not minlength:
      minlength = MIN_LENGTH
    sys.exit('There are no matches of length {}.'.format(minlength))

  return idTOmatchlists, gidsused, minlength




def getMatchesOnIntervals(O: InputInfo,
                          idTOinterval: Dict[str, Tuple[int, int]],
                          minlength: int, #usegids: Iterable[str],
                          scratchdir) -> Tuple[Dict[str, List[Match]], Set, int]:
  """
  Return the matches over the given intervals.

  :note: if the ids in idTOinterval are a subset of O.gidset, then only get
         motches for those ids.

  @param O:             the InputInfo object.
  @param idTOinterval:  maps genome id to interval over which we will cut.      
                        exclude genomes that don't have a key.                 
                        include the whole genome for keys that have positive  
                        lengths less than 3.                                   
                        If start is after end, then include the begining and 
                        end (as though the sequence wraps).

  @param minlength:     set the minimum gt length to this if not None
                        (computed automatically if not set).
  @param scratchdir:    temp dir to put matches in.

  @return: (idTOmatchlists, gidsused, minlen) where idTOmatchlists maps gid to
            lists of matches, gidused is the set of all gids with at least one
            match, and minlen in the minimum length used for gt
  """
  scratchdir = getGTScratch(scratchdir)
  os.mkdir(scratchdir)

  tempO = O
  seqfile = O.seqfile
  gidset = set(idTOinterval.keys())
  if O.selectgenomes or gidset < O.gidset: #Select the lines:
    seqfile = scratchdir+'tmpseqsubset.fa'

    sliceFastaFile(O.seqfile, seqfile, includelines=O.getIncludedLines(gidset))
    tempO = O.getGIDSubset(gidset)

  slicefile = scratchdir+'tmpslice.fa'
  slicematchfile = scratchdir+'tmpslicematches.txt'

    #Make a list of intervals in the order of the fasta file.
  intervals = []
  for gid in tempO.getGIDsSortedByI():
    if gid in idTOinterval:
      interval = idTOinterval[gid]
      if interval[0] == -1:                #Take care of intervals coming from
        interval = (0, interval[1])        #dummy first nodes.

      diff = interval[1]-interval[0]
      if diff < 3 and diff >=0:            #Add characters to empty intervals.
        intervals.append(None)
      else:
        intervals.append(interval)
    else:
      intervals.append((0,3))

  if intervals:
    sliceFastaFile(seqfile, slicefile, intervals)
  else:
    slicefile = f'{scratchdir}/{O.seqfile}'

  slicematchfile,minlength = runGT(slicefile, scratchdir, slicematchfile,
                                   minlength)
  idTOmatchlists,gidsused,_ = readMatchFile(slicematchfile, tempO.iTOid,
                                            tempO.gidset)

  shutil.rmtree(scratchdir)

  if not idTOmatchlists:
    return None, None, minlength

    #Remap the substring indices to genomic indices (will wrap if necessary).
  idTOmatchlists = fixIndices(idTOmatchlists, idTOinterval, O)

  if O.ignoredups:
    #idTOmatchlists, gidTOtrim = removeMatchOverlaps(idTOmatchlists)
    idTOmatchlists, _ = ignoreDuplications(idTOmatchlists)

  return idTOmatchlists, gidsused, minlength





def fixIndices(idTOmatchlists, idTOinterval, O):
  """
  Adjust the indices in idTOmatchlists according to the offsets in idTOs.

  @note: Sometime intervals are computed on a portion of the genome that
         starts at the end of the linear sequence and end at the beginning
         (i.e. an interval that wraps around).
         In this case the new start index will be larger than the new end.
         If this is the case the Match could be split up.

  @warn: modifies the matches in idTOmatchlists.

  @param idTOmatchlists: map gid to lists of matches
  @param idTOinterval:   map genome id to interval over which we will cut.      
  @param O:              InputInfo object
  """
      #Map gid to start index of the interval:
  idTOstart = {gid: max(0,i[0]) for gid, i in idTOinterval.items()}

  matches = set()     #Build a set so we visit each match exactly once.
  for _, ml in idTOmatchlists.items():
    for m in ml:
      matches.add(m)

  newidTOmatchlists = defaultdict(list)
  for m in matches:
    gid1 = m.gid1
    gid2 = m.gid2
    len1 = O.idTOlen[gid1]
    len2 = O.idTOlen[gid2]
                                 #Add the appropriate ammount to the matches
                                 #so that the indices are correct
                                 #(this will wrap indicies if necessary).
    m.adjustIndices(idTOstart[gid1], idTOstart[gid2],
                    O.idTOlen[gid1], O.idTOlen[gid2])

    s1,e1, s2,e2 = m.s1,m.e1, m.s2,m.e2
    if s2 > e2 and s1 > e1:     #If matches wrap around in both genomes:
      newmatches = []
      if len1-s1 < len2-s2:     #First match closer to the end than second.
        p1,p2,p3 = len1-s1, len2-s2-(len1-s1), e2       #The 3 part lengths.
        triplesA = ((gid1, s1,s1+p1), (gid2, s2,s2+p1))
        newmatches.append(Match(gid1,gid2, triples=triplesA))
        if p2:                  #If wrap around doesn't happen at same place.
          triplesB = ((gid1, 0,p2), (gid2, s2+p1,s2+p1+p2))
          newmatches.append(Match(gid1,gid2, triples=triplesB))
        triplesC = ((gid1, p2,p2+p3), (gid2, 0,p3))
        newmatches.append(Match(gid1,gid2, triples=triplesC))
      else:                      #Second match closer to the end than first.
        p1,p2,p3 = len2-s2, len1-s1-(len2-s2), e1       #The 3 part lengths.
        triplesA = ((gid1, s1,s1+p1), (gid2, s2,s2+p1))
        newmatches.append(Match(gid1,gid2, triples=triplesA))
        if p2:                  #If wrap around doesn't happen at same place.
          triplesB = ((gid1, s1+p1,s1+p1+p2), (gid2, 0,p2))
          newmatches.append(Match(gid1,gid2, triples=triplesB))
        triplesC = ((gid1, 0,p3), (gid2, p2,p2+p3))
        newmatches.append(Match(gid1,gid2, triples=triplesC))

      newidTOmatchlists[gid1].extend(newmatches)
      newidTOmatchlists[gid2].extend(newmatches)
      
    elif s1 > e1:               #If matches wrap around in genome 1:
      p1,p2 = len1-s1, e1                               #The 2 part lengths.
      triplesA = ((gid1, s1,s1+p1), (gid2, s2,s2+p1))
      newmatchA = Match(gid1,gid2, triples=triplesA)
      triplesB = ((gid1, 0,p2), (gid2, s2+p1,e2))
      newmatchB = Match(gid1,gid2, triples=triplesB)
      newidTOmatchlists[gid1].extend((newmatchA,newmatchB))
      newidTOmatchlists[gid2].extend((newmatchA,newmatchB))
    elif s2 > e2:               #If matches wrap around in genome 2:
      p1,p2 = len2-s2, e2                               #The 2 part lengths.
      triplesA = ((gid1, s1,s1+p1), (gid2, s2,s2+p1))
      newmatchA = Match(gid1,gid2, triples=triplesA)
      triplesB = ((gid1, s1+p1,e1), (gid2, 0,p2))
      newmatchB = Match(gid1,gid2, triples=triplesB)
      newidTOmatchlists[gid1].extend((newmatchA,newmatchB))
      newidTOmatchlists[gid2].extend((newmatchA,newmatchB))
    else:
      newidTOmatchlists[gid1].append(m)
      newidTOmatchlists[gid2].append(m)

  return newidTOmatchlists



def getAnchors(eqclasses, gidset, annotations=None) -> List[MNode]:
  """
  Return a list of anchors in the form of MNodes.

  :warn: if normalizing, L{MNode.setNormFuncs()} must be called on the
         returned anchors

  :param eqclasses: a list of frozensets holding equivalent matches
  :type  eqclasses: list of Match sets
  :param gidset:    the set of gids in use (not being ignored)

  :return: the classes that are anchors
  :rtype:  list of MNodes
  """
  anchors = []
  for i,c in enumerate(eqclasses):
    n = MNode(c, code=i, annotations=annotations)
    if len(n.gids) == len(gidset):
      anchors.append(n)

  return anchors


def makeDummyNodes(gidset, idTOlen, code):
  """
  Return a first and last dummy node given the gidset and the genome lengths.
  """
  first = MNode(triples=((gid,-1,0) for gid in gidset), code=code+1,
                dummy='Start')
  last = MNode(triples=((gid, idTOlen[gid], idTOlen[gid]+1)
                        for gid in gidset), code=code+2, dummy='End')
  return first,last


def getAnchorsNclasses(eqclasses, gidset, idTOlen):
  """
  Compute anchors and equivalence classes of intervals in MNode form.

  @param eqclasses: a list of MNodes corresponding to equivalent intervals
  @type  eqclasses: list of MNodes
  @param gidset:    the set of gids in use (not being ignored)
  @param idTOlen:   maps gid to genome length

  @return: (anchors, nclasses, anchTOi) where anchors are the anchors in the
           dataset with a dummy first and last anchor, nclasses are the
           eqclasses in node form, and anchTOi is the index of the anchor in
           the nclasses list.
  @rtype:  anchors is a list of MNodes, nclasses is a list of MNodes, and
           anchTOi maps MNode to integer index
  """
    #Sort the classes by building the graph and then getting a linear extension:
  mg = MatchGraph(eqclasses, addloners=False)
  if not mg.isDAG():
    sys.exit('ERROR: the matches do not correspond to a partial order'+\
             ' (play with -f or -l).')
  nodes = mg.getLinearExtension()

                                   #Make a dummy first anchor.
  first = MNode(triples=((gid,0,0) for gid in gidset))
  anchTOi = {first:-1}
  anchors = [first]
  numgenomes = len(gidset)
  for i,n in enumerate(nodes):     #Find all the real anchors.
    if len(n.gids) == numgenomes:
      anchTOi[n] = i
      anchors.append(n)
                                   #Make a dummy last anchor.
  last = MNode(triples=((gid, idTOlen[gid]+1, idTOlen[gid]+1)
                        for gid in gidset))
  anchTOi[last] = len(nodes)
  anchors.append(last)

  return anchors, nodes, anchTOi



#def adaptEqClasses(eqclasses, seqfile, idTOi, idTOlen, gidset, minlength,
#                   fixoverlaps):
#  """
#  Divide the genome by "anchors" (equivalence classes on matches from all
#  genomes) and compute new equivalence classes for each portion of genome
#  between the anchors.
#
#  @param eqclasses:      a list of frozensets holding equivalent matches
#  @param seqfile:        the fasta file with the sequences
#  @param idTOi:          maps gid to integer id in the match file (fasta order)
#  @param idTOlen:        map gid to genome length
#  @param gidset:         the set of gids in use (ignore others)
#  @param minlength:      the length parameter that was used for matching
#  @param fixoverlaps:    fix overlaps in the matches
#
#  @return: new equivalence classes
#  @rtype:  a set of frozensets holding equivalent matches
#  """
#    #Filter out the anchors from all the match classes:
#  anchors, nclasses, anchTOi = getAnchorsNclasses(eqclasses, gidset,
#                                                  getGenomeLengths(seqfile,
#                                                                   idTOi))
#
#    #Recompute the equivalence classes on all adjacent anchors:
#  retclasses = []
#  #minlength = None       #Force adapted length for intervals between anchors.
#  for n1,n2 in pairwise(anchors):
#    intervallens = [t2[1]-t1[2] for t1,t2 in zip(n1.triples,n2.triples)]
#
#    if max(intervallens) > ADAPT_LEN:  #Recompute for this interval:
#      try:
#        newclasses,_,_,_ = recomputeClassesForNodes(n1,n2, minlength, seqfile,
#                                                    idTOi, idTOlen, gidset,
#                                                    fixoverlaps)
#      except GraphOnIntervalError as e:
#        printerr(e)
#    else:                #Fill in gap with old classes:
#      newclasses = [n.matchclass for n in nclasses[anchTOi[n1]+1:anchTOi[n2]]]
#
#    if n2 != anchors[-1]:
#      newclasses.append(n2.matchclass)
#    retclasses += newclasses
#
#  return retclasses





def spansFromBreakpoints(idTObreakpoints):
  """
  Return a dictionary mapping gid to pairs that represent the spans
  (intervals) between breakpoints.
  """
  idTOspans = {}
  for gid, breakpoints in idTObreakpoints.items():
    idTOspans[gid] = list(pairwise(breakpoints))
  return idTOspans


def fixMatchOverlaps(idTOmatchlists):
  """
  Fix indirect overlaps in the matches.  Overlaps are found between pairs
  of matches sharing the same two genomes.  These overlapping regions will
  map transitively through the matches to other genomes.  Remove the
  transitive overlapping from all matches.

  @param idTOmatchlists: maps gid to a list of matches.

  @return: new version of idTOmatchlists such that the matches don't use any
           indices from any overlapping interval.
  """
    #idTOiTOlocset[gid][i] is the set of all equivalent index pairs to (gid, i).
  idTOiTOlocset = mapIndicesTransitively(idTOmatchlists)

  #idTOiTOlocset = mapIndicesTransitivelyUF(idTOmatchlists)
  #checkTransitivelyMapedIndices(idTOiTOlocset):

  idTOforbidden = defaultdict(set)  #The forbidden indices for each genome:
  for gid,iTOlocset in idTOiTOlocset.items():
    for i,locset in iTOlocset.items():
      if len(set(tempgid_i[0] for tempgid_i in locset)) != len(locset):
        #printnow('tandem repeats at ({},{}):\n{}'.\
        #         format(gid, i,' '.join(map(str,locset))))
        for gid,i in locset:
          idTOforbidden[gid].add(i)

  retidTOmls = defaultdict(list)
  for gid,mlist in idTOmatchlists.items():
    for m in mlist:
      mindices = set(range(m[gid].s, m[gid].e))
      overlap = idTOforbidden[gid] & mindices
      if overlap:
        retidTOmls[gid].extend(chopMatchByIndices(m, gid, overlap))
      else:
        retidTOmls[gid].append(m)

  return retidTOmls



def checkMapIndicesTransitively(idTOiTOlocset):
  """
  Verify the output of mapIndicesTransitively().
  """
  idTOiTOlocset_temp = defaultdict(dict)
  for gid,iTOlocset in idTOiTOlocset.items():
      for i,locset in iTOlocset.items():
          if i in idTOiTOlocset_temp[gid]:
              if locset != idTOiTOlocset_temp[gid][i]:
                  print(gid, i, locset, idTOiTOlocset_temp[gid][i])
          else:
              print("Missing position", gid, i)

  print(idTOiTOlocset_temp)
  print(idTOiTOlocset)
  assert( idTOiTOlocset_temp == idTOiTOlocset )



def chopMatchByIndices(match, gid, indices):
  """
  Chop up the match so that there are no indices in the resulting match(es)
  correponding to the given list.
  """
  #printerr('chopping match at {} {}: {}'.format(gid,list(indices),match))
  matchset = set([match])
  for i in indices:
    for m in (m for m in matchset if i in m[gid]): #should be at most one.
      middle,right = m.cut(gid,i+1)
      left,_ = middle.cut(gid,i)
      matchset.remove(m)
      if len(left):
        matchset.add(left)
      if len(right):
        matchset.add(right)
  return matchset



def ignoreDuplications(idTOmatchlists: Dict[str, List[Match]]) -> Dict[str, List[Match]]:
  """
  Remove all matches that overlap another match.

  @param idTOmatchlists: maps gid to a list of matches.

  @return: a version of idTOmatchlists that has no matches that overlap with
           each other.
  """
  #return idTOmatchlists, None             #TEMP: bypass duplication ignoring
  blacklist = set()
  gidTOremoved = defaultdict(set)
  for gid, mlist in idTOmatchlists.items():
    mlist.sort(key=lambda m: m[gid].s)
    for i, mi in enumerate(mlist):
      j = i+1
      while j < len(mlist) and mlist[j][gid].s <= mi[gid].e:
        if mi[gid].otherid == mlist[j][gid].otherid:
          #print 'removing ({}): {} | {}'.format(gid, mi, mlist[j])
          gidTOremoved[gid].add(mi)
          gidTOremoved[gid].add(mlist[j])
          gidTOremoved[mi[gid].otherid].add(mi)
          gidTOremoved[mi[gid].otherid].add(mlist[j])
          blacklist.add(mi)
          blacklist.add(mlist[j])
        j += 1

  newidTOmatchlists = {}
  for gid, mlist in idTOmatchlists.items():
    newlist = []
    for m in mlist:
      if m not in blacklist:
        newlist.append(m)

    newidTOmatchlists[gid] = newlist

      #Check the results agains the slow (allpairs) version of the code:
  #kept, removed = ignoreDuplicationsSLOW(idTOmatchlists)
  #for gid in gidTOremoved:            #Check against slow version.
  #  mset1 = set(gidTOremoved[gid])
  #  mset2 = set(removed[gid])
  #  diff = mset1 ^ mset2
  #  if diff:
  #    raise(Exception('ignoreDuplications ({}):\n{}\n{}'.format(gid,
  #                                                              mset2-mset1,
  #                                                              mset1-mset2)))

  return newidTOmatchlists, blacklist



def ignoreDuplicationsSLOW(idTOmatchlists):
  """
  Remove all matches that overlap another match between the same pair of
  genomes.

  @param idTOmatchlists: maps gid to a list of matches.

  @return: a version of idTOmatchlists that has no matches that overlap with
           each other.
  """
  printerr('s dups...')
  #return idTOmatchlists, None
  gidTOremoved = defaultdict(set)
  for gid, mlist in idTOmatchlists.items():
    #for i, m1 in enumerate(sorted(mlist, key=lambda m: m[gid].s)):
    for m1,m2 in combinations(mlist, 2):   #WARN: checking ALL pairs.
      if(m1[gid].otherid == m2[gid].otherid and
         intersects(m1[gid].s, m1[gid].e, m2[gid].s, m2[gid].e)):
        #print 'removing ({}): {} | {}'.format(gid, m1,m2)
        gidTOremoved[gid].add(m1)
        gidTOremoved[gid].add(m2)
        gidTOremoved[m1[gid].otherid].add(m1)
        gidTOremoved[m2[gid].otherid].add(m2)

  printerr('s do', end='')

  newidTOmatchlists = {}
  for gid, mlist in idTOmatchlists.items():
    newlist = []
    removedset = gidTOremoved[gid]
    for m in mlist:
      if m not in removedset:
        newlist.append(m)

    newidTOmatchlists[gid] = newlist

  printerr('ne.')

  return newidTOmatchlists, gidTOremoved


def removeMatchOverlaps(idTOmatchlists, threshold=1):
  """
  Remove overlaps in the matches.

  @note: the order in which the gids are visited has a bearing on which 
         matches are trimmed.

  @param idTOmatchlists: maps gid to a list of matches.
  @param threshold:      fix overlaps with at most this length.
  """
  #return idTOmatchlists, None
  gidTOtrim = defaultdict(list)  #Map id to interval that was trimmed.
  removed = True
  while(removed):
    removed = False
    for gid, mlist in idTOmatchlists.items():
      for m1,m2 in combinations(mlist, 2):
        if m1[gid].otherid == m2[gid].otherid:
          partsremoved = removeOverlapFrom1(m1, m2, gid, threshold)

          if partsremoved:
            removed = True
            print('removed: {}'.format(partsremoved))
            for g, interval in partsremoved:
              gidTOtrim[g].append(interval)

      #Clear out empty matches:
  newidTOmatchl = defaultdict(list)
  for gid, mlist in idTOmatchlists.items():
    for m in sorted(mlist, key=lambda m: m[gid].s):
      if not m.empty():
        newidTOmatchl[gid].append(m)
      else:
        printerr('- - - - - EMPTY:\n{}'.format(m))

  return newidTOmatchl, gidTOtrim



def removeOverlapFrom1(m1, m2, gid1, threshold):
  """
  Remove the overlap in the given matches if there is more overlap than the
  given threshold in the given genome.

  @param m1:  a match
  @param m2:  a match
  @param gid: the overlap is in this genome
  @param threshold:  fix overlaps with at most this length

  @return: ((gid1, int1), (gid2, int2.1), (gid2, int2.2)) where int1 is a pair
           representing the (open) interval of indices removed in the given
           genome, while int2.1 and int2.2 are the two intervals removed in
           the other genome.
  """
  m1g1 = m1[gid1]
  gid2 = m1g1.otherid
  m1g2 = m1[gid2]
  m2g1 = m2[gid1]
  m2g2 = m2[gid2]

  diff = m2g1.e - m1g1.s
  if(diff >= threshold and
     (m1g1.s >= m2g1.s and m1g1.s <= m2g1.e and
      m1g1.e >= m2g1.e)):
      retval = ((gid1, (m1g1.s, m2g1.e)),
                (gid2, (m1g2.s, m1g2.s+diff)),
                (gid2, (m2g2.e-diff, m2g2.e)))
      m1g1.s, m2g1.e = m1g1.s+diff, m2g1.e-diff
      m1g2.s, m2g2.e = m1g2.s+diff, m2g2.e-diff
      return retval

  diff = m1g1.e - m2g1.s
  if(diff >= threshold and
     (m1g1.s <= m2g1.s and m1g1.e >= m2g1.s and
      m1g1.e <= m2g1.e)):
      retval = ((gid1, (m2g1.s, m1g1.e)),
                (gid2, (m2g2.s, m2g2.s+diff)),
                (gid2, (m1g2.e-diff, m1g2.e)))
      m1g1.e, m2g1.s = m1g1.e-diff, m2g1.s+diff
      m1g2.e, m2g2.s = m1g2.e-diff, m2g2.s+diff
      return retval

  return False


def removeOverlap(m1, m2, threshold):
  """
  Remove the overlap in the given matches if there is more overlap than the
  given threshold.

  @param m1:  a match
  @param m2:  a match
  @param threshold:  fix overlaps with at most this length

  @return: return True if we removed an overlap
  """
  return remove1and2Symmetric(m1, m2, threshold)

def remove1and2Symmetric(m1, m2, threshold):                        
  """                                                      
  If there is an overlap in the matches, remove the overlapping part of m1
  and the overlapping part of m2.                                         
  """                                                      
  diff = m2[m1.gid1].e-m1.s1                               
  if(diff <= threshold and
     (m1.s1 > m2[m1.gid1].s and m1.s1 < m2[m1.gid1].e and
      m1.e1 > m2[m1.gid1].e)):
      m1.s1, m2[m1.gid1].e = m1.s1+diff, m2[m1.gid1].e-diff
      m1.s2, m2[m1.gid2].e = m1.s2+diff, m2[m1.gid2].e-diff
      return True                                          
                                                           
  diff = m1.e1-m2[m1.gid1].s                               
  if(diff <= threshold and
     (m1.s1 < m2[m1.gid1].s and m1.e1 > m2[m1.gid1].s and
      m1.e1 < m2[m1.gid1].e)):
      m1.e1, m2[m1.gid1].s = m1.e1-diff, m2[m1.gid1].s+diff
      m1.e2, m2[m1.gid2].s = m1.e2-diff, m2[m1.gid2].s+diff
      return True

  diff = m2[m1.gid2].e-m1.s2
  if(diff <= threshold and
     (m1.s2 > m2[m1.gid2].s and m1.s2 < m2[m1.gid2].e and
      m1.e2 > m2[m1.gid2].e)):
      m1.s1, m2[m1.gid1].e = m1.s1+diff, m2[m1.gid1].e-diff
      m1.s2, m2[m1.gid2].e = m1.s2+diff, m2[m1.gid2].e-diff
      return True

  diff = m1.e2-m2[m1.gid2].s
  if(diff <= threshold and
     (m1.s2 < m2[m1.gid2].s and m1.e2 > m2[m1.gid2].s and
      m1.e2 < m2[m1.gid2].e)):
      m1.e1, m2[m1.gid1].s = m1.e1-diff, m2[m1.gid1].s+diff
      m1.e2, m2[m1.gid2].s = m1.e2-diff, m2[m1.gid2].s+diff
      return True

  return False


def remove1from2(m1, m2, threshold):
  """
  If there is an overlap in the matches, remove the part of m1 overlapping
  on m2.
  """
  removed = False
  diff = m2[m1.gid1].e-m1.s1
  if(m1.s1 > m2[m1.gid1].s and m1.s1 < m2[m1.gid1].e and m1.e1 > m2[m1.gid1].e
     and diff <= threshold):
    m1.s1 = m1.s1 + diff
    m1.s2 = m1.s2 + diff
    removed = True
  diff = m2[m1.gid2].e-m1.s2
  if(m1.s2 > m2[m1.gid2].s and m1.s2 < m2[m1.gid2].e and m1.e2 > m2[m1.gid2].e
     and diff <= threshold):
    m1.s1 = m1.s1 + diff
    m1.s2 = m1.s2 + diff
    removed = True
  return removed


def overlaps(s1,e1, s2,e2):
  """
  Return the length of overlap if the given intervals overlap
  (without containment).
  """
  if s1 > s2 and s1 < e2 and e1 > e2:
    return e2-s1
  if s1 < s2 and e1 > s2 and e1 < e2:
    return e1-s2
  return 0

def intersects(s1,e1, s2,e2):
  """
  Return True if the intervals intersect (with containment or not).
  """
  if s1 >= s2 and s1 <= e2:
    return True
  if s2 >= s1 and s2 <= e1:
    return True
  return False


#} ________________________________________________________________________

#{ Alignment functions:

def getAlignmentFromAnchors(eqclasses, O, blockdir='',
                            lengthbound=12, percbound=0.60):
  """
  Return a gapless alignment that is a concatination of all the blocks between
  anchors that have the same number of nucleotides between them.

  @param blockdir:    If set, then save a bunch of fast files in the given
                      directory containing gapless alignments that should
                      be free of recombination.
  @param lengthbound: The size of a block between consective anchors to check
                      for high enough similarity.
  @param percbound:   The similarity block between consective anchors must
                      have in order for it to be considered part of a bigger
                      block.  The supposition is that lower similarity implies
                      recombination.
  """
  if O.verbose: printerr('Computing anchors...')
  anchors = getAnchors(eqclasses, O.gidset, O.printnormalized)

    #Make a vector good[i] == True iff the ith and i+1st anchors have the
    #same number of nucleotides between them, and the alignment block
    #passes the lengthbound or percbound tests:
  if O.verbose: printerr('Computing blocks...')
  anchors.sort()
  good = []
  for a, b in pairwise(anchors):
    if equallySpacedAnchors(a, b):
      alignment = getGapplessAlignmentForPair(a, b, O.records, O.idTOi)
      good.append(alignment.get_alignment_length()-len(a)-len(b) < lengthbound
                  or
                  percPerfectColumns(alignment, len(a), len(b)) >= percbound)
    else:
      good.append(False)

    #Find longest stretches of good values:
  blocks = []
  i = 0
  while(i < len(good)):
    j = i
    while(j < len(good) and good[j]): j += 1
    blocks.append((anchors[i], anchors[j]))
    i = j+1

    #Write the concatination of the blocks to an alignment file:
  if O.verbose: printerr('Writing file...')
  alignments = []
  iTOid = invdict(O.idTOi)
  iTOdescription = defaultdict(lambda: '|')

  for a,b in blocks:
    alignment = getGapplessAlignmentForPair(a, b, O.records, O.idTOi)

    for i in range(len(alignment)):
      iTOdescription[i] += alignment[i].description
    alignments.append(alignment)

  alignment = MultipleSeqAlignment([SeqRecord(Seq(''))] * len(O.idTOi))
  for a in alignments:
    alignment += a
 
  for i,gid in iTOid.items():
    name, description = alignments[0][i].id.split(' ', 1)
    alignment[i].id = '{}_{}'.format(gid, name)
    alignment[i].description = '|{}|{}'.format(description, iTOdescription[i])

  print('alignment length: {}'.format(alignment.get_alignment_length()))
  print('# diverse columns: {}'.format(numDiverseColumns(alignment)))
  print('# informative columns: {}'.format(numInformativeColumns(alignment)))

  if blockdir:
    writeAlignmentBlocks(blockdir, alignments)

  return alignment


def writeAlignmentBlocks(directory, alignments):
  """
  Write the given alignments to the given direcory.
  """
  if not os.path.exists(directory):
    os.makedirs(directory)
  else:
    printerr('WARNING: directory {} already exits.'.format(directory))

  for i, a in enumerate(sorted(alignments, reverse=True,
                               key=lambda a: a.get_alignment_length())):
                               #key=lambda a: percPerfectColumns(a))):
    fn = 'block_{0:04d}_{1}_{2}_{3:.2f}.fasta'.format(i,
                                                      a.get_alignment_length(),
                                                      len(a),
                                                      percPerfectColumns(a))
    AlignIO.write(a, '{}/{}'.format(directory, fn), 'fasta')


def percPerfectColumns(alignment, startignore=0, endignore=0):
  """
  Return the percentage of perfect columns for the given alignment.

  @param startignore: ignore this many characters at the start of the sequence
  @param endignore:   ignore this many characters at the end of the sequence
  """
  assert(alignment.get_alignment_length() - startignore - endignore > 0)
  a = alignment[:, startignore: alignment.get_alignment_length()-endignore]
  length = a.get_alignment_length()
  return float(length - numDiverseColumns(a)) / length
  



def equallySpacedAnchors(a, b):
  """
  Return True if there are the same number of nucleotides for all genomes
  between the given blocks.
  """
  diffs = [b[gid][0] - a[gid][1] for gid in a.gids]
  return all(d == diffs[0] for d in diffs)


def numInformativeColumns(alignment):
  """
  Return the number of columns of the given alignment that have at least two
  occurrences of at least two values.
  """
  numcols = alignment.get_alignment_length()
  count = 0
                  #Sort each column and see how the characters change:
  for i in range(numcols):
    current = ''
    duplicount = 0
    beenseen = False
    for c in sorted(alignment[:, i]):
      if current == c:
        if not beenseen:  #We have seen this character exactly once.
          duplicount += 1
          beenseen = True
      else:
        current = c
        beenseen = False

    if duplicount > 1:
      count += 1

  return count 


def numDiverseColumns(alignment):
  """
  Return the number of columns of the given alignment that have more than one
  value.
  """
  numcols = alignment.get_alignment_length()
  count = 0
  for i in range(numcols):
    if any(k != l for k,l in pairwise(alignment[:, i])):
      count += 1

  return count 


def getGapplessAlignmentForPair(a, b, records, idTOi,
                                addgaps=False):
  """
  Produce a gappless MultipleSeqAlignment for all sequences between the given
  nodes.

  @warn: assumes equal gid sets for a and b.

  @param addgaps: add all-gap rows for gids that are missing from the nodes
  """
  assert(a.gids == b.gids)

  newrecords = []
  for gid in a.gids:
    record = records[idTOi[gid]]

    start, end = a.idTOinterGEN[gid][0], b.idTOinterGEN[gid][1]
    if start < end:                                                      
      sequence = record.seq[start:end]
      length = end - start
    else:                        
      sequence = record.seq[start:]+record.seq[:end]
      length = start - end

    record = SeqRecord(sequence, id=record.description)
    record.description = '[{}-{}]'.format(start, end)
    newrecords.append(record)

  alignment = MultipleSeqAlignment(newrecords)
  if addgaps:
    newrecords = []
    for gid in idTOi:
      if gid not in a.gids:
        newrecords.append(SeqRecord(Seq('-' * length,
                                       id=records[idTOi[gid]].description)))
    alignment.extend(newrecords)

  return alignment



#} ________________________________________________________________________


#{ Module naming via blocks:


def computeModulesFromBlocks(eqclasses, O, matchdir: str, refineanchors=False,
                             ignoresmaller=MIN_MODULESIZE,
                             printalignments=True, graphposition=None,
                             alignvertices='', vertexadir='',
                             anchorpoints=None):
  """
  Compute the string represenation of the phages based on blocks between
  anchors. Each character is a module between anchors.

  @param matchdir:        temp dir to put matches in
  @param refineanchors:   try a smaller m between each pair of anchors.
  @param ignoresmaller:   ignore modules smaller than this
                          (should be at least MIN_MODULESIZE)
  @param printalignments: print the alignments for each module
  @param graphposition:   if there is a module that spans this position, then
                          return the graph for it
  @type  graphposition:   tuple of the form ('A', 2001)
  @param alignvertices:   write an alignment for all the anchors, as well as for
                          the nodes bigger than MIN_MODULESIZE
  @param vertexadir:      write alignments for all anchors, as well as for
                          the nodes bigger than MIN_MODULESIZE
  @param anchorpoints:    save (DIALIGN) anchor-point pairs to this file
  @type  anchorpoints:    str
  """
      #Compute the initial anchors:
  if O.verbose: printerr('Computing anchors...')
  anchors = getAnchors(eqclasses, O.gidset, O.printnormalized)
  anchors.sort()
  if len(anchors) < 2:
    raise(Exception('Less than 2 anchors found!'))

  mg = InterGraph(nodes=anchors, addloners=False, idTOstr=O.idTOstr,
                  compactnodes=False, seqfile=O.seqfile, idTOi=O.idTOi,
                  addends=True, gidset=O.gidset, idTOlen=O.idTOlen,
                  anchorgraph=True)
      #Remove cycles in the anchors:
  if not mg.isDAG():
    printerr('WARNING: the anchors form a cycle!')
    printerr('         Computing the Feedback vertex set on {} vertices.'.
             format(len(anchors)))
    fvs = getMinFeedbackVertexSet(mg.G)
    if not fvs:
      printerr('ERROR: feedback vertex set not found! (change k)')

    printerr('         Removed {} vertices.'.format(len(fvs)))

    newnodelist = [n for n in anchors if n not in fvs]
    mg = InterGraph(nodes=newnodelist, addloners=False, idTOstr=O.idTOstr,
                    compactnodes=False, seqfile=O.seqfile, idTOi=O.idTOi,
                    addends=True, gidset=O.gidset, idTOlen=O.idTOlen,
                    anchorgraph=True)


      #Compute the modules between the anchors:
  if O.verbose: printerr('Computing modules...')
  minlength = 0            #Min match length 15, but try larger if cycles.
  modules: List[List[Set[str]]] = []
  idTOintervals: Dict[str, List[Tuple[int,int]]] = defaultdict(list) #Map gid to intervals for the modules.
  absents = []
  retgraph = None
  nodealignlist = []
  for a, b in pairwise(anchors):
    if alignvertices:
      nodealignlist.append(a.getAlignment(O))

    distance = a.maxDistanceTo(b)
    if distance >= ignoresmaller:             #If there is space for a match.
      if O.verbose:
        printerr('A{}: {}'.format((a.idTOinter['A'][1]+1, b.idTOinter['A'][0]),
                                  distance))
      try:
        if refineanchors and distance > 100:  #Use anchors for smaller m.
          ag, _, _ = getGraphOnInterval(a, b, True, O, minlength, matchdir,
                                        includeends=True)

          for a, b in pairwise(sorted(ag.getLinearExtension())):
            distance = a.maxDistanceTo(b)
            if distance >= ignoresmaller:     #If there's space for a match.
              if O.verbose:
                printerr('*A{}: {}'.format((a.idTOinter['A'][1]+1,
                                            b.idTOinter['A'][0]),
                                           distance))

              posg = getModulesBetween(a, b, modules, absents, idTOintervals, O,
                                       minlength, matchdir,
                                       graphposition, nodealignlist,
                                       not vertexadir)
              if posg:
                retgraph = posg

        else:              #Use the original anchors.
          posg = getModulesBetween(a, b, modules, absents, idTOintervals, O,
                                   minlength, matchdir,
                                   graphposition, nodealignlist,
                                   not vertexadir)
          if posg:
            retgraph = posg

      except GraphOnIntervalError:
        if O.verbose: printerr('No vertices in interval.')
                           #Skip this pair since there are no matches.
      #gc.collect()        #Force garbage collection of the old graph.

      #Write or print the results:
  if alignvertices:
    nodealignlist.append(b.getAlignment(O))

    if vertexadir:
      writeAlignmentBlocks(vertexadir, nodealignlist)
    else:
      fullalignment = reduce(lambda a, b: a+b, nodealignlist)
      AlignIO.write(fullalignment, alignvertices, 'fasta')

  elif not anchorpoints:
    variantgap = '-'
    for gid in sorted(O.gidset):
      printnow('{}: '.format(gid), end='')
      for partition, absentset in zip(modules, absents):
        if gid in absentset:
          printnow(variantgap, end='')
        else:
          variant = next(v for v in partition if gid in v)  #Find the variant.
          printnow(sorted(variant)[0], end='')
      printnow()

    printnow(f'\nTotal anchor length = {sum(len(a) for a in anchors)}')
    printnow('\nIntervals:')
    for gid in sorted(O.gidset):
      printnow('{}: {}'.format(gid, ' '.join(map(str, idTOintervals[gid]))))

    if printalignments:
      printnow('\nAlignments:')
      for i, module in enumerate(modules):
        printnow('\nmodule {} _________'.format(i))
        empty = set(O.gidset)
        for j, variant in enumerate(module):
          empty -= variant
          variantl = sorted(variant)
          interval = idTOintervals[variantl[0]][i]
          printnow(f'\nmodule {i} variant {j} '
                   f'({interval[1] - interval[0]}):\n'
                   f'{" ".join(variantl)}')
          printSeqsForGIDs(variant, i, idTOintervals, O)

        if empty:
          printnow('\nmodule {} empty:\n{}'.format(i, ' '.join(sorted(empty))))
          printSeqsForGIDs(empty, i, idTOintervals, O)

  return retgraph

def getMinFeedbackVertexSet(graph, maxk=10):
  """
  Return a minimum feedback vertex set of this graph.
  """
  for k in range(maxk):
    fvs = fvs_via_ic(graph, k)   #Compute feedback vertex.
    if fvs:
      return fvs

  return set()


def printSeqsForGIDs(gids, i, idTOintervals, O):
  """
  Print the sequences corresponding to the ith intervals for the given gids.
  """
  for gid in sorted(gids):
    record = O.records[O.idTOi[gid]]

    start, end = idTOintervals[gid][i]
    if start < end:                   #The interval doesn't wrap.
      sequence = record.seq[start:end]
    else:                              #The interval wraps around
      sequence = record.seq[start:] + record.seq[:end]

    record = SeqRecord(sequence, id=record.description)
    record.description = '[{}-{}]'.format(start, end)
    printnow('{} ({}-{}): {}'.format(gid, start, end, record.seq))
    #printnow('\t{}: {}'.format(gid, record.seq))


def getModulesBetween(a, b, modules: List[List[Set[str]]], absents,
                      idTOintervals: Dict[str, List[Tuple[int,int]]],
                      O, minlength,
                      matchdir: str, position=None, alignvertices=[],
                      addgaps=True):
  """
  Compute modules and gaps (put in absent) between the given anchors.

  @param matchdir: temp dir to store matches in
  @param position: if there is a module that spans this position, then
                   return the graph for it
  @type  position: tuple of the form ('A', 2001)
  @param alignvertices: add the vertex information for vertices longer than
                        MIN_MODULESIZE.
  @param addgaps:       add gaps to vertex alignments for missing genomes
  """
  g, _, _ = getGraphOnInterval(a, b, False, O, minlength, matchdir,
                               includeends=True, compactnodes=False)
  module, absent = getModule(g.iterNodes(), O.gidset)

  if alignvertices:
    for node in g.iterNodes():
      if len(node) >= MIN_MODULESIZE and len(node.gids) > 1:
        alignvertices.append(node.getAlignment(O, addgaps))

  retgraph = None   #Return the graph if module corresponds to given position.
  if(position and position[0] in a.idTOinter and
     position[1] >= a.idTOinter[position[0]][0] and
     position[1] <= b.idTOinter[position[0]][1]):
    retgraph = g
    printnow('Showing graph for interval {}:{}-{}'.
             format(position[1], a.idTOinter[position[0]][0],
                    b.idTOinter[position[0]][1]))
  
  if module:
    modules.append(module)
    for gid in sorted(O.gidset):
      idTOintervals[gid].append((a.idTOinter[gid][0], b.idTOinter[gid][1]))
    absents.append(absent)

  return retgraph


def getModule(nodes, gidset):
  """
  Return a module if there exists an informative one in the given node set.

  @return: the module (a partition of gidset), and a set of gids that were not
           in any node, or (None, None)
  @rtype:  a list of sets
  """
  module, absent = partitionSetByBlocks(gidset, nodes)
  if (module and absent) or len(module) > 1:
    return module, absent

  return None, None



def partitionSetByBlocks(baseset: Set[str], nodes, minlen=MIN_MODULESIZE):
  """
  Each node defines a split of the gid set (between those ids in the node, and
  those not in the node). Partition the given set based on the given nodes.

  @param baseset: the set to partition
  @param nodes:   the nodes to partition with
  @param minlen:  ignore nodes that are shorter than this

  @return: The partition and a set of gids that were not in any node
  @rtype:  a list of sets, and a set
  """
  partition: List[Set[str]] = [baseset]
  absent: Set[str] = set(baseset)
  for n in nodes:
    if len(n) >= minlen:                    #Ignore small blocks.
      newpartition = []
      absent -= n.gids
      for s in partition:
        s1 = s & n.gids                     #Elements in the node.
        if not s1:
          newpartition.append(s)            #There's no intersection.
        else:
          newpartition.append(s1)
          if len(s1) != len(s):
            newpartition.append(s - n.gids) #Elements not in the node.

      partition = newpartition

    #Remove the last set if it is composed of gids that have no Node.
  if partition[-1] == absent:
    partition.pop()

  return partition, absent



#def refineAnchors(anchors, seqfile, idTOi, idTOlen, gidset, m=MIN_LENGTH):
#  """
#  Between each pair of anchors, recompute a new set of anchors with a smaller
#  match length.
#
#  @warn: Assumes the anchors are in the right order (sorted)!
#  """
#    #Recompute the equivalence classes on all adjacent anchors:
#  retclasses = []
#  #minlength = None       #Force adapted length for intervals between anchors.
#  for n1,n2 in pairwise(anchors):
#    intervallens = [t2[1]-t1[2] for t1,t2 in izip(n1.triples, n2.triples)]
#
#    if max(intervallens) > MIN_LENGTH:  #Recompute for this interval:
#      try:
#        newclasses,_,_,_ = recomputeClassesForNodes(n1,n2, m, seqfile, idTOi,
#                                                    idTOlen, gidset)
#      except GraphOnIntervalError as e:
#        printerr(e)
#    else:                #Fill in gap with old classes:
#      newclasses = [n.matchclass for n in nclasses[anchTOi[n1]+1:anchTOi[n2]]]
#
#    if n2 != anchors[-1]:
#      newclasses.append(n2.matchclass)
#    retclasses += newclasses
#
#  return retclasses



#} ________________________________________________________________________

#{ Debugging:

def getMatchesWithinStr(start, end, idTOmatchlists, gids=set()):
  """
  Print all the matches that start within the given interval.

  @param start: the start coordinate
  @param end:   the end coordinate
  @param idTOmatchlists: map gid to lists of matches
  @param gids:  only consider matches with both genomes having these gids
  """
  st = ''
  for gid, ml in idTOmatchlists.items():
    if (not gids) or gid in gids:
      withinrange = [m for m in ml if ((m.s1 > start and m.s1 < end) or
                                      (m.s2 > start and m.s2 < end))]
      withgids = withinrange
      if gids:
        withgids = [m for m in withinrange if m.gid1 in gids and m.gid2 in gids]
      st += '{}: '.format(gid)
      st += ','.join(map(str, sorted(withgids, key=lambda m: m[gid].s)))+'\n'
  return st


def getMatchesSpanningIndexStr(spotlist, idTOmatchlists, gids=set()):
  """
  Print all the matches that span a given index.

  @param spotlist: list of the spot of interest
  @param idTOmatchlists: map gid to lists of matches
  @param gids:  only consider matches with a genome from these gids
  """
  st = ' '.join(map(str, gids))+'\n'
  for gid in sorted(idTOmatchlists.keys()):
    ml = idTOmatchlists[gid]
    withgids = ml
    if gids:
      withgids = [m for m in ml if m.gid1 in gids or m.gid2 in gids]
    spanning = [m for m in withgids if spansSpots(m, spotlist)]
    st += '{}: '.format(gid)
    st += ','.join(map(str, sorted(spanning, key=lambda m: m[gid].s)))+'\n'
  return st

def spansSpots(m, spotlist):
  """
  Return True if the given match spans any of the spots in spotlist.
  """
  return any((m.s1 <= s and m.e1 >= s) or (m.s2 <= s and m.e2 >= s)
             for s in spotlist)


def checkMatchesAgainstIntervals(idTOmatchlists, idTOintervals):
  """
  A sanity check.  Make sure no match spans an interval and that every interval
  has a match.
  """
  for gid, matchlist in idTOmatchlists.items():
    for m in matchlist:
      othergid = m[gid].otherid
      if not any(m == n for n in idTOmatchlists[othergid]):
        sys.exit('Failed assertion: missing match {} in genome {}:\n{}'.\
                 format(m, othergid, idTOmatchlists[othergid]))

    #for (s,e) in idTOintervals[gid]:
    #  if not any(map(lambda m: m[gid].s == s and m[gid].e == e, matchlist)):
    #    sys.exit('Failed Assertion: missing interval {} in\n{}'.\
    #             format((s,e), matchlist))

    #for m in matchlist:
    #  bpset = set(chain(*idTOintervals[gid]))
    #  assert(m[gid].s in bpset and m[gid].e in bpset)



def checkEquivalenceClasses(eqclasses, idTOmatchlists):
  """
  Check consistency between the matches and the equivalence classes.

  @warn: quadratic in the number of equivalence classes.
  """
     #Every match is in a class:
  for matchlist in idTOmatchlists.values():
    for m in matchlist:
      assert(any(m in c for c in eqclasses))

     #There is no intersection between the intervals in the classes:
  for c1, c2 in combinations(eqclasses,2):
    s1 = set(chain(*(m.getTriples() for m in c1)))
    s2 = set(chain(*(m.getTriples() for m in c2)))
    if s1 & s2:
      print(id(s1), [str(id(c)) for c in c1])
      print(id(s2), [str(id(c)) for c in c2])
      sys.exit("Assertion failure: non-empty intersection in\n{}\nand\n{}.".\
               format(c1,c2))


def checkIntervalsForOverlaps(idTOintervals):
  """
  Check for overlapping intervals.

  @note: there will be many of these.

  @param idTOintervals: maps gid to a list of intervals.
  """
  for gid, ilist in idTOintervals.items():
    for (s1,e1),(s2,e2) in combinations(ilist,2):
      if((s1 > s2 and s1 < e2 and e1 > e2) or
         (s1 < s2 and e1 > s2 and e1 < e2)):
        printnow('Overlapping intervals {} and {} in genome {}.'.\
                 format((s1,e1),(s2,e2),gid), stream=sys.stderr)


def checkMatchesForOverlaps(idTOmatchlists):
  """
  Check for overlapping intervals between the same pairs of genomes.

  @param idTOmatchlists: maps gid to a list of matches.
  """
  maxoverlap = 0
  for gid,mlist in idTOmatchlists.items():
    mlist.sort(key=lambda m: m[gid].s)
    for i in range(len(mlist)):
      j = i
      while(j < len(mlist) and mlist[j][gid].s < mlist[i][gid].e):
        if(mlist[i][gid].otherid == mlist[j][gid].otherid and
           mlist[j][gid].e > mlist[i][gid].e):
          overlap = mlist[i][gid].e - mlist[j][gid].s
          printnow('WARNING: matches overlap by {}:\n {}\n {}'.\
                   format(overlap,mlist[i],mlist[j]), stream=sys.stderr)
          maxoverlap = max(maxoverlap, overlap)
        j += 1

  return maxoverlap

def checkMatchesForOverlapsSLOW(idTOmatchlists):
  """
  Check for overlapping intervals between the same pairs of genomes.

  @param idTOmatchlists: maps gid to a list of matches.
  """
  maxoverlap = 0
  for mlist in idTOmatchlists.values():
    for m1,m2 in combinations(mlist,2):
      if m1.gid1 in m2 and m1.gid2 in m2:
        o1 = overlaps(m1.s1,m1.e1, m2[m1.gid1].s, m2[m1.gid1].e)
        o2 = overlaps(m1.s2,m1.e2, m2[m1.gid2].s, m2[m1.gid2].e)
        if o1 or o2:
          printnow('WARNING: matches overlap by {}:\n {}\n {}'.\
                   format(max(o1,o2),m1,m2), stream=sys.stderr)
          maxoverlap = max(maxoverlap, o1, o2)

  return maxoverlap



#} ____________________________________________________________________________



def findLongestUncutFullMatch(idTObreakpoints, idTOmatchlists):
  """
  Given all the breakpoints of every genome, return a dictionary mapping
  gid interval, in the coordinates for that genome.  All of these intervals
  match to the same interval of one of the genomes.  Return the longest
  uncut (by a match) interval.
 
  @param idTObreakpoints: list of all breakpoint bountdaries for each genome
  @type  idTObreakpoints: maps gid to a list of indices (integers)
  @param idTOmatchlists: list of all Match objects for each genome
  @type  idTOmatchlists: maps gid to a list of Matches
  """
    #Associate with pairs of breakpoints the matches that span them:
  idTOintTOmatches = defaultdict(lambda: defaultdict(list)) #Interval to match.
  for gid, matchlist in idTOmatchlists.items():
    for m in matchlist:
      (id1, s1, e1), (id2, s2, e2) = m.getTriples()
      s1i = idTObreakpoints[id1].index(s1)
      e1i = idTObreakpoints[id1].index(e1)
      for p in pairwise(idTObreakpoints[id1][s1i:e1i+1]):
        idTOintTOmatches[id1][p].append(m)

      s2i = idTObreakpoints[id2].index(s2)
      e2i = idTObreakpoints[id2].index(e2)
      for p in pairwise(idTObreakpoints[id2][s2i:e2i+1]):
        idTOintTOmatches[id2][p].append(m)

    #Find the longest full (i.e. matches to every gid at least once) match:
  gids = list(idTObreakpoints.keys())
  longestfull = (0,(0,0),None)
  for gid, bplist in idTObreakpoints.items():
    for interval in pairwise(bplist):
      if fullMatch(idTOintTOmatches[gid][interval], gids):
        intlen = interval[1]-interval[0]
        if intlen > longestfull[1][1]-longestfull[1][0]:
          longestfull = (gid, interval, idTOintTOmatches[gid][interval])

  return getIntervalsInCommon(*longestfull)




def fullMatch(matchlist, gids):
  """
  Return True if for each gid there is a match.

  @param matchlist: list of Match objects
  @param gids:      list of genome ids
  """
  present = set()
  for (id1,_,_),(id2,_,_) in map(Match.getTriples, matchlist):
    present.add(id1)
    present.add(id2)
  return present == set(gids)


def getIntervalsInCommon(refid, refint, matchlist):
  """
  Given a reference gid and interval, return a dictionary mapping any in a  
  match, to the corresponding matched interval in that genome
  (in gapless genome coordinates).
  """
  if not matchlist:
    return None

  intervals = {refid: refint}
  for m in matchlist:
    oid = m[refid].otherid

    startapos = m.nucPosToAlignPos(refid, refint[0])
    endapos = m.nucPosToAlignPos(refid, refint[1])
    ostart = m.alignPosToNucPos(oid, startapos)
    oend = m.alignPosToNucPos(oid, endapos)

    if oid in intervals:
      assert(intervals[oid] == (ostart, oend)) #Not true if the same spot in
                                               #refid maps to multiple in oid.
    intervals[oid] = (ostart, oend)

  return intervals


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class GTError(Exception):
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return self.value
