#!/usr/bin/python
# Krister Swenson                                                Spring 2015
#
"""
Functions for working with genome sequences.
"""
import os
import sys
import re

from Bio import SeqIO,Seq
from typing import Set



### Constants:



def sliceFasta(infile, outfile, accTOinterval):
  """
  Take a slice of all the sequences in the given fasta file and write it to
  a new outfile.

  Only include the genomes that are keys in accTOinterval.  If the values of
  accTOinterval are interval pairs, then slice these pairs.

  @param infile:        the input fasta file
  @param outfile:       the output fasta file
  @param accTOinterval: map accession number (or anything in the description)
                        to the interval to keep.  The None interval implies that
                        the entire string is kept.
  """
  writerecs = []
  headerre = re.compile(r'(.+)\|:\d+-\d+(.*)')
  for r in SeqIO.parse(infile, 'fasta'):
    found = None
    for acc in accTOinterval:
      if re.search(acc, r.description):
        found = acc
        break

    if found:
      if accTOinterval[acc]:    #If there is an interval.
        start,end = accTOinterval[acc]
        r = r[start:end]

        #idstr, descstr = r.description.rsplit('|',1)
        m = headerre.match(r.description)
        if m:
          r.id = '{}|:{}-{}'.format(m.group(1),start,end)
          r.description = m.group(2)

      writerecs.append(r)

  print(writerecs)
  SeqIO.write(writerecs, outfile, "fasta")



def sliceFastaFile(infile, outfile, coordinates=None, whitelist=[],
                   blacklist=[], cleanfile=False, includelines=None):
  """
  Take a slice of all the sequences in the given fasta file and write it to
  a new outfile.

  @note: If coordinate pair is None for a given index, then the complete
         sequences are used.

  @note: None can be used to denote the begining or end of the sequence.  For
         example, (4,None) will take index 4 to the end of the sequence.

  @note: If the first coordinate is greater than the second, then slice the
         rotated sequence (i.e. concatinate the beginning to the end).

  @param infile:      the input fasta file
  @param outfile:     the output fasta file
  @param coordinates: (start, end) tuple or list of such pairs. If None, 
                      then include the entire genomes.  If one of the
                      two is None, then go to the end of the sequence.
                      If a list, any missing coordinates (the length of
                      coordinates is not equal to the number of genomes 
                      considered) will default to the empty genome.
                      If start is after end, then include the begining and
                      end (as though the sequence wraps).
  @param whitelist:   keep these genomes
  @param blacklist:   remove these genomes
  @param cleanfile:   remove genomes that have nothing but gap characters,
                      and replace digits with gap characters.
  @param includelines:only include genomes from these lines (e.g. [2,4] means
                      to include the second and fourth genomes in the file).
  """
  writerecs = []
  for i, r in enumerate(SeqIO.parse(infile, 'fasta')):
    if((not includelines or
        i in includelines) and
       (not whitelist or
        any(re.search(w, r.description) for w in whitelist)) and
       (not blacklist or
        not any(re.search(w, r.description) for w in blacklist)) and
       not undetermined(r)):

          #Set the coordinates if they're specified:
      if not coordinates:
        start, end = 0, None

      elif type(coordinates) == list:
        if i >= len(coordinates):
          start = end = None
        elif coordinates[i]:
          start, end = coordinates[i]
        else:
          start, end = 0, None

      else:
        start, end = coordinates

          #Slice the sequence:
      newr = r
      if start == None and end == None:
        newr.seq = Seq.Seq('')
      else:
        if start == None:
          newr = r[:end]
          start = 1
        elif end == None:
          newr = r[start:]
          end = len(r)
        elif start <= end:
          newr = r[start:end]
        else:
          newr = r[start:]+r[:end]

          #Construct the new id line:
      assert end != None and start != None
      if end - start < len(r.seq):
        if '|' in newr.description:
          idstr, descstr = newr.description.rsplit('|',1)
        else:
          idstr, descstr = newr.description, ''
        newr.id = '{}|:{}-{}'.format(idstr, start, end)
        newr.description = descstr

      removeDigits(newr)

      writerecs.append(newr)

  SeqIO.write(writerecs, outfile, "fasta")


def removeDigits(record):
  """
  Replace digits with gaps in the given sequence.
  """
  newseq = Seq.Seq('')
  for char in record.seq:
    if char.isdigit():
      newseq += '-'
    else:
      newseq += char

  record.seq = newseq


def undetermined(record):
  """
  Return True if the sequence has only undetermined characters (e.g. x,-,=).
  """
  udchars = set(['-', '=', 'x'])
  for char in record.seq:
    if char not in udchars:
      return False

  return True


def printRecordDescriptors(fastafile):
  """
  Print to stdout the descriptors in the given fasta file.
  """
  print('\n'.join('{}: {}'.format(i, r.description)
                  for i,r in enumerate(SeqIO.parse(fastafile, 'fasta'))))
  

def getGenomeLengths(seqfile, idTOi, gidset: Set=None):
  """
  Return the genome lengths from the given file in the order that the genomes
  appear.

  :param gidset: Restrict the mapping to these gids.

  @return: map from gid to genome length
  """
  retlist = []
  for r in SeqIO.parse(seqfile, 'fasta'):
    retlist.append(len(r))
  return {gid: retlist[idTOi[gid]] for gid in idTOi.keys()
          if not gidset or gid in gidset}

