# Krister Swenson                                                Fall 2014
# Nicolas Pompidor                                               Summer 2015
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Classes for building a graph corresponding to a set of matches.
"""
import sys
import os
import copy, re
import networkx as nx
import math
import traceback
import numpy as np
#import matplotlib.pyplot as plt
from operator import itemgetter
from typing import Set
from typing import FrozenSet
from typing import Iterable
from typing import Tuple
from typing import List
from typing import Callable

from collections import defaultdict as defdict
from itertools import combinations, chain

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment

from pygraphviz import AGraph

from .Annotations import Annotations
from .pyinclude.usefulfuncs import pairwise
from .pyinclude.usefulfuncs import printerr
from .pyinclude.usefulfuncs import invdict
from .pyinclude.genseqfuncs import getGenomeLengths
from .pyinclude.ncbi import accFromDescription
from .pyinclude.ncbi import nameFromDescription


from .shapes import DNode
from .shapes import Pen 
from .shapes import AnnotShape 
from .shapes import TextShape 



MINDISPLAY_SIZE = 20  #The minimize size for a node to be displayed.

GAPCHAR = '-'
NAMENODELEFT = '-1'
NAMENODERIGHT = '-2'


#        _________________________
#_______/        Classes          \_____________________________________________


class Match(object):
  """
  A match from a BLAST local search or a gt/vmatch output.

  Each match has two sides, each with gid, start, and end.
  The fastest way to access data from the match is to explicity use the side
  (e.g. self.sd1.s is the start of the interval for the first side).
  The convenient (but slower) way to write the same thing is "self.s1".

  The side can be referenced by gid in the following way: "self[gid].s".
  In summary, "self.sd1.s == self.s1 == self[self.gid1].s".

  @ivar sd1:   the first side of the match
  @type sd1:   MatchSide
  @ivar gid1:  the first genome id
  @type gid1:  string
  @ivar s1:    the start of the first interval
  @type s1:    integer
  @ivar e1:    the end of the first interval
  @type e1:    integer
  @ivar triple1: (gid1, s1, e1)
  @ivar sd2:   the second side of the match
  @ivar gid2:  the second genome id
  @ivar s2:    the start of the second interval
  @ivar e2:    the end of the second interval
  @ivar triple2: (gid2, s2, e2)

  @ivar hit:   the hit object from the BLAST search U{http://biopython.org/DIST/docs/api/Bio.Blast.Record.HSP-class.html}

  The hit has the following form::

    score           BLAST score of hit.  (float)
    bits            Number of bits for that score.  (float)
    expect          Expect value.  (float)
    num_alignments  Number of alignments for same subject.  (int)
    identities      Number of identities (int) if using the XML parser.
                    Tuple of numer of identities/total aligned (int, int)
                    if using the (obsolete) plain text parser.
    positives       Number of positives (int) if using the XML parser.
                    Tuple of numer of positives/total aligned (int, int)
                    if using the (obsolete) plain text parser.
    gaps            Number of gaps (int) if using the XML parser.
                    Tuple of numer of gaps/total aligned (int, int) if
                    using the (obsolete) plain text parser.
    align_length    Length of the alignment. (int)
    strand          Tuple of (query, target) strand.
    frame           Tuple of 1 or 2 frame shifts, depending on the flavor.
    
    query           The query sequence.
    query_start     The start residue for the query sequence.  (1-based)
    query_end       The end residue for the query sequence.  (1-based)
    match           The match sequence.
    sbjct           The sbjct sequence.
    sbjct_start     The start residue for the sbjct sequence.  (1-based)
    sbjct_end       The end residue for the sbjct sequence.  (1-based)
  """
  #Match
  def __init__(self, qid, sid, hit=None, match=None, triples=None):
    """
    Build a Match from the given information.
    Either hit or match (gt/vmatch) or a pair of triples must be specified.

    @param qid:     query id
    @param sid:     subject id
    @param hit:     a blast hit object with the format specified above
    @param match:   a tuple (qstart,qend,sstart,send) for a match from gt/vmatch
    @type match:    a 4-tuple
    @param triples: a pair of triples (qid,qstart,qend) for a match
    @type triples:  pair of triples
    """
    self.sd1 = None
    self.sd2 = None
    if hit:
      self.sd1 = MatchSide(qid, hit.query_start, hit.query_end, hit.query, sid)
      self.sd2 = MatchSide(sid, hit.sbjct_start, hit.sbjct_end, hit.sbjct, qid)
    elif match:
      (qstart,qend, sstart,send) = match
      self.sd1 = MatchSide(qid, qstart, qend, None, sid)
      self.sd2 = MatchSide(sid, sstart, send, None, qid)
    elif triples:
      (nqid,qstart,qend),(nsid,sstart,send) = triples
      if nqid != qid or nsid != sid:
        traceback.print_stack()
        sys.exit("ERROR: triple and instance gids don't match in Match():\n"+
                 "{} == {} and {} == {}".format(nqid,qid, nsid,sid))
      assert(qend-qstart == send-sstart)
      self.sd1 = MatchSide(qid, qstart, qend, None, sid)
      self.sd2 = MatchSide(sid, sstart, send, None, qid)
    else:
      traceback.print_stack()
      sys.exit("Bad instatiation of Match object: hit, match, or triples"+
               " must be given.")

    self.sd1.other, self.sd2.other = self.sd2, self.sd1
    self._mrecord = {self.sd1.gid: self.sd1, self.sd2.gid: self.sd2}
    self.hit = hit



  def __len__(self):
    return self.sd1.e-self.sd1.s

  def __getitem__(self, gid:str):
    return self._mrecord[gid]

  def __contains__(self, gid):
    return gid in self._mrecord

  def adjustIndices(self, offset1, offset2, genlen1, genlen2):
    """
    Add the given offsets to the start and end indices.
    If the result is an index greater then the genome length, then wrap the
    index to the beginning.

    @param offset1: the offset to use for genome 1
    @param offset2: the offset to use for genome 2
    @param genlen1: the length of genome 1
    @param genlen2: the length of genome 2
    """
    self.sd1.s, self.sd1.e = self.sd1.s+offset1, self.sd1.e+offset1
    if self.sd1.s > genlen1:
      self.sd1.s -= genlen1
    if self.sd1.e > genlen1:
      self.sd1.e -= genlen1


    self.sd2.s, self.sd2.e = self.sd2.s+offset2, self.sd2.e+offset2
    if self.sd2.s > genlen2:
      self.sd2.s -= genlen2
    if self.sd2.e > genlen2:
      self.sd2.e -= genlen2



  def getTriples(self):
    """
    Return the pair of triples corresponding to this match.

    @return: (gid1, s1, e1), (gid2, s2, e2)
    """
    return self.sd1.triple(), self.sd2.triple()

  def getTriplesRev(self):
    """
    Return the pair of triples corresponding to this match in reverse order.

    @return: (gid2, s2, e2), (gid1, s1, e1)
    """
    return self.triple2, self.triple1


  def cut(self, gid, position):
    """
    Cut this match into two at the given position.  The given position is the
    start of the new match on the right side.

    @warning: only works with gt/vmatch Matches (this should be a subclass)
              since it doesn't cut the sequences (seq1 or seq1) or the hit.

    @return: two new matches resulting from cutting at the given position.
    @rtype:  (Match, Match)
    """
    assert(position >= self[gid].s and position <= self[gid].e)
    #assert(self.sd1.e-self.sd1.s == self.sd2.e-self.sd2.s)

    diff = position-self[gid].s

    gid1 = gid
    s1 = self[gid].s
    e1 = position
    gid2 = self[gid].otherid
    s2 = self[gid].other.s
    e2 = self[gid].other.s+diff
    if gid == self.sd1.gid:
      lm = Match(gid1, gid2, match=(s1,e1,s2,e2))
    else:
      lm = Match(gid2, gid1, match=(s2,e2,s1,e1))

    s1 = position
    e1 = self[gid].e
    gid2 = self[gid].otherid
    s2 = self[gid].other.s+diff
    e2 = self[gid].other.e
    if gid == self.sd1.gid:
      rm = Match(gid1, gid2, match=(s1,e1,s2,e2))
    else:
      rm = Match(gid2, gid1, match=(s2,e2,s1,e1))

    #assert(lm.e1-lm.s1 == lm.e2-lm.s2)
    #assert(rm.e1-rm.s1 == rm.e2-rm.s2)

    return lm,rm


  def cutCOPY(self, gid, position):
    """
    Cut this match into two at the given position.  The given position is the
    start of the new right-side match.

    OLD VERSION that does a lot of copying.

    @warning: only works with gt/vmatch Matches (this should be a subclass)
              since it doesn't cut the sequences (seq1 or seq1) or the hit.

    @return: two new matches resulting from cutting at the given position.
    @rtype:  (Match, Match)
    """
    assert(position >= self[gid].s and position <= self[gid].e)

    lm = copy.deepcopy(self)
    rm = copy.deepcopy(self)

    diff = position-self[gid].s
    lm[gid].e = position
    lm[gid].other.e = lm[gid].other.s+diff

    rm[gid].s = position
    rm[gid].other.s = rm[gid].other.s+diff

    assert(lm.e1-lm.s1 == lm.e2-lm.s2)
    assert(rm.e1-rm.s1 == rm.e2-rm.s2)

    return lm,rm


  def getOtherPos(self, gid, i):
    """ Return the equivalent position in the other genome. """
    assert(i in self[gid])
    return self[gid].other.s + i-self[gid].s


  def nucPosToAlignPos(self, gid, position):
    """
    Return the alignment position (1-indexed), given a nucleotide position
    (1-indexed) within the interval of one of sides of the match.

    @note: clearly this returns the same position if this match has no gaps.

    @param gid:      genome id of one of the halves of the match
    @param position: the nucleotide position 
    """
    npos = position-self[gid].s
    i=1
    for n in self[gid].seq:
      if not npos:
        break
      if n != GAPCHAR:
        npos -= 1
      i += 1

    if npos:
      printerr('WARNING: index given to nucPosToAlignPos too great.')

    return i


  def alignPosToNucPos(self, gid, apos):
    """
    Return the nucleotide position (1-indexed), given an alignment position
    (1-indexed) within the interval of one of the sides of the match.

    @note: clearly this returns the same position if this match has no gaps.

    @param gid:  genome id of one of the halves of the match
    @param apos: the nucleotide position 
    """
    if apos > len(self[gid].seq):
      printerr('WARNING: index given to alignPosToNucPos too great.')

    gaps = self[gid].seq[:apos].count(GAPCHAR)
    return self[gid].s+apos-gaps-1

  def empty(self):
    return (self.sd1.e - self.sd1.s == 0)


  def __hash__(self):
    return hash(str(self))

  def __eq__(self, other):
    return str(self) == str(other)

  def __str__(self):
    return str(self.getTriples())

  def __repr__(self):
    return str(self)

  #Getters and Setters:
  @property
  def triple1(self):
    return self.sd1.triple()

  @property
  def gid1(self):
    return self.sd1.gid
  @gid1.setter
  def gid1(self, val):
    self.sd1.gid = val

  @property
  def s1(self):
    return self.sd1.s
  @s1.setter
  def s1(self, val):
    self.sd1.s = val

  @property
  def e1(self):
    return self.sd1.e
  @e1.setter
  def e1(self, val):
    self.sd1.e = val

  @property
  def seq1(self):
    return self.sd1.seq
  @seq1.setter
  def seq1(self, val):
    self.sd1.seq = val

  @property
  def otherid1(self):
    return self.sd1.otherid
  @otherid1.setter
  def otherid1(self, val):
    self.sd1.otherid = val

  @property
  def other1(self):
    return self.sd1.other
  @other1.setter
  def other1(self, val):
    self.sd1.other = val

  @property
  def triple2(self):
    return self.sd2.triple()

  @property
  def gid2(self):
    return self.sd2.gid
  @gid2.setter
  def gid2(self, val):
    self.sd2.gid = val

  @property
  def s2(self):
    return self.sd2.s
  @s2.setter
  def s2(self, val):
    self.sd2.s = val

  @property
  def e2(self):
    return self.sd2.e
  @e2.setter
  def e2(self, val):
    self.sd2.e = val

  @property
  def seq2(self):
    return self.sd2.seq
  @seq2.setter
  def seq2(self, val):
    self.sd2.seq = val

  @property
  def otherid2(self):
    return self.sd2.otherid
  @otherid2.setter
  def otherid2(self, val):
    self.sd2.otherid = val

  @property
  def other2(self):
    return self.sd2.other
  @other2.setter
  def other2(self, val):
    self.sd2.other = val


class MatchSide(object):
  """
  One side of the two sided match.

  @ivar gid:     the genome id
  @type gid:     string
  @ivar s:       the start index
  @ivar e:       the end index
  @ivar seq:     the sequence
  @ivar otherid: the gid of the other side of a match
  @ivar other:   the other MatchSide (must be set after)
  """
  #MatchSide
  def __init__(self, gid, s, e, seq, otherid):
    """
    Initialize a match side.

    @warn: self.other must be linked after by the initializer.
    """
    self.gid = gid
    self.s = s
    self.e = e
    self.seq = seq
    self.otherid = otherid
    self.other = None          #This must be set after by the initializer.

  def triple(self):
    return (self.gid, self.s, self.e)

  def getOtherPos(self, i):
    """ Return the equivalent position in the other genome. """
    assert(i in self)
    return self.other.s + i-self.e

  def __contains__(self, i):
    return i >= self.s and i < self.e




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MatchGraph(object):
  """
  A node represents a non-empty set of matching substrings from a set of
  genomes.  An edge represents adjacent substrings in the genomes.

  :ivar G:      the graph
  :type G:      networkx DiGraph
  :ivar blocks: list of tuples (A,B,bl) where a and b are endpoint nodes of a
                block and bl is the set of nodes in the block (initialized
                by L{categorizeBlocks()}).

  :ivar iscontracted:   True if this graph has been contracted
  :ivar idTOstr:        map gid to string describing that genome
  :type idTOstr:        dictionary
  :ivar idTOlen:        map genome id to genome length
  :type idTOlen:        dictionary
  :ivar nodeTOsubgraph: map a contracted MNode to the subgraph it corresponds to
  :ivar gids:           the gids for this graph

  :ivar _codecounter:   an integer that hasn't been assigned to a node yet
  :ivar _edgecolors:    list of colors to use for the edges
  :ivar _seqfile:       the fasta file this graph is based on
  :ivar _iTOid:         map line in _seqfile to genome id
  :ivar _idTOi:         map genome id to line in _seqfile
  :ivar mindisplaysize: show any node shorter than this in a compacted form

  :ivar _norm:          the NormIndFuncs object for normalized indices
  """


#{ Graph initialization:

  #MatchGraph
  def __init__(self, eqclasses: Iterable[FrozenSet[Match]]=None,
               graph: 'MatchGraph'=None, nodes: List['MNode']=None,
               addloners=True, idTOstr=None, addends=False,
               compactnodes=True, seqfile=None, idTOi=None, iTOid=None,
               gidset=None, idTOlen=None,
               annotations: Annotations='funny', normfuncs: 'NormIndFuncs'=None,
               **kwargs):
    """
    Build a MatchGraph.  It is initialized from one of the three:
      - from a set of non-overlapping matches partitioned into
        equivalence classes.  Each equivalence class is a node along with each
        interval in a genome that isn't matched to anything.
      - from a networkx graph of MNodes
      - from a list of MNodes

    :note: the graph attribute G.graph['codeTOnode'] is kept so that we can
           recover the MNode from the "URL" in the node given by graphviz "dot".

    :param eqclasses:    a set of frozensets holding matches
    :param graph:        the networkx graph to create this one from
    :type  graph:        networkx DiGraph
    :param nodes:        create this graph from the given nodes
    :type  nodes:        list of MNodes
    :param addloners:    replace substring without a match covering them with
                         a new node if True
    :param idTOstr:      map genome id to string
    :param gidset:       the set of gids this graph is on
    :param addends:      add nodes to the ends representing the unmatched parts 
    :type  addends:      False, or a map from gid to genome length
    :param compactnodes: Compact nodes with few nucleotides
                         (see mindisplaysize)
    :type  compactnodes: boolean
    :param seqfile:      fasta file this graph is based on
                         (must be given if contracted nodes are to be labeled
                         with alignment length)
    :type  seqfile:      fasta text file. one of idTOi or iTOid must also be
                         specified
    :param idTOi:        map genome id to line in seqfile
    :param annotations:  show these annotations on the MNodes
    :param normfuncs:    if normfuncs.normalizing() is True, then normalize the
                         graph according to the functions in normfuncs.
    """
    super().__init__(**kwargs)

    self._norm: NormIndFuncs = normfuncs

    self.blocks = []
    self.iscontracted = False
    self.nodeTOsubgraph = {}  
    self.idTOstr = idTOstr
    self.annotations = annotations
    if idTOstr:
      self.gids = frozenset(self.idTOstr)
    elif gidset:
      self.gids = gidset
    if iTOid:
      self._iTOid = iTOid
      self._idTOi = invdict(iTOid)
    else:
      self._idTOi = idTOi
      self._iTOid = invdict(idTOi)
    self._seqfile = seqfile
    self._alphafile = ''
    if seqfile:
      if not self._iTOid:
        sys.exit('ERROR: must give idTOi or iTOid if seqfile is given.')
      self.idTOlen = getGenomeLengths(seqfile, self._idTOi, gidset)
    if idTOlen:
      self.idTOlen = idTOlen

    self.mindisplaysize = 0
    if compactnodes:
      self.mindisplaysize = MINDISPLAY_SIZE

    self._edgecolors = ['darkgreen', 'red', 'gold', 'darkviolet', 'deeppink',
                        'blue', 'dimgray', 'limegreen', 'orange', 'brown',
                        'cadetblue', 'powderblue', 'orchid', 'gray', 'crimson',
                        'deepskyblue', 'olivedrab', 'turquoise',
                        'darkslateblue', 'tan', 'mediumvioletred',
                        'lightsalmon', 'darkgoldenrod', 'chartreuse',
                        'slateblue', 'pink', 'darksalmon', 'springgreen',
                        'dodgerblue', 'lightgray', 'antiquewhite', 'royalblue',
                        'cyan', 'black']

    if graph:
      self.G: nx.DiGraph = graph
      for n in self.G:
        self.G.graph['codeTOnode'][n.code] = n
      self._codecounter = max((n.code for n in graph))+1
      return

    elif nodes:
      self.G = nx.DiGraph()
      self.G.graph['codeTOnode'] = {}
      self.G.add_nodes_from(n for n in nodes if not addends or not n.isDummy())
      newnodes = nodes
      for n in nodes:
        n.setNormFuncs(self._norm)

    elif eqclasses:
      self.G = nx.DiGraph()
      self.G.graph['codeTOnode'] = {}
      newnodes = [MNode(c, code=i, annotations=self.annotations,
                        norm=self._norm)
                  for i,c in enumerate(eqclasses)]
      self.G.add_nodes_from(newnodes)

    else:
      traceback.print_stack()
      sys.exit('ERROR: initialize MatchGraph with graph or eqclasses or nodes.')

    if self._norm and self._norm.normalizing():  #Normalize the indices.
      for n in self.G:
        n.normalize()

    if not len(self.G):
      sys.exit('There are no nodes in this graph.')

    self._codecounter = max((n.code for n in self.G))+1

    for n in self.G:                    #Assign the node codes:
      self.G.graph['codeTOnode'][n.code] = n
      self.G.nodes[n]['URL'] = n.code

    if addends and self.isDAG():       #Add the start and end nodes.
      self.addDummyNodes()

    self._addEdges(addloners)           #Add the edges.


   
  def _addEdges(self, addloners=False):
    """
    Add the edges implied by the triples.

    @note: this should take linear time in the number of matches.
    @note: the global function I is used so that if the indices need to be
           normalized (rotated when there is a cycle) then they are.

    @param addloners: replace substring without a match covering them with
                      a new node if True
    """
              #Map gid to sorted list of all nodes containing it:
    idTOnodeandind = defdict(set)
    for node in self.G:                    #Build sets.
      for (gid,s,e) in node.triples:
        idTOnodeandind[gid].add((node,(s,e)))

    idTOnodelist = {}                      #Sort them.
    for gid,nodestartlist in idTOnodeandind.items():
      idTOnodelist[gid] = [s[0] for s in sorted(nodestartlist,
                                                key=lambda ns: ns[1])]

    for gid,nodelist in idTOnodelist.items():
      for n,m in pairwise(nodelist):
        self._addEdgeForAdjacent(gid, n,m, addloners)



  def _addEdgeForAdjacent(self, gid, n,m, addloners=True):
    """
    Given two nodes that should have an edge between them, add the edge if
    it doesn't exist.  Add nodes if there are missing loner nodes (that
    correspond to unmatched regions) and addloners is True.

    @param gid:       add the edge for this gid
    @param m:         source node
    @param n:         destination node
    @param addloners: if True, then add nodes corresponding to unmatched
                      regions.
    """
    nend = n[gid][1]
    mstart = m[gid][0]
    dist = mstart-nend


    if addloners and dist >= 1:       #No match for substring so make new node.
      if self.normalizing():
        triple = [(gid, self._norm.Ii(gid, nend), self._norm.Ii(gid, mstart))]
      else:
        triple = [(gid, nend, mstart)]
      new = MNode(triples=triple, code=self._codecounter,
                  annotations=self.annotations, norm=self._norm) 
      if self.normalizing():
        new.normalize()

      self.G.graph['codeTOnode'][new.code] = new
      self._codecounter += 1
      self.G.add_node(new)
      self.G.nodes[new]['URL'] = new.code
      self.G.add_edge(n, new, gnms=set([(gid,0)]), DEdges=[])
      self.G.add_edge(new, m, gnms=set([(gid,0)]), DEdges=[])

    elif m not in self.G[n]:          #If we haven't seen the edge yet.
      self.G.add_edge(n,m, gnms=set([(gid,dist)]), DEdges=[])
    else:
      self.G[n][m]['gnms'].add((gid,dist))




  def _addEdgesSLOW(self, idTOmatchlists):
    """
    Add the edges implied by the triples.
    Do this by checking all pairs of nodes.
    """
              #Map triple to index in ordered list of matches for each genome:
    tripleTOindex = {}
    for gid, ml in idTOmatchlists.items():
      for i,t in enumerate(sorted(set([m[gid].triple() for m in ml]))):
        tripleTOindex[t] = i

              #Check all pairs of vertices to see if they need an edge:
    for n1,n2 in combinations(self.G.nodes(), 2):
      for t1 in n1.triples:
        for t2 in n2.triples:
          if t1[0] == t2[0]:  #If they are from the same genome.
            gid = t1[0]
            i1 = tripleTOindex[t1]
            i2 = tripleTOindex[t2]
            if i2-i1 == 1:
              if n2 in self.G[n1]:
                self.G[n1][n2]['gnms'].add((gid,t2[1]-t1[2]))
              else:
                self.G.add_edge(n1,n2, gnms=set([(gid,t2[1]-t1[2])]), DEdges=[])
            elif i2-i1 == -1:
              if n1 in self.G[n2]:
                self.G[n2][n1]['gnms'].add((gid,t1[1]-t2[2]))
              else:
                self.G.add_edge(n2,n1, gnms=set([(gid,t1[1]-t2[2])]), DEdges=[])

    self._replaceSingleGenomeEdges(self.G)


  def _replaceSingleGenomeEdges(self, dg):
    """
    Find all non-zero length edges that correspond to a single genome and
    replace them by a node.
    """
              #Replace non-zero-length edges with node labeled by single gid:
    toreplace = []    #Get the edges to replace with a vertex...
    for v in dg:
      for nbr,eattrs in dg.adj[v]:
        length = next(iter(eattrs['gnms']))[1]
        if length:
          toreplace.append((v,nbr))
  
                      #Now replace an edge with one or more vertices...
    for (n,m) in toreplace:
      gids = list(map(itemgetter(0), dg[n][m]['gnms']))
      dg.remove_edge(n,m)
      for gid in gids:
        new = MNode(triples=[(gid, n[gid][1], m[gid][0])],
                    code=self._codecounter, annotations=self.annotations,
                    norm=self._norm)
        self._codeTOnode()[new.code] = new
        self._codecounter += 1
        dg.add_node(new)
        dg.nodes[new]['URL'] = new.code
        dg.add_edge(n, new, gnms=set([(gid,0)]), DEdges=[])
        dg.add_edge(new, m, gnms=set([(gid,0)]), DEdges=[])



  def _fixOvergrownNode(self, node):
    """
    Replace an overgrown node with a bunch that have the same parents and
    children.  The matches are organized into the new nodes by length.
    An overgrown node is one that has multiple sequences per genome.
    These are caused by repetitive motifs occurring in different multiplicity
    among the genomes.

    @param node: the overgrown node
    @type  node: MNode
    """
    idTOinterval = {}             #Find highest and lowest indices per gid:
    for (gid,s,e) in node.triples:
      if gid in idTOinterval:
        idTOinterval[gid][0] = min(idTOinterval[gid][0], s)
        idTOinterval[gid][1] = max(idTOinterval[gid][1], e)
      else:
        idTOinterval[gid] = [s,e]

    lenTOgid = defdict(list)      #Organize genomes by length:
    for gid,(s,e) in idTOinterval.items():
      lenTOgid[e-s].append(gid)

    newnodes = []                 #Build the equivalence classes on length:
    for gidl in lenTOgid.values():
      matchclass = set()
      for id1,id2 in combinations(gidl, 2):
        matchclass.add(Match(id1,id2,
                             match=idTOinterval[id1]+idTOinterval[id2]))
      if matchclass:
        new = MNode(matchclass, code=self._codecounter,
                    annotations=self.annotations, norm=self._norm)
      else:
        new = MNode(triples=[(gidl[0],idTOinterval[gidl[0]][0],
                              idTOinterval[gidl[0]][1])],
                    code=self._codecounter, annotations=self.annotations,
                    norm=self._norm)
      newnodes.append(new)
      self._codeTOnode()[new.code] = new
      self._codecounter += 1

    edgestoremove = []
    for n in self.G.predecessors(node):
      edgestoremove.append((n,node))
      for new in newnodes:
        gidintersect = n.gids & new.gids
        if gidintersect:
          self.G.add_edge(n, new, gnms=list(zip(gidintersect,[0]*len(gidintersect))),
                          DEdges=[])
    for n in self.G.successors(node):
      edgestoremove.append((node,n))
      for new in newnodes:
        gidintersect = n.gids & new.gids
        if gidintersect:
          self.G.add_edge(new, n, gnms=list(zip(gidintersect,[0]*len(gidintersect))),
                          DEdges=[])
    for e in iter(edgestoremove):
      self.G.remove_edge(*e)
    self.G.remove_node(node)


  def _idTOextremes(self):
    """
    Return dictionaries mapping gid to the lowest (resp. highest) index it has
    in any node.
    """
    idTOfirsti = defdict(lambda: sys.maxsize)
    idTOlasti = defdict(lambda: 0)
    for n in self.G:
      for gid, (s,e) in n.idTOinter.items():
        if s < idTOfirsti[gid]:
          idTOfirsti[gid] = s
        if e > idTOlasti[gid]:
          idTOlasti[gid] = e

    return idTOfirsti, idTOlasti

#} .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .

  def getTitleFilename(self):
    """
    Return a filename suitable to put in a window title. If self._alphafile is
    set, then it gets precidence over self._seqfile.
    """
    filename = self._alphafile if self._alphafile else self._seqfile
    return os.path.basename(filename)

  def getFastaFile(self):
    """
    Return the fasta sequence filename.
    """
    return self._seqfile

  def setAlphaFile(self, alphafile:str):
    """
    Set the alpha graph filename.
    """
    self._alphafile = alphafile

  def getAlphaFile(self):
    """
    Return the alpha graph.
    """
    return self._alphafile

  def setGIDs(self, gidset: Set[str]):
    self.gids = gidset

  def _codeTOnode(self):
    """
    Return the dictionary mapping url code from a DNode to its MNode.
    """
    return self.G.graph['codeTOnode']

  def simpleCycles(self):
    """ Generate all simple cycles of the graph. """
    return nx.simple_cycles(self.G)

  def isDAG(self):
    """ Return true if the graph is a DAG. """
    return nx.is_directed_acyclic_graph(self.G)

  def graphIsContracted(self):
    return self.iscontracted

  def iterNodes(self):
    """ Return an iterator to the nodes in the graph. """
    return self.G

  def size(self):
    """ Return the number of nodes in the graph. """
    return self.G.number_of_nodes()

  def setFastaFile(self, seqfile):
    """
    Set a new sequence file to use for this graph and all subgraphs.

    @note: This is useful when a graph is saved along with its fasta
           information.  A temporary fasta file is created and then the
           new file is updated on this graph.
    """
    self._seqfile = seqfile
    for subgraph in self.nodeTOsubgraph.values():
      subgraph._seqfile = seqfile


  def nonSingletonNodeCount(self):
    """ Return the number of non-singleton nodes in the graph. """
    count = 0
    for n in self.G:
      if len(n.gids) > 1:
        count += 1
    return count


  def getLinearExtension(self):
    """ Return a list of the nodes ordered by a topological sort. """
    try:
      return list(nx.topological_sort(self.G))
    except nx.exception.NetworkXUnfeasible as e:
      raise GraphOnIntervalError(str(e))
    except Exception as e:
      print(f'BUG: getLinearExtension() called on cyclic graph:')
      traceback.print_stack()


  def getAnchors(self):
    """
    Return a the MNodes that are anchors.
  
    @return: the MNodes that are anchors
    @rtype:  list of MNodes
    """
    anchors = []
    for n in self.G:
      if len(n.gids) == len(self.gids):
        anchors.append(n)
  
    return anchors


  def getAnchorsIter(self):
    """
    Return a the MNodes that are anchors.
  
    @return: the MNodes that are anchors
    @rtype:  list of MNodes
    """
    for n in self.G:
      if len(n.gids) == len(self.gids):
        yield n



  def addDummyNodes(self):
    """
    Add dummy Start and End nodes to the graph.

    @note: idTOlen must be set (the seqfile was given during innitialization)
    @note: at least one node for every gid must exist
    """
    Ii = None
    if self.normalizing():    #Make start node:
      Ii = self._norm.Ii
      triples = ((gid, Ii(gid, 0), Ii(gid, 1)) for gid in self.idTOlen)
    else:
      triples = ((gid, -1, 0) for gid in self.idTOlen)
    first = MNode(triples=triples, code=self._codecounter,
                  dummy='Start', annotations=self.annotations,
                  norm=self._norm)
    self.G.graph['codeTOnode'][first.code] = first
    self._codecounter += 1

    if self.normalizing():
      first.normalize()
                              #Make end node:
      triples = ((gid, Ii(gid, self.idTOlen[gid]-1),
                  Ii(gid, self.idTOlen[gid]-1)) for gid in self.gids)
    else:
      triples = ((gid, self.idTOlen[gid], self.idTOlen[gid]+1)
                 for gid in self.gids)
    last = MNode(triples=triples, code=self._codecounter, dummy='End',
                 annotations=self.annotations, norm=self._norm)
    self.G.graph['codeTOnode'][last.code] = last
    self._codecounter += 1
    if self.normalizing():
      last.normalize()

    #firstnodes = self._getFirstNodes()
    self.G.add_node(first)
    self.G.nodes[first]['URL'] = first.code

    #lastnodes = self._getLastNodes()
    self.G.add_node(last)
    self.G.nodes[last]['URL'] = last.code



  def _getFirstNodes(self):
    """
    Return a minimum size set of nodes such that the left-most node containing
    each genome is in the list.
    """
    rightmost = set()
    for n in nx.topological_sort(self.G):
      rightmost.add(n)
      if frozenset().union(*(m.gids for m in rightmost)) == set(self.gids):
        break
    return rightmost


  def _getLastNodes(self):
    """
    Return a minimum size set of nodes such that the right-most node containing
    each genome is in the list.
    """
    leftmost = set()             #For future NX version use reverse=True
    for n in reversed(list(nx.topological_sort(self.G))):
      leftmost.add(n)
      if frozenset().union(*(m.gids for m in leftmost)) == set(self.gids):
        break

    return leftmost



  def categorizeBlocks(self, nested=False, smallest=False, largest=False,
                       contract=False, ignoreends=False, stuck=False,
                       name="test"):
    """
    Group nodes into blocks if there exists an A and B such that all paths from
    A to any node reachable by B goes through B, and there are the same
    number of nucleotides in every genome.

    Contract blocks into single nodes if contract is True.

    @warn: this only works for DAGs (could loop infinitely on a cycle).

    @param nested:     keep the blocks that don't partially overlap with another
    @param smallest:   keep the smallest blocks
    @param largest:    keep the largest blocks
    @param contract:   contract largest blocks into a single node
    @param ignoreends: ignore a block between first and last anchor nodes
    """
    blocks = self._findBlocks(ignoreends)
    if contract:
      largest, smallest, nested = True, False, False
      self.iscontracted = True

    if nested:      #Blocks that don't partially overlap with another block:
      for (A,B,bl) in blocks:
        #if not any(map(lambda (x,y,h): bl&h and h-bl and bl-h,blocks)):Python2
        if not any(bl&h and h-bl and bl-h for _,_,h in blocks):
          self.blocks.append((A,B,bl))

    elif smallest:  #Keep blocks that are not superblocks of another block:
        for (A,B,bl) in blocks:
          #if not any(map(lambda (x,y,h): self._subset(h,bl), blocks)): Python2
          if not any(self._subset(h, bl) for _,_,h in blocks):
            self.blocks.append((A,B,bl))

    elif largest:   #Blocks that are not subblocks of another (and same idset):
      largest = self._getLargestBlocks(blocks)

      for (A,B,bl) in largest:
        if contract:
          self._joinNodes(A,B,bl)
        else:
          self.blocks.append((A,B,bl))
    if stuck:
      stuckblocks = self._findStuck(ignoreends,name)
      for (A,B,bl) in stuckblocks:
        self.blocks.append((A,B,bl))


  def _eqDists(self, a,b):
    """
    Return True if the intervals for genomes a and b have equal distances.
    
    @note: If the sets of gids for a and b are different, return False.
    """
    if not isinstance(a,MNode) or not isinstance(b,MNode) or a.gids != b.gids:
      return False

    dist = 0
    for gid,_,e1 in a.triples:
      if not dist:
        dist = b[gid][0]-e1
      elif dist != b[gid][0]-e1:
        return False

    return True


  def getSamplingOLD(self, remlen=10, keeplen=1000, gap=1000):
    """
    Return a periodic sampling of the nodes of the graph.

    @note: this is supposed to be used with graph a comprised only of anchors.

    @param remlen:  nodes shorter than this will be discarded
    @param keeplen: keep all nodes longer than this
    @param gap:     only keep nodes farther apart than this
    """
    nodepairs = []
    print(len(self.G))
    for i,node in enumerate(self.getLinearExtension()):
      if node.length > remlen:
        nodepairs.append((i,node))

    nodes = []
    lasti = 0
    for i,node in nodepairs:
      if node.length > keeplen or not lasti or i-lasti > gap:
        lasti = i
        nodes.append(node)
    return self.__init__(nodes=nodes, addloners=False, seqfile=self._seqfile,
                         idTOi=self._idTOi)
      
      


  def _getLargestBlocks(self, blocks):
    """
    Return a list of maximal blocks (blocks that are not contained in
    any other block).
    """
      #Map nodes to blocks and then take the largest block for each node.
    nTOblocks = defdict(set)
    for fullblock in blocks:
      _,_,bl = fullblock
      for n in bl:
        nTOblocks[n].add(fullblock)

    blacklist = set()
    for _,blks in list(nTOblocks.items()):
      longest = max(blks, key=lambda A_B_bl: len(A_B_bl[2]))
      blks.remove(longest)
      blacklist |= blks

    bset = set(blocks)
    bset -= blacklist
    return bset



  def _findStuck(self, ignoreends=False, name=""):
    """
    Return the set of stuck blocks.
    Create "nameFile"_stuck.txt and "nameFile"_contract.txt
     
    Definition of stuck block : Genomes are stuck between 2 nodes A and B 
    if 2 or more genomes are included in both A and B, and standard deviation
    of these genomes is less than 10% of the average length of these genomes.
    Moreover, each node between A and B must not contain all stuck genomes.

    @return: a set which contains a list of tuples (A,B,block) where A and B
             are the framing nodes of the stuck block and block is the set of
             nodes in between them
    """

    res=re.search(r".*?([^/]+)\.",name)
    stuck = set()
    lengths = []
    genomes = []
    stuckfile = open(res.group(1)+"_stuck.txt", "w")
    contractfile = open(res.group(1)+"_contract.txt", "w")
    for genome in self.idTOstr:
      description = genome + ":" + accFromDescription(self.idTOstr[genome])+"\n"
      stuckfile.write(description)
      contractfile.write(description)

    for a,b in combinations(self.getLinearExtension(),2):
      if a.isContracted() : 
        stuck.add((a,a,frozenset(set())))
      if b.isContracted() : 
        stuck.add((b,b,frozenset(set())))
      del(lengths[:])
      del(genomes[:])
      if not ignoreends or (self.G.in_degree(a) and self.G.out_degree(b)):
        if len(a.gids) > 1 and len(b.gids) > 1:
          for gid in a.gids:
            if gid in b.gids:
             if (b[gid][0] - a[gid][1])>0:
               lengths.append(b[gid][0] - a[gid][1])
               genomes.append(gid)
         
          if len(lengths)>1: # calcul variance and average length
            average = 0
            for i in lengths :
              average+=i
            average = average/len(lengths)
            diffAverage = []
            for j in lengths :
              diffAverage.append(j-average)
            totalDiffAverage = 0
            for k in diffAverage:
              totalDiffAverage+=k*k
            variance = (totalDiffAverage)/len(lengths)
            
            if math.sqrt(variance)<=0.1*average:
              noJump = self._noJump(a,b,genomes) # look if each node between A and B do not contain all genomes stuck
              if noJump:
                stuck.add((a,b,noJump))
                for gid in genomes:
                  txt = "\n" + gid + "," + str(a[gid][0]) + "," + str(b[gid][1])
                  stuckfile.write(txt)
                stuckfile.write("\n")

              
            # delete genome witch has largest difference from the average
            while(len(lengths)>2 and math.sqrt(variance)>0.1*average):
              ref = 0
              counter = 0
              genomes2 = genomes[:]
              for i in diffAverage:
                if (math.fabs(i)>ref) :
                  ref = math.fabs(i)
                  value = counter
                counter += 1
              del lengths[value]
              del genomes2[value]
              average = 0
              for i in lengths :
                average+=i
              average = average/len(lengths)
              diffAverage = []
              for j in lengths :
                diffAverage.append(j-average)
              totalDiffAverage = 0
              for k in diffAverage:
                totalDiffAverage+=k*k
              variance = (totalDiffAverage)/len(lengths)
             
              if math.sqrt(variance)<=0.1*average:
                noJump = self._noJump(a,b,genomes2)  # look if each node between A and B do not contain all genomes2 stuck
                if noJump:
                  stuck.add((a,b,noJump))
                  for gid in genomes2:
                    txt = "\n" + gid + "," + str(a[gid][0]) + "," + str(b[gid][1])
                    stuckfile.write(txt)
                  stuckfile.write("\n")

    contracted = set()
    for (A,B,bl) in stuck:
      if A==B:
        contracted.add((A,A,bl))
        contractfile.write('\n{}/{}'.format(A.similarity,A.length))
        contractfile.write('')
        for gid in A.gids:
          txt = "\n"+gid+","+str(A[gid][0])+","+str(B[gid][1])
          contractfile.write(txt)
          stuckfile.write(txt)
        stuckfile.write("\n")
        contractfile.write("\n")

    contractfile.close()
    stuckfile.close()
    print('{} stuck blocks.'.format(len(stuck)))
    print('{} contracted nodes.'.format(len(contracted)))
    return stuck

    
  def _noJump(self, A, B, genomesStuck):
    """
    Return the set of all nodes between A and B which contain at least 1 genome
    of genomesStuck, but return false if one node contains entirely
    genomesStuck.

    @param genomesStuck: genomes stuck between A and B
    @type genomesStuck: list gid
    """
    visited = set()
    useless = set()
    Bset = set([B])
    nextlayer = set(self.G.successors(A)) - Bset
    while(nextlayer):
      newnextlayer = set()
      visited |= nextlayer
      for n in nextlayer:
        genomesCommon = 0
        for gid in genomesStuck:
          if gid in n.gids:
              genomesCommon+=1

        if all(gid in n.gids for gid in genomesStuck):
          return False
        elif genomesCommon != 0:
          newnextlayer |= (set(self.G.successors(n)) - Bset)
        else:
          useless.add(n)
        nextlayer = newnextlayer

    if len(frozenset(visited.difference(useless))) > 1:
        return frozenset(visited.difference(useless))
    else: return False



  def _findBlocks(self, ignoreends=False):
    """
    Group nodes into blocks if there exists an A and B such that all paths from
    A to any node reachable by B goes through B, and there are the same
    number of nucleotides in every genome.

    @warn: this only works for DAGs (could loop infinitely on a cycle).

    @param ignoreends: ignore a block between first and last nodes

    @return: a list of tuples (A,B,block) where A and B are the framing nodes
             of the block and block is the set of nodes in between them
    """
      #Just look at all pairs and visit the graph between them to check if
      #there are arcs to genome sets outside of the set of A and B.
      #TODO: improve efficiency (don't compute intersecting blocks).
    blocks = []
    for a,b in combinations(self.getLinearExtension(),2):
      if not ignoreends or (self.G.in_degree(a) and self.G.out_degree(b)):
        if len(a.gids) > 1 and a.gids == b.gids:

          diffs = [b[gid][0] - a[gid][1] for gid in a.gids]
          if all(d == diffs[0] for d in diffs):

            block = self.isDisconnectable(a, b)
            if block:
              blocks.append((a,b,block))

    return blocks


  def getBlockDistribution(self):
    """
    Return a list of lengths of all smallest hard blocks in the graph for
    any pair of genome sets.
    """
    blocks = self._findBlocks()

    blengths = []
    for (A,B,bl) in blocks:
      if not any(self._subset(h,bl) and X.gids == A.gids for X,Y,h in blocks):
        gid = next(iter(A.gids))
        length = B[gid][0]-A[gid][1]
        blengths.append(length)
        #printerr('{}\n{}\n{}\n\n'.format(length, A,B))
        #self.blocks.append((A,B,bl))

    return sorted(blengths, reverse=True)


  def getDiamondDistribution(self):
    """
    Return a list of lengths of all hard diamonds in the graph.
    A "diamond" is a pair of nodes (A,C) such that every path from A to C is
    of length 2.
    A diamond is "hard" if every B with A->B->C has the same length.
    """
    dlengths = []
    for A in self.G:
      nofns = set((n for n1 in self.G.successors(A)
                     for n in self.G.successors(n1)))
      if len(nofns) == 1:
        middlelens = [n.length for n in self.G.successors(A)]
        if(len(middlelens) >= 2 and
           middlelens.count(middlelens[0]) == len(middlelens)):
          #printerr("length {}".format(middlelens[0]))
          #printerr("{} ->-> {}".format(A,list(nofns)), end='\n\n')
          dlengths.append(middlelens[0])
    return sorted(dlengths, reverse=True)


  def _subset(self, h,b):
    return h!=b and h<=b

  def _superset(self, h,b):
    return h!=b and h>=b


  def _joinNodes(self, A, B, block):
    """
    Join the given nodes into a single node, removing the subgraph between
    them.

    @warn: assumes L{isDisconnectable(A,B)} is True.

    @param A:     first node
    @param B:     last node
    @param block: all nodes in-between
    """
    triples = []
    for gid in A.gids:  #Create the triples for the new node:
      if self.normalizing():
        triples.append((gid, self._norm.Ii(gid, A[gid][0]),
                             self._norm.Ii(gid, B[gid][1])))
      else:
        triples.append((gid, A[gid][0], B[gid][1]))

    if self._seqfile:  #If the seqfile is known, label nodes with MSA:
      similarity = self._getContractedBlockSimilarity(A,B, self._seqfile)
    else:
      similarity = A.length + B.length
      for n in block:   #Get the length of all full nodes:
        if n.gids == A.gids:
          similarity += n.length

          #Create the new meta-node.
    new = MNode(triples=triples, similarity=similarity,
                code=self._codecounter, annotations=self.annotations,
                norm=self._norm)
    self._codeTOnode()[new.code] = new
    self._codecounter += 1
    if self.normalizing():
      new.normalize()

          #Store the subgraph that we're removing.
    bunch = list(chain((A,B), block))
    self.nodeTOsubgraph[new] = self.getCopiedSubgraph(bunch)
    #new.setSubgraph(self.getCopiedSubgraph(bunch))

          #Modify the graph.
    self.G.add_node(new)
    self.G.nodes[new]['URL'] = new.code
    for n in self.G.predecessors(A):
      self.G.add_edge(n,new, gnms=self.G[n][A]['gnms'], DEdges=[])
    for n in self.G.successors(B):
      self.G.add_edge(new,n, gnms=self.G[B][n]['gnms'], DEdges=[])
    self.G.remove_nodes_from(bunch)



  def _getContractedBlockSimilarity(self, A,B, seqfile):
    """
    Nodes A and B define the endpoints of a block where all genomes have the
    same length.  This implies a gapless alignment on this block.  Return the
    number of columns of this alignment that have all the same characters.

    @param A:       first MNode
    @param B:       last MNode
    @param seqfile: the file with sequences in it
    """
    seqs = []
    if A > B:   #Ensure the right order (this can happen if edges have been
      A,B = B,A  # removed from the graph, when removing cycles).

    for i,rec in enumerate(SeqIO.parse(seqfile, 'fasta')):
      gid = self._iTOid[i]       #Map fasta order to gid.
      if gid in A.gids:
        s = A.idTOinterGEN[gid][0]
        e = B.idTOinterGEN[gid][1]
        if s < e:                  #If the original indices don't wrap around
          seqs.append(rec.seq[s:e]) #then take the slice.
        else:                       #Otherwise rotate.
          seqs.append(rec.seq[s:]+rec.seq[:e])

    matrix = np.array(seqs, dtype=np.dtype('U1'), copy=False)
    count = 0
    for i in range(matrix.shape[1]):
      if all(matrix[0,i] == v for v in matrix[:,i]):
        count += 1

    return count


  def getNodesBetweenAnchors(self, a, b):
    """
    Return the nodes that are between the two given anchors.

    @warn: the given nodes must be anchors.

    @param a: left anchor
    @type  a: MNode
    @param b: right anchor
    @type  b: MNode
    """
    return self.isDisconnectable(a, b)


  def getCopiedSubgraph(self, nodes):
    """
    Return a MatchGraph that is the induced subgraph of the given nodes.

    @note: must override this in derived classes

    @param nodes: the nodes to limit the subgraph to
    @type  nodes: iterable of MNodes
    """
    return MatchGraph(graph=self.G.subgraph(nodes).copy(),
                      seqfile=self._seqfile, iTOid=self._iTOid,
                      idTOlen=self.idTOlen, normfuncs=self._norm)


  def isDisconnectable(self, A, B):
    """
    Return the set of all nodes N between A and B if either
    1) all paths from A to a node N go through B, or
    2) there is a path to B that goes through N.
    Otherwise, return False.  I{Rename to isBlock?}

    @note: the implementation of this relies on the gid sets held in each node.

    @return: False if the subgraph is not disconnectable, otherwise the set of
             nodes in the subgraph.
    @rtype:  set of MNodes or False
    """
    if A.gids != B.gids:                #If they don't have the same gid set.
      return False

    if not nx.has_path(self.G, A, B):   #If there's no path from A to B
      A,B = B,A
      if not nx.has_path(self.G, A, B): #and there's no path from B to A.
        return False                     

    visited = set()
    Bset = set([B])
    nextlayer = set(self.G.successors(A)) - Bset
    while(nextlayer):
      newnextlayer = set()
      visited |= nextlayer
      for n in nextlayer:
        if not n.gids <= A.gids:
          return False
        newnextlayer |= (set(self.G.successors(n)) - Bset)

      nextlayer = newnextlayer

    return frozenset(visited)



  def _organizeAGbyBlocks(self, ag):
    """
    Organize the given agraph (pygraphviz graph) by the hierarchical blocks
    in self.blocks.

    @param ag: a pygraphviz graph
    """
      #The code that marks subgraphs according to self.blocks depends on
      #self.blocks being hierarchical (i.e. no partially overlapping blocks)...

      #Find the parent of each node (and roots of each block tree):
    self.blocks.sort(key=lambda A_B_s: len(A_B_s[2]), reverse=False)
    gTOparent = {}
    roots = []
    for _,_,g in self.blocks:
      root = True
      for _,_,h in self.blocks:
        if g != h and g <= h:
          gTOparent[g] = h
          root = False
          break
      if root:
        roots.append(g)

      #Find the children of each node:
    gTOchildren = defdict(list)
    for c,p in gTOparent.items():
      gTOchildren[p].append(c)

      #Build the subgraphs based on the forest:
    nodestack = roots
    subgraphstack = []
    i=0
    for r in roots:
      subgraphstack.append(ag.add_subgraph(r, name='cluster_'+str(i)))
      i += 1
    while(nodestack):
      n = nodestack.pop()
      nsubgraph = subgraphstack.pop()
      for c in gTOchildren[n]:
        nodestack.append(c)
        subgraphstack.append(nsubgraph.add_subgraph(c,name='cluster_'+str(i)))
        i += 1
        

  def _getPairedNodes(self, g, ag):
    """
    Pair nodes in the given agraph (pygraphviz graph) that are equidistant for
    all consituent genomes.

    @param g:  the MatchGraph
    @param ag: the pygraphviz graph version of g
    """
    pairedset = set()

    strTOgnode = {str(n):n for n in g}
    for e in ag.edges():
      a,b = e
      if self._eqDists(strTOgnode[str(a)], strTOgnode[str(b)]):
        pairedset.add(e)

    return pairedset



  def _pairNodes(self, g, ag):
    """
    Pair nodes in the given agraph (pygraphviz graph) that are equidistant for
    all consituent genomes.

    @param g:  the MatchGraph
    @param ag: the pygraphviz graph version of g
    """
    i=0
    for a,b in pairwise(nx.topological_sort(g)):
      if self._eqDists(a,b):
        ag.add_subgraph((a,b), name='cluster_'+str(i))
        i += 1


  def switchToNormalizedMode(self, startgid=None, start=None):
    """
    Switch to normalized mode by setting the variables L{self._norm.I()} and
    L{self._norm.Ii()}. L{self._norm.I()} maps a genome index to a new
    rotated index. L{self._norm.Ii()} is the inverse of L{self._norm.I()}
    (original indices will only be used for user output).

    If start is defined, then rotate to the smallest anchor that is greater
    than this index, of the startgid genome.

    Remap indices according to the first connected component of nodes that
    comprise a full set of genomes.

    @param startgid: the gid for which the start index belongs.
    @param start:    rotate at least to this index when normalizing.
    """
    if self.normalizing():
      raise(Exception('Calling switchToNormalizedMode in normalizing mode.'))  


    anchors = self.getAnchors()
    if not anchors:
      commonsets = self.getCommonSubsets()
      try:
        raise UnrelatedPhagesError([self.gidsToAccessions(s)
                                    for s in commonsets])
      except Exception:
        raise UnrelatedPhagesError([[accFromDescription(self.idTOstr[gid])
                                     for gid in s] for s in commonsets])

    smallest = dict()
    if startgid and start:
      for anchor in anchors:
        if(anchor[startgid][0] >= start and
           (not smallest or anchor[startgid] < smallest[startgid])):
          smallest = anchor
    else:
      smallest = min(anchors)

    if not smallest or (startgid and start and smallest[startgid][0] < start):
      sys.exit('ERROR: no anchor greater than {} in species {}.'.
               format(start, startgid))

    self._norm.setIndexRemapFuncs([smallest], self.idTOlen)


  def normalizing(self):
    """
    Return true if normalized indices are being used.
    """
    return self._norm and self._norm.normalizing()


  def gidsToAccessions(self, gidset):
    """
    Convert a set of gids to a set of accessions.
    """
    return [accFromDescription(self.idTOstr[gid]) for gid in gidset]




  def getCommonSubsets(self, cutoff=0.001):
    """
    Return subsets of gids that occur over a certain percentage of the genome.
    """
    gidsetTOcount = defdict(lambda: 0)
    gidsetTOlen = defdict(lambda: 0)
    for n in self.G:
      if len(n.gids) > 1:
        gidsetTOcount[n.gids] += 1
        gidsetTOlen[n.gids] += n.length

    genomelength = float(min(self.idTOlen.values()))

    #common = filter(lambda (s,l): l/genomelength > cutoff,
    #                gidsetTOlen.iteritems())                                
    common = [s_l for s_l in gidsetTOlen.items() if s_l[1]/genomelength>cutoff]
 
    sets = set()
    for gset, _ in sorted(common, key=lambda s_c: len(s_c[0]), reverse=True):
      if not any(gset < s for s in sets):
        sets.add(gset)

    return sets




  def _getSmallCoveringCC(self):
    """
    Return a smallest connected component that covers all gids.

    @warn: !not finished!
    """
    allgids = set(self.idTOstr.keys())
    plengths = list(nx.all_pairs_shortest_path_length(self.G))

       #Visit all nodes of the graph, marking the distance to the closest
       #reachable node for each gid:
    nTOidTOdist = defdict(lambda: defdict(lambda: (sys.maxsize, None)))
    for n,m in combinations(self.G, 2):
      if m in plengths[n]:
        self._updateClosest(nTOidTOdist, n,m, plengths[n][m])
      if n in plengths[m]:
        self._updateClosest(nTOidTOdist, n,m, plengths[m][n])

       #Now get the node that has the minimum sum of distance for all gids:
       # (this is not the tree distance: the same path can count twice :( )
    shortest = (sys.maxsize, None)
    minval = sys.maxsize
    for n in self.G:
      #val = sum(map(itemgetter(0), nTOidTOdist[n].itervalues()))    
      val = sum(v[0] for v in nTOidTOdist[n].values())
      if set(nTOidTOdist[n].keys()) == allgids:
        minval = min(val, minval)
      if set(nTOidTOdist[n].keys()) == allgids and val < shortest[0]:
        shortest = (val, n)

       #!!!!! For some reason the graph seams to be different every time?!
    #cc = set(map(itemgetter(1), nTOidTOdist[shortest[1]].itervalues()))
    cc = set(map(itemgetter(1), iter(nTOidTOdist[shortest[1]].values())))
    assert False #NOTFINISHED!
    return cc




  def _updateClosest(self, closest, n,m, distance):
    """
    Update the distance dictionary for node n based on the node m at the
    given distance.

    @param closest: maps node to node to gid to distance
                    (all pairs must be defined using defaultdicts)
    @param n: node to update distance dictionary on
    @param m: with shortest path L{distance} from L{n}
    @param distance: length of shortest path from L{n} to L{m}
    """
    for gid in m.gids:
      if gid in n.gids:
        closest[n][gid] = (0,n)
      elif closest[n][gid][0] > distance:
        closest[n][gid] = (distance, m)


  def _getSmallestByGid(self, gid):
    """
    Get the node with the smalled value given the gid.
    """
    smallest = dict() 
    for n in self.G:
      if gid in n and (not smallest or n[gid] < smallest[gid]):
        smallest = n
    return smallest

  def getAccessionsInComponents(self):
    """
    Return a list of sets of accessions representing those in each seperate
    connected component.
    """
    gidsets = []
    for cc in nx.weakly_connected_components(self.G):
      gids = []
      for n in cc:
        gids.extend(accFromDescription(self.idTOstr[gid]) for gid in n.gids)
      gidsets.append(set(gids))
    return gidsets



  def getGidsInComponents(self):
    """
    Return a list of sets of gids representing the gids in each seperate
    connected component.
    """
    gidsets = []
    for cc in nx.weakly_connected_components(self.G):
      gids = set()
      for n in cc:
        gids |= n.gids
      gidsets.append(gids)
    return gidsets


  def switchToOriginalMode(self):
    """
    Use only original indices when building graphs from now on.
    """
    self._norm.reset()

  def getNormFuncs(self) -> 'NormIndFuncs':
    return self._norm




#{ I/O

  def toGEXF(self, filename):
    """
    Save the graph as a gexf file (for use with gephi).
    """
    #for node in self.G.nodes.keys():
    #  self.G.nodes[node]['label'] = str(node)
    #nx.relabel_gexf_graph(self.G) 

    for n in self.G:
      #for _,eattr in list(self.G.adj[n].items()):
      for _,eattr in self.G.adj[n].items():
        eattr.pop('gnms')

    nx.write_gexf(self.G, filename)


  def toFile(self, filename, fast=False, addnames=False):
    """
    Save the graph as an image in a file.

    @param filename: the filename to create (type of file depends on extension)
    @param fast:     do a quick and dirty rendering
    @param addnames: if set, then add nodes showing the names of the genomes
                     at the begining and end
    @type addnames:  False, or a dictionary mapping gid to phage description
    """
    ag = self.toAgraph(self.G, fast, addnames=addnames)
    ag.draw(filename, prog='dot')


  def toAgraph(self, graph: nx.DiGraph, fast=False, addnames=False,
               grouppairs=False):
    """
    Return an Agraph (pygraphviz) version of this graph appropriately
    compressed, labeled, and laid out.

    @note: if self.blocks is set, then we hierarchically organize subgraphs

    @param fast:     do a dirtier but quicker layout
    @param addnames: if set, then add nodes showing the names of the genomes
                     at the begining and end
    @type addnames:  False, or a dictionary mapping gid to phage description
    @param grouppairs: if True, group pairs of nodes that have the same distance
                       between them for all genomes
    """
    widthdivisor = 300.0  #For scaling the node widths based on sequence length.
    gidset = set()

        #Setup the ports and labels for the nodes in the eventual multigraph:
    for n in graph:
      label = ''

      if self._norm and self._norm.printnormalized:
        tr = n.triples
      else:
        tr = n.triplesGEN
                       #Format the node with tabular text:
      for gid, start, end in tr:
        gidset.add(gid)
        if(n.isDummy() or not self.mindisplaysize or
           n.length > self.mindisplaysize):
          if n.isDummy():
            label += '<{}>{}:[{}-{}]|'.format(gid,gid,start+1,start+1)
          else:
            label += '<{}>{}:[{}-{}]|'.format(gid,gid,start+1,end)
        else:
          label += '<{}>{}|'.format(gid,gid)
      label += n.getLengthRatioStr()
      graph.nodes[n]['label'] = label
      graph.nodes[n]['width'] = n.length/widthdivisor

        #Transform the graph into an Agraph:
    labelTOset = self._labelEdges(graph)
    if self.blocks:                #Make it ugly but organize hierarchically:
      ag = nx.drawing.nx_agraph.to_agraph(graph)
      self._organizeAGbyBlocks(ag)
    else:                           #Make it pretty:
      mg = nx.MultiDiGraph(graph)
      if addnames:
        self.addGenomeLabelNodes(mg, addnames, labelTOset)
      ag = nx.drawing.nx_agraph.to_agraph(mg)
      groupedpairs = set()
      if grouppairs:
        groupedpairs = self._getPairedNodes(mg, ag)
      self._prettifyAgraph(ag, labelTOset, gidset, groupedpairs)

        #Set graph attributes:
    if fast:
      ag.graph_attr['splines'] = 'line'
      ag.graph_attr['nslimit'] = '0'
      ag.graph_attr['nslimit1'] = '0'
    ag.graph_attr['rankdir'] = 'LR'
    ag.node_attr['shape'] = 'Mrecord'

    return ag


  def _labelEdges(self, g: nx.DiGraph):
    """
    Assign labels to the edges based on the sets of genomes on them.
    """
    labelTOset = {}
    for n in g:
      for _,eattr in g.adj[n].items():
        label = ' '.join([a for a,b in sorted(eattr['gnms'])])
        eattr['label'] = label
        labelTOset[label] = eattr['gnms']

    return labelTOset



  def addGenomeLabelNodes(self, g, idTOstr, labelTOset):
    """
    Add nodes with the given genome labels to the beginning and end.

    @param g:          a nx graph
    @param idTOstr:    map gid to genome descriptor
    @param labelTOset: map edge label to pairs of (gid, dist)
    """
      #Find the first and last nodes for each gid:
    idTOfirst = {}
    idTOlast = {}
    for n in g:
      for gid, s,e in n.triples:
        if gid not in idTOfirst or idTOfirst[gid][gid] > (s,e):
          idTOfirst[gid] = n
        if gid not in idTOlast or idTOlast[gid][gid] < (s,e):
          idTOlast[gid] = n

      #Add labels to beginning nodes (one node per first node):
    firstTOids = defdict(list)
    for gid, n in idTOfirst.items():
      firstTOids[n].append(gid)

    for n, gids in firstTOids.items():
      newnode = 'namenodeleft_'+'_'.join(gids)
      g.add_node(newnode)
      label = ''
      for gid in sorted(gids):
        label += '<{}>{}|'.format(gid,nameFromDescription(idTOstr[gid]))
      g.nodes[newnode]['label'] = label
      g.nodes[newnode]['URL'] = NAMENODELEFT
      g.nodes[newnode]['color'] = 'white'
      g.nodes[newnode]['rank'] = 'source'
      label=' '.join(sorted(gids))
      labelTOset[label] = list(zip(sorted(gids), [0]*len(gids)))
      g.add_edge(newnode, n, label=label, DEdges=[])



      #Add labels to beginning (each node seperately):
    #for gid, n in idTOfirst.iteritems():
    #  newnode = 'namenodeleft_'+str(gid)
    #  g.add_node(newnode)
    #  label = '<{}>{}'.format(gid,nameFromDescription(idTOstr[gid]))
    #  g.nodes[newnode]['label'] = label
    #  g.nodes[newnode]['URL'] = -1
    #  g.nodes[newnode]['color'] = 'white'
    #  g.nodes[newnode]['rank'] = 'source'
    #  label=str(gid)
    #  labelTOset[label] = zip([gid], [0])
    #  g.add_edge(newnode, n, label=label, DEdges=[])


      #Add labels to beginning (all one node):
    #firstTOids = defdict(list)
    #for gid, n in idTOfirst.iteritems():
    #  firstTOids[n].append(gid)

    #newnode = 'namenodeleft'
    #g.add_node(newnode)
    #label = ''
    ##for gid, n in sorted(idTOfirst.iteritems()):
    ##  label += '<{}>{}|'.format(gid,nameFromDescription(idTOstr[gid]))
    #for gids in firstTOids.itervalues():
    #  for gid in sorted(gids):
    #    label += '<{}>{}|'.format(gid,nameFromDescription(idTOstr[gid]))
 
    #g.nodes[newnode]['label'] = label.rstrip('|')
    #g.nodes[newnode]['URL'] = -1
    #g.nodes[newnode]['color'] = 'white'

    #for n, gids in firstTOids.iteritems():
    #  label=' '.join(sorted(gids))
    #  labelTOset[label] = zip(sorted(gids), [0]*len(gids))
    #  g.add_edge(newnode, n, label=label, DEdges=[])



      #Add labels to end nodes (one node per first node):
    lastTOids = defdict(list)
    for gid, n in idTOlast.items():
      lastTOids[n].append(gid)

    for n, gids in lastTOids.items():
      newnode = 'namenoderight'+'_'.join(gids)
      g.add_node(newnode)
      label = ''
      for gid in sorted(gids):
        label += '<{}>{}|'.format(gid,nameFromDescription(idTOstr[gid]))
      g.nodes[newnode]['label'] = label
      g.nodes[newnode]['URL'] = NAMENODERIGHT
      g.nodes[newnode]['color'] = 'white'
      g.nodes[newnode]['rank'] = 'source'
      label=' '.join(sorted(gids))
      labelTOset[label] = list(zip(sorted(gids), [0]*len(gids)))
      g.add_edge(n, newnode, label=label, DEdges=[])


      #Add labels to end (all in one fake node):
    #lastTOid = defdict(list)
    #for gid, n in idTOlast.iteritems():
    #  lastTOid[n].append(gid)

    #newnode = 'namenoderight'
    #g.add_node(newnode)
    #label = ''
    #for gid, n in sorted(idTOlast.iteritems()):
    #  label += '<{}>{}|'.format(gid,nameFromDescription(idTOstr[gid]))
    #g.nodes[newnode]['label'] = label.rstrip('|')
    #g.nodes[newnode]['URL'] = -1
    #g.nodes[newnode]['color'] = 'white'

    #for n, gids in lastTOid.iteritems():
    #  #g.add_edge(n, newnode, gnms=zip(sorted(gids), [0]*len(gids)), DEdges=[])
    #  label=' '.join(sorted(gids))
    #  labelTOset[label] = zip(sorted(gids), [0]*len(gids))
    #  g.add_edge(n, newnode, label=label, DEdges=[])



  def _prettifyAgraph(self, ag:AGraph, labelTOset, gidset, groupedpairs=set()):
    """
    Add colors and ports and other pretty things.

    @param ag:           the agraph
    @param labelTOset:   map edge label to set of paris (gid, int)
    @param gidset:       the set of gids for this graph
    @param groupedpairs: put boxes around these pairs of nodes
    @type groupedpairs:  set of ag edges
    """
    idTOcolor = {gid: self._edgecolors[self._idTOi[gid]%len(self._edgecolors)]
                 for gid in gidset}
    i=0
    for e in ag.edges():
      source, target = e
      if e.attr['label']:
        gids = labelTOset[e.attr['label']]
        ag.remove_edge(e)
        if e in groupedpairs:
          ag.add_subgraph(e, name='cluster_'+str(i))
          i += 1

        sgidTOint = self._getGidToInterval(source)
        tgidTOint = self._getGidToInterval(target)

        for gid,_ in gids:
          ag.add_edge(source, target, key=gid, DEdges=[])
          
          e1 = ag.get_edge(source, target, key=gid)
          e1.attr['tailport'] = gid
          e1.attr['headport'] = gid
          e1.attr['color'] = idTOcolor[gid]
          if gid in sgidTOint and gid in tgidTOint:
            diff = tgidTOint[gid][0]-sgidTOint[gid][1]-1
            if diff:
              #e1.attr['label'] = diff
              e1.attr['style'] = 'dashed'  #or 'dotted'
      else:
        raise Exception('Unexpected edge encountered.')



  def _getGidToInterval(self, agraphnode):
    """
    Parse the given string representation of a node to get the gid and interval
    information (yuk).

    @note: dummy left-most and right-most nodes will have empty dictionaries.

    @return: map of gid to interval
    """
    gidTOint = {}
    for m in MNode._nodere.finditer(str(agraphnode)):
      gid,s,e = m.group(1),m.group(2),m.group(3)
      gidTOint[gid] = (int(s),int(e))
    return gidTOint
    


  def printContractStats(self):
    """
    Print the mean, max, min, and standard deviation of the ration related to
    the contracted nodes.
    """
    ratios = []
    for n in self.G:
      if n.isContracted():
        ratios.append(float(n.similarity)/n.length)
    print()
    print('contracted node ratios:')
    print('  mean:    {}'.format(np.mean(ratios)))
    print('  dev:     {}'.format(np.std(ratios)))
    print('  min,max: {},{}'.format(min(ratios),max(ratios)))

#} .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MNode(DNode):
  """
  This is a node in a MatchGraph.

  There are "genome" indices and "graph" indices.  These are the same unless
  the graph has been normalized (graph indices are new normalized indices).

  @note: Reference the interval for gid 'A' using n['A'] for node n.

  @note: when adding an L{MNode} to a normalized graph (after the constructor
         is called and the graph is normalized) you must inverse map the
         indices with L{self._norm.Ii()} that are given to L{MNode()}, and then
         call L{MNode.normalize()} on the created L{MNode}.

  @ivar triples:    sorted list of triples (gid, start, end) from the matching
                    sequences that this node represents (graph indices).
  @ivar triplesGEN: the triples with genome indices (before normalization).
  @ivar idTOinter:  maps gid to interval graph index pairs.
  @ivar idTOinterGEN: maps gid to genome index pairs.
  @ivar gids:       the set of gids for this node.
  @ivar length:     the length of the matches/intervals.
  @ivar matchclass: the set of matches that are all equivalent.
  @ivar blocks:     set of blocks that this node takes part in (for hierarchical
                    organization of blocks with same lengths).
  @ivar similarity: a measure of similarity for the sequences in this node if it
                    is a contracted node.  False if it is not a contracted
                    subgraph.
  @ivar code:       a unique int identifying this node.
  @ivar dummy:      if this node is a dummy (first or last) node then this
                    holds a string containing a label.
  @ivar printnormalized: print the normalized indices rather than genome indices
  """

  #MNode
  def __init__(self, eqclass=None, triples=None, similarity=0,
               code: int=0, dummy='',
               annotations: Annotations=None, norm: 'NormIndFuncs'=None,
               **kwargs):
    """
    Make a node based on the given equivalence class of matches or based
    on the given triples (the eqclass has priority).

    :warn: use of an MNode in the interactive environment requires the code
           variable to be set to a unique integer (the 'url' of an xdot Node
           gets set to this)

    :param norm:    the normalization object for indices
    :param eqclass: sets of equivalent Matches
    :type  eqclass: set of sets
    :param triples: list of triples (gid, start, end)
    :type  triples: list of triples
    :param similarity: a measure of similarity over the sequences of this node
                       if it is a contracted node.
    :type  similarity: integer  (0 if not contracted)
    :param code:       unique identifier
    :type  code:       integer
    :param dummy:   if this node is a dummy (first or last) node then this
                    holds a string containing a label.
    :param annotations:show these annotations
    """
    super().__init__(**kwargs)

    self._norm: NormIndFuncs = norm

    if eqclass:
      self.triples = list(set(chain(*[m.getTriples() for m in eqclass])))
      self.matchclass = eqclass
    elif triples:
      self.triples = list(set(triples))
      #self.matchclass = frozenset((Match(triples=triples)
      #                             for triples in pairwise(self.triples)))
      self.matchclass = frozenset()
    else:
      sys.exit("Error: can't instantiate an empty MNode.")

    self.triples.sort()
    self.triplesGEN = self.triples
    t = next(iter(self.triples))
    self.length = t[2]-t[1]      #May be wrong if indices wrap (must normalize)
    self.idTOinter = {t[0]: (t[1],t[2]) for t in self.triples}
    self.idTOinterGEN = self.idTOinter
    self.gids = frozenset(self.idTOinter.keys())
    self.similarity = similarity
    self.code = code
    self.dummy = dummy
    self.annotations: Annotations = annotations

  def setNormFuncs(self, norm: 'NormIndFuncs'=None):
    self._norm = norm

  def getNormFuncs(self, norm: 'NormIndFuncs'=None):
    return self._norm

  def normalizing(self):
    return self._norm and self._norm.I

  def normalize(self):
    """
    Normalize the indices according to the function L{self._norm.I()}.

    @note: If L{self._norm.I()} not set then die.
    """
    if not self._norm or not self._norm.normalizing():
      traceback.print_stack()
      raise(Exception('ERROR: normalize() called on MNode() before '+
                      'switchToNormalizedMode()\n      has been called.'))

    I = self._norm.I
    self.triples = []
    for gid, s,e in self.triplesGEN:
      end = I(gid,e)
      if not end:            #If end index should be the end of the genome.
        end = I(gid,e-1)+1
      self.triples.append((gid, I(gid,s), end))

    self.idTOinter = {gid:(s,e) for gid, s,e in self.triples}

    t = next(iter(self.triples))  #Fix the length since it's possible the
    self.length = t[2]-t[1]       #original indices wrapped around.



  def getLengthRatioStr(self):
    """
    Return a string with the length ratio.
    """
    label = str(self.length)
    if self.isContracted():
      percentage = int(100*(float(self.similarity)/self.length))
      label = '{}% {}'.format(percentage, label)
    elif self.dummy:
      label = self.dummy
    return label

  def isDummy(self):
    """
    Returns True if this is a dummy node.
    """
    return bool(self.dummy)

  def isContracted(self):
    """
    Return True if this is a contracted node.
    """
    return self.similarity

  def maxDistanceTo(self, node):
    """
    Return the maximum distance over all gids to the given node.
    """
    if self < node:
      n1, n2 = self, node
    else:
      n1, n2 = node, self

    intervallens = []
    for t1,t2 in zip(n1.triplesGEN, n2.triplesGEN):
      if self._norm and self._norm.normalizing():   #Use normalized indices to
        secondindex = self._norm.I(t2[0], t2[1])    #calculate length.
        if secondindex == None:
          secondindex = self._norm.I(t2[0], t2[1]-1)+1

        length = secondindex - self._norm.I(t1[0], t1[2])
      else:
        length = t2[1]-t1[2]
      intervallens.append(length)

    return max(intervallens)


  def getAlignment(self, O, addgaps=False):
    """
    Produce a gappless MultipleSeqAlignment for all sequences in the node.
  
    @param addgaps: add all-gap rows for gids that are missing from the MNode
    """
    idTOrecord = {}
    for gid in self.gids:
      record = O.records[O.idTOi[gid]]
  
      start, end = self.idTOinterGEN[gid][0], self.idTOinterGEN[gid][1]
      if start < end:                                                      
        sequence = record.seq[start:end]
      else:                        
        sequence = record.seq[start:]+record.seq[:end]
  
      record = SeqRecord(sequence, id=record.id, description=record.description)
      idTOrecord[gid] = record
  
    newrecords = []
    for i, gid in O.iTOid.items():
      if gid in self.gids:
        newrecords.append(idTOrecord[gid])

      elif addgaps:
        newrecords.append(SeqRecord(Seq('-' * len(self)),
                                    id=O.records[i].id,
                                    description=O.records[i].description))

    return MultipleSeqAlignment(newrecords)

  def activate_annotation(self, idTOlen):
    """
    Show the annotation associated with this node.  If L{self.annot} is
    not already defined, then innitialize it.

    @param idTOlen: map gid to genome length
    """
    if not self.annotations:
      print(f'WARNING: trying to active annotation with being innitialized.')
      return False

    if not self.annot:
      text, height = self._get_annotation_text(idTOlen)
      y = self.y - self.h  #Use the height of the node to distance the annot.

      penAnnot = Pen() #Parameters: color,fillcolor,linewidth,fontname, 
                       #bold,italic,underline,superscrpit,subscript,
                       #strikethrough,overline
      ts = TextShape(penAnnot, self.x, y, TextShape.CENTER, 0, text)
      annot = AnnotShape(ts, Pen())

      self.annot = annot

    self.annot.visible = True



  def _get_annotation_text(self, idTOlen) -> Tuple[str, int]:
    """
    Return the string for the given AnnotShape.

    @param idTOlen: map gid to genome length
    """
    text = ""
    height = 0       #Number of rows.
    len_line_max = 0
    len_text = 0
    for i, (gid, s,e) in enumerate(self.triplesGEN):
      len_text = len(text)

      if s > e:      #If the interval wraps around:
        li_annotation = self.annotations.getAnnotations(gid, s, idTOlen[gid])
        li_annotation.extend(self.annotations.getAnnotations(gid, 0, e))
      else:
        li_annotation = self.annotations.getAnnotations(gid, s, e)

      text += '{}:'.format(gid)
      for j, (desc, start,end) in enumerate(li_annotation):
        if j%3 == 0 and j != 0:
          text += '\n'
          text += "   "
          height += 1

        text += ' {} [{}-{}]'.format(desc, start+1, end)

        if j != len(li_annotation)-1:
          text += ','
        if len_line_max < len(text) - len_text:
          len_line_max = len(text) - len_text

      height += 1

      if i < len(self.triplesGEN) - 1:
        text += '\n'

    return text, height


  #def setSubgraph(self, subgraph):
  #  """
  #  Set the subgraph that this contracted MNode represents.
  #  """
  #  self.subgraph = subgraph

  #def getSubgraph(self):
  #  """
  #  Get the subgraph that this contracted MNode represents.  Return False if
  #  this node is not contracted.
  #  """
  #  if self.isContracted():
  #    return self.subgraph
  #  return False


  _nodere = re.compile(r'(\w+)\:\[(\d+)\-(\d+)\]')
  def __str__(self):
    """
    Return a nice string representation of the intervals corresponding to
    this node.

    @note: interval [A,B] is 1-indexed (the segment has Ath and Bth characters).
    """
    if self._norm and self._norm.printnormalized:
      tr = self.triples
    else:
      tr = self.triplesGEN
    return '\n'.join('{}:[{}-{}]'.format(t[0], t[1]+1, t[2]) for t in tr)+\
           '\n'+self.getLengthRatioStr()


  def __repr__(self):
    return str(self)

  def __hash__(self):
    return hash(' '.join(str(t) for t in self.triplesGEN))

  def __len__(self):
    return self.length

  #def __cmp__(self, other):
  #  if self < other:
  #    return -1
  #  elif self == other:
  #    return 0
  #  else:
  #    return 1

  def __lt__(self, other):
    """
    Compare the two nodes based on graph indices of a common gid.
    If they share no gids, then compare string representations.

    @note: if other is not an MNode, then return True.
    """
    if not isinstance(other, MNode):
      return True

    sharedgids = self.gids & other.gids
    if not sharedgids:
      return str(self) < str(other)

    gid = max(sharedgids)
    return self.idTOinter[gid] < other.idTOinter[gid]

  def __eq__(self, other):
    return not self<other and not other<self
  def __ne__(self, other):
    return self<other or other<self
  def __gt__(self, other):
    return other<self
  def __ge__(self, other):
    return not self<other
  def __le__(self, other):
    return not other<self

  def __getitem__(self, gid):
    """
    Get the interval (graph indices) for a given gid.
    """
    return self.idTOinter[gid]

  def __contains__(self, gid):
    return gid in self.idTOinter



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


class NormIndFuncs(object):
  """
  Carry information for normalized indices.

  The attribute I is a function that maps genome indices to the
  normalized graph indices, while Ii is the inverse, mapping graph indices
  to the original genomic indices. These functions are only set for cases when
  setIndexRemapFuncs() is called

  :ivar printnormalized: print normalized indices (rather than genomic indices)
  :ivar I: If the graph is normalized, then I(gid, i) maps a non-normalized
           genome index i to a normalized one.  L{Ii()} does the inverse.
           To test if we are normalizing use L{self._norm.normalizing()}.
           To setup L{I()} do the following:
           L{switchToNormalizedMode()} is called on a particular
           instance of a MatchGraph (this sets L{I()} and L{Ii()} according
           to an acyclic normalization of that graph).  Then the normalize=True
           must be given in the constructor MatchGraph() if you want it to
           have normalized indices (in the implementation of MatchGraph, nodes
           that are created after the constructor is called must inverse map the
           indices with L{Ii()} that are given to L{MNode()}, and then call
           L{MNode.normalize()} on the node).

  :ivar Ii:Inverse of I. Map normalized index to non-normalized (the original
           genome index).
  """
  def __init__(self, printnormalized=False):
    self.printnormalized = printnormalized
    self.I: Callable = None
    self.Ii: Callable = None
    self._idTOlen: Dict[str, int] = {}
    self._idTOstart: Dict[str, int] = {} # Map gid to first occurence of that
                                         # gid in the given set.

  def normalizing(self):
    return bool(self.I)

  def reset(self):
    """
    Reset the normalization.
    """
    self._idTOlen = {}
    self._idTOstart = {}
    self.I = None
    self.Ii = None


  def setIndexRemapFuncs(self, nodeset, idTOlen):
    """
    Set L{self.I} and L{self.Ii}.

    L{self.I} is a function that maps indices so that the given connected
    component of nodes comes before all others in the new ordering. L{self.Ii}
    is the inverse function.

    This is done so that remapped indices can be used when building the linear
    ordering in the graph, thus artifically normalizing (rotating) the
    sequences an appropriate amount.

    The first function returned maps (old) genome indices to (new) graph
    indices. The second does the inverse.

    @warn: the index equal to the genome length is not mapped; None is returned
           instead. thus when mapping the end index of intervals you must check
           for this case since the right-side (end) index is non-inclusive in
           python indexing (e.g. a length-10 genome has an interval [4-10) and
           9 is the maximum index; 10 will map to None).

    @param nodeset: a set of nodes that includes at least one occurence of all
                    genomes.
    @param idTOlen: map gid to genome length
    """
    for n in nodeset:
      for gid in n.gids:
        if gid not in self._idTOstart or self._idTOstart[gid] > n[gid][0]:
          self._idTOstart[gid] = n[gid][0]

    self.Ii = self.indRemapInv
    self.I = self.indRemap
    self._idTOlen = idTOlen

  def indRemapInv(self, gid,i):
    "Map the given index back to the original non-normalized index."
    newi = i + self._idTOstart[gid]
    if newi >= self._idTOlen[gid]:
      newi = newi - self._idTOlen[gid]
    return newi

  def indRemap(self, gid,i):
    "Remap the given index in the given genome as though it were normalized."
    newi = i - self._idTOstart[gid]
    if newi < 0:
      newi = self._idTOlen[gid] + newi
    elif i == self._idTOlen[gid]:     #If we map to the end of the genome
      return None               #then wrap the index (see note above).

    assert(newi >= 0 and newi <= self._idTOlen[gid])
    if self.Ii:
      assert(self.Ii(gid, newi) == i)
    return newi



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class UnrelatedPhagesError(Exception):
  def __init__(self, gidsets):
    """
    Raise an error because these gidsets are independent.
    """
    self.gidsets = gidsets

  def __str__(self):
    setstr = ''
    for gidset in self.gidsets:
      setstr += '\n\nSET [{}]: {}'.format(len(gidset), list(gidset))

    return '       Here are (possibly overlapping) subsets that occur '+\
           'together often.\n'+\
           '       Try running Alpha on one of these...'+setstr


class GraphOnIntervalError(Exception):
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return self.value
