# Krister Swenson                                                Fall 2014
#
# Copyright 2015 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
import networkx as nx
import pickle
import operator
import time
import queue
import multiprocessing as mp
from threading import Thread
from threading import Event
from typing import Tuple
from typing import Callable
from typing import List
from typing import FrozenSet
from typing import Iterable
from typing import Optional

from Bio import SeqIO, AlignIO

from .xdot import DotWindow
from .xdot import UI_SPECIFICATION
from .shapes import DNode
from .shapes import ClickTarget
from .Annotations import Annotations
from .matchfuncs import nodesToIntervals
from .matchfuncs import getGapplessAlignmentForPair
from .matchfuncs import recomputeClassesForIntervals
from .matchfuncs import getMatchesAndEQClasses
from .matchfuncs import getAnchors
from .matchfuncs import GTError
from .InterGraph import InterGraph
from .Match import MatchGraph
from .Match import Match
from .Match import NormIndFuncs
from .Match import GraphOnIntervalError
from .Match import UnrelatedPhagesError
from .Match import MNode
from .Match import NAMENODELEFT
from .Match import NAMENODERIGHT
from .Match import MINDISPLAY_SIZE
from .iofuncs import InputInfo
from .userconfig import setLastOpened

from .pyinclude.usefulfuncs import printnow
from .pyinclude.usefulfuncs import printerr
from .pyinclude.usefulfuncs import invdict


## CONSTANTS:
THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))
MAX_ZOOM_RATIO = 1.7 #Don't zoom to fit a graph wider than this.


    # Set the css definition:
provider = Gtk.CssProvider()
provider.load_from_path(f'{THIS_FILE_DIR}/glade/alpha.css')
Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
                                         provider,
                                         Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)



#        _________________________
#_______/        Classes          \_____________________________________________


#  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

class PhageUI(DotWindow):
  """
  This is the graphical UI for phage alignment.
  Contracted nodes in graphs can be "drilled into" to reveal subgraphs, and
  subsequently backed out of.
  It uses the modified xdot.py to display the graph.

  The events that can happen on the UI with their associate functions are the
  following:
    1. "clicked" -> L{_onClick()}
      - triggered when a click on the main graph widget happens
      - potential results:
        - the graph is moved
        - a node is (un)highlighted

    2. "doenter" -> L{_doEnter()}
      - triggered when Enter key is pressed
      - potential results:
        - with one contracted node highlighted the graph is shown for this node
        - with two nodes highlighted the graph is recomputed between these
          nodes (taking into consideration the value in the match length box)

    3. "gograph" -> L{_anchorsToGraph()}
      - triggered when the "graph" button or 'g' is pressed
      - potential results:
        - with two nodes highlighted we compute the contracted graph on the
          corresponding interval
        - otherwise, compute the graph on the interval for all visible nodes

    4. "changelen" -> L{_changeLength()}
      - triggered when the Enter is pressed in the match length box
      - potential results:
        - if one or more nodes are highlighted then recompute the graph on
          that interval with the entered length
        - otherwise, recompute the graph on visible nodes with the new length

    5. "showtoggle" -> L{_toggleShowNodes()}
      - triggered when the "show" button or 's' is pressed
      - results:
        - toggle showing the small nodes or replacing them with dashed arrows

    6. "goback" -> L{_goBack()}
      - triggered when the back arrow button or Delete is pressed
      - results:
        - go to the previous graph in the stack if there is one

    7. "cycletoggle" -> L{_toggleRemoveCycles()}
      - triggered when the "cycle" button or 'c' is pressed
      - results:
        - when activated, remove cycles from the graph the next time the
          graph button if press or we enter between two nodes.

    8. "expandgraph" -> L{_showExpandedGraph()}
      - triggered when the button 'e' is pressed
      - results:
        - in anchor mode, do nothing.
        - if no nodes are highlighted, show the expanded graph.
        - if a contracted node is highlighted, show the expanded graph.
        - if two nodes are highlighted, show the expanded graph between them.


  :ivar app:          the AlphaApp that hold shared app information
  :ivar graph:        the InterGraph that is currently being shown
  :ivar O:            the InputInfo describing the input file
  :ivar anchormode:   True if currently displayed graph has only anchors
  :ivar minlength:    the current minlength used for recomputing matches
  :ivar graphstack:   stack holding the graphs we've drilled into
  :ivar graphTOattrs: map graph to (x,y,zoom,anchormode) state
  :ivar removecycles: if True, then remove cycles from the next computed graph,
                      otherwise we find a minimum match length such that the
                      graph is acyclic
  :ivar norm:         object used to deal with normalized indices
  :ivar _childQ:      communicate with child windows
  :ivar _parentQ:     communicate with parent windows
  :ivar _opendialog:  the currently open OpenAlphaDialog
  :ivar _childmonitorT: child monitor thread (for monitoring spawning PhageUIs)
  :ivar _loadgraphT:  thread to wait for graph loading in this window
  :ivar _getgraphQ:   Queue used to send back the local getGraph() results
  :ivar _getgraphP:   process that is computing getGraph()
  :ivar _getgraphK:   Event() used to kill getGraph() process
  """
    # _monitorChildStates() will get messages from spawned graph
    # computations. Message are pairs with a type and a value. Here are the
    # types:
  NEWGRAPH_DONE = 0   # Done computing the graph. The pid is the value.
  NEWGRAPH_ERROR = 1  # Failed with an error. A string is the value.

  #PhageUI
  def __init__(self, graph: InterGraph, app: 'AlphaApp',
               O: InputInfo=None, minlen=15, anchormode=False, fast=False,
               showall=None, getsampling=True, newdialog=False,
               toplevel=True, titlepair=None, parentqueue: mp.Queue=None,
               opendialog: 'OpenAlphaDialog'=None):
    """
    Setup a GUI with the given graph in it.
    If anchormode is given then show the graph as anchors.
    If the graphintervalfunc is given then the possibility of entering between
    nodes is activated.
    If graph is None, and opendialog is False, then create an empty object.

    :param graph:       the InterGraph to show
    :param appinfo:     the AlphaApp that holds shared application settings
    :param O:           the overall shared InputInfo
    :param minlen:      the l to use with gt/vmatch
    :param anchormode:  show the anchors only
    :param fast:        do a quick and dirty layout with graphviz
    :param showall:     show all the nodes (not just the big enough ones)
    :param getsampling: if anchormode == True, then sample the anchors
    :param newdialog:   if True, open a dialog for the user to choose a file.
    :param toplevel:    if False, hide some elements (like "Open")
    :param titlepair:   (title, subtitle) set the title and subtitle
    :param parentqueue: Queue to talk to the parent window with
    """
    self.app: 'AlphaApp' = app
    self.minlength = minlen
    self.anchormode = anchormode
    self.O: InputInfo = O
    self._parentQ = parentqueue
    self._opendialog: 'OpenAlphaDialog' = opendialog
    self.graphstack: List[InterGraph] = []

        #For new graphs in this UI:
    self._loadgraphT: Thread = None
    self._getgraphQ: mp.Queue = None
    self._getgraphP: mp.Process = None
    self._getgraphK: mp.Event = None
        #For child PhageUIs:
    self._childmonitorT: Thread = None
    self._pidTOchild: Dict[int, mp.Process] = {}

    self.norm: NormIndFuncs = graph.getNormFuncs()

    if not graph:
      return

    titlepair = titlepair if titlepair else graph.titlepair
    
    self.initPhageUI(graph, O, minlen, fast, showall,
                     getsampling, toplevel, titlepair)


  def initPhageUI(self, graph: InterGraph, O: InputInfo=None, minlen=15,
                  fast=False, showall=None, getsampling=True,
                  toplevel=True, titlepair=None):
    """
    Actually create the PhageUI.
    """
    self._firsttime = toplevel     #: Set to False after first sampling

    self.minlength = None
    self.removecycles = False
    if self.app.graphintervalfunc and O:
      super().__init__(navigable=True, anchormode=self.anchormode,
                       graph=graph, application=self.app)
      self.minlength = minlen
      self.dwidget.connect('savegraph', self._saveGraph)
      self.dwidget.connect('opengraph', self._openGraph)
      self.dwidget.connect('doenter', self._doEnter)
      self.dwidget.connect('gograph', self._anchorsToGraph)
      self.dwidget.connect('changelen', self._changeLength)
      self.dwidget.connect('showtoggle', self._toggleShowNodes)
      self.dwidget.connect('cycletoggle', self._toggleRemoveCycles)
      self.dwidget.connect('expandgraph', self._showExpandedGraph)
      self.updateLengthDisplay(self.minlength)
      self.updateLengthEntry(0)
    else:
      super().__init__(navigable=False, application=self.app)

    if not toplevel:
      self.setOpenVisibility(False)

    self.connect('destroy', self._onDestroy)

    if titlepair:
      self.setWindowTitle(*titlepair)

    self.dwidget.connect('clicked', self._onClick)
    self.dwidget.connect('goback', self._goBack)
    self.waitingkill.connect('clicked', self._stopGetGraphProcess)

    sampleafter = 8
    if self.anchormode:
      if not getsampling: sampleafter = None

    self.graphstack: List[InterGraph] = [graph]
    self.graphTOattrs = {graph: (0, 0, 1.0, self.anchormode, self.minlength,
                                 self.showtogglebutton.get_active(), [])}

    try:
      self.loadGraph(graph, self.dwidget, None, fast, sampleafter,
                     showall=showall)
    except GraphOnIntervalError as e:
      sys.exit(str(e))

    self._childQ = self.app.mpcontext.Queue()
    self._spawnChildCommunicationHandler()

    if O.seqfile[:len(self.app.matchdir)] != self.app.matchdir:
      setLastOpened(os.path.abspath(O.seqfile))  #If it's not a temporary file.

    self.updateButtonStates()

#{ Callback functions:
  def _onDestroy(self, window):
    """
    Kill children that are still computing.
    """
    for child in self.graphoverlaybox.get_children():
      pid = int(child.get_name())
      self._pidTOchild[pid].terminate()
      self._pidTOchild.pop(pid)

    self._stopGetGraphProcess()

    self._childmonitorT.killevent.set()   #Kill the child monitor thread.
    self.app.remove_window(self)

  def _stopGetGraphProcess(self, widget=None):
    """
    Stop the running getGraph process.
    """
    if self._getgraphP:
      self._getgraphK.set()           #Ask nicely
      time.sleep(1)                   #Wait
      if self._getgraphP.is_alive():
        self._getgraphP.terminate()   #Terminate if necessary
                                      #Tell _loadGraphBetweenNodes_finish()
      self._getgraphQ.put((None, None, None, None,
                           'Graph generation terminated'))
    else:
      self.showWaiting(False)         #This shouldn't happen

  def _changeLength(self, entry):
    """
    Change the length of the matches and reconstruct the graph.
    """
    widget = self.dwidget
    widget.grab_focus()
    oldminlen = self.minlength

    graph = self.graph
    if widget.highlight and len(widget.highlight) == 2:
      first = widget.highlight[0]
      last = widget.highlight[1]
    else:
      first, last = self._getExtremities(graph)

    if first.gids != last.gids:
      self.showMessage('Cannot change length when extrementies have'+\
                       ' different genome sets!')
      self.minlength = oldminlen
      self.updateLengthDisplay(oldminlen)
      self.updateLengthEntry(0)
      return

    self.loadGraphBetweenNodes(first, last, self.anchormode, self.anchormode,
                               oldminlen)


  def _toggleShowNodes(self, widget):
    """
    Either show the hidden nodes (with dashed lines) or hide them.

    @note: the toggle state has already been changed by showtoggle event
           handler.
    """
    if self.anchormode:   #Don't change the state if we're showing anchors.
      return

    keephighlight = None
    if widget.highlight and len(widget.highlight):
      keephighlight = widget.highlight

    graph = self.graph
    if self.showtogglebutton.get_active() != graph.showingAll():
      printmessage = False
      if graph.showingAll() and not graph.sizeDifference():
        printmessage = True

      self.loadingStart()
      graph.toggleActive()
      self.set_dotcode(dgraph=graph)

      if keephighlight:
        self._transferHighLightByURL(keephighlight, widget.graph.nodes)
      self.loadingStop()

      if printmessage:
        self.showMessage('Graph is too small to omit small nodes!')


  def _toggleRemoveCycles(self, widget):
    """
    Either load a graph while removing cycles, or show a graph while increasing
    the match length until the graph is acyclic.
    """
    self.removecycles = not self.removecycles


  def _showMenu(self, widget, event, node):
    """
    Build context menu for the highlighted node.

    @param node: the MNode clicked on
    """
    menu = Gtk.Menu()
    self.___menu = menu  #NOTE: do this so that menu isn't garbage collected!
    item_annot = Gtk.MenuItem("Annotations", True)
    item_align = Gtk.MenuItem("Export Multiple Alignment", True)
    item_split = Gtk.MenuItem("Split", True)
    item_merge = Gtk.MenuItem("Merge", True)
    item_indices = Gtk.MenuItem("Export Indices", True)

    item_annot.connect("activate", self._onAnnotation, node)
    item_align.connect("activate", self._exportAlignment, node)
    item_split.connect("activate", self._makeSplitDialogue, node)
    item_merge.connect("activate", self._onMerge)

    menu.append(item_annot)
    menu.append(item_align)
    menu.append(item_split)
    menu.append(item_merge)
    menu.append(item_indices)

    if widget.highlight and len(widget.highlight) == 2:
      first = widget.highlight[0]
      second = widget.highlight[1]
      if first > second:
        first, second = second, first

      if self.anchormode == False:
        if self.is_fusionnable(first, second):
          item_merge.show()

      item_indices.connect("activate", self._exportIndices, first, second)
      item_indices.show()
    else:
      if self.anchormode == False and node.length > 1 and self.O.annotations:
        item_split.show()

    if self.O.annotations:
      item_annot.show()

    item_align.show()

    menu.popup(None,None,None,None, event.button, event.time)


  def _doSplit(self, widget, window, node, scale):
    """
    Create 2 nodes as a result of fission of the given node.

    @param node:  the MNode to split
    @param scale: the HScale that has the spot to split at
    """
    spot = int(scale.get_value())+1
    window.destroy()            #Get rid of the dialog window.

               #Create the new triples:
    triple_left: List[Tuple[str, str, int]] = []
    triple_right: List[Tuple[str, str, int]] = []
    for gid, start,end in node.triples:        #Get triples in graph indices:
      triple_left.append((gid, start, start + spot))
      triple_right.append((gid, start + spot, end))

               #Create the new nodes.
    newnodes = self.graph.replaceNodeSplit(triple_left, triple_right, node)

               #Load graph with new nodes highlighted.
    self.loadGraph(self.graph, self.dwidget, highlight=newnodes)

               #Zoom to the new nodes.
    self.dwidget.zoom_to_area(newnodes[0].x1 + 200, newnodes[0].y1 - 150,
                              newnodes[0].x2 - 200, newnodes[0].y2 + 150)



  def _updateScales(self, scale, scales, indlab, sizelabel, annotlabs, gid,
                    firstgraphi, annotlist, nodelen):
    """
    Synchronize all the scales in fission dialogue.

    @param scale:      the Scale
    @param scales:     list of all scales
    @param indlab:     the index Label
    @param sizelabel:  the size Label box
    @param annotlabs:  the annotation Label boxes
    @param gid:        the gid for this scale
    @param firstgraphi:the starting graph index for the associated node
    @param annotlist:  list of trios (annotation, start, end)
    @param nodelen:    the length of the node
    """
    scalespot = int(scale.get_value())
    spot = firstgraphi+scalespot

        #Update the label:
    norm = self.graph.getNormFuncs()
    if norm.normalizing():
      genomespot = norm.Ii(gid, spot)
      indlab.set_label(self._getLabelStr(gid, genomespot+1))
    else:
      indlab.set_label(self._getLabelStr(gid, spot+1))

        #Update the annotations:
    self._updateAnnotations(gid, spot, annotlist, annotlabs)

    sizelabel.set_label(self._getSizeLabelStr(scalespot+1, nodelen-scalespot-1))

        #Change the others:
    for s in scales:
      if s.get_value() != scalespot:
        s.set_value(scalespot)


  def _updateAnnotations(self, gid, graphspot, annotlist, annotlabs):
    """
    Update the annotation Labels with the given genome coordinate.
    The left (right) Label will show the last (first) label in the left (right)
    node if a split were done.
    

    @param gid:       the genome of interest
    @param graphspot: the graph index that is currently chosen
                      (a split would happen after this index)
    @param annotlist: list of trios (annotation, start, end)
    @param annotlabs: the annotation Label boxes
    """
    before = []
    justbefore = None #The annotation we're on the border of.
    middle = []
    justafter = None  #The annotation we're on the border of.
    after = []

        #Classify everything:
    for atrio in annotlist:
      annot, s,e = atrio
      if graphspot > s.graph and graphspot < e.graph:
        middle.append(atrio)

      elif graphspot <= s.graph:
        after.append(atrio)
        if graphspot == s.graph:
          justafter = atrio

      elif graphspot >= e.graph:
        if graphspot == e.graph:
          justbefore = atrio

        before.append(atrio)

      else:
        raise(Exception('No sir!'))

    if not before:
      before.append(('', Index(),Index()))
    else:
      before.sort(key=lambda annotrio: annotrio[2].graph)
    if not after:
      after.append(('', Index(),Index()))
    else:
      after.sort(key=lambda annotrio: annotrio[1].graph)

    annotleft, annotmiddle, annotright = annotlabs

        #Set the annotation labels:
    if middle:
      annotmiddle.set_label(', '.join(a for a,_,_ in middle))
      annotleft.set_label('')
      annotright.set_label('')
      if justbefore:
        annotleft.set_label(justbefore[0])
        annotmiddle.set_label('...')
        annotright.set_label(', '.join(a for a,_,_ in middle))
      if justafter:
        if not justbefore:
          annotleft.set_label(', '.join(a for a,_,_ in middle))
        annotmiddle.set_label('...')
        annotright.set_label(justafter[0])
    else:
      annotleft.set_label(before[-1][0])
      annotmiddle.set_label('|')
      annotright.set_label(after[0][0])

    annotleft.set_justify(Gtk.Justification.LEFT)
    annotright.set_justify(Gtk.Justification.RIGHT)





  def scale_moved(self, widget, scale, longeur, li_choix, li_choix_pc,
                  li_annotation, list_pb, list_lb1, list_lb2, li_start,
                  li_inter):
    """
    Update the length chosen by the user on the scale widget.
    """
    spot = scale.get_value()

    #maj des progressbar
    for i in range(len(li_choix)):
      j=0
      while(j<len(li_choix[i])-1 and spot>=li_choix[i][j]):
        j = j+1
      if len(li_choix[i])!=0:
        list_pb[i].set_fraction(li_choix_pc[i][j])
        list_pb[i].set_text(str(li_choix[i][j]))

    #maj des labels de previsualisation
    for i in range(len(li_annotation)):
      list_lb1[i].set_text("")
      list_lb2[i].set_text("")
      debut1 = li_start[i]
      fin1 = li_start[i]+spot-1
      debut2 = li_start[i]+spot
      fin2 = li_start[i]+longeur-1

      j=0
      str1 = "  left:|"
      str2 = "right:|"
      while(j<len(li_inter[i])-1):
        debut_annotation = li_inter[i][j]
        fin_annotation = li_inter[i][j+1]

        if (fin1>=debut_annotation and fin1<=fin_annotation) or (debut1<=fin_annotation and debut1>=debut_annotation) or (debut1<=debut_annotation and fin1>=fin_annotation):
          str1+=str(li_annotation[i][j/2])+" | "

        if (fin2>=debut_annotation and fin2<=fin_annotation) or (debut2<=fin_annotation and debut2>=debut_annotation) or (debut2<=debut_annotation and fin2>=fin_annotation):
          str2+=str(li_annotation[i][j/2])+" | "

        j = j+2
      list_lb1[i].set_text(str1)
      list_lb2[i].set_text(str2)
    
	


  def _onNextButton(self, button, scale, borderlist):
    """
    Move the scale to the next greater value in the sorted borderlist.
    """
    spot = scale.get_value()
    for border in borderlist:
      if border > spot:
        scale.set_value(border)
        return

    if borderlist:
      scale.set_value(borderlist[0])

 
  def _onPrevButton(self, button, scale, borderlist):
    """
    Move the scale to the next smaller value in the sorted borderlist.
    """
    spot = scale.get_value()
    for border in reversed(borderlist):
      if border < spot:
        scale.set_value(border)
        return

    if borderlist:
      scale.set_value(borderlist[-1])

  def _getAnnotations(self, node: MNode):
    """
    Read in annotations for the given MNode.

    @param node: get the annotation overlapping this node

    @return: idTOannots and idTOborders where idTOannots maps gid to list of
             annotation trios (annotation, Index_start, Index_end), while
             idTOborders maps gid to border (start or stop of annotation).
    """
    idTOannots = {}
    idTOborders = {}   #Borders in genome indices.

    norm = node.getNormFuncs()
    for gid, start, end in node.triplesGEN:
      if norm.normalizing():            #Set first genome index:
        first = norm.Ii(gid, node[gid][0]+1)
      else:
        first = node[gid][0]+1

      if start > end:                   #This node wraps around.
        genomelen = self.graph.idTOlen[gid]
        annots = self.O.annotations.getAnnotations(gid, start, genomelen)
        borders = []
        for _,s,e in annots:
          if s >= start and s < genomelen:
            borders.append(Index(s, True, gid, first, normfuncs=norm))
          if e >= start and e < genomelen:
            borders.append(Index(e, False, gid, first, normfuncs=norm))

        annots2 = self.O.annotations.getAnnotations(gid, 0, end)
        for _,s,e in annots2:
          if s >= 0 and s < end:
            borders.append(Index(s, True, gid, first, normfuncs=norm))
          if e >= 0 and e < end:
            borders.append(Index(e, False, gid, first, normfuncs=norm))

        idTOborders[gid] = borders
        annots.extend(annots2)
      else:
        annots = self.O.annotations.getAnnotations(gid, start, end)
        borders = []
        for _,s,e in annots:
          if s >= start and s < end:
            borders.append(Index(s, True, gid, first, normfuncs=norm))
          if e >= start and e < end:
            borders.append(Index(e, False, gid, first, normfuncs=norm))
        idTOborders[gid] = borders

      newannots = []
      for a,s,e in annots:
        s -= 1
        e -= 1
        if e > self.graph.idTOlen[gid]:
          e = self.graph.idTOlen[gid]  #Annotation longer than genome.
        newannots.append((a, Index(s, True, gid, first, normfuncs=norm),
                          Index(e, False, gid, first, normfuncs=norm)))

      idTOannots[gid] = newannots

    return idTOannots, idTOborders


  def _makeSplitDialogue(self, widget, node: MNode):
    """
    Make the dialogue to facilitate a node fission.

    @param node: the MNode we will split.
    """
    idTOannots, idTOborders = self._getAnnotations(node)

    fix = Gtk.Fixed()

    #lab = Gtk.Label(label="Split the node after:") 
    #fix.put(lab, 10, 16)
    sizelabel = Gtk.Label(self._getSizeLabelStr(1, node.length-1))
    fix.put(sizelabel, 10, 16)

       #Make a Scale for each genome:
    ycoord = 40    #The starting y coordinate.
    hstep = 50     #The distance between rows.
    scales = []
    labels = []
    norm = node.getNormFuncs()
    for gid in sorted(idTOborders.keys()):
      borderlist = idTOborders[gid]
      if norm.normalizing():      #Set first genome index:
        firstgindex = norm.Ii(gid, node[gid][0]+1)
      else:
        firstgindex = node[gid][0]+1

                   #The gid and index label:
      indlab = Gtk.Label(self._getLabelStr(gid, firstgindex))
      fix.put(indlab, 10, ycoord+10)

                   #The slider bar:
      scale, scaleinds = self._makeScaleSlider(node.length, firstgindex, gid,
                                               ycoord, hstep, borderlist)
      scales.append(scale)
      fix.put(scale, 80, ycoord)

                   #The next and previous buttons:
      prevbutton = Gtk.Button('<', None)
      prevbutton.set_size_request(42, 20)
      fix.put(prevbutton, 810, ycoord+6)
      prevbutton.connect("clicked", self._onPrevButton, scale, scaleinds)
      nextbutton = Gtk.Button('>', None)
      fix.put(nextbutton, 850, ycoord+6)
      nextbutton.set_size_request(42, 20)
      nextbutton.connect("clicked", self._onNextButton, scale, scaleinds)

                   #The annotations around the current location:
      annotleft = Gtk.Label('')
      annotleft.set_justify(Gtk.Justification.LEFT)
      annotleft.set_size_request(360, 0)
      fix.put(annotleft, 50, ycoord+30)

      annotmiddle = Gtk.Label('')
      annotmiddle.set_justify(Gtk.Justification.CENTER)
      annotmiddle.set_size_request(400, 0)
      fix.put(annotmiddle, 208, ycoord+30)

      annotright = Gtk.Label('')
      annotright.set_justify(Gtk.Justification.RIGHT)
      annotright.set_size_request(360, 0)
      fix.put(annotright, 410, ycoord+30)
      annotlabs = (annotleft, annotmiddle, annotright)
      labels.append((gid, indlab, annotlabs))

      self._updateAnnotations(gid, node[gid][0]+1, idTOannots[gid], annotlabs)

      ycoord += hstep

    for scale, (gid, indlab, annotlabs) in zip(scales, labels):
      scale.connect("value-changed", self._updateScales, scales, indlab,
                    sizelabel, annotlabs, gid, node[gid][0], idTOannots[gid],
                    node.length)

       #Draw the window:
    window = Gtk.Window()
    window.set_title("Split a node")
    window.set_size_request(900, ycoord+20)
    window.set_position(Gtk.WindowPosition.CENTER)

       #Button to realize the fission:
    button = Gtk.Button("Split It",None)    
    button.set_size_request(500, 20)
    fix.put(button, 360, 10)
    button.connect("clicked", self._doSplit, window, node, scale) 

    vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    vbox.pack_start(fix, True, True, 0)
    window.add(vbox)    
    window.show_all()


  def _getSizeLabelStr(self, leftsize, rightsize):
    """
    Get the size label string.
    """
    return 'New Node Sizes: {}, {}'.format(leftsize, rightsize)


  def _makeScaleSlider(self, size, firstgindex, gid, ycoord, height, borderlist,
                       xcoord=80):
    """
    Make the slider bar with the scales on it for this genome.

    @param size: the size of the slider
    @param size: the first genome index for this node
    @param gid:  the gid for the firstgindex
    @param ycoord: the y coordinate to place the slider at
    @param height: the height of the slider area
    @param borderlist: list of border Indices
    @param xcoord: the x coordinate to place the slider at
    """
    scale = Gtk.Scale()
    scale.set_range(0, size-2)      #Exclude the extremities.
    scale.set_draw_value(False)     #Don't print the value.
    scale.set_size_request(720, height)
    scale.set_increments(1, 10)
    scale.set_digits(0)
    scaleinds = self._getScaleIndsFromGenomeInds(firstgindex, gid, borderlist)
    scaleinds = [i for i in sorted(set(scaleinds)) if i > 0 and i < size]
    self._addMarksToScale(scale, scaleinds)
    return scale, scaleinds

        

  def _getScaleIndsFromGenomeInds(self, firstgindex, gid, borderlist):
    """
    Return a list of border indices for the scale, given genomic indices.

    @param firstgindex: genomic index that index 1 of the scale corresponds to
    @param gid:   the genome id
    @param borderlist: the list of border Indices
    """
    scalelist = []
    for border in borderlist:
      if border.genome < firstgindex:
        scalelist.append(self.graph.idTOlen[gid] +
                         border.genome - firstgindex)
      else:
        scalelist.append(border.genome-firstgindex)

    return scalelist


  def _addMarksToScale(self, scale, indices):
    """
    Add marks to the scale at each given index.

    @param scale: the scale widget
    @type scale:  Gtk.Scale
    @param indices: the list of border indices
    """
    for index in indices:
      scale.add_mark(index, Gtk.PositionType.TOP, None)


  def _getLabelStr(self, gid, index):
    return '{}: {}'.format(gid, index) 


  def _onMerge(self, widget):
    """
    Merge two highlighted nodes.
    """
    assert(len(self.dwidget.highlight) == 2)
    graph = self.graph
    n1 = self.dwidget.highlight[0]
    n2 = self.dwidget.highlight[1]

    triple_new = []
    if n1.gids == n2.gids:            #horizontal fusion
      vertical = False
      if n1 > n2:
        n1,n2 = n2,n1

      for i in range(len(n1.triples)):
        triple_new.append((n1.triples[i][0],n1.triples[i][1],n2.triples[i][2]))

    elif n1.gids.isdisjoint(n2.gids): #vertical fusion
      vertical = True
      triple_new = sorted(n1.triples+n2.triples, key=operator.itemgetter(0))

    else:
      self.showMessage('BUG: trying to merge two nodes that cannot be.')
      return

   
    new = self.graph.replaceNodeMerge(triple_new, n1, n2, vertical)

      #Load graph with new nodes
    self.loadGraph(self.graph, self.dwidget, highlight=[])

      # Highlight nodes created by fission to see them easier
    graph = self.graph
    self.highlightNothing(self.dwidget)
    self.handleHighlightClick(self.dwidget, graph, new)

      # Zoom to the fission area
    self.dwidget.zoom_to_area(new.x1+200,new.y1-150, new.x2-200,new.y2+150)
      

  def _onAnnotation(self, widget, node: MNode):
    """
    Callback for annotation click.
    """
    self.toggle_annotation(node, self.graph.idTOlen) 


  def _exportAlignment(self, widget, node):
    """
    Export the alignement for the given node.
    """
    dialog = Gtk.FileChooserDialog("Export an alignment", self,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

    random_gid = next(iter(node.gids))
    filename = '{}_{}_{}-{}.fasta'.format(node.length, random_gid,
                                          node.idTOinterGEN[random_gid][0],
                                          node.idTOinterGEN[random_gid][1])
    dialog.set_current_name(filename)
    dialog.set_do_overwrite_confirmation(True)

    filter_fasta = Gtk.FileFilter()
    filter_fasta.set_name(".fasta")
    filter_fasta.add_pattern("*.fasta")
    dialog.add_filter(filter_fasta)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any file")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      fname = dialog.get_filename()
    else:
      fname = None

    dialog.destroy()

    if fname:
      records = list(SeqIO.parse(self.O.seqfile, 'fasta'))
      alignment = getGapplessAlignmentForPair(node, node, records, self.O.idTOi)
      AlignIO.write(alignment, fname, 'fasta')


  def _exportIndices(self, widget, left, right):
    """
    Export the alignement for the given node.
    """
    dialog = Gtk.FileChooserDialog("Export indices to file", self,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

    random_gid = next(iter(left.gids))
    filename = 'intervals_{}_{}-{}.txt'.\
               format(random_gid, left.idTOinterGEN[random_gid][0],
                      right.idTOinterGEN[random_gid][1])
    dialog.set_current_name(filename)
    dialog.set_do_overwrite_confirmation(True)

    filter_fasta = Gtk.FileFilter()
    filter_fasta.set_name(".txt")
    filter_fasta.add_pattern("*.txt")
    dialog.add_filter(filter_fasta)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any file")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      fname = dialog.get_filename()
    else:
      fname = None

    dialog.destroy()

    if fname:
      self._writeIntervalsFile(fname, left, right)


  def _writeIntervalsFile(self, fname, left, right):
    """
    Write a file where each line is of the form 'A:2000-5000'.
    """
    with open(fname, 'w') as f:
      for gid in sorted(left.gids):
        f.write('{}:{}-{}\n'.format(gid, left.idTOinterGEN[gid][0],
                                    right.idTOinterGEN[gid][1]))


  def _onClick(self, widget, url, event):
    """
    Handle a node click event.
    """
    if url == NAMENODELEFT or url == NAMENODERIGHT:#If a name node was clicked
      self.highlightNothing(widget)                #highlight nothing.
      return True

    self.showMessage()
    self.showMessageBottom()
    if event.button == 2:             #Drill out of a node:
      return self._goBack(widget)
      
    graph = self.graph
    x, y = int(event.x), int(event.y)
    shape = widget.get_element(x, y)
    if(not shape or
       (not isinstance(shape, DNode) and not isinstance(shape, ClickTarget))):
      self.highlightNothing(widget)   #If we've clicked nowhere or on an edge.
      return True
                                      #Show a menu for the node:
    elif isinstance(shape, ClickTarget) or event.button == 3:
      if isinstance(shape, ClickTarget):
        node = shape.node
      else:
        node = self.dwidget.get_element(event.x, event.y)

      if node:
        self._showMenu(widget, event, node)

    elif event.button == 1:             #Highlight a node:
      return self.handleHighlightClick(widget, graph, shape)


    return True

  def _goBack(self, widget):
    """
    Pop up the stack of graphs.
    """
    self.showMessage()
    if len(self.graphstack) > 1:
      oldgraph = self.graphstack.pop()
      self.graphTOattrs[oldgraph] = (widget.x, widget.y, widget.zoom_ratio,
                                     self.anchormode, self.minlength,
                                     self.showtogglebutton.get_active(),
                                     self.dwidget.highlight)
      graph = self.graph
      (x,y,zoom, self.anchormode,
       self.minlength, showall, newhighlight) = self.graphTOattrs[graph]
      self.loadGraph(graph, widget, (x,y,zoom), highlight=newhighlight,
                     showall=showall, sampleafter=None)
  
      self.updateLengthDisplay(self.minlength)
      self.updateLengthEntry(0)
      self.updateButtonStates()
      
    return True

  def _showExpandedGraph(self, widget, newwindow=False):
    """
    Show the graph without contracting any nodes.

    @param newwindow: open the next graph in a new window (irregardless of
                      whether self.newwintogglebutton is active)
    """
    self.showMessage()         #Blank message box.
    if not self.anchormode and not self.graph.graphIsContracted():
      return True           #The graph is already expanded.

    if widget.highlight and len(widget.highlight) == 2:  #If there are two
      graph = self.graph                                 #nodes highlighted.
      n1 = widget.highlight[0]
      n2 = widget.highlight[1]

    elif widget.highlight and len(widget.highlight) == 1:#1 node highlighted.
      n1 = widget.highlight[0]
      n2 = n1

    else:           #Transform the entire graph:
      graph = self.graph
      nodes = graph.getLinearExtension()   #Could FAIL if there is a cycle.
      n1, n2 = nodes[0], nodes[-1]

    if self.newwintogglebutton.get_active() or newwindow:
      self.spawnNewUI(n1, n2, False, True)
      return True

    self.showWaiting(True)
    self.loadGraphBetweenNodes(n1,n2, False, self.anchormode, self.minlength,
                               True)

    return True


  def _anchorsToGraph(self, widget, newwindow=False):
    """
    On a gograph event we switch from showing just the anchor to showing
    the whole graph.

    @param newwindow: open the next graph in a new window (irregardless of
                      whether self.newwintogglebutton is active)
    """
    self.showMessage()                                     #Blank message box.

    if self.anchormode:
      if widget.highlight and len(widget.highlight) == 2:  #If there are two
        graph = self.graph                                 #nodes highlighted.
        n1 = widget.highlight[0]
        n2 = widget.highlight[1]

      elif widget.highlight and len(widget.highlight) == 1:#1 node highlighted.
        self.showMessage('Highlight two or zero nodes!')
        return False

      else:           #Transform the entire graph:
        graph = self.graph
        if not graph.isDAG():
          self.showMessage('ERROR: there is a cycle in the graph.')
          return False
        nodes = graph.getLinearExtension()
        n1,n2 = nodes[0], nodes[-1]

      if self.newwintogglebutton.get_active() or newwindow:
        self.spawnNewUI(n1, n2, False)                     #New PhageUI.
        return True

      self.showWaiting(True)
      self.loadGraphBetweenNodes(n1,n2, False, self.anchormode, self.minlength)

    return True



  def _saveGraph(self, widget):
    """
    Pickle this graph along with the fasta file, and state information.
    """
    dialog = Gtk.FileChooserDialog("Save a .alpha file", self,
                                   Gtk.FileChooserAction.SAVE,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

    dialog.set_current_name('currentgraph.alpha')
    dialog.set_do_overwrite_confirmation(True)

    filter_alpha = Gtk.FileFilter()
    filter_alpha.set_name(".alpha")
    filter_alpha.add_pattern("*.alpha")
    dialog.add_filter(filter_alpha)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any file")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      fname = dialog.get_filename()
    else:
      fname = None

    dialog.destroy()

    if fname:
      with open(self.O.seqfile) as fastaf:
        with open(fname, 'wb') as f:
          graph = self.graph
          graph.makePickleable()
          pickle.dump((graph, fastaf.readlines(), self.minlength,
                       self.anchormode, graph.showingAll()),
                       f, pickle.HIGHEST_PROTOCOL)



  def _openGraph(self, widget):
    """
    Open a graph using either a fasta file or a .alpha file.
    """
    self.app.openFileDialog()


  def _doEnter(self, widget, newwindow=False):
    """
    Show a new graph from the node(s) that are highlighted.

    @note: if a single node is highlighted then drill into the node
    @note: activates the show button

    @param newwindow: open the next graph in a new window (irregardless of
                      whether self.newwintogglebutton is active)
    """
    self.showMessage()

    graph = self.graph
    if not widget.highlight or len(widget.highlight) != 2:
      if widget.highlight and len(widget.highlight) == 1: #1 node highlighted.
        self.showMessage('Entering node...')
        if self.drillIn(widget, graph, widget.highlight[0]):
          if not self.showtogglebutton.get_active():
            self.enableShowToggleButton(True)    #Show all detail.
          return True

        else:
          self.showMessage('You can only do this with a contracted vertex.')
          return False

      else:
        self.showMessage('Must have node(s) highlighted.')
        return False

    n1 = widget.highlight[0]
    n2 = widget.highlight[1]

        #If we are meant to creat a new window:
    if newwindow or self.newwintogglebutton.get_active():
      self.spawnNewUI(n1, n2, self.anchormode)
      return True

    self.showWaiting(True)
    return self.loadGraphBetweenNodes(n1, n2, self.anchormode,
                                      self.anchormode, self.minlength)


#}  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


  def spawnNewUI(self, n1: MNode, n2: MNode, anchormode, showexpanded=False):
    """
    Spawn a new window with the new graph computed between the given nodes.

    :param showexpanded: show all of the (big enough) nodes in the next graph
    """
    if n1 > n2:
      n1, n2 = n2, n1

    newlength = self.getLengthEntry()

    try:
      idTOinterval = nodesToIntervals(n1, n2, newlength, True)
    except GraphOnIntervalError as e:
      self.showMessage(f'No graph created: {e}')
      return

    firstgid = sorted(n1.gids)[0]
    title = f'{firstgid}: ' +\
            f'{(n1.idTOinterGEN[firstgid][0], n2.idTOinterGEN[firstgid][1])}'

    filename = self.O.alphafile if self.O.alphafile else self.O.seqfile
    subtitle = f'Alpha - ({os.path.basename(filename)})'

    p = self.app.spawnGetGraph(self.O.seqfile, newlength, n1.gids,
                               self.graph._norm, idTOinterval, anchormode, self.O,
                               showexpanded, (title, subtitle),
                               self._childQ, self.removecycles)

    self._addLoadingBox(p, title)
    self._pidTOchild[p.pid] = p


  def _addLoadingBox(self, p: mp.Process, name: str):
    """
    Create an overlay box while computing the graph to open in another PhageUI.
    """
    builder = Gtk.Builder()
    builder.add_objects_from_file(UI_SPECIFICATION, ('newui_box',
                                                     'newui_label',
                                                     'newUI_kill_button'))
    box = builder.get_object('newui_box')
    box.set_name(str(p.pid))

    label = builder.get_object('newui_label')
    label.set_text(f' Creating Graph \n {name} ')

    killbutton = builder.get_object('newui_kill_button')
    killbutton.connect('clicked', self._killChild, p, box)

    self.graphoverlaybox.pack_start(box, False, False, 2)
    self.graphoverlaybox.show_all()


  def _killChild(self, button: Gtk.Button, p: mp.Process, box: Gtk.Box):
    """
    Terminate this child and remove the box widget.
    """
    p.terminate()
    self._pidTOchild.pop(p.pid)
    box.destroy()



  def _spawnChildCommunicationHandler(self):
    """
    Spawn the thread that will monitor the messages coming back from the
    child processes that open new PhageUI windows.
    """
    self._childmonitorT = Thread(target=self._monitorChildStates, daemon=True)
    self._childmonitorT.killevent = Event()
    self._childmonitorT.start()

  def _monitorChildStates(self: 'PhageUI'):
    """
    Wait for messages coming back from the child PhagUIs.
    """
    while True:
      try:
        message = self._childQ.get(True, 2)
      except queue.Empty:
        pass
      except EOFError:
        return

      else:
        if self._childmonitorT and self._childmonitorT.killevent.is_set():
          return

        if message:
          mtype, value = message
          if mtype == self.NEWGRAPH_DONE:
            self._pidTOchild.pop(value)
            for child in self.graphoverlaybox.get_children():
              if child.get_name() == str(value):
                child.destroy()

          elif mtype == self.NEWGRAPH_ERROR:
            self.showMessage(value)

          else:
            print(f'ERROR: unknown message type received!')

  def _getExtremities(self, graph):
    """
    Return the largest and smallest in the given graph (based on in/out degree).
    """
    for n in graph.G.nodes():
      if graph.G.in_degree(n) == 0:
        smallest = n
      if graph.G.out_degree(n) == 0:
        largest = n

    return smallest, largest


  def _getExtremities_USING_GID(self, nodes):
    """
    Return the largest and smallest of the given nodes (based on random gid).
    """
    smallest = largest = nodes[0]
    gid = next(iter(smallest.gids))
    for n in nodes:
      if smallest.idTOinter[gid] > n.idTOinter[gid]:
        smallest = n
      if largest.idTOinter[gid] < n.idTOinter[gid]:
        largest = n
    return smallest, largest



  def loadGraphBetweenNodes(self, n1: MNode, n2: MNode, newanchormode,
                            oldanchormode, oldminlen, expand=False):
    """
    Call app.graphintervalfunc (using getGraph()) to recompute the graph
    between these two node and then display it.

    :note: if newwintogglebutton is active, then open a new window

    :param n1: node 1
    :type n1:  an MNode
    :param n2: node 1
    :type n2:  an MNode
    :param newanchormode: the anchor mode for the new graph
    :param oldanchormode: the anchor mode that we should default to in case of
                          a problem
    :param oldminlen: the min match length that we should default to in case of
                      a problem
    :param expand:    do not contract the resulting graph.
    """
    if self._loadgraphT:
      self.showMessage('Already computing new graph!')
      return

    if n1.gids == n2.gids:
      if n1 > n2:     #Make sure n1 is on the left.
        n1,n2 = n2,n1

      newlength = self.getLengthEntry()
      if newlength == None:
        return False

      message = 'graph'
      if newanchormode:
        message = 'anchors'

      idTOinterval = nodesToIntervals(n1, n2, self.minlength, True)

      widget = self.dwidget
      oldstate = (widget.x, widget.y, widget.zoom_ratio, oldanchormode,
                  oldminlen, self.showtogglebutton.get_active(),
                  self.dwidget.highlight)

      self._getgraphQ = self.app.mpcontext.Queue() #Wait for InterGraph on Queue
      self._loadgraphT = Thread(target=self._loadGraphBetweenNodes_finish,
                                args=(self._getgraphQ, oldstate), daemon=True)
      self._loadgraphT.start()

      self._getgraphK = self.app.mpcontext.Event()
      args = (self.O.seqfile, self._getgraphQ, newlength, n1.gids, (None, None),
              n1.getNormFuncs(), idTOinterval, newanchormode, self.O, expand,
              self.graph.titlepair, not expand, False, self.O.annotations,
              self.app.matchdir, False, None, None, self._getgraphK)
      self._getgraphP = self.app.mpcontext.Process(target=getGraph,
                                                   args=args, daemon=False)
      self._getgraphP.start()                      #Compute the InterGraph

      return True
    else:
      self.showMessage('Highlight two nodes with the same phage sets.') 
      self.showWaiting(False)

    return False


  def _loadGraphBetweenNodes_finish(self, waitQ: queue, oldstate: Tuple):
    """
    Wait for, and then load a graph computed by the getGraph().
    """
    graph, O, minlen, pid, message = waitQ.get()
    self._loadgraphT = None
    self._getgraphP = None
    self._getgraphK = None
    self._getgraphQ = None
    self.showWaiting(False)

    if not graph:
      self.showMessage(message)
      return False

    oldstacksize = len(self.graphstack)

    widget = self.dwidget
    self.graphTOattrs[self.graph] = oldstate
    self.graphstack.append(graph)
    self.loadGraph(graph, widget)

    self.minlength = minlen
    self.updateLengthDisplay(minlen)
    self.updateLengthEntry(0)

    self.anchormode = graph.isAnchorGraph()
    self.updateButtonStates()

    return False                      #Remove from GLib event sources.


  def _getGraphOnIntWrapper(self, getgraphargs, oldstate):
    """
    The multithreaded wrapper meant to call self.app.graphintervalfunc.
    Return a graph computed between the given nodes. Only compute the anchors
    if anchormode is True. The return value is sent to
    loadGraphBetweenNodes_finish() so that the result can be loaded into the
    DotWidget.

    @note: returns (using GLib.idle_add) the tuple (graph, minlength, cycles)
           where minlength is the new min length of matches and cycles is
           True if there were cycles detected.

    @param getgraphargs: tuple containing the arguments to give to
                         self.app.graphintervalfunc()
    @param oldstate:     tuple to push to the graphstack after successful
                         computation of new graph
    """
    try:
      G,l,cycle = self.app.graphintervalfunc(*getgraphargs)

    except GraphOnIntervalError as e:
      return None, None, None, e, oldstate
    
    except VmatchError as e:
      return None, None, None, e, oldstate

    GLib.idle_add(self._loadGraphBetweenNodes_finish, G, l, cycle, None,
                  oldstate)





  def handleHighlightClick(self, widget, graph, node):
    """
    Highlight the appropriate nodes given a click.
    """
                               #If there's aready two nodes highlighted:
    if widget.highlight and len(widget.highlight) == 2:
      if node in widget.highlight:  #Unhighlight already activated node.
        widget.highlight.remove(node)
        newhighlight = widget.highlight[:]
        #widget.set_highlight(None)
        #widget.set_highlight(newhighlight)
        self._highlightSingleNode(widget, newhighlight[0])
        return True
      else:
        self._highlightSingleNode(widget, node)
        return True
                               #If there's aready something highlighted:
    if widget.highlight and widget.highlight[0] in graph:
      first = widget.highlight[0]
      second = node
      if first == second:
        self.highlightNothing(widget)
        return True

      if first.gids == second.gids:  #If they have the same set of gids.
        widget.set_highlight(widget.highlight+[node])
        self._showSequence(node)
        return True

      elif self.is_fusionnable(first,second):
        self._showSequence(node)
        widget.set_highlight(widget.highlight+[node])
        return True

      else:
        self._highlightSingleNode(widget, node)
        return True
    else:                     #If this is the first highlighted node.
      self._highlightSingleNode(widget, node)
      return True


  def _highlightSingleNode(self, widget, node):
    """
    Highlight a single node.  If it's short enough, then show the nucleotides
    in the corner.
    """
    self._showSequence(node)

             #Highlight the node:
    widget.set_highlight(None)
    widget.set_highlight([node])

    
  def _showSequence(self, node):
    """
    Show the nucleotide sequence from this node if it's short enough.
    """
             #Show the nucleotide sequence:
    if node.length <= MINDISPLAY_SIZE and not node.isDummy():
      gid, start, end = node.triplesGEN[0]
      record = list(SeqIO.parse(self.O.seqfile, 'fasta'))[self.O.idTOi[gid]]
      if start < end:
        seq = record.seq[start:end]
      else:
        seq = record.seq[start:]+record.seq[:end]
      self.showMessageBottom(str(seq))



  def is_fusionnable(self, first, second):
    if first.gids != second.gids:
      for g in first.gids:
        for g2 in second.gids:
          if g==g2:
            return False
      if first.length==second.length:
        if(nx.has_path(self.graph.Gact,first,second) or
           nx.has_path(self.graph.Gact,second,first)):
          return False
        return True
    else : 
      #if n'existe pas de chemin entre les 2
      for n in self.graph.Gact.successors(first):
        if n is second:
          return True
      for n in self.graph.Gact.successors(second):
        if n is first:
          return True
      if(nx.has_path(self.graph.Gact,first,second) or
         nx.has_path(self.graph.Gact,second,first)):
        return False
      else:
        return True



  def drillIn(self, widget, graph: InterGraph, node: MNode):
    """
    Drill down into the node to load the subgraph represented therein.

    @param widget: the widget to display things in
    @param graph:  the graph with the node we drill into
    @param node:   the node to drill into
    @type node:    an xdot DNode

    @return: True if a new graph was loaded, otherwise false.
    """
    if node.isContracted():
          #NOTE: a node created by a fission will not be in the stack. Thus
          #      we recompute the graph for the intervals without adding it
          #      to the stack.
      if node not in graph.nodeTOsubgraph:  #Recompute the subgraph.
        return self.loadGraphBetweenNodes(node, node, False, False,
                                          self.minlength)
      subgraph = graph.nodeTOsubgraph[node]
      #subgraph = n.getSubgraph()

      self.loadingStart()
      if not subgraph.iscontracted:
        subgraph.categorizeBlocks(contract=True, ignoreends=True)

      self.graphstack.append(subgraph)
      self.graphTOattrs[graph] = (widget.x, widget.y, widget.zoom_ratio,
                                  self.anchormode, self.minlength,
                                  self.showtogglebutton.get_active(),
                                  self.dwidget.highlight)
  
      zoomtrio = None
      showall = None    #NOTE: we may want to ignore this and always showall
      if subgraph in self.graphTOattrs:
        (x,y,zoom,_,length,showall,highlight) = self.graphTOattrs[subgraph]
        if length == self.minlength:
          zoomtrio = (x,y,zoom)

      if not showall:
        showall = True
      self.loadGraph(subgraph, widget, zoomtrio)

      if showall != None:
        self.showtogglebutton.set_active(showall)
  
      self.loadingStop()
      return True

    return False

  def enableLengthEntry(self, state=True):
    """
    Set the length entry box state to the given state.

    @param state: True if this should be active
    @type state:  boolean
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.lengthenter.set_sensitive(state)

  def updateButtonStates(self):
    """
    Enable or disable the buttons according to the state of the current PhageUI.
    """
    if self.anchormode:                               #Viewing anchors.
      self.enableGoGraphButton()
      self.enableExpandButton()
      self.enableMoveSwitch(False)
      self.moveswitch.set_active(False)
      self.enableShowToggleButton(False)

    elif not self.graph.graphIsContracted():          #Viewing expanded graph.
      self.enableGoGraphButton(False)
      self.enableExpandButton(False)
      self.enableMoveSwitch(True)
      self.enableShowToggleButton(True)

    else:                                             #Viewing contracted graph.
      self.enableGoGraphButton(False)
      self.enableExpandButton(True)
      self.enableMoveSwitch(True)
      self.enableShowToggleButton(True)

  def enableGoGraphButton(self, state=True):
    """
    Set the go graph button state to the given state.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.gographbutton.set_sensitive(state)

  def enableExpandButton(self, state=True):
    """
    Set the expand graph button state to the given state.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.expandbutton.set_sensitive(state)


  def enableMoveSwitch(self, state=True):
    """
    Set the toggle button state to the given state.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.moveswitch.set_sensitive(state)

  def enableShowToggleButton(self, state=True):
    """
    Set the toggle button state to the given state.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.showtogglebutton.set_sensitive(state)

  def updateLengthEntry(self, text):
    """
    Load the given text into the length entry box.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.lengthenter.set_text(str(text))

  def updateLengthDisplay(self, text):
    """
    Load the given text into the length display box.
    """
    #A vestige of when PhagUI was not derived from DotWindow
    self.lengthdisplay.set_text(str(text))

  def getLengthEntry(self):
    """
    Read the length from the length entry box.
    """
    try:
      length = int(self.lengthenter.get_text())
    except:
      self.showMessage('Must give an integer in the length box!')
      return None
    return length

  def setOpenVisibility(self, state=True):
    self.openbutton.set_visible(state)

  def setWindowTitle(self, title, subtitle):
    self.headerbar.set_title(title)
    self.headerbar.set_subtitle(subtitle)


  def highlightNothing(self, widget):
    """
    Highlight no nodes.
    """
    self.findentry.set_text('')
    widget.set_highlight(None)


  def loadGraph(self, graph:InterGraph, widget, zoomtrio=None, fast=False,
                sampleafter=8, highlight=[], showall=None):
    """
    Load the given graph and set the given zoom in the given widget.

    @note: Put the graph onto the graphstack before calling this so that the
           stacklength is displayed properly.  We modify the graphstack if
           sampling is done.

    @param graph:       the graph to load
    @type graph:        MatchGraph (or derived class)
    @param widget:      the widget to load it into
    @param zoomtrio:    the position and zoom to set the window to
    @type  zoomtrio:    (x,y,zoom)
    @param fast:        do a quick rendering of the graph
    @param sampleafter: sample the nodes after the graph has this number of
                        nodes. if None, then never sample
    @param highlight:   list of nodes in the graph to highlight
    @param showall:     if this is not None, then set the toggle state to this
    """
    if showall != None:
      self.showtogglebutton.set_active(showall)

    if graph.isAnchorGraph():
      if sampleafter != None and graph.size() > sampleafter and graph.isDAG():
        newgraph = graph.getSampling(addends=self._firsttime)
        self._firsttime = False       #Sample the nodes if acyclic.
        if newgraph.size() > 3:
          graph = newgraph
          self.graphstack[-1] = graph

      graph.prepare(fast, self.O.idTOstr, True, False)
    else:
      showsmall = self.showtogglebutton.get_active()

      graph.prepare(fast, self.O.idTOstr, showsmall=showsmall)

    self.norm = graph.getNormFuncs()

    self.set_dotcode(dgraph=graph)

    self._transferHighLightByURL(highlight, widget.graph.nodes)

    if zoomtrio:
      widget.x, widget.y, widget.zoom_ratio = zoomtrio

    stacksize = len(self.graphstack)-1
    stackstr = '{:<3} {}'.format(stacksize, '|'*stacksize)
    self.stacklength.set_text(stackstr)

    self.enableShowToggleButton(not self.anchormode)
    self.enableMoveSwitch(not self.anchormode)


  def _transferHighLightByURL(self, fromnodes, tonodes):
    """
    Find the nodes in tonodes that correspond to fromnodes, and then
    highlight them.  Use the URL to do so.

    @param fromnodes: nodes that should be highlighted in tonodes
    @param tonodes:   nodes from which we choose some to be highlighted
    """
    if fromnodes:
      urls = [n.url for n in fromnodes]  #Find codes of previously highlighted
    else:
      urls = []
    newhighlight = [n for n in tonodes if n.url in urls]
    self.dwidget.set_highlight(newhighlight) #and highlight the new ones.


  def showWaiting(self, show=True):
    """
    Show or hide the waiting overlay.
    """
    self.waitingoverlaybox.set_no_show_all(not show)
    if show:
      self.waitingoverlaybox.show_all()
    else:
      self.waitingoverlaybox.hide()

  def loadingStart(self, name=''):
    """ Print a loading message.  """
    self.showMessage('LOADING '+name+'...')

  def loadingStop(self):
    """ Stop the loading message.  """
    self.showMessage()

  def showMessage(self, message=''):
    """
    Show a warning in an overlay.

    @note: if no argument is given, then make remove the overlay
    """
    self.messagelabel.set_text(message)
    self.messageoverlaybox.set_visible(message)

    self.messagelabel.show_all()

  def showMessageBottom(self, message=''):
    """
    Show a warning in the lower corner of the window.

    @note: if no argument is given, then blank out the message box.
    """
    self.messagetext.set_markup(message)
    self.messagetext.show_all()

  def smartZoom(self):
    """
    Zoom to fit if the graph is small enough, otherwise zoom to the leftmost
    node.
    """
    rect = self.dwidget.get_allocation()
    zoomratio = self.dwidget.graph.width/float(rect.width)
    if zoomratio > MAX_ZOOM_RATIO:
      self.zoomToLeftmost()
    else:
      self.dwidget.zoom_to_fit()


  def zoomToLeftmost(self):
    """
    Zoom in the window to the leftmost node in the graph.
    """
    self.dwidget.zoom_to_leftmost()

  @property
  def graph(self) -> InterGraph:
    return self.graphstack[-1]
  @graph.setter
  def graph(self, graph: InterGraph):
    self.graphstack[-1] = graph


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -




class VmatchError(Exception):
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return self.value


class Index(object):
  """
  Maintains an index's value for genomic coordinates and graph coordinates
  (these are different when a graph is normalized).

  @ivar genome: the genome index
  @ivar graph:  the graph index
  @ivar node:   the index relative to the node it's in
  @ivar start:  True if this is a start index
  """

  def __init__(self, genomei=None, isstart=True, gid=None, first=None,
               graphi=None, normfuncs: NormIndFuncs=None):
    """
    Make in Index.

    :param genomei:   genome index
    :param isstart:   True if this index is a start index
    :param gid:       the gid for normalizing the index with (only needed if
                      we are normalizing).
    :param first:     the first index corresponding to the node
    :param graphi:    graph index
    :param normfuncs: the object that knows how to normalize the indices
    """
    if genomei != None:
      assert normfuncs
      self.genome = genomei
      if normfuncs.normalizing():
        if gid == None:
          raise(Exception('Must give gid when constructing normalized Index.'))
        self.graph = normfuncs.I(gid, genomei)
        if self.graph == None:   #The index should be at that end.
          self.graph = normfuncs.I(gid, genomei-1)
 
        if first:
          first = normfuncs.I(gid, first)
          if first == None:      #The index should be at that end.
            first = normfuncs.I(gid, first-1)
      else:
        self.graph = genomei

    elif graphi != None:
      assert normfuncs
      self.graph = graphi
      if normfuncs.normalizing():
        if gid == None:
          raise(Exception('Must give gid when constructing normalized Index.'))


        asself.graph = normfuncs.Ii(gid, graphi)
      else:
        self.graph = graphi

    else:
      self.graph = 0
      self.genome = 0

    if first != None:        #Set the node index.
      self.node = self.graph-first
    else:
      self.node = None

    self.start = isstart






################################################################################
################################################################################


def getGraph(seqfile, returnqueue: mp.Queue, minlength=None,
             usegids=None, normpair=(None, None), normfuncs: NormIndFuncs=None,
             idTOinterval=None, anchormode=True, O: InputInfo=None,
             showexpanded=False, titlepair=None, checkforcycles=True,
             removecycles=False, annotations: Annotations=None,
             matchdir: str='', addends=False, messagefunc: Callable=None,
             cyclebumpfunc: Callable=None,
             killevent: Event=None) -> Tuple[InterGraph, int, int]:
  """
  Return on the return queue a new graph to be opened. This could be an anchor
  graph, a contracted graph, or an expanded graph.

  :warn: modifies idTOinterval
  :note: meant to be called using app.mpcontext.Process in a new process

  :param seqfile:      fasta file
  :param returnqueue:  put the return value on this queue
  :param minlength:    minimum match length
  :param usegids:      gids is a set of gids to use
  :param normpair:     the (gid, index) where normalizing should begin at
  :param normfuncs:    the functions to use when normalizing indices
  :param idTOinterval: restrict matches to these intervals.  for intervals
                       with pairs of length 1, include matches that correspond
                       to the given interval.  (WARNING: modified)
  :param anchormode:   don't show the entire graph, just anchors
  :param O:            the InputInfo describing the seqfile
  :param showexpanded: if anchormode is False, then contract nodes?
  :param toplevel:     if False, change things (e.g. hide the Open button and
                       don't add the end nodes)
  :param titlepair:    (title, subtitle)
  :param contextqueue: a Queue used to send information to the parent process.
                       A message including just the child PID will be send
                       when the child is going to show itself.
  :param checkforcycles: change matchlen until acyclic (if anchormode=False)
  :param removecycles: remove cycles from the new graph
  :param annotations:  the Annotations
  :param matchdir:     temp dir to store matches
  :param addends:      add the end min and max nodes to the graph
  :param messagefunc:  send status update messages to this function
                       (can only be used when getGraph called as Thread)
  :param cyclebumpfunc:send cycle number updates to this function
                       (can only be used when getGraph called as Thread)
  :param killevent:    kill computatation if an event is sent to this

  :return: nothing. Put a new InterGraph, the InputInfo, the matchlength for
           the graph, and this pid into the returnqueue. If the killevent is
           received then put nothing on the queue.
  """
  if not O:
    O = InputInfo(selectgenomes=bool(usegids))
    O.setGenomeMapping(seqfile, includegids=usegids)

  if annotations and not annotations.isReady(): #Download
    if not annotations.download(O.idTOstr, messagefunc):
      annotations = None                        #Disable annotations
      O.annotations = None

  if killevent and killevent.is_set(): return None

  if idTOinterval:                #Make sure matches are computed only for the
    if usegids:
      O.fillMissingIdTOint(idTOinterval, usegids)
    else:
      O.setIdTOint(idTOinterval)  #intervals with gids.

  try:
    if not normfuncs:
      normfuncs = NormIndFuncs(O.printnormalized)

    if not anchormode:   #Display the whole graph:
      eqclasses, ig, matchlen, cyclesdetected =\
        recomputeClassesForIntervals(O.idTOint, minlength, O, matchdir,
                                     usegids, normfuncs,
                                     checkforcycles, removecycles,
                                     True, addends, messagefunc,
                                     cyclebumpfunc, annotations, killevent)

      if killevent and killevent.is_set(): return

      if not ig:
        raise GraphOnIntervalError('No matches in that interval.')

      if not showexpanded and checkforcycles:
        ig.categorizeBlocks(contract=True, ignoreends=False)

    else:                 #Display only the anchors:
      eqclasses, _, matchlen = getMatchesAndEQClasses(O, minlength, matchdir,
                                                      messagefunc)

      if killevent and killevent.is_set(): return

      ig, matchlen = getAcyclicAnchorGraph(eqclasses, O, matchlen, usegids,
                                           normpair, normfuncs,
                                           addends=addends,
                                           annotations=annotations,
                                           matchdir=matchdir,
                                           cyclebumpfunc=cyclebumpfunc,
                                           killevent=killevent)

  except GraphOnIntervalError as e:
    message = f'Cannot compute graph:\n\t{e}'
    print(message)
    if messagefunc: messagefunc(message)
    returnqueue.put((None, None, None, os.getpid(), message))
    return
  except UnrelatedPhagesError as e:
    message = f'Cannot compute graph:\n\t{e}'
    print(message)
    if messagefunc: messagefunc(message)
    returnqueue.put((None, None, None, os.getpid(), message))
    return
  except VmatchError as e:
    message = f'Cannot compute graph:\n\t{e}'
    print(message)
    if messagefunc: messagefunc(message)
    returnqueue.put((None, None, None, os.getpid(), message))
    return
  except GTError as e:
    message = f'Problem with installation:\n{e}'
    print(message)
    if messagefunc: messagefunc(message)
    returnqueue.put((None, None, None, os.getpid(), message))
    return

  if killevent and killevent.is_set(): return
  ig.setTitlePair(titlepair)
  returnqueue.put((ig, O, matchlen, os.getpid(), ''))


def getAcyclicAnchorGraph_OLD(eqclasses: List[FrozenSet[Match]],
                              O: InputInfo, minlength, usegids: Iterable[str],
                              normpair: Tuple=(None,None),
                              normfuncs: NormIndFuncs=None,
                              vmatch=False, getsampling=True, addends=False,
                              annotations: Annotations=None,
                              matchdir: str='',
                              cyclebumpfunc: Callable=None,
                              killevent: Event=None) -> Tuple[InterGraph, int]:
  """
  Divide the genome by "anchors" (equivalence classes on matches from all
  genomes). Return an InterGraph with the anchors as nodes.

  :note: if there is a cycle, attempt to "normalize" (i.e. rotate) the
         sequences by building the whole graph.
  :note: augment minlength until we have a cycleless anchor graph.
  :note: restrict matches to these intervals in O.idTOint.  for intervals
         with pairs of length 1, include matches that correspond
         to the given interval
  :note: if O.idTOint is non-empty but normpair isn't
         set, then normalize the graph to the beginning of the interval.

  :param eqclasses: a list of frozensets holding equivalent matches
  :param O:         the InputInfo object
  :param minlength: the matchlength
  :param usegids:   include only these gids in the graph
  :param normpair:  normalize the graph so that the first node is not before
                    this index (gid, index)
  :param normfuncs: the functions to use when normalizing indices
  :param vmatch:    use vmatch instead of gt (genometools)
  :param getsampling:  sample the anchors when there are many of them
  :param addends:   add start and end vertices
  :param cyclebumpfunc:send cycle number updates to this as we compute
  :param killevent: terminate computation on this event

  :return: the InterGraph and minlen, otherwise (None, None) if terminated.
  """
  if not normpair[0]:  #Set the normpair if it's not set.
    normpair = getNonEmptyInterval(O.idTOint)
  if not normfuncs:
    normfuncs = NormIndFuncs(O.printnormalized)

  if cyclebumpfunc: cyclebumpfunc(minlength)

  repsmessage = True
  while(True):        #Loop until we have found a DAG:
    anchors = getAnchors(eqclasses, usegids, annotations)

    if len(anchors) < 3:
      raise GraphOnIntervalError('There are not enough anchors in that interval.')

    if killevent and killevent.is_set(): return (None, None)
    if not normpair[0] and anchors:   #No normalization specified.
      ig = InterGraph(nodes=anchors, addloners=False, idTOstr=O.idTOstr,
                      addends=addends, compactnodes=False, seqfile=O.seqfile,
                      idTOi=O.idTOi, gidset=usegids, idTOlen=O.idTOlen,
                      anchorgraph=True, annotations=annotations,
                      normfuncs=normfuncs)
      #NOTE: We could add a cycle removal function (feedback vertex) here.
      if ig.isDAG():
        printnow('')
        break

    if killevent and killevent.is_set(): return (None, None)
      #Try to build the whole graph soas to normalize.
    ig = MatchGraph(eqclasses, idTOstr=O.idTOstr, compactnodes=False,
                    addends=False, seqfile=O.seqfile, idTOi=O.idTOi,
                    gidset=usegids, idTOlen=O.idTOlen, normfuncs=normfuncs)
    if len(ig.getGidsInComponents()) > 1:
      raise UnrelatedPhagesError(ig.getAccessionsInComponents())

    if killevent and killevent.is_set(): return (None, None)
    if normpair and not normfuncs.normalizing():
      ig.switchToNormalizedMode(*normpair) #Set MatchGraph.norm

    ig = InterGraph(eqclasses, idTOstr=O.idTOstr, compactnodes=False,
                    addends=False, seqfile=O.seqfile, gidset=usegids,
                    idTOi=O.idTOi, idTOlen=O.idTOlen,
                    anchorgraph=True, annotations=annotations,
                    normfuncs=normfuncs)

    if killevent and killevent.is_set(): return (None, None)
    if ig.isDAG():
      anchors = ig.getAnchors()
      ig = InterGraph(nodes=anchors, addloners=False, idTOstr=O.idTOstr,
                      compactnodes=False, seqfile=O.seqfile, idTOi=O.idTOi,
                      addends=addends, gidset=usegids, idTOlen=O.idTOlen,
                      anchorgraph=True, annotations=annotations,
                      normfuncs=normfuncs)
      if ig.isDAG():
        printnow('\nindices normalized.')
        break                  #If we are cycleless.

       #If there is still a cycle then bump up the match length:
    if repsmessage:
      printnow('searching for an acyclic match length. standby..', end='')
      repsmessage = False
    printnow('.', end='')

    ig.switchToOriginalMode()  #Switch back to normal indices.

    minlength += 1
    if cyclebumpfunc: cyclebumpfunc(minlength)
    if killevent and killevent.is_set(): return (None, None)

       #Compute the equivalences classes:
    eqclasses, _, minlength = getMatchesAndEQClasses(O, minlength, matchdir)
    if killevent and killevent.is_set(): return (None, None)

  if not repsmessage:
    printnow('m = '+str(minlength))

  return ig, minlength


def getAcyclicAnchorGraph(eqclasses: List[FrozenSet[Match]],
                          O: InputInfo, minlength, usegids: Iterable[str],
                          normpair: Tuple=(None,None),
                          normfuncs: NormIndFuncs=None,
                          vmatch=False, getsampling=True, addends=False,
                          annotations: Annotations=None,
                          matchdir: str='',
                          cyclebumpfunc: Callable=None,
                          killevent: Event=None) -> Tuple[InterGraph, int]:
  """
  Divide the genome by "anchors" (equivalence classes on matches from all
  genomes). Return an InterGraph with the anchors as nodes.

  :note: if there is a cycle, attempt to "normalize" (i.e. rotate) the
         sequences by building the whole graph.
  :note: augment minlength until we have a cycleless anchor graph.
  :note: restrict matches to these intervals in O.idTOint.  for intervals
         with pairs of length 1, include matches that correspond
         to the given interval
  :note: if O.idTOint is non-empty but normpair isn't
         set, then normalize the graph to the beginning of the interval.

  :param eqclasses: a list of frozensets holding equivalent matches
  :param O:         the InputInfo object
  :param minlength: the matchlength
  :param usegids:   include only these gids in the graph
  :param normpair:  normalize the graph so that the first node is not before
                    this index (gid, index)
  :param normfuncs: the functions to use when normalizing indices
  :param vmatch:    use vmatch instead of gt (genometools)
  :param getsampling:   sample the anchors when there are many of them
  :param addends:   add start and end vertices
  :param cyclebumpfunc: send cycle number updates to this as we compute
  :param killevent: terminate computation on this event

  :return: the InterGraph and minlen, otherwise (None, None) if terminated.
  """
  if not normpair[0]:  #Set the normpair if it's not set.
    normpair = getNonEmptyInterval(O.idTOint)
  if not normfuncs:
    normfuncs = NormIndFuncs(O.printnormalized)
  if cyclebumpfunc: cyclebumpfunc(minlength)

  # Find the upper bound of minlength by doubling the minlength as long as normalize_indices returns None
  dag_not_found = True
  while dag_not_found:
    ig = normalize_indices(eqclasses, O, usegids, normpair, normfuncs,
                           addends, annotations, killevent)

    if ig is not None:
      dag_not_found = False
    else:
      minlength = 2* minlength
      if cyclebumpfunc:
        cyclebumpfunc(minlength)

      # Recompute equivalence classes
      eqclasses, _, minlength = getMatchesAndEQClasses(O, minlength, matchdir)

  # Once a DAG has been found, we now know the upper bound (ceiling) of our minimum match length.
  # Now run a binary search to find the minimum match length
  ceiling = minlength
  floor = minlength//2

  while floor < ceiling:
    match_length = (ceiling + floor)//2
    if cyclebumpfunc:
      cyclebumpfunc(match_length)

    # Recompute equivalence classes
    eqclasses, _, match_length = getMatchesAndEQClasses(O, match_length, matchdir)

    ig = normalize_indices(eqclasses, O, usegids, normpair, normfuncs, addends,
                           annotations, killevent)

    if ig is None:
      floor = match_length + 1
    else:
      ceiling = match_length

  # In the case where our last computation resulted in the floor moving up,
  # the function will return ig as None. Therefore, recompute eqclasses and ig
  # outside the loop.
  eqclasses, _, _ = getMatchesAndEQClasses(O, floor, matchdir)
  ig = normalize_indices(eqclasses, O, usegids, normpair, normfuncs, addends,
                         annotations, killevent)
  return ig, floor


def normalize_indices(eqclasses: List[FrozenSet[Match]],
                      O: InputInfo, usegids: Iterable[str],
                      normpair: Tuple=(None,None), normfuncs: NormIndFuncs=None,
                      addends=False, annotations: Annotations=None,
                      killevent: Event=None) -> Optional[InterGraph]:
  """
  Normalize the indices. Return the InterGraph if it is acyclic, otherwise
  return None.
  """
  anchors = getAnchors(eqclasses,usegids,annotations)

  # If no normalization is specified, test if simplified anchor graph is a DAG.
  # If it is, then we are done.
  if not normpair[0] and anchors:
    ig = InterGraph(nodes=anchors, addloners=False, idTOstr=O.idTOstr,
                    addends=addends, compactnodes=False, seqfile=O.seqfile,
                    idTOi=O.idTOi, gidset=usegids, idTOlen=O.idTOlen,
                    anchorgraph=True, annotations=annotations,
                    normfuncs=normfuncs)
    # NOTE: We could add a cycle removal function (feedback vertex) here.
    if ig.isDAG():
      printnow('')
      return ig

  if killevent and killevent.is_set(): return None

  # If non-normalized anchor graph is not DAG, then we need to normalize
  # To normalize, build the whole graph first.
  ig = MatchGraph(eqclasses, idTOstr=O.idTOstr, compactnodes=False,
                  addends=False, seqfile=O.seqfile, idTOi=O.idTOi,
                  gidset=usegids, idTOlen=O.idTOlen, normfuncs=normfuncs)
  if len(ig.getGidsInComponents()) > 1:
    raise UnrelatedPhagesError(ig.getAccessionsInComponents())

  if killevent and killevent.is_set(): return None
  if normpair and not normfuncs.normalizing():
    ig.switchToNormalizedMode(*normpair)  # Set MatchGraph.norm

  # Now the that static (shared between all instances of MatchGraph)
  # normalization mappings are set, we can build the graph.
  ig = InterGraph(eqclasses, idTOstr=O.idTOstr, compactnodes=False,
                  addends=False, seqfile=O.seqfile, gidset=usegids,
                  idTOi=O.idTOi, idTOlen=O.idTOlen,
                  anchorgraph=True, annotations=annotations,
                  normfuncs=normfuncs)

  if killevent and killevent.is_set(): return None

  # If the complete graph is DAG, then simplify to anchor graph
  if ig.isDAG():
    anchors = ig.getAnchors()
    ig = InterGraph(nodes=anchors, addloners=False, idTOstr=O.idTOstr,
                    compactnodes=False, seqfile=O.seqfile, idTOi=O.idTOi,
                    addends=addends, gidset=usegids, idTOlen=O.idTOlen,
                    anchorgraph=True, annotations=annotations,
                    normfuncs=normfuncs)

    # If simplified anchor graph is DAG, then we are done.
    if ig.isDAG():
      printnow('\nindices normalized.')
      return ig

  # In the case that normalization does not give a DAG, then we know it is not
  # possible for the specified length. Return None to caller.
  else:
    ig.switchToOriginalMode()
    return None


############################################################################################
############################################################################################



def getNonEmptyInterval(idTOinterval):
  """
  Return the gid and start of the interval from a non-empty interval, otherwise
  return (None, None).
  """
  for gid, (start, end) in idTOinterval.items():
    if end-start > 2:
      return gid, start

  return None, None
