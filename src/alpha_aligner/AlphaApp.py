# Copyright 2020 Krister Swenson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import gi
import re
import queue
import multiprocessing as mp
from multiprocessing.context import SpawnContext
from threading import Thread
from threading import Event
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Pango
from collections import defaultdict as ddict
from typing import List
from typing import Tuple
from typing import Dict
from typing import Callable
from typing import FrozenSet
from typing import Iterable
from typing import Optional

from .Annotations import Annotations
from .PhageUI import PhageUI
from .PhageUI import getGraph
from .PhageUI import getAcyclicAnchorGraph
from .InterGraph import InterGraph
from .Match import Match
from .Match import NormIndFuncs
from .xdot import UI_SPECIFICATION
from .matchfuncs import MIN_LENGTH
from .userconfig import getEmail
from .userconfig import setEmail
from .userconfig import getLastOpened
from .iofuncs import InputInfo
from .iofuncs import loadGraphFromFile

from .pyinclude.usefulfuncs import printnow
from .pyinclude.usefulfuncs import invdict

## CONSTANTS:
THIS_FILE_DIR = os.path.dirname(os.path.realpath(__file__))





class OpenAlphaDialog(Gtk.ApplicationWindow):
  """
  The dialog to use on Alpha startup.  This gets all the necessary information
  from the user to launch the first PhageUI with at graph in it.

  1. This dialog is run from an AlphaApp handler thread.
  2. _finish() is called when "Launch Graph" is clicked, and app.launchAlphaApp
     will be called. App.launchAlphaApp will use a secondary
     thread is to compute the new graph (getGraph()). Update
     messages are sent to the OpenAlphaDialog as the computations are done.
  3. When finished, the compute thread will call add_idle(closeDialog) to
     close this dialog, before launching a new PhageUI with add_idlew)
  """

  def __init__(self, matchlen, parent: 'AlphaApp'):
    """
    Make a dialogue for choosing a file.
    You can retreive the user-set values with getValues() and close the dialog
    with closeDialog().

    :param parent:   the AlphaApp that opened this dialog
    """
    super().__init__(title='Welcome to Alpha', application=parent)

    self.matchlen = matchlen
    self.filename = None
    self.fastaoptions = []
    self.O: InputInfo = None   #Will eventually be set in _updateWithFasta()
    self.app: AlphaApp = parent

      #UI widgets:
    self.launch_button = None
    self.fastastack = None
    self.fastaswitcher = None
    self.genomelist = None
    self.normalizelist = None
    self.intervalslist: Gtk.ListStore = None

    self._acyclicsearchlabel = None
    self._workingbox = None
    self._sizetuple = (700, 500)

    self.set_default_size(*self._sizetuple)
    self.set_border_width(10)

    self._isready = False         #Switches once "Lauch Alpha" is pressed.
    self._thread: Thread = None   #Is set only after "Launch Alpha" is pressed.
  
    self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=3)

    self.overlay = Gtk.Overlay(vexpand=True, valign=Gtk.Align.FILL,
                               hexpand=True, halign=Gtk.Align.FILL)
    self.add(self.overlay)
    self.overlay.add(self.vbox)

    filehbox = Gtk.Box(spacing=3)

    # Text for fasta file entry:
    file_toolbar = Gtk.Toolbar(expand=False)
    self.filenametext = Gtk.Label(label='', ellipsize=Pango.EllipsizeMode.START)
    self.filenametext.set_width_chars(60)
    fastatoolitem = Gtk.ToolItem()
    fastatoolitem.add(self.filenametext)
    file_toolbar.add(fastatoolitem)
  
    # Fasta or Alpha file button:
    choose_fasta_button = Gtk.Button.new_with_label("Choose File")
    choose_fasta_button.connect("clicked", self._chooseFastaOrAlphaFile)
    choose_fasta_button.grab_focus()
    choose_fasta_button.get_style_context().add_class('suggested-action')
    filehbox.pack_start(choose_fasta_button, False, False, 0)
    filehbox.pack_start(file_toolbar, False, False, 0)

    self.vbox.pack_start(filehbox, False, False, 0)

    # Launch button:
    self.launch_button = Gtk.Button.new_with_label("Launch Alpha")
    self.launch_button.connect("clicked", self._finish)
    self.launch_button.set_sensitive(False)
    self.vbox.pack_start(self.launch_button, False, False, 0)
    self.fastaoptions.append(self.launch_button)

    # Match length entry:
    options_toolbar = Gtk.Toolbar()
    self.vbox.pack_start(options_toolbar, False, False, 0)

    matchtext = Gtk.Label(label='Minimum Match Length:')
    matchtext_item = Gtk.ToolItem()
    matchtext_item.add(matchtext)

    self.matchlenentry = Gtk.Entry()
    self.matchlenentry.set_tooltip_text('Match Length')
    self.matchlenentry.set_text(str(self.matchlen))
    self.matchlenentry.set_width_chars(4)
    self.matchlenentry.set_sensitive(False)
    self.fastaoptions.append(self.matchlenentry)
    mathlentoolitem = Gtk.ToolItem()
    mathlentoolitem.add(self.matchlenentry)
    self.matchlenentry.connect("key-release-event", self._on_length_enter)
    self.matchlenentry.connect("focus-in-event", self._on_length_focus)
    self.matchlenentry.connect("activate", self._finish)

    self.showannotationsbutton = Gtk.CheckButton('Use NCBI annotations.')
    self.showannotationsbutton.set_sensitive(False)
    self.showannotationsbutton.set_active(getEmail())
    if not getEmail():
      self.showannotationsbutton.connect('toggled', self._checkEmail)
    self.fastaoptions.append(self.showannotationsbutton)
    annotationitem = Gtk.ToolItem(margin_left=15)
    annotationitem.add(self.showannotationsbutton)

    options_toolbar.add(matchtext_item)
    options_toolbar.add(mathlentoolitem)
    options_toolbar.add(annotationitem)

    # Annotations checkbox:

    self.vbox.pack_start(Gtk.HSeparator(), False, False, 0)

    # Other options:
    self.whole_graph = Gtk.CheckButton('Show the whole graph, not just the anchors.')
    self.whole_graph.connect('toggled', self._warnWholeGraph)
    self.whole_graph.set_sensitive(False)
    self.fastaoptions.append(self.whole_graph)
    self.vbox.pack_start(self.whole_graph, False, False, 0)

    selectionhbox = Gtk.Box()
    self.vbox.pack_start(selectionhbox, False, False, 3)

    # Fill in the last opened file:
    lastopenedfile = getLastOpened()
    if lastopenedfile and os.path.exists(lastopenedfile):
      self._updateWithFasta(lastopenedfile)

    #self.connect('destroy', Gtk.main_quit)

    # Connect to signal to warn when someone tries to close the window:
    self._delete_handlerid = self.connect('delete-event', self._onDelete)

    # Set the icon for the window:
    #self.set_icon_from_file(f'{THIS_FILE_DIR}/images/alpha-aligner.svg')

    self.show_all()

  def closeDialog(self):
    """
    Close this dialog window.
    """
    self.disconnect(self._delete_handlerid) #Don't trigger close check dialog.
    self.close()

  def bumpCycleCheck(self, matchlength: int):
    """
    Update the "searching for acyclic matchlength" message in the working box
    overlay. Write a message the first time, and just add a "." the subsequent
    times.
    """
    message = f'Checking for cycles with match length {matchlength}...'
    if self._acyclicsearchlabel:
      self._acyclicsearchlabel.set_text(message)
    else:
      self._acyclicsearchlabel = Gtk.Label(message,
                                           halign=Gtk.Align.START,
                                           wrap=True)
      self._acyclicsearchlabel.get_style_context().add_class('working_text_class')
      self._acyclicsearchlabel.set_margin_left(20)
      self._acyclicsearchlabel.set_margin_right(20)
      self._workingbox.pack_start(self._acyclicsearchlabel, False, False, 5)

    self._workingbox.show_all()

  def updateMessage(self, message):
    """
    Add a message to the working box overlay.
    """
    if self._workingbox:
      label = Gtk.Label(message, halign=Gtk.Align.START, wrap=True)
      label.get_style_context().add_class('working_text_class')
      label.set_margin_left(20)
      label.set_margin_right(20)
      self._workingbox.pack_start(label, False, False, 5)
      self._workingbox.show_all()
    else:
      printnow('WARNING: unexpected state of _workingbox.')

  def useAnnotations(self):
    return self.showannotationsbutton.get_active()

  def getValues(self):
    """
    Return the values that this dialog has collected from the user.

    :return: (filename, matchlength, selectgids, gids, normpair,
              idTOinterval, wholegraph, O, showexpanded, toplevel),
             where selectgids is True if gids is a subset of the genomes in
             the fastafile, normpair is the (gid, index)
             where normalizing should begin at, idTOinterval restricts
             matches to these intervals, wholegraph tells us to show
             more than the anchors.
    """
       #Normalized index:
    normalizelist = [row for row in self.normalizelist if row[1]]
    normpair = (None, None)
    if normalizelist:
      normpair = (normalizelist[0][3], int(normalizelist[0][2]))

       #Interval Selection:
    idTOinterval = {}
    for _, start, end, gid in self.intervalslist:
      start, end = int(start), int(end)
      if end - start > 0:
        idTOinterval[gid] = (start, end)

       #Genome Selection:
    selectedgids = set(gl[2] for gl in self.genomelist if gl[1])

       #Update O:
    self.O = InputInfo(self.O.seqfile, selectedgids, idTOint=idTOinterval,
                       selectgenomes=len(selectedgids) == len(self.genomelist))
    self.O.setGenomeMapping(self.O.seqfile, includegids=selectedgids)

    if self.useAnnotations():
      self.O.annotations = Annotations()  #Turn on annotations by setting this.

    return (self.filename, self.matchlen,
            len(selectedgids) == len(self.genomelist),
            selectedgids, normpair,
            idTOinterval, self.whole_graph.get_active(),
            self.O, False, True)

  def _warnWholeGraph(self, widget=None):
    self.whole_graph.set_label(f'Show the whole graph, not just the anchors.'
                               f' (WARNING: use with caution)')

  def _onDelete(self, widget, *data):
    """
    Warn the user before closing.

    @note: this will be connected to an event only if close has not been called.
    """
    result = None
    if self._isready:   #If Launch Alpha button has been pressed
      builder = Gtk.Builder()
      builder.add_objects_from_file(UI_SPECIFICATION, ('quit_working_dialog',))
      quitdialog = builder.get_object('quit_working_dialog')

      result = quitdialog.run()
      quitdialog.destroy()

      if result == Gtk.ResponseType.CANCEL:
        return True

      if self._thread:
        self._thread.killevent.set()    #Tell the child thread to stop.
        self._thread.join(10)

    self.app.closeFileDialog()

  def _finish(self, widget=None):
    """
    Store the filename and matchlen if a file was chosen.  Otherwise, open
    the file choosing dialogue. Called when the "Launch Alpha" button is
    pressed.
    """
    self.filename = self.filenametext.get_text()

    try:
      self.matchlen = int(self.matchlenentry.get_text())
    except ValueError:
      self.matchlen = MIN_LENGTH  #If there's an invalid length in the entry.

    if os.path.isfile(self.filename):
      _, extension = os.path.splitext(self.filename)
      if re.match(r'\.al\w*', extension):   #An alpha file:
        ig, O, minlen, anchormode, _ = loadGraphFromFile(self.filename,
                                                         self.app.matchdir)
        if self.useAnnotations():
          O.annotations = Annotations()  #Activate annotations.

        self._thread = self.app.launchAlphaApp(ig, O.idTOint, O, minlen,
                                               anchormode)


      else:                                 #A fasta file:
           #Add the transparent box to cover everying.
        overlaybox = Gtk.Box(spacing=2, halign=Gtk.Align.FILL, hexpand=True,
                             valign=Gtk.Align.FILL, vexpand=True)
        self.overlay.add_overlay(overlaybox)

            #Add the box to put messages in.
        builder = Gtk.Builder()
        builder.add_objects_from_file(UI_SPECIFICATION, ('intro_working_box',))
        self._workingbox = builder.get_object('intro_working_box')
        overlaybox.pack_start(self._workingbox, False, False, 2)
        overlaybox.show_all()

        self._isready = True
          #Can't use GLib.idle_add() because the message updates would wait until
          #after the launchAlphaApp() process ended.
        (_, matchlength, _, gids, normpair,
         idTOinterval, wholegraph, O, showexpanded, toplevel) = self.getValues()
        self._thread = self.app.launchAlphaApp(None, idTOinterval, O,
                                               matchlength, not wholegraph)
        #threading.Thread(target=self.app.launchAlphaApp,
        #                 args= (idTOinterval, self.app, O,
        #                        matchlength, not wholegraph, toplevel),
        #                 daemon=True).start()
  
    else:
      self._showMissingFileDialogue(self.filename)
    
  def _showMissingFileDialogue(self, filename):
    """
    Print a warning that the given file does not exist.
    """
    showMessageDialog(self, f'Error: bad file!',
                      f'The file "{filename}" no longer exists, '
                      f'please choose a different file.')

  def _chooseFastaOrAlphaFile(self, widget):
    """
    Open a dialogue to choose a fasta file.
    """
    parentwindow = widget.get_toplevel()
    dialog = Gtk.FileChooserDialog("Choose a .fasta or .alpha file",
                                   parentwindow, Gtk.FileChooserAction.OPEN,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

    filter_both = Gtk.FileFilter()
    filter_both.set_name(".fasta or .alpha")
    filter_both.add_pattern("*.fa*")
    filter_both.add_pattern("*.alph*")
    dialog.add_filter(filter_both)

    filter_fasta = Gtk.FileFilter()
    filter_fasta.set_name(".fasta")
    filter_fasta.add_pattern("*.fa*")
    dialog.add_filter(filter_fasta)

    filter_alpha = Gtk.FileFilter()
    filter_alpha.set_name(".alpha")
    filter_alpha.add_pattern("*.alph*")
    dialog.add_filter(filter_alpha)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any file")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()
    filename = ''
    if response == Gtk.ResponseType.OK:
      filename = dialog.get_filename()

    if filename:
      _, extension = os.path.splitext(filename)
      if re.match(r'\.al\w*', extension):  #An alpha file.
        self._updateWithAlphafile(filename)
      else:                                #A fasta file.
        self._updateWithFasta(filename)

    dialog.destroy()


  def _chooseIntervalsFile(self, widget):
    """
    Open a dialogue to choose an intervals file.
    """
    parentwindow = widget.get_toplevel()
    dialog = Gtk.FileChooserDialog("Choose a file listing intervals",
                                   parentwindow, Gtk.FileChooserAction.OPEN,
                                   (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                    Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

    filter_txt = Gtk.FileFilter()
    filter_txt.set_name(".txt")
    filter_txt.add_pattern("*.txt")
    dialog.add_filter(filter_txt)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Any file")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()
    if response == Gtk.ResponseType.OK:
      self._fillIntervals(dialog.get_filename())

    dialog.destroy()


  def _updateWithAlphafile(self, filename):
    """
    Update the selection interface when given the .alpha file.
    """
    for option in self.fastaoptions:
      option.set_sensitive(False) 
    self.launch_button.set_sensitive(True)

    self.filenametext.set_text(filename)
    self.showannotationsbutton.set_sensitive(True)

    if self.fastastack:
      self.fastastack.destroy()
      self.fastaswitcher.destroy()
      self.loadintervals.destroy()
      self.resize(*self._sizetuple)
      self.genomelist = None
      self.normalizelist = None
      self.intervalslist: Gtk.ListStore = None

    self.show_all()


  def _updateWithFasta(self, filename):
    """
    Update the selection interface when given the fasta file.
    """
    if filename:
      [option.set_sensitive(True) for option in self.fastaoptions]
    else:
      [option.set_sensitive(False) for option in self.fastaoptions]

    self.filenametext.set_text(filename)

    ii = InputInfo()
    ii.setGenomeMapping(filename)
    self.O = ii
    iTOid = invdict(ii.idTOi)

    if self.genomelist:      #If we've already opened a fasta file:
      assert self.normalizelist and self.intervalslist
      self.genomelist.clear()
      self.normalizelist.clear()
      self.intervalslist.clear()

    else:
      self.fastastack = Gtk.Stack()
      self.fastastack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
      self.fastastack.set_transition_duration(1000)

             #Genome selection:
      self.genomelist = Gtk.ListStore(str, bool, str)  #hide GID in last str

      intview = Gtk.TreeView(model=self.genomelist)
      intview.connect('draw', self._hideIntervalsButton)
      renderer_text = Gtk.CellRendererText()
      intview.append_column(Gtk.TreeViewColumn("Genome", renderer_text,text=0))
      renderer_toggle = Gtk.CellRendererToggle()
      renderer_toggle.connect("toggled", self._toggle_cell)
      intview.append_column(Gtk.TreeViewColumn("Selection", renderer_toggle,
                                                active=1))

      self.fastastack.add_titled(intview, "select", "Genome Selection")


             #Normalization:
      self.normalizelist = Gtk.ListStore(str, bool, str, str)

      tv_norm = Gtk.TreeView(model=self.normalizelist)
      tv_norm.connect('draw', self._hideIntervalsButton)
      renderer_text = Gtk.CellRendererText()
      tv_norm.append_column(Gtk.TreeViewColumn("Genome", renderer_text, text=0))
      renderer_radio = Gtk.CellRendererToggle()
      renderer_radio.set_radio(True)
      renderer_radio.connect("toggled", self._on_cell_radio_toggled)
      tv_norm.append_column(Gtk.TreeViewColumn("Selection", renderer_radio,
                                               active=1))
      
      def text_edited(widget, path, text, thelist, index):
        digits = self._getDigits(text)
        if digits:
          thelist[path][index] = digits

      renderer_text = Gtk.CellRendererText()
      renderer_text.set_property("editable", True)
      renderer_text.connect("edited", text_edited, self.normalizelist, 2)
      tv_norm.append_column(Gtk.TreeViewColumn("Index", renderer_text,
                                               text=2))


             #Select Intervals:
      self.intervalslist = Gtk.ListStore(str, str, str, str)
      intview = Gtk.TreeView(model=self.intervalslist)
      renderer_text = Gtk.CellRendererText()
      intview.append_column(Gtk.TreeViewColumn("Genome", renderer_text, text=0))
      
      renderer_text = Gtk.CellRendererText()
      renderer_text.set_property("editable", True)
      renderer_text.connect("edited", text_edited, self.intervalslist, 1)
      intview.append_column(Gtk.TreeViewColumn("Start Index", renderer_text,
                                               text=1))

      renderer_text = Gtk.CellRendererText()
      renderer_text.set_property("editable", True)
      renderer_text.connect("edited", text_edited, self.intervalslist, 2)
      intview.append_column(Gtk.TreeViewColumn("End Index", renderer_text,
                                               text=2))

             #Add the normalize and select to the switcher:
      self.fastastack.add_titled(intview, "intervals", "Select Intervals")
      self.fastastack.add_titled(tv_norm, "normalize", "Normalize To")

             #Finish the switcher:
      self.switcherhbox = Gtk.Box(spacing=3)
      self.vbox.pack_start(self.switcherhbox, False, False, 0)

      self.fastaswitcher = Gtk.StackSwitcher()
      self.fastaswitcher.set_stack(self.fastastack)
      self.switcherhbox.pack_start(self.fastaswitcher, False, False, 0)

      self.loadintervals = Gtk.Button.new_with_label("Load Intervals")
      self.loadintervals.connect("clicked", self._chooseIntervalsFile)
      self.loadintervals.get_style_context().add_class('suggested-action')
      self.switcherhbox.pack_start(self.loadintervals, False, False, 10)

      scrollwin = Gtk.ScrolledWindow()
      scrollwin.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
      scrollwin.add(self.fastastack)

      self.vbox.pack_start(scrollwin, True, True, 0)

      intview.connect('draw', self._showIntervalsButton)

             #Add the rows for the Genome Selection:
    for i in sorted(iTOid):
      self.genomelist.append([ii.idTOstr[iTOid[i]], True, iTOid[i]])

             #Add the rows for the Normalize indices:
    for i in sorted(iTOid):
      self.normalizelist.append([ii.idTOstr[iTOid[i]], False, '0', iTOid[i]])

             #Add the rows for the intervals:
    for i in sorted(iTOid):
      self.intervalslist.append([ii.idTOstr[iTOid[i]], '0', '0', iTOid[i]])

    self.show_all()


  def _showIntervalsButton(self, widget, event):
    self.loadintervals.show()

  def _hideIntervalsButton(self, widget, event):
    self.loadintervals.hide()

  def _toggle_cell(self, widget, path):
    assert self.genomelist
    self.genomelist[path][1] = not self.genomelist[path][1]

  def _on_cell_radio_toggled(self, widget, path):
    selected_path = Gtk.TreePath(path)
    assert self.normalizelist
    for row in self.normalizelist:
      row[1] = (row.path == selected_path)


  def _on_length_focus(self, widget, entry):
    widget.set_text("")

  numberkeys = set([Gdk.KEY_0,Gdk.KEY_1,Gdk.KEY_2,
                    Gdk.KEY_3,Gdk.KEY_4,Gdk.KEY_5,
                    Gdk.KEY_6,Gdk.KEY_7,Gdk.KEY_8,
                    Gdk.KEY_9,Gdk.KEY_Left,Gdk.KEY_Right,
                    Gdk.KEY_Delete,Gdk.KEY_BackSpace])

  def _on_length_enter(self, widget, event, data=None):
    """
    This controls what can be entered into the lengthenter box.abs        """
    if event.keyval not in self.numberkeys: #Delete non-numbers.
      text = widget.get_text()
      pos = widget.get_position()
      widget.set_text(self._getDigits(text))
      pos = widget.set_position(pos-1)


  def _getDigits(self, s):
    """
    Return a string with no non-digit characters.abs        """
    return ''.join(c for c in s if c.isdigit())

  def _askUserForEmail(self):
    """
    Open a dialog to request an email from the user.
    """
    builder = Gtk.Builder()
    builder.add_objects_from_file(UI_SPECIFICATION, ('email_dialog',))
    emaildialog = builder.get_object('email_dialog')
    result = emaildialog.run()

    email = ''
    if result == Gtk.ResponseType.OK:
      email = builder.get_object('email_entry').get_text()

    emaildialog.destroy()

    return email

  def _checkEmail(self, widget):
    """
    Ask the user for an email address if it's not already set.

    @note: triggered when the box is toggled.
    """
    if self.showannotationsbutton.get_active() and not getEmail():
      email = self._askUserForEmail()
      if email:
        setEmail(email)
      else:
        self.showannotationsbutton.set_active(False)

  def _fillIntervals(self, filename):
    iTOid = self.O.iTOid
    idTOstr = self.O.idTOstr

    idTOpair: Dict[str, Tuple[int, int]] = {}
    with open(filename) as f:
      for line in f:
        if not line.isspace():
          m = re.match(r'\s*(\S+)\s*\:\s*(\d+)\s*-\s*(\d+)', line)
          if not m:
            builder = Gtk.Builder()
            builder.add_objects_from_file(UI_SPECIFICATION,
                                          ('bad_interval_spec_dialog',))
            badspecdialog = builder.get_object('bad_interval_spec_dialog')
            message = f'Expected line of form "A:1000-2000" ' +\
                      f'(where A is a genome ID or name).\n\n' +\
                      f'Got\n\t"{line.strip()}"\ninstead.'
            badspecdialog.format_secondary_text(message)
            badspecdialog.run()
            badspecdialog.destroy()
            return

          else:
            idTOpair[m.group(1)] = (m.group(2), m.group(3))

    for row in self.intervalslist:
      gid = row[3]
      if gid in idTOpair:
        row[1], row[2] = idTOpair[gid]



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class AlphaApp(Gtk.Application):
  """
  This is the GtkApplication that will manage all the windows and some of the
  application-wide signals.

  Special reserved Gtk.Application function names do the following:
  (https://wiki.gnome.org/HowDoI/GtkApplication)
    
    - do_startup: sets up the application when it first starts

    - do_shutdown: performs shutdown tasks

    - do_activate: shows the default first window of the application (like a
      new document). This corresponds to the application being launched by the
      desktop environment.

    - do_open: opens files and shows them in a new window. This corresponds to
      someone trying to open a document (or documents) using the application
      from the file browser, or similar.

  :ivar _opendialog:   in use OpenAlphaDialog (used to ensure uniqueness)
  :ivar _pidTOparentQ: map child window pid to parent communication queue
  """

  def __init__(self, mpcontext: SpawnContext, matchdir: str,
               graphintervalfunc: Callable, graph: Optional[InterGraph]=None,
               eqclasses: Optional[List[FrozenSet[Match]]]=None,
               O: Optional[InputInfo]=None, minlen=15,
               anchormode=False, fast=False, showall=None, getsampling=True,
               annotations: Optional[Annotations]=None, **kwargs):
    """
    Setup the Application, which will keep a record of the windows, and will
    handle the global quit shortcut (mainly for macOS). Call self.run() to
    start the AlphaApp.

    :note: all of the arguments with default values are meant to come from
           the commandline
    :note: do_setup and then do_activate are called with self.run()

    :param mpcontext:   multiprocessing Context to use to spawn processes
    :param matchdir:    the temporary directory to put matches in
    :param graph:       the InterGraph to show
    :param eqclasses:   the eqclasses to compute anchors for
                        (anchormode should also be True)
    :param graphintervalfunc: the function that returns a new graph, given
                              an interval
    :param O:           the overall shared InputInfo
    :param minlen:      the l to use with gt/vmatch
    :param anchormode:  show the anchors only
    :param fast:        do a quick and dirty layout with graphviz
    :param showall:     show all the nodes (not just the big enough ones)
    :param getsampling: if anchormode == True, then sample the anchors
    :param annotations: If given, then show annotations.
    """
    appid = 'swenson.alpha'
    super().__init__(application_id=appid,
                     flags=Gio.ApplicationFlags.NON_UNIQUE,
                     **kwargs)
    self._opendialog: OpenAlphaDialog = None

    self.mpcontext: SpawnContext = mpcontext
    self.matchdir: str = matchdir
    self.graphintervalfunc: Callable = graphintervalfunc

    self._defaultsubtitle = 'ALignment of PHAges'
    self._pidTOparentQ: Dict[int, SpawnContext.Queue] = ddict(lambda: None)

    self._graph: InterGraph = graph
    self._graphQ: SpawnContext.Queue = None
    self._eqclasses: List[FrozenSet[Match]] = eqclasses

    self._O: InputInfo = O
    self._minlen: int = minlen
    self._anchormode: bool = anchormode
    self._fast: bool = fast
    self._showall: bool = showall
    self._getsampling: bool = getsampling

  def do_startup(self):
    """
    Connect startup handlers before do_activate.
    """
    Gtk.Application.do_startup(self)

    action = Gio.SimpleAction.new("quit", None)
    action.connect("activate", self.on_quit)
    self.add_action(action)

  def do_activate(self):
    """
    If self._graph or self._eqclasses are not given, then Open the
    OpenAlphaDialog.
    """
          #Open a PhageUI with a graph or make a graph from eqclassses:
    if self._graph or self._eqclasses:
      if self._anchormode and self._eqclasses:
        pui = displayGraphByAnchors(self._eqclasses, self,
                                    self._O, self._minlen,
                                    getsampling=self._getsampling)
      else:
        pui = PhageUI(self._graph, self, self._O,
                      self._minlen, self._anchormode, self._fast,
                      self._showall, self._getsampling, not self._graph, True)

      self.add_window(pui)

          #Use the OpenAlphaDialog to get parameters for opening a PhageUI:
    else: # (launchAlphaApp will be called with the "Launch Alpha" button)
      self.openFileDialog()

  def openFileDialog(self):
    """
    Open the OpenAlphaDialog.
    """
    if self._opendialog:
      self._opendialog.present()
    else:
      self._opendialog = OpenAlphaDialog(self._minlen, self)
      self.add_window(self._opendialog)

  def closeFileDialog(self):
    self.remove_window(self._opendialog)
    self._opendialog = None

  #def do_shutdown(self):
  #  print(f'do_shutdown!')
  #  super(AlphaApp, self).do_shutdown()
  #  return True
  #  self.quit()

  def on_quit(self, action=None, param=None):
    self.quit()

  def launchAlphaApp(self, ig:InterGraph, idTOinterval: Dict[str, Tuple],
                     O: InputInfo, minlength: int, anchormode=False,
                     titlepair: Tuple[str, str]=None,
                     removecycles=False, showexpanded=False) -> Thread:
    """
    Open a new PhageUI in this AlphaApp. This is called from the
    openAlphaDialog once the "Launch Alpha" button is pressed.

    :note: Update messages are sent to the self._opendialog (this necessitates
           the use of Thread instead of Process since OpenAlphaDialog is not
           pickleable)
    :note: getGraph() is called to compute the new graph in a new thread.
           The new graph will be sent to self._graphQ when it is ready.
           (_waitForGraph() will receive the message)

    :param ig:          a graph to display (if the user chose a .alpha file)
    :param idTOinterval:restrict matches to these intervals.  for intervals
                        with pairs of length 1, include matches that correspond
                        to the given interval.
    :param O:           fasta file and other input information
    :param minlength:   minimum match length
    :param anchormode:  show just anchors, not the whole graph
    :param titlepair:   (title, subtitle)
    :param removecycles:should we attempt to remove cycles from the next graph?
    :param showexpanded:show all of the (large-enough) nodes

    :return: the thread that is computing the new graph
    """
    if ig:    #Opened a .alphafile.
      self._openPhageUI(ig, O, self._minlen, None, True)

    else:     #Opened a fasta file.
      graphQ = queue.Queue()            #Waiting for InterGraph on Queue
      Thread(target=self._waitForGraph, args=(graphQ, True),
                                              daemon=True).start()

      killevent = Event()
      args = (O.seqfile, graphQ, minlength, O.gidset, (None, None), None,
              idTOinterval, anchormode, O, showexpanded, titlepair, anchormode,
              removecycles, O.annotations, self.matchdir, True,
              get_openAlphaDialogMessage(self._opendialog),
              get_openAlphaDialogBumpCycle(self._opendialog),
              killevent)

      p = Thread(target=getGraph, args=args, daemon=True)
      p.killevent = killevent
      p.start()                         #Compute the InterGraph
      return p

  def spawnGetGraph(self, seqfile: str, minlength: int, usegids: Iterable,
                    normfuncs: NormIndFuncs,
                    idTOinterval: Dict[str, Tuple], anchormode: bool,
                    O: InputInfo, showexpanded: bool, titlepair: bool,
                    parentqueue: SpawnContext.Queue=None,
                    removecycles: bool=False) -> mp.Process:
    """
    Spawn a new process that runs getGraph() to compute a graph to be opened in 
    a new PhageUI by _waitForGraph().
    
    :note: getGraph() is called to compute the new graph in a new thread.
           The new graph will be sent to self._graphQ when it is ready.
           (_waitForGraph() will receive the message)

    :parem parentqueue: child uses queue to notify parent of graph completion
    """
    graphQ = self.mpcontext.Queue()   #Waiting for the InterGraph on Queue
    Thread(target=self._waitForGraph, args=(graphQ, False), daemon=True).start()

    args = (seqfile, graphQ, minlength, usegids, (None, None), normfuncs,
            idTOinterval, anchormode, O, showexpanded, titlepair,
            anchormode or not showexpanded,
            removecycles, O.annotations, self.matchdir)

    p = self.mpcontext.Process(target=getGraph, args=args, daemon=False)
    p.start()                         #Compute the InterGraph

    self._pidTOparentQ[p.pid] = parentqueue  #Remember which queue to use for
                                             #this child.
    return p

  def _waitForGraph(self, queue:queue.Queue, toplevel=False):
    """
    Wait for a graph to be computed on self._graphQ. Create a PhageUI when
    the InterGraph is received from the queue.
    
    :note: queue.put() is called by getGraph()

    :param toplevel: the PhageUI to be opened is a top-level PhageUI, so add
                     the open button etc.
    """
    ig, O, matchlen, pid, message = queue.get(True)

    assert(pid)              #Should have the pid even with errors
    parentqueue = self._pidTOparentQ.pop(pid, None)

    if not ig:
      if self._opendialog:
        self._opendialog.updateMessage(f'** Restart Alpha with new parameters **')
      if parentqueue:
        parentqueue.put((PhageUI.NEWGRAPH_ERROR, message))
        parentqueue.put((PhageUI.NEWGRAPH_DONE, pid), True)
      return

    elif parentqueue:        #Notify parent PhageUI that child is ready
      parentqueue.put((PhageUI.NEWGRAPH_DONE, pid), True)

    GLib.idle_add(self._openPhageUI, ig, O, matchlen, parentqueue, toplevel)


  def _openPhageUI(self, ig: InterGraph, O:InputInfo, matchlen: int,
                   parentqueue: queue.Queue, toplevel: bool=False):
    """
    Open a new phage dialog with the given InterGraph.
    
    :note: The parent will be notified on the parentqueue.

    :param toplevel: the PhageUI to be opened is a top-level PhageUI, so add
                     the open button, etc.
    """
    if self._opendialog and not parentqueue:
      ig.setTitlePair(('Alpha',
                       f'{self._defaultsubtitle} - ({ig.getTitleFilename()})'))

    if ig.isAnchorGraph():
      ui = PhageUI(ig, self, O, matchlen, True,
                   getsampling=True, toplevel=self._opendialog,
                   parentqueue=parentqueue)

    else:
      if self._opendialog:   #TODO: cleanup - ifelse probably not necessary
        ui = PhageUI(ig, self, O, matchlen, False, False, None,
                     True, False, True, ig.titlepair, parentqueue)
      else:
        ui = PhageUI(ig, self, O, matchlen, toplevel=toplevel,
                     parentqueue=parentqueue)

    self.add_window(ui)

    if self._opendialog and not parentqueue:
      self._opendialog.closeDialog()
      self.remove_window(self._opendialog)
      self._opendialog = None



#_______________________________________________________________________________
#_______________________________________________________________________________



def displayGraphByAnchors(eqclasses: List[FrozenSet[Match]], app: AlphaApp,
                          O: InputInfo, minlength, normpair=(None, None),
                          vmatch=False, getsampling=True, toplevel=True,
                          titlepair=None, contextqueue: mp.Queue=None,
                          opendialog: OpenAlphaDialog=None) -> PhageUI:
  """
  Divide the genome by "anchors" (equivalence classes on matches from all
  genomes) and display them.
  This way new equivalence classes can be computed for portions of the genome
  between selected anchors.

  :note: if there is a cycle, attempt to "normalize" (i.e. rotate) the
         sequences by building the whole graph.
  :note: augment minlength until we have a cycleless anchor graph.
  :note: restrict matches to these intervals in O.idTOint.  for intervals
         with pairs of length 1, include matches that correspond
         to the given interval
  :note: if O.idTOint is non-empty but normpair isn't
         set, then normalize the graph to the beginning of the interval.

  :param eqclasses: a list of frozensets holding equivalent matches
  :param app:       the AlphaApp
  :param O:         the InputInfo object
  :param normpair:  normalize the graph so that the first node is not before
                    this index (gid, index)
  :param vmatch:    use vmatch instead of gt (genometools)
  :param getsampling:  sample the anchors when there are many of them
  :param toplevel:     if False, hide some of the elements and don't add dummies
  :param titlepair:    (title, subtitle) set the title and subtitle
  :param contextqueue: a Queue used to send information to the parent process
  :param opendialog:   send updates to this as we compute things. close it
                       once we're done

  :return: the opened the PhageUI, otherwise None.
  """
  ig, matchlen = getAcyclicAnchorGraph(eqclasses, O, minlength, O.gidset,
                                       normpair, None, vmatch, getsampling,
                                       toplevel, O.annotations, app.matchdir,
                                       get_openAlphaDialogBumpCycle(opendialog))

  if ig:
    if opendialog: #TODO: maybe no nead for ifelse.
      return PhageUI(ig, app, O, matchlen, True, False, None,
                     getsampling, False, toplevel, titlepair, contextqueue)
    else:
      return PhageUI(ig, app, O, matchlen, True,
                     getsampling=getsampling, toplevel=toplevel,
                     titlepair=titlepair, parentqueue=contextqueue)
  else:
    raise Exception('There are not enough anchors in the interval.')

  return None



def get_openAlphaDialogMessage(opendialog: OpenAlphaDialog) -> Callable:
  """
  Return the openAlphaDialogMessage() function with the opendialog parameter
  already set.
  """
  if opendialog:
    return lambda message: openAlphaDialogMessage(opendialog, message)

  return None

def openAlphaDialogMessage(opendialog: OpenAlphaDialog, message: str):
  """
  Send a this message to be printed on the overlay in the OpenAlphaDialog.

  :note: this is the thread-safe way of doing this, rather than calling
         updateMessage() directly.
  """
  GLib.idle_add(opendialog.updateMessage, message)

def get_openAlphaDialogBumpCycle(opendialog: OpenAlphaDialog) -> Callable:
  """
  Return the openAlphaDialogBumpCycle() function with the opendialog parameter
  already set.
  """
  if opendialog:
    return lambda message: openAlphaDialogBumpCycle(opendialog, message)

  return None

def openAlphaDialogBumpCycle(opendialog: OpenAlphaDialog, matchlen: int):
  """
  Update the OpenAlphaDialog overlay with this matchlength.
  """
  GLib.idle_add(opendialog.bumpCycleCheck, matchlen)


def showMessageDialog(parent, primary: str, secondary: str=None) -> None:
  """
  Create a MessageDialog with the given message.

  @param parent:   the parent window
  @param primary:  the primary message
  @param seconary: a secondary message
  """
  dialog = Gtk.MessageDialog(parent, 0, Gtk.MessageType.INFO,
                             Gtk.ButtonsType.OK, primary)
  dialog.format_secondary_text(secondary)
  dialog.run()
  dialog.destroy()

