# Alpha -- ALignment of PHAges #
_(for Linux, macOS, and Windows)_

Alpha is a browser designed for detailed comparative study of bacteriophage communities.

Bacteriophages are characterized by extreme mosaicism. Since mostly co-linearly ordered modules
contain different variants that have very little sequence similarity, traditional multiple alignments
are not appropriate for bacteriophage genomes.

We avoid this difficulty by considering whole bacteriophage alignments as
partial orders. Given a fasta file, Alpha builds these partial orders and displays them interactively.

## Introduction ##

Use Alpha to browse structural variations and recombinations within the mosaic context of the community. Indels and large modular variants could look like this:
![Indels and Variants](userdocs/fig/indels_and_variants.png  "indels_and_variants")
phage _D_ (*B236*) is missing a region of roughly 1kb near coordinates 13567-13574,
while phage _A_ (*phiETA3*) is missing a block shared by _A_ and _B_ in the region near 13457-13474.

Here's an example showing the result of some recombinations:
![recombinations](userdocs/fig/recombinations.svg  "recombinations")
Phages _A_ and _D_ are very similar over a 1345 bp region. This region is dissimilar to the 1321bp 
region shared by _B_ and _C_. On the contrary, to the right _A_ and _C_ are grouped while _B_ and _D_ are grouped.
A traditional alignment attempts to align these dissimilar regions. A Clustal Omega alignment of the left side of this image shows a well aligned portion followed by a long stretch of nonsense, before eventually becoming well aligned again:
![alignment](userdocs/fig/alignment_excised.png "alignment")

### Tutorial ###

A tutorial can be found at [www.lirmm.fr/~swenson/alpha/using_alpha.html](http://www.lirmm.fr/~swenson/alpha/using_alpha.html), or in PDF format at [userdocs/using_alpha.pdf](https://bitbucket.org/thekswenson/alpha/raw/master/userdocs/using_alpha.pdf).

### The Software ###

*These are quick usage instructions. See the documentation with the Tutorial for a more complete treatment.*

__alpha__:

To get started, open Alpha and provide it with [a .fasta file](https://bitbucket.org/thekswenson/alpha/raw/master/data/tutorial/Eta3EtCie.fasta "tutorial fasta").
Check *Use NCBI Annotations* if you want to see annotations. Press *Launch Alpha*.
The *anchors* (anchors are gappless alignments that include all input genomes) are displayed first:
![anchorview](userdocs/fig/basic_view.png "anchor view")
Highlight the A:[1115-1128] node and the A:[2246-2270] node and press the __Shift-G__ hotkey. The alignment graph pertaining to that part of the genomes will be displayed in a new window. Right-click, or click on the node's target to get a menu and choose "Annotations".  The results are depicted here:
![region with annotations](userdocs/fig/region_with_annotations.png "regione with annotations")

*You can also launch Alpha from the commandline. Type `alpha -h` to get usage information.*

__sequencetool__:

Download a set of sequences from NCBI. *See the [HTML](http://www.lirmm.fr/~swenson/alpha/using_alpha.html) or [PDF](https://bitbucket.org/thekswenson/alpha/raw/master/userdocs/using_alpha.pdf) instructions for more information.*

__matchtool__:
Explore exact matches between genomes. *See the [HTML](http://www.lirmm.fr/~swenson/alpha/using_alpha.html) or [PDF](https://bitbucket.org/thekswenson/alpha/raw/master/userdocs/using_alpha.pdf) instructions for more information.*

--------------------------------------------------------------------------------
## Installation ##

Custom installation instructions that will work on any Linux distribution, or with Conda,
or with older macOS versions are available on [the wiki installation page](https://bitbucket.org/thekswenson/alpha/wiki/Installation%20Instructions).
We recommend that you use one of the simple methods below.

### Ubuntu Linux ###
We have setup [a PPA](https://launchpad.net/~thekswenson/+archive/ubuntu/alpha-aligner) for Alpha.
The oldest packages we have are for Ubuntu 22.04. For an older version of Ubuntu, follow the instructions on
[the wiki installation page](https://bitbucket.org/thekswenson/alpha/wiki/Installation%20Instructions).
To install Alpha from the PPA by pasting the following lines into the terminal:

```
sudo add-apt-repository universe
sudo add-apt-repository ppa:thekswenson/alpha-aligner
sudo apt update
sudo apt install alpha-aligner
```

### MacOS ###
We support macOS going back to High-Sierra. For an older version, we recommend using conda and the instructions on
[the wiki installation page](https://bitbucket.org/thekswenson/alpha/wiki/Installation%20Instructions).

1. Download [the .dmg file](https://mega.nz/folder/zhxkwYBT#TDxKRv3teQgAGOxIDZwNEg).
2. Double-click it.
3. Drag the Alpha icon into the application folder.
4. When you open Alpha from the applications folder for the first time, you must right-click and choose "open" (since the app is not signed).

### Windows ###
Alpha works well using the Windows Subsystem for Linux (WSL).

1. Install WSL: go to the Microsoft Store and install [Ubuntu 22.04.1 LTS](https://apps.microsoft.com/store/detail/ubuntu/9PDXGNCFSCZV)
    * if this is your first time using WSL you may need to follow [these directions](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
2. Open the WSL Ubuntu app in windows
3. Follow the installation instruction for Linux
4. Type `alpha` at the prompt

--------------------------------------------------------------------------------
## Bugs and Features ##

Please report bugs or feature requests on the issue tracker at:
https://bitbucket.org/thekswenson/alpha/issues/new

--------------------------------------------------------------------------------
## Notes on Runtime ##

The running time and memory usage of Alpha for small sets of phages will
not be an issue.  Larger memory footprints and waiting times could be
encounter when the number of phages gets into the twenties.  In these cases,
a large
the "-g" should not be used, and the "-m" option should be used so that only
the anchors are displayed for large matches (e.g. start with "-m 150").
Specific regions of the genomes can then be explored independently by selecting
two nodes and pressing the "graph" button.

The running time and memory usage of Alpha is primarily dependent on the number
of exact matches between all pairs of sequences.  Therefore, the following
factors all affect resource consumption:

1. the number of sequences,
2. the length of the sequences,
3. the (dis)similarity of the sequences.

For a fixed number of sequences, the worst case occurs when sequences are all
mostly similar yet contain many mutations.

This problem has been somewhat alleviated by the use of Cython.  Further
improvements are planned.

--------------------------------------------------------------------------------
## Credits ##

The graphical rendering of graphs is based on the program xdot (https://github.com/jrfonseca/xdot.py).

Exact matches are computed using GenomeTools (http://genometools.org/).
